﻿using System;
using System.IO.IsolatedStorage;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Microsoft.Phone.Info;

namespace Starbucks.Utilities
{
    /// <summary>
    /// 
    /// </summary>
    public class ConfigurationSettings
    {
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public static string DeviceUniqueId
        {
            get
            {
                byte[] uniqueId = (byte[])DeviceExtendedProperties.GetValue("DeviceUniqueId");
                return BitConverter.ToString(uniqueId);
            }
        }
        /// <summary>
        /// 
        /// </summary>
        public static string IdUsuario
        {
            get
            {
                return Convert.ToString(IsolatedStorageSettings.ApplicationSettings["idusuario"]);
            }
        }
        /// <summary>
        /// 
        /// </summary>
        public static string Token
        {
            get
            {
                return Convert.ToString(IsolatedStorageSettings.ApplicationSettings["token"]);
            }
        }
        /// <summary>
        /// 
        /// </summary>
        public static string IdTarjeta
        {
            get
            {
                return Convert.ToString(IsolatedStorageSettings.ApplicationSettings["idTarjeta"]);
            }
        }
        /// <summary>
        /// 
        /// </summary>
        public static string SERVICE_REWARDS
        {
            get
            {
                return IsolatedStorageSettings.ApplicationSettings["URL_WS_REWARDS"].ToString();
            }

        }
        /// <summary>
        /// 
        /// </summary>
        public static string SERVICE_SMART
        {
            get
            {
                return IsolatedStorageSettings.ApplicationSettings["URL_WS_SMART"].ToString();
            }
        }
        /// <summary>
        /// 
        /// </summary>
        public static int TIMEOUT
        {
            get
            {
                return 35 * 1000;
            }
        }
    }
}
