﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Starbucks.Utilities
{
    public class TipoBancoUtil
    {
        /// <summary>
        /// 
        /// </summary>
        public const string AMEX = "American Express";
        /// <summary>
        /// 
        /// </summary>
        public const string VISA = "Visa";
        /// <summary>
        /// 
        /// </summary>
        public const string MASTERCARD = "MasterCard";
        /// <summary>
        /// 
        /// </summary>
        /// <param name="tipo"></param>
        /// <returns></returns>
        public static  Uri GetLogo(string tipo)
        {
            Dictionary<string, string> Logos = new Dictionary<string, string>() { { "American Express", "amexlogo.png" }, { "MasterCard", "mastercardlogo.png" }, { "Visa", "visalogo.png" } };
            return new Uri("/Resources/" + Logos[tipo], UriKind.RelativeOrAbsolute);
        }
    }
}
