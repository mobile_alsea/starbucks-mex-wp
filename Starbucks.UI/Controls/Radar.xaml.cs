﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Runtime.InteropServices;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;
using Microsoft.Devices.Sensors;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;

namespace Starbucks.UI.Controls
{
    #region enumeration
    /// <summary>
    /// Defines the orientation type
    /// </summary>
    public enum Orientation
    {
        /// <summary>
        /// Portrait Orientation
        /// </summary>
        Portrait,
        /// <summary>
        /// Landscape Orientation
        /// </summary>
        Landscape
    }
    #endregion

    public partial class Radar : UserControl
    {
        #region internal fields
        private const double RotatedTimeSpane = 50;
        private const int OneCircleDegree = 360;
        private const int NormalSpeed = 10;
        private const int MappingScale = 8000;
        private const int VenueSize = 5;
        private const int MaxDistance = 1500; // meters
        private const int MinDistance = 500; // meters

        private DispatcherTimer _rotatedTimer;
        private DispatcherTimer _timer;
        private PlaneProjection _projection;
        private Compass _compass;
        private List<UIElement> _venueEllipse = new List<UIElement>();
        private BitmapImage PortraitScanImage =
            new BitmapImage(new Uri("/Resources/RA/radar_portrait.png",
                                UriKind.Relative));
        private BitmapImage LandscapeScanImage =
            new BitmapImage(new Uri("/Resources/RA/radar_landscape.png",
                                UriKind.Relative));
        private Orientation _orientation;
        private double _trueHeading;
        private double _rotationZ;
        private bool _clockwise;
        private int _speed;
        private int _distance;
        private double? _deviceLongitude = null;
        private double? _deviceLatitude = null;
        private int _rotateAngle = 0;
        #endregion

        #region property
        /// <summary>
        /// Gets the max distance of surrounding current position.
        /// </summary>
        public int Distance
        {
            get
            {
                return _distance;
            }
            set
            {
                int distance = (int)value;
                if (distance >= MinDistance && distance <= MaxDistance)
                {
                    _distance = distance;
                }
            }
        }

        /// <summary>
        /// Gets or sets the orientation of radar.
        /// </summary>
        public Orientation Orientation
        {
            get
            {
                return _orientation;
            }
            set
            {
                _orientation = value;
                switch (_orientation)
                {
                    case Orientation.Portrait:
                        scanImage.Source = PortraitScanImage;
                        break;
                    case Orientation.Landscape:
                        scanImage.Source = LandscapeScanImage;
                        break;
                    default:
                        break;
                }
            }
        }

        /// <summary>
        /// Gets the locations of surrounding venues.
        /// </summary>
        private List<Point> VenueLocations { get; set; }
        #endregion

        #region external methods
        /// <summary>
        /// Sets the rotation angle of radar
        /// </summary>
        /// <param name="angle">The rotation angle</param>
        /// <returns>
        /// Returns none.
        /// </returns>
        public void SetRotate(int angle)
        {
            _rotateAngle = angle;
        }

        /// <summary>
        /// Sets a new device location.
        /// </summary>
        /// <param name="lon">The longitude of the new device location</param>
        /// <param name="lat">The latitude of the new device location</param>
        /// <returns>
        /// Returns none.
        /// </returns>
        /// <exception cref="System.ArgumentNullException">
        /// The function may throws a ArgumentNullException.
        /// </exception>
        public void SetDeviceLocation(double lon, double lat)
        {
            _deviceLongitude = lon;
            _deviceLatitude = lat;

            UpdateView(); // Updates radar view again.
        }

        /// <summary>
        /// Adds a new venue location to radar.
        /// </summary>
        /// <param name="lon">The longitude of the new device location</param>
        /// <param name="lat">The latitude of the new device location</param>
        /// <returns>
        /// Returns none.
        /// </returns>
        /// <exception cref="System.ArgumentNullException">
        /// The function may throws a ArgumentNullException.
        /// </exception>
        public void AddVenueLocation(double lon, double lat)
        {
            Point venuLocation = new Point(lon, lat);
            VenueLocations.Add(venuLocation);

            Point p;
            GetRadarPosition(venuLocation, out p);
            AddVenue(p);
        }

        /// <summary>
        /// Removes all venue locations in radar.
        /// </summary>
        /// <returns>
        /// Returns none.
        /// </returns>
        /// <exception cref="System.ArgumentNullException">
        /// The function may throws a ArgumentNullException.
        /// </exception>
        public void RemoveVenueLocations()
        {
            RemoveVenues();
            VenueLocations.Clear();
        }

        /// <summary>
        /// Sends radar scanning.
        /// </summary>
        /// <returns>
        /// Returns none.
        /// </returns>
        public void Start()
        {
            try
            {
                _rotatedTimer.Start();
                _timer.Start();
                if (_compass != null)
                {
                    _compass.Start();
                }
            }
            catch (COMException come)
            {
                Debug.WriteLine(come.Message);
            }
            catch (InvalidOperationException ioe)
            {
                Debug.WriteLine(ioe.Message);
            }
            catch (Exception e)
            {
                Debug.WriteLine(e.Message);
            }
        }

        /// <summary>
        /// Stops radar scanning.
        /// </summary>
        /// <returns>
        /// Returns none.
        /// </returns>
        public void Stop()
        {
            _rotatedTimer.Stop();
            _timer.Stop();
            if (_compass != null)
            {
                _compass.Stop();
            }
        }
        #endregion

        #region constructor
        /// <summary>
        /// Constructor
        /// </summary>
        public Radar()
        {
            InitializeComponent();

            Distance = MinDistance;

            VenueLocations = new List<Point>();
            radarCanvas.Projection = new PlaneProjection();
            _projection = (PlaneProjection)this.radarCanvas.Projection;

            try
            {
                if (Compass.IsSupported)
                {
                    _compass = new Compass();
                    _compass.TimeBetweenUpdates = TimeSpan.FromMilliseconds(20);
                    _compass.CurrentValueChanged +=
                        new EventHandler<SensorReadingEventArgs<CompassReading>>(
                            OnCurrentValueChanged);
                }
            }
            catch (COMException come)
            {
                Debug.WriteLine(come.Message);
            }
            catch (InvalidOperationException ioe)
            {
                Debug.WriteLine(ioe.Message);
            }
            catch (Exception e)
            {
                Debug.WriteLine(e.Message);
            }
            finally
            {
                _timer = new DispatcherTimer();
                _timer.Interval = TimeSpan.FromMilliseconds(50);
                _timer.Tick += new EventHandler(OnTimerTick);

                _rotatedTimer = new DispatcherTimer();
                _rotatedTimer.Interval = TimeSpan.FromMilliseconds(RotatedTimeSpane);
                _rotatedTimer.Tick += new EventHandler(OnRotationTimerTick);

                _speed = NormalSpeed;
            }
        }
        #endregion

        #region internel methods
        /// <summary>
        /// Updates radar view.
        /// </summary>
        /// <returns>
        /// Returns none.
        /// </returns>
        private void UpdateView()
        {
            RemoveVenues();

            foreach (Point location in VenueLocations)
            {
                Point p;
                GetRadarPosition(location, out p);
                AddVenue(p);
            }
        }

        /// <summary>
        /// Removes all venues from user control, 
        /// but this method won't remove model data.
        /// </summary>
        /// <returns>
        /// Returns none.
        /// </returns>
        /// <exception cref="System.ArgumentNullException">
        /// The function may throws a ArgumentNullException.
        /// </exception>
        private void RemoveVenues()
        {
            _venueEllipse.Clear();
            foreach (UIElement element in radarCanvas.Children)
            {
                if (element != radarEllipse &&
                    element != NPole &&
                    element != SPole)
                {
                    _venueEllipse.Add(element);
                }
            }

            foreach (UIElement element in _venueEllipse)
            {
                radarCanvas.Children.Remove(element);
            }
        }

        /// <summary>
        /// Adds a veneu to user control.
        /// </summary>
        /// <param name="p">The venue position</param>
        /// <returns>
        /// Returns none.
        /// </returns>
        /// <exception cref="System.ArgumentNullException">
        /// The function may throws a ArgumentNullException.
        /// </exception>
        private void AddVenue(Point p)
        {
            Ellipse ellipse = new Ellipse
            {
                Margin = new Thickness(p.X, p.Y, 0, 0),
                Width = VenueSize,
                Height = VenueSize,
                Fill = new SolidColorBrush(Colors.LightGray),
                HorizontalAlignment = HorizontalAlignment.Center,
                VerticalAlignment = VerticalAlignment.Center
            };

            radarCanvas.Children.Add(ellipse);
        }

        /// <summary>
        /// Gets venue's position in radar.
        /// </summary>
        /// <param name="location">The venue location</param>
        /// <param name="p">The venue's position in radar</param>
        /// <returns>
        /// Returns none.
        /// </returns>
        /// <exception cref="System.ArgumentNullException">
        /// The function may throws a ArgumentNullException.
        /// </exception>
        private void GetRadarPosition(Point location, out Point p)
        {
            p = new Point();

            if (_deviceLongitude == null ||
                _deviceLatitude == null)
            {
                throw new ArgumentNullException("Device location was uninitialized!");
            }
            else
            {
                double lon = location.X - (double)_deviceLongitude;
                double lat = (double)_deviceLatitude - location.Y;

                double scale = ((double)MinDistance) / ((double)Distance);
                int mappingScale = (int)(scale * MappingScale);

                lon *= mappingScale;
                lat *= mappingScale;

                p.X = lon + (double)(radarCanvas.ActualWidth / 2);
                p.Y = lat + (double)(radarCanvas.ActualHeight / 2);
            }
        }

        /// <summary>
        /// Callback method of compass.
        /// </summary>
        /// <param name="sender">The radar control</param>
        /// <param name="e">The compass reading args</param>
        /// <returns>
        /// Returns none.
        /// </returns>
        /// <exception cref="System.ArgumentNullException">
        /// The function may throws a ArgumentNullException.
        /// </exception>
        private void OnCurrentValueChanged(object sender,
                    SensorReadingEventArgs<CompassReading> e)
        {
            _trueHeading = e.SensorReading.TrueHeading;
        }

        /// <summary>
        /// Callback method of timer.
        /// </summary>
        /// <param name="sender">The radar control</param>
        /// <param name="e">The event args</param>
        /// <returns>
        /// Returns none.
        /// </returns>
        /// <exception cref="System.ArgumentNullException">
        /// The function may throws a ArgumentNullException.
        /// </exception>
        private void OnTimerTick(object sender, EventArgs e)
        {
            SetRotationAngle(_trueHeading + _rotateAngle);
        }

        /// <summary>
        /// Callback method of rotation timer.
        /// </summary>
        /// <param name="sender">The radar control</param>
        /// <param name="e">The event args</param>
        /// <returns>
        /// Returns none.
        /// </returns>
        /// <exception cref="System.ArgumentNullException">
        /// The function may throws a ArgumentNullException.
        /// </exception>
        private void OnRotationTimerTick(object sender, EventArgs e)
        {
            if (Math.Abs(_projection.RotationZ - _rotationZ) < _speed)
            {
                _projection.RotationZ = _rotationZ;
                return;
            }

            if (_clockwise)
            {
                _projection.RotationZ -= _speed;
                if (_projection.RotationZ < 0)
                {
                    _projection.RotationZ = OneCircleDegree - _projection.RotationZ;
                }
            }
            else
            {
                _projection.RotationZ += _speed;
                if (_projection.RotationZ > OneCircleDegree)
                {
                    _projection.RotationZ = _projection.RotationZ - OneCircleDegree;
                }
            }
        }

        /// <summary>
        /// Sets rotation angle.
        /// </summary>
        /// <param name="angle">rotation angle</param>
        /// <returns>
        /// Returns none.
        /// </returns>
        /// <exception cref="System.ArgumentNullException">
        /// The function may throws a ArgumentNullException.
        /// </exception>
        private void SetRotationAngle(double angle)
        {
            _rotationZ = angle % OneCircleDegree;

            if (_rotationZ < _projection.RotationZ)
            {
                if (_projection.RotationZ - _rotationZ >
                    _rotationZ + (OneCircleDegree - _projection.RotationZ))
                {
                    _clockwise = false;
                }
                else
                {
                    _clockwise = true;
                }
            }
            else
            {
                if (_rotationZ - _projection.RotationZ >
                    _projection.RotationZ + (OneCircleDegree - _rotationZ))
                {
                    _clockwise = true;
                }
                else
                {
                    _clockwise = false;
                }
            }
        }
        #endregion
    }
}
