﻿using System;
using System.Collections.Generic;
using System.Device.Location;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Controls.Maps;
using Microsoft.Phone.Shell;
using Starbucks.UI.Model;


namespace Starbucks.UI.Controls
{
    public partial class Pin2 : UserControl
    {
        private Pushpin currentPin;

        public Pin2()
        {
            InitializeComponent();
        }

        private void imgGlobo_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            if (!App.TiendasViewModel.IsLoadPin)
            {
                var mapaContex = imgGlobo.DataContext as Tienda;
                var location = new GeoCoordinate(Convert.ToDouble(mapaContex.latitud), Convert.ToDouble(mapaContex.longitud));
                currentPin = new Pushpin
                {
                    Location = location,
                    Template = Resources["PushpinControlTemplateMap"] as ControlTemplate,
                    DataContext = mapaContex
                };
                currentPin.Location = location;
                if (App.TiendasViewModel.CurrentMap != null)                
                    App.TiendasViewModel.CurrentMap.SetView(new GeoCoordinate(mapaContex.latitud, mapaContex.longitud), 16);
                App.TiendasViewModel.PinActivo = currentPin;
                App.TiendasViewModel.AgregaPinActivo();
                App.TiendasViewModel.IsLoadPin = true;
                App.TiendasViewModel.IsCambioPin = false;
            }
            else
            {
                var tiendaOld = App.TiendasViewModel.PinActivo.DataContext as Tienda;
                var tiendaNew = imgGlobo.DataContext as Tienda;
                if (tiendaOld.nombre != tiendaNew.nombre)
                {
                    App.TiendasViewModel.EliminaPinActivo();
                    var location = new GeoCoordinate(Convert.ToDouble(tiendaNew.latitud), Convert.ToDouble(tiendaNew.longitud));
                    currentPin = new Pushpin
                    {
                        Location = location,
                        Template = Resources["PushpinControlTemplateMap"] as ControlTemplate,
                        DataContext = tiendaNew
                    };
                    currentPin.Location = location;
                    App.TiendasViewModel.CurrentMap.SetView(new GeoCoordinate(tiendaNew.latitud, tiendaNew.longitud), 16);
                    App.TiendasViewModel.PinActivo = currentPin;
                    App.TiendasViewModel.AgregaPinActivo();
                    App.TiendasViewModel.IsLoadPin = true;
                    App.TiendasViewModel.IsCambioPin = true;
                }
            }
        }

        //private void CtrlTileDescripcion_LostFocus(object sender, RoutedEventArgs e)
        //{
        //    App.TiendasViewModel.CurrentMapControl.Items.Remove(currentPin);
        //    IsLoad = false;
        //}
    }
}
