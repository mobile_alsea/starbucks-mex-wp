﻿using System;
using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using Microsoft.Phone.Controls;
using Starbucks.UI.Controller;
using Starbucks.UI.Helper;
using Starbucks.UI.Model;

namespace Starbucks.UI.Controls
{
    public partial class MessageBox : Grid
    {
        private PhoneApplicationPage _page;
 
        private MessageBox()
        {
            InitializeComponent();
        }
 
        public event EventHandler<MessageBoxEventArgs> Closed;
 
        protected virtual void OnClosed(Starbucks.UI.Model.CustomMessageBoxResult result)
        {
            _page.BackKeyPress -= Page_BackKeyPress;
            var handler = this.Closed;
            if (handler != null)
            {
                handler(this, new MessageBoxEventArgs { Result = result });
            }
            Remove();
        }
 
        public static MessageBox Show(string message, string caption, string yesButtonText, string noButtonText = null)
        {

           

            MessageBox msgBox = new MessageBox();
            msgBox.HeaderTextBlock.Text = caption;
            msgBox.MessageTextBlock.Text = message;
            msgBox.YesButton.Content = yesButtonText;
            if (string.IsNullOrWhiteSpace(noButtonText))
                msgBox.NoButton.Visibility = Visibility.Collapsed;
            else
                msgBox.NoButton.Content = noButtonText;
            msgBox.Insert();
            return msgBox;


        }
 
        private void Insert()
        {
            var frame = Application.Current.RootVisual as Microsoft.Phone.Controls.PhoneApplicationFrame;
            _page = frame.Content as PhoneApplicationPage;
            _page.BackKeyPress += Page_BackKeyPress;
 
            var grid = System.Windows.Media.VisualTreeHelper.GetChild(_page, 0) as Grid;
            if (grid != null)
            {
                if (grid.RowDefinitions.Count > 0)
                {
                    Grid.SetRowSpan(this, grid.RowDefinitions.Count);
                }
                grid.Children.Add(this);
            }

            SwivelTransition transitionIn = new SwivelTransition();
            transitionIn.Mode = SwivelTransitionMode.BackwardIn;
 
            ITransition transition = transitionIn.GetTransition(MessagePanel);
            transition.Completed += (s, e) => transition.Stop();
            transition.Begin();
 
            if (_page.ApplicationBar != null)
                _page.ApplicationBar.IsVisible = false;
        }
 
        private void Remove()
        {
            var frame = Application.Current.RootVisual as Microsoft.Phone.Controls.PhoneApplicationFrame;
            var page = frame.Content as PhoneApplicationPage;
            var grid = System.Windows.Media.VisualTreeHelper.GetChild(page, 0) as Grid;
 
            SwivelTransition transitionOut = new SwivelTransition();
            transitionOut.Mode = SwivelTransitionMode.BackwardOut;
 
            ITransition transition = transitionOut.GetTransition(MessagePanel);
            transition.Completed += (s, e) =>
                {
                    transition.Stop();
                    if (grid != null)
                        grid.Children.Remove(this);
                    if (page.ApplicationBar != null)
                        page.ApplicationBar.IsVisible = true;
                };
            transition.Begin();
        }
 
        private void Page_BackKeyPress(object sender, CancelEventArgs e)
        {
            OnClosed(Starbucks.UI.Model.CustomMessageBoxResult.Cancel);
            e.Cancel = true;
        }
        
        private void YesButton_Click(object sender, RoutedEventArgs e)
        {
            OnClosed(Starbucks.UI.Model.CustomMessageBoxResult.Yes);
        }
 
        private void NoButton_Click(object sender, RoutedEventArgs e)
        {
            OnClosed(Starbucks.UI.Model.CustomMessageBoxResult.No);
        }

        private void CheckBox_Checked(object sender, RoutedEventArgs e)
        {
            App.Settings[Constantes.MostrarMensaje_RA] = true;
        }

        private void CheckBox_Unchecked(object sender, RoutedEventArgs e)
        {
            App.Settings.Remove(Constantes.MostrarMensaje_RA);
        }

        private void Grid_Loaded(object sender, RoutedEventArgs e)
        {
            Visibility dbgisibility = (Visibility)Application.Current.Resources["PhoneDarkThemeVisibility"];

            if (dbgisibility != Visibility.Visible)
            {
                var img = new System.Windows.Media.Imaging.BitmapImage();
                img.UriSource = new Uri("/Resources/elemntos_mapa/Ejemplo1a.png", UriKind.Relative);
                imgjmp01.Source = img;

                var img2 = new System.Windows.Media.Imaging.BitmapImage();
                img2.UriSource = new Uri("/Resources/elemntos_mapa/Ejemplo2a.png", UriKind.Relative);
                imgjmp02.Source = img2;
            }
            else
            {
                var img = new System.Windows.Media.Imaging.BitmapImage();
                img.UriSource = new Uri("/Resources/elemntos_mapa/Ejemplo1.png", UriKind.Relative);
                imgjmp01.Source = img;

                var img2 = new System.Windows.Media.Imaging.BitmapImage();
                img2.UriSource = new Uri("/Resources/elemntos_mapa/Ejemplo2.png", UriKind.Relative);
                imgjmp02.Source = img2;
            }
           

        }
    }
}
