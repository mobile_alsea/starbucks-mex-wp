﻿using System;
using System.Collections.Generic;
using System.Device.Location;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using Starbucks.UI.Model;

namespace Starbucks.UI.Controls
{
    public partial class Pin : UserControl
    {
        public Pin()
        {
            InitializeComponent();
            Loaded += Pin_Loaded;
        }

        void Pin_Loaded(object sender, RoutedEventArgs e)
        {
            CtrlTileDescripcion.Focus();
        }

        public Visibility TileDescripcion
        {
            set
            {
                CtrlTileDescripcion.Visibility = value;
            }
        }

        private void CtrlTileDescripcion_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            var control = sender as Button;
            if (control != null)
            {
                var mapaContex = control.DataContext as Tienda;
                App.TiendasViewModel.SelectedItemTienda = mapaContex;
                App.ViewModel.SelectedListHorarios = mapaContex.horario;
                (Application.Current.RootVisual as PhoneApplicationFrame).Navigate(new Uri("/View/Tiendas/Detalle.xaml", UriKind.Relative));
            }
        }

        private void CtrlTileDescripcion_LostFocus(object sender, RoutedEventArgs e)
        {
            if (!App.TiendasViewModel.IsCambioPin)
                App.TiendasViewModel.EliminaPinActivo();
            else
                App.TiendasViewModel.IsCambioPin = false;
        }
    }
}
