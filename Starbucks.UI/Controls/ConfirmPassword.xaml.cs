﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using System.Windows.Media;
using System.Windows.Input;
using System.Text.RegularExpressions;

namespace Starbucks.UI.Controls
{
    public partial class ConfirmPassword : UserControl
    {
        string _enteredPasscode = "";
        string _passwordChar = "*";
        string passwordResult = "";

        public ConfirmPassword()
        {
            InitializeComponent();
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void PasswordTextBox_KeyUp(object sender, KeyEventArgs e)
        {
            //modify new passcode according to entered key
            _enteredPasscode = GetNewPasscode(_enteredPasscode, e);
            if (_enteredPasscode.Length <= PasswordTextBox.MaxLength)
            {
                //replace text by *
                PasswordTextBox.Text = Regex.Replace(_enteredPasscode, @".", _passwordChar);

                //take cursor to end of string
                PasswordTextBox.SelectionStart = PasswordTextBox.Text.Length;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        public string Password
        {
            get
            {
                return passwordResult;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="oldPasscode"></param>
        /// <param name="keyEventArgs"></param>
        /// <returns></returns>
        private string GetNewPasscode(string oldPasscode, KeyEventArgs keyEventArgs)
        {
            string newPasscode = string.Empty;
            switch (keyEventArgs.Key)
            {
                case Key.D0:
                case Key.D1:
                case Key.D2:
                case Key.D3:
                case Key.D4:
                case Key.D5:
                case Key.D6:
                case Key.D7:
                case Key.D8:
                case Key.D9:
                    if (oldPasscode.Length < PasswordTextBox.MaxLength)
                    {
                        newPasscode = oldPasscode + (keyEventArgs.PlatformKeyCode - 48);
                        passwordResult = oldPasscode +(keyEventArgs.PlatformKeyCode - 48);
                    }
                    else
                    {
                        newPasscode = oldPasscode;
                        passwordResult = oldPasscode;
                    }
                    break;
                case Key.Back:
                    if (oldPasscode.Length > 0)
                    {
                        newPasscode = oldPasscode.Remove(oldPasscode.Length - 1);
                        passwordResult = oldPasscode.Remove(oldPasscode.Length - 1);
                    }
                    break;
                default:
                    //others
                    newPasscode = oldPasscode;
                    break;
            }
            return newPasscode;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="tipo"></param>
        public void setImageCVV(string tipo)
        {
            ImageSourceConverter converter = new ImageSourceConverter();
            if (tipo == "American Express")
            {
                this.imgCVV.Source = (ImageSource)converter.ConvertFromString("/Resources/cvvamex.png");
                PasswordTextBox.MaxLength = 4;
            }
            else
            {
                this.imgCVV.Source = (ImageSource)converter.ConvertFromString("/Resources/cvv.png");
                PasswordTextBox.MaxLength = 3;
            }
        }
    }
}
