﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media.Animation;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using Starbucks.UI.Helper;
using Starbucks.UI.Model;

namespace Starbucks.UI.Controls
{
    public partial class CustomTileFlip : UserControl
    {
        private Storyboard _frontToBack;
        private Storyboard _backToFront;
        private bool _front;

        public CustomTileFlip()
        {
            InitializeComponent();
            _frontToBack = this.Resources["Sb_B"] as Storyboard;
            _frontToBack.AutoReverse = false;
            _frontToBack.FillBehavior = FillBehavior.HoldEnd;
            _backToFront = this.Resources["Sb_F"] as Storyboard;
            _backToFront.AutoReverse = false;
            _backToFront.FillBehavior = FillBehavior.HoldEnd;
            _front = false;
        }

        public void Flip()
        {
            if (_front)
            {
                _front = false;
                _backToFront.Begin();
            }
            else
            {
                _front = true;
                _frontToBack.Begin();
            }
        }

        private void TileBack_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            if (DataContext.GetType() == typeof(TileFlipBebida))
            {
                var entidad = DataContext as TileFlipBebida;
                if (entidad != null)
                {
                    App.ViewModel.TileFlipBebidaSelected = entidad;
                    App.BebidasViewModel.SelectedItemBebida = entidad.Second;
                    (Application.Current.RootVisual as PhoneApplicationFrame).Navigate(new Uri(StarbucksResource.View_DetalleBebidas, UriKind.Relative));
                }
            }
            else if (DataContext.GetType() == typeof(TileFlipCafe))
            {
                var entidad = DataContext as TileFlipCafe;
                if (entidad != null)
                {
                    App.ViewModel.TileFlipCafeSelected = entidad;
                    App.CafesViewModel.SelectedItemCafe = entidad.Second;
                    (Application.Current.RootVisual as PhoneApplicationFrame).Navigate(new Uri(StarbucksResource.View_DetalleCafes, UriKind.Relative));
                }
            }
            else if (DataContext.GetType() == typeof(TileFlipAlimentos))
            {
                var entidad = DataContext as TileFlipAlimentos;
                if (entidad != null)
                {
                    App.ViewModel.TileFlipAlimentosSelected = entidad;
                    App.AlimentosViewModel.SelectedItemComida = entidad.Second;
                    (Application.Current.RootVisual as PhoneApplicationFrame).Navigate(new Uri(StarbucksResource.View_DetalleAlimentos, UriKind.Relative));
                }
            }
        }

        private void TileFront_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            if (DataContext.GetType() == typeof(TileFlipBebida))
            {
                var entidad = DataContext as TileFlipBebida;
                if (entidad != null)
                {
                    App.ViewModel.TileFlipBebidaSelected = entidad;
                    App.BebidasViewModel.SelectedItemBebida = entidad.First;
                    (Application.Current.RootVisual as PhoneApplicationFrame).Navigate(new Uri(StarbucksResource.View_DetalleBebidas, UriKind.Relative));
                }
            }
            else if (DataContext.GetType() == typeof(TileFlipCafe))
            {
                var entidad = DataContext as TileFlipCafe;
                if (entidad != null)
                {
                    App.ViewModel.TileFlipCafeSelected = entidad;
                    App.CafesViewModel.SelectedItemCafe = entidad.First;
                    (Application.Current.RootVisual as PhoneApplicationFrame).Navigate(new Uri(StarbucksResource.View_DetalleCafes, UriKind.Relative));
                }
            }
            else if (DataContext.GetType() == typeof(TileFlipAlimentos))
            {
                var entidad = DataContext as TileFlipAlimentos;
                if (entidad != null)
                {
                    App.ViewModel.TileFlipAlimentosSelected = entidad;
                    App.AlimentosViewModel.SelectedItemComida = entidad.First;
                    (Application.Current.RootVisual as PhoneApplicationFrame).Navigate(new Uri(StarbucksResource.View_DetalleAlimentos, UriKind.Relative));
                }
            }
        }
    }
}
