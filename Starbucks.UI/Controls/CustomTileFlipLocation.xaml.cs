﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media.Animation;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using Starbucks.UI.Helper;
using Starbucks.UI.Model;

namespace Starbucks.UI.Controls
{
    public partial class CustomTileFlipLocation : UserControl
    {
        private Storyboard _frontToBack;
        private Storyboard _backToFront;
        private bool _front;

        public CustomTileFlipLocation()
        {
            InitializeComponent();
            _frontToBack = this.Resources["Sb_B"] as Storyboard;
            _frontToBack.AutoReverse = false;
            _frontToBack.FillBehavior = FillBehavior.HoldEnd;
            _backToFront = this.Resources["Sb_F"] as Storyboard;
            _backToFront.AutoReverse = false;
            _backToFront.FillBehavior = FillBehavior.HoldEnd;
            _front = false;
        }

        public void Flip()
        {
            if (_front)
            {
                _front = false;
                _backToFront.Begin();
            }
            else
            {
                _front = true;
                _frontToBack.Begin();
            }
        }

        private void TileBack_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            if (DataContext.GetType() == typeof(TileFlipTiendas))
            {
                var entidad = DataContext as TileFlipTiendas;
                if (entidad != null)
                {
                    App.ViewModel.TileFlipTiendaSelected = entidad;
                    App.TiendasViewModel.SelectedItemTienda = entidad.Second;
                    App.ViewModel.SelectedListHorarios = entidad.Horarios;
                    (Application.Current.RootVisual as PhoneApplicationFrame).Navigate(new Uri("/View/Tiendas/Detalle.xaml", UriKind.Relative));
                }
            }
        }

        private void TileFront_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            if (DataContext.GetType() == typeof(TileFlipTiendas))
            {
                var entidad = DataContext as TileFlipTiendas;
                if (entidad != null)
                {
                    App.ViewModel.TileFlipTiendaSelected = entidad;
                    App.TiendasViewModel.SelectedItemTienda = entidad.First;
                    App.ViewModel.SelectedListHorarios = entidad.Horarios;
                    (Application.Current.RootVisual as PhoneApplicationFrame).Navigate(new Uri("/View/Tiendas/Detalle.xaml", UriKind.Relative));
                }
            }
        }
    }
}
