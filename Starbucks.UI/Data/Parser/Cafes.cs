﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using Starbucks.UI.Model;

namespace Starbucks.UI.Data.Parser
{
    public class Cafes
    {
        public static List<Cafe> ListCafes { get; set; }
        public static int version { get; set; }

        public static List<string> ObtenerPerfiles()
        {
            var perfiles = ListCafes.OrderBy(c => c.perfil).Select(c => c.perfil).Distinct().ToList();
            return perfiles;
        }

        public static List<Cafe> ObtenerListaCafes(string perfil)
        {
            var lista = ListCafes.Where(c => c.perfil.Equals(perfil)).ToList();
            return lista;
        }
    }
}
