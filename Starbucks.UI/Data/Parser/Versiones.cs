﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Collections.Generic;

namespace Starbucks.UI.Data.Parser
{
    public class Versiones
    {
        public static List<Starbucks.UI.Model.Version> ListVersiones { get; set; }

        public static List<Starbucks.UI.Model.Version> ObtenerListaVersiones()
        {
            return ListVersiones;
        }
    }
}
