﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using Starbucks.UI.Model;

namespace Starbucks.UI.Data.Parser
{
    public class Bebidas
    {
        public static List<Bebida> ListBebidas { get; set; }
        public static int version { get; set; }

        public static List<string> ObtenCategory(string temperatura)
        {
            var categorias = ListBebidas.Where(c=> c.temperatura.Equals(temperatura)).Select(c => c.categoria).Distinct().ToList();
            return categorias;
        }

        public static List<string> ObtenSubcategory(string temperatura, string categoria)
        {
            var subCategory = ListBebidas.Where(c => c.temperatura.Equals(temperatura) && c.categoria.Equals(categoria)).OrderBy(c=> c.subcategoria).Select(c => c.subcategoria).Distinct().ToList();
            return subCategory;
        }

        public static List<Bebida> ObtenerListaBebidas(string subcategoria)
        {
            if (string.IsNullOrEmpty(subcategoria))
                new ArgumentNullException();
            
            var lista = ListBebidas.Where(c => c.subcategoria.Equals(subcategoria)).OrderBy(c=> c.nombre).ToList();
            return lista;
        }

        public static List<Bebida> ObtenerTodasLasBebidas()
        {
            return ListBebidas;
        }
    }
}
