﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using Starbucks.UI.Model;

namespace Starbucks.UI.Data.Parser
{
    public class Tiendas
    {
        public static List<Tienda> ListTiendas { get; set; }
        public static int version { get; set; }

        public static List<Tienda> ObtenerListaTiendas()
        {
            return ListTiendas;
        }
    }
}
