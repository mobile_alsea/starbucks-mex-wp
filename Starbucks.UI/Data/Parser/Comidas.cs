﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using Starbucks.UI.Model;

namespace Starbucks.UI.Data.Parser
{
    public class Comidas
    {
        public static List<Comida> ListComidas { get; set; }
        public static int version { get; set; }

        public static List<string> ObtenerCategorias()
        {
            var categorias = ListComidas.OrderBy(c=> c.categoria).Select(c=> c.categoria).Distinct().ToList();
            return categorias;
        }

        public static List<Comida> ObtenerListaComida(string categoria)
        {
            if (string.IsNullOrEmpty(categoria))
                new ArgumentNullException();

            var lista = ListComidas.Where(c => c.categoria.Equals(categoria)).OrderBy(c => c.nombre).ToList();
            return lista;
        }
    }
}
