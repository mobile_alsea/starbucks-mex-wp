﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Xml.Linq;
using Starbucks.UI.Helper;
using System.IO;
using System.IO.IsolatedStorage;

namespace Starbucks.UI.Data
{
    public delegate void ServiceRequestEventHandler<T>(ServiceEventArgs<T> e);
    public delegate void ServiceRequestEventHandler(ServiceEventArgs e);

    public class ServiceEventArgs<T>
    {
        public bool ErrorInMakingRequest { get; set; }
        public bool ErrorParser { get; set; }
        public T Data { get; set; }
        public object infoObject { get; set; }
        public bool TimeOut { get; set; }
    }

    public class ServiceEventArgs
    {
        public bool ErrorInMakingRequest { get; set; }
        public bool ErrorParser { get; set; }
        public string Data { get; set; }
        public Stream stream { get; set; }
        public object infoObject { get; set; }
        public bool TimeOut { get; set; }
    }

    public class ServiceWebRequestState<T>
    {
        public HttpWebRequest request;
        public string SessionId;
        public string info;
        public object infoObject;
        public ServiceRequestEventHandler<List<T>> AsyncCallback;
        public ServiceWebRequestState()
        {
            request = null;
        }
    }

    public class ServiceRequest
    {
        private WebClientServices _client;
        private Random aleatorioCache = new Random();
        private int index = 1000;

        public void BeginWebRequest<T>(string service, ServiceRequestEventHandler<List<T>> handler) where T : class, new()
        {
            var tipo = new T();
            Boolean inCache = false;
            
            ServiceWebRequestState<T> state = new ServiceWebRequestState<T>();
            state.AsyncCallback = handler;
            _client = new WebClientServices();
            string uriService = string.Empty;

            IsolatedStorageSettings settings = IsolatedStorageSettings.ApplicationSettings;


            if (tipo.GetType() == typeof(Model.Bebida) && settings.Contains("version_bebidas"))
            {
                int oldVersion = Convert.ToInt32(settings["version_bebidas"]);
                if (Data.Parser.Bebidas.version <= oldVersion)
                {
                    ServiceEventArgs<List<T>> args = new ServiceEventArgs<List<T>>();
                    string xml = loadXML("bebidas.txt");
                    if (xml != null)
                    {
                        var result = Parser(xml, Accion.Bebidas);
                        args.Data = result as List<T>;

                        state.AsyncCallback(args);
                        inCache = true;
                    }
                }
            }
            else if (tipo.GetType() == typeof(Model.Cafe) && settings.Contains("version_cafes"))
            {
                int oldVersion = Convert.ToInt32(settings["version_cafes"]);
                if (Data.Parser.Cafes.version <= oldVersion)
                {
                    ServiceEventArgs<List<T>> args = new ServiceEventArgs<List<T>>();
                    string xml = loadXML("cafes.txt");
                    if (xml != null)
                    {
                        var result = Parser(xml, Accion.Cafe);
                        args.Data = result as List<T>;

                        state.AsyncCallback(args);
                        inCache = true;
                    }
                }
            }
            else if (tipo.GetType() == typeof(Model.Comida) && settings.Contains("version_comidas"))
            {
                int oldVersion = Convert.ToInt32(settings["version_comidas"]);
                if (Data.Parser.Comidas.version <= oldVersion)
                {
                    ServiceEventArgs<List<T>> args = new ServiceEventArgs<List<T>>();
                    string xml = loadXML("comidas.txt");
                    if (xml != null)
                    {
                        var result = Parser(xml, Accion.Comida);
                        args.Data = result as List<T>;

                        state.AsyncCallback(args);
                        inCache = true;
                    }
                }
            }
            else if (tipo.GetType() == typeof(Model.Tienda) && settings.Contains("version_tiendas"))
            {
                int oldVersion = Convert.ToInt32(settings["version_tiendas"]);
                if (Data.Parser.Tiendas.version <= oldVersion)
                {
                    ServiceEventArgs<List<T>> args = new ServiceEventArgs<List<T>>();
                    string xml = loadXML("tiendas.txt");
                    if (xml != null)
                    {
                        var result = Parser(xml, Accion.Tiendas);
                        args.Data = result as List<T>;

                        state.AsyncCallback(args);
                        inCache = true;
                    }
                }
            }

            if (inCache == false)
            {
                _client.DownloadStringCompleted += (o, e) =>
                {
                    AsyncCallBackParser<T>(e);
                };
                uriService = string.Format("{0}?{1}", service, aleatorioCache.Next(index));
                _client.DownloadStringAsync(new Uri(uriService, UriKind.RelativeOrAbsolute), state);
            }
        }

        private void AsyncCallBackParser<T>(DownloadStringCompletedEventArgs e) where T : class, new()
        {
            IsolatedStorageSettings settings = IsolatedStorageSettings.ApplicationSettings;
            ServiceWebRequestState<T> state = e.UserState as ServiceWebRequestState<T>;
            ServiceEventArgs<List<T>> args = new ServiceEventArgs<List<T>>();
            bool errorInRequest = false;
            bool timeOut = false;
            _client._stopTimer = true;

            if (!e.Cancelled)
            {
                if (null == e.Error)
                {
                    try
                    {
                        var tipo = new T();
                        if (tipo.GetType() == typeof(Model.Usuario))
                        {
                            var result = Parser(e.Result, Accion.Usuario);
                            args.Data = result as List<T>;
                        }

                        if (tipo.GetType() == typeof(Model.Bebida))
                        {
                            saveXML("bebidas.txt", e.Result);
                            if (!settings.Contains("version_bebidas"))
                                settings.Add("version_bebidas", Data.Parser.Bebidas.version);
                            else
                                settings["version_bebidas"] = Data.Parser.Bebidas.version;
                            settings.Save();
                            var result = Parser(e.Result, Accion.Bebidas);
                            args.Data = result as List<T>;
                        }
                        else if (tipo.GetType() == typeof(Model.Cafe))
                        {
                            saveXML("cafes.txt", e.Result);
                            if (!settings.Contains("version_cafes"))
                                settings.Add("version_cafes", Data.Parser.Cafes.version);
                            else
                                settings["version_cafes"] = Data.Parser.Cafes.version;
                            settings.Save();
                            var result = Parser(e.Result, Accion.Cafe);
                            args.Data = result as List<T>;
                        }
                        else if (tipo.GetType() == typeof(Model.Comida))
                        {
                            saveXML("comidas.txt", e.Result);
                            if (!settings.Contains("version_comidas"))
                                settings.Add("version_comidas", Data.Parser.Comidas.version);
                            else
                                settings["version_comidas"] = Data.Parser.Comidas.version;
                            settings.Save();
                            var result = Parser(e.Result, Accion.Comida);
                            args.Data = result as List<T>;
                        }
                        else if (tipo.GetType() == typeof(Model.Tienda))
                        {
                            saveXML("tiendas.txt", e.Result);
                            if (!settings.Contains("version_tiendas"))
                                settings.Add("version_tiendas", Data.Parser.Tiendas.version);
                            else
                                settings["version_tiendas"] = Data.Parser.Tiendas.version;
                            settings.Save();
                            var result = Parser(e.Result, Accion.Tiendas);
                            args.Data = result as List<T>;
                        }
                        else if (tipo.GetType() == typeof(Model.Version))
                        {
                            var result = Parser(e.Result, Accion.Version);
                            args.Data = result as List<T>;
                        }
                    }
                    catch (Exception ex)
                    {
                        args.ErrorParser = true;
                        Log.EscribeLog(string.Format("Error de parseo >>>>{0}", ex.Message));
                    }
                }
                else
                {
                    errorInRequest = true;
                    Log.EscribeLog(string.Format("Error de red {0}", e.Error.Message));
                }
            }
            else
            {
                timeOut = true;
                Log.EscribeLog("TIMEOUT");
            }

            args.ErrorInMakingRequest = errorInRequest;
            args.TimeOut = timeOut;
            state.AsyncCallback(args);
        }

        private IList Parser(string xml, Accion accion)
        {
            IList result = null;
            var listBebidas = new List<Model.Bebida>();
            var listCafes = new List<Model.Cafe>();
            var listComidas = new List<Model.Comida>();
            var listTiendas = new List<Model.Tienda>();
            var listVersiones = new List<Model.Version>();
            var user = new List<Model.Usuario>();

            var doc = XDocument.Parse(xml);
            switch (accion)
            {
                case Accion.Usuario:
                    var dataUser = from c in doc.Descendants("usuario")
                                   select new Model.Usuario
                                   {
                                       Cumple = (string)c.Element("birthDate"),
                                       NumeroTarjeta = (string)c.Element("cardNumber"),
                                       Estrellas = (int)c.Element("estrellas1"),
                                       Saldo = (double)c.Element("saldo"),
                                       FechaRegistro = (string)c.Element("startDate")
                                   };
                    if (dataUser != null)
                        user.AddRange(dataUser.ToList());
                    result = user;
                    break;
                case Accion.Cafe:
                    var dataCafe = from c in doc.Descendants("cafe")
                       select new Model.Cafe
                       {
                           nombre = (string)c.Element("nombre"),
                           descripcion = (string)c.Element("descripcion"),
                           disponibilidad = (string)c.Element("disponibilidad"),
                           perfil = (string)c.Element("perfil"),
                           forma = (string)c.Element("forma"),
                           imagenprincipal = (string)c.Element("imagenprincipal"),
                           sabores = ObtenerSaboresCafe(c),
                           imagenes = ObtenImagenesCafe(c),
                           saboresDesc = ObtenerSaboresCafeDesc(c),
                           imagen = c.Elements("imagenes").Elements("imagen").First().FirstNode.ToString()
                       };
                    if (dataCafe != null)
                        listCafes.AddRange(dataCafe.ToList());
                    result = listCafes;
                    break;
                case Accion.Comida:
                    var dataComida = from c in doc.Descendants("comida")
                       select new Model.Comida
                       {
                           nombre = (string)c.Element("nombre"),
                           descripcion = (string)c.Element("descripcion"),
                           descripcioncorta = (string)c.Element("descripcioncorta"),
                           disponibilidad = (string)c.Element("disponibilidad"),
                           categoria = (string)c.Element("categoria"),
                           imagen = (string)c.Element("imagen")
                       };
                    if (dataComida != null)
                        listComidas.AddRange(dataComida.ToList());
                    result = listComidas;
                    break;
                case Accion.Bebidas:
                    var data = from c in doc.Descendants("bebida")
                       select new Model.Bebida { 
                           nombre = (string)c.Element("nombre"),
                           descripcion = (string)c.Element("descripcion"),
                           descripcioncorta = (string)c.Element("descripcioncorta"),
                           disponibilidad = (string)c.Element("disponibilidad"),
                           temperatura = (string)c.Element("temperatura"),
                           categoria = (string)c.Element("categoria"),
                           subcategoria = (string)c.Element("subcategoria"),
                           marcadobebida = (string)c.Element("marcadobebida"),
                           imagen = (string)c.Element("imagen")
                       };
                    if (data != null)
                        listBebidas.AddRange(data.ToList());
                    result = listBebidas;
                    break;
                case Accion.Tiendas:
                    var dataTiendas = from c in doc.Descendants("tienda")
                       select new Model.Tienda
                       {
                           nombre = (string)c.Element("nombre"),
                           direccion = (string)c.Element("direccion"),
                           codigopostal = (string)c.Element("codigopostal"),
                           ciudad = (string)c.Element("ciudad"),
                           provincia = (string)c.Element("provincia"),
                           zona = (string)c.Element("zona"),
                           latitud = (double)c.Element("latitud"),
                           longitud = (double)c.Element("longitud"),
                           servicios = ObtenerServicios(c),
                           horario = ObtenerHorarios(c)
                       };
                    if (dataTiendas != null)
                        listTiendas.AddRange(dataTiendas.ToList());
                    result = listTiendas;
                    break;
                case Accion.Version:
                    var dataVersion = from c in doc.Descendants("seccion")
                                      select new Model.Version
                                      {
                                          nombre = (string)c.Element("nombre"),
                                          version = (string)c.Element("version"),
                                          fichero = (string)c.Element("fichero"),
                                      };
                    if (dataVersion != null)
                        listVersiones.AddRange(dataVersion.ToList());
                    result = listVersiones;
                    break;
                default:
                    break;
            }
            return result;
        }

        private List<string> ObtenerSaboresCafe(XElement c)
        {
            var result = new List<string>();
            foreach (var item in c.Elements("sabores").Elements("sabor"))
            {
                result.Add(item.FirstNode.ToString());
            }
            return result;
        }

        private List<string> ObtenImagenesCafe(XElement c)
        {
            var result = new List<string>();
            foreach (var item in c.Elements("imagenes").Elements("imagen"))
            {
                result.Add(item.FirstNode.ToString());
            }
            return result;
        }

        private string ObtenerSaboresCafeDesc(XElement c)
        {
            var descripcion = string.Empty;
            var length = c.Elements("sabores").Elements("sabor").Count();
            int i = 1;
            foreach (var item in c.Elements("sabores").Elements("sabor"))
            {
                if (i == length)
                    descripcion += string.Format(" y {0}", item.FirstNode.ToString());
                else if (length >= 3 && (length - 1) != i)
                {
                    descripcion += string.Format("{0}, ", item.FirstNode.ToString());
                }
                else
                    descripcion += string.Format("{0} ", item.FirstNode.ToString());
                i++;
            }
            return descripcion;
        }

        private List<string> ObtenerServicios(XElement c)
        {
            var result = new List<string>();
            foreach (var item in c.Elements("servicios").Elements("servicio"))
            {
                result.Add(item.FirstNode.ToString());
            }
            return result;
        }

        private List<Model.Dia> ObtenerHorarios(XElement c)
        {
            var result = new List<Model.Dia>();
            foreach (var item in c.Elements("horario").Elements("dia"))
            {
                result.Add(new Model.Dia
                {
                    nombre = (string)item.Element("nombre"),
                    horainicio = (string)item.Element("horainicio"),
                    horafin = (string)item.Element("horafin")
                });
            }
            return result;
        }

        private void saveXML(string fileName,string xml)
        {
            IsolatedStorageFile myFile = IsolatedStorageFile.GetUserStoreForApplication();

            StreamWriter sw = new StreamWriter(new IsolatedStorageFileStream(fileName, FileMode.Create, myFile));
            sw.WriteLine(xml); //Wrting to the file
            sw.Close();
        }

        public string loadXML(string fileName)
        {
            IsolatedStorageFile myFile = IsolatedStorageFile.GetUserStoreForApplication();

            StreamReader reader = new StreamReader(new IsolatedStorageFileStream(fileName, FileMode.Open, myFile));
            string xml = reader.ReadToEnd();
            reader.Close();
            return xml;
        }
    }
}
