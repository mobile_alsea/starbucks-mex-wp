﻿using Starbucks.UI.Helper;
using System;
using System.Net;
using System.Windows.Threading;

namespace Starbucks.UI.Data
{
    public class WebClientServices : WebClient
    {
        private HttpWebRequest conn;
        private DispatcherTimer tmr;

        public bool _stopTimer { get; set; }
        public int TimeOut { get; set; }

        [System.Security.SecurityCritical()]
        public WebClientServices()
        {
            TimeOut = 60;
        }

        public WebClientServices(int timeout)
        {
            TimeOut = timeout;
        }

        protected override WebRequest GetWebRequest(System.Uri address)
        {
            HttpWebRequest request;
            try
            {
                tmr = new DispatcherTimer();
                tmr.Interval = TimeSpan.FromSeconds(TimeOut);
                tmr.Tick += new EventHandler(tmr_Tick);
                _stopTimer = false;

                request = base.GetWebRequest(address) as HttpWebRequest;
                conn = request;
                tmr.Start();
                Log.EscribeLog(address.ToString());
                return request;
            }
            catch (Exception ex)
            {
                Log.EscribeLog(ex.Message);
            }
            return null;
        }

        private void tmr_Tick(object sender, EventArgs e)
        {
            if (!_stopTimer)
            {
                tmr.Stop();
                conn.Abort();
                Log.EscribeLog(">>>>>> SERVICIO CANCELADO POR TIMEOUT");
            }
        }
    }
}
