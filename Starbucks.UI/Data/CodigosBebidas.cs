﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Starbucks.UI.Model;

namespace Starbucks.UI.Data
{
    public class CodigosBebidas
    {
        public static List<Codigo> Filtrado { get; set; }
        public static List<Codigo> Espresso { get; set; }
        public static List<Codigo> Infusiones { get; set; }
        public static List<Codigo> TesLatte { get; set; }
        public static List<Codigo> Chocolates { get; set; }
        public static List<Codigo> LecheBebidas { get; set; }
        public static List<Codigo> ConCafe { get; set; }
        public static List<Codigo> SinCafe { get; set; }
        public static List<Codigo> Jarabes { get; set; }
        public static List<Codigo> Personalizacion { get; set; }
        public static List<Codigo> Leches { get; set; }
        public static List<Arte> Arte { get; set; }

        public CodigosBebidas()
        {
            Filtrado = new List<Codigo>();
            Espresso = new List<Codigo>();
            Infusiones = new List<Codigo>();
            TesLatte = new List<Codigo>();
            Chocolates = new List<Codigo>();
            LecheBebidas = new List<Codigo>();
            ConCafe = new List<Codigo>();
            SinCafe = new List<Codigo>();
            Jarabes = new List<Codigo>();
            Personalizacion = new List<Codigo>();
            Leches = new List<Codigo>();
            Arte = new List<Model.Arte>();

            ListFiltrado();
            ListEspresso();
            ListInfusiones();
            ListTesLatte();
            ListChocolates();
            ListLeche();
            ListConCafe();
            ListSinCafe();
            ListJarabes();
            ListPersonalizacion();
            ListLeches();
            ListArte();
        }

        private void ListArte()
        {
            Arte.Add(new Arte { Nombre = "Cafè del Día", Alto = "/Resources/arte/cafe_del_dia_alto.png", Grande = "/Resources/arte/cafe_del_dia_grande.png", Venti = "/Resources/arte/cafe_del_dia_venti.png" });
            Arte.Add(new Arte { Nombre = "Café Frappuccino", Alto = "/Resources/arte/cafe_frappuccino_alto.png", Grande = "/Resources/arte/cafe_frappuccino_grande.png", Venti = "/Resources/arte/cafe_frappuccino_venti.png" });
            Arte.Add(new Arte { Nombre = "Café helado", Alto = "/Resources/arte/cafe_helado_alto.png", Grande = "/Resources/arte/cafe_helado_grande.png", Venti = "/Resources/arte/cafe_helado_venti.png" });
            Arte.Add(new Arte { Nombre = "Café Misto", Alto = "/Resources/arte/cafe_misto_alto.png", Grande = "/Resources/arte/cafe_misto_grande.png", Venti = "/Resources/arte/cafe_misto_venti.png" });
            Arte.Add(new Arte { Nombre = "Caffè Expresso Americano", Alto = "/Resources/arte/cafe_espresso-americano-alto.png", Grande = "/Resources/arte/cafe_espresso-americano-grande.png", Venti = "/Resources/arte/cafe_espresso-americano-venti.png" });
            Arte.Add(new Arte { Nombre = "Caffè Latte", Alto = "/Resources/arte/caffe_latte_alto.png", Grande = "/Resources/arte/caffe_latte_grande.png", Venti = "/Resources/arte/caffe_latte_venti.png" });
            Arte.Add(new Arte { Nombre = "Caffè Mocha", Alto = "/Resources/arte/caffe_mocha_alto.png", Grande = "/Resources/arte/cafe_mocha_grande.png", Venti = "/Resources/arte/cafe_mocha_venti.png" });
            Arte.Add(new Arte { Nombre = "Caffè Mocha Blanco", Alto = "/Resources/arte/cafe_mochablanco_alto.png", Grande = "/Resources/arte/cafe_mochablanco_grande.png", Venti = "/Resources/arte/cafe_mochablanco_venti.png" });
            Arte.Add(new Arte { Nombre = "Cappuccino", Alto = "/Resources/arte/cappuccino_alto.png", Grande = "/Resources/arte/cappuccino_grande.png", Venti = "/Resources/arte/cappuccino_venti.png" });
            Arte.Add(new Arte { Nombre = "Caramel Frappuccino", Alto = "/Resources/arte/caramel_frappuccino_alto.png", Grande = "/Resources/arte/caramel_frappuccino_grande.png", Venti = "/Resources/arte/caramel_frappuccino_venti.png" });
            Arte.Add(new Arte { Nombre = "Caramel Macchiato", Alto = "/Resources/arte/caramel_macchiato_alto.png", Grande = "/Resources/arte/caramel_macchiato_grande.png", Venti = "/Resources/arte/caramel_macchiato_venti.png" });
            Arte.Add(new Arte { Nombre = "Chai Cream Frappuccino", Alto = "/Resources/arte/chai_cream_frappuccino_alto.png", Grande = "/Resources/arte/chai_cream_frappuccino_grande.png", Venti = "/Resources/arte/chai_cream_frappuccino_venti.png" });
            Arte.Add(new Arte { Nombre = "Chocolate", Alto = "/Resources/arte/chocolate_alto.png", Grande = "/Resources/arte/chocolate_grande.png", Venti = "/Resources/arte/chocolate_venti.png" });
            Arte.Add(new Arte { Nombre = "Chocolate Blanco", Alto = "/Resources/arte/chocolate_blanco_alto.png", Grande = "/Resources/arte/chocolate_blanco_grande.png", Venti = "/Resources/arte/chocolate_blanco_venti.png" });
            Arte.Add(new Arte { Nombre = "Chocolate Cream Frappuccino", Alto = "/Resources/arte/chocolate_cream_frappuccino_alto.png", Grande = "/Resources/arte/chocolate_cream_frappuccino_grande.png", Venti = "/Resources/arte/chocolate_cream_frappuccino_venti.png" });
            Arte.Add(new Arte { Nombre = "Espresso Clásico", Alto = "/Resources/arte/espresso_clasico_alto.png", Grande = "/Resources/arte/espresso_clasico_grande.png", Venti = "/Resources/arte/espresso_clasico_venti.png" });
            Arte.Add(new Arte { Nombre = "Espresso con Panna", Alto = "/Resources/arte/espresso_con_panna_alto.png", Grande = "/Resources/arte/espresso_con_panna_grande.png", Venti = "/Resources/arte/espresso_con_panna_venti.png" });
            Arte.Add(new Arte { Nombre = "Espresso Macchiato", Alto = "/Resources/arte/espresso_macchiato_alto.png", Grande = "/Resources/arte/espresso_macchiato_grande.png", Venti = "/Resources/arte/espresso_macchiato_venti.png" });
            Arte.Add(new Arte { Nombre = "Frambuesa Grosella Frappuccino", Alto = "/Resources/arte/frambuesa_grosella_frappuccino_alto.png", Grande = "/Resources/arte/frambuesa_grosella_frappuccino_grande.png", Venti = "/Resources/arte/frambuesa_grosella_frappuccino_venti.png" });
            Arte.Add(new Arte { Nombre = "Frapuccino Chip", Alto = "/Resources/arte/frappuccino_chip_alto.png", Grande = "/Resources/arte/frappuccino_chip_grande.png", Venti = "/Resources/arte/frappuccino_chip_venti.png" });
            Arte.Add(new Arte { Nombre = "Fresa Cream Frappuccino", Alto = "/Resources/arte/fresa_cream_frappuccino_alto.png", Grande = "/Resources/arte/fresa_cream_frappuccino_grande.png", Venti = "/Resources/arte/fresa_cream_frappuccino_venti.png" });
            Arte.Add(new Arte { Nombre = "Green Tea Frappuccino", Alto = "/Resources/arte/green_tea_frappuccino_alto.png", Grande = "/Resources/arte/green_tea_frappuccino_grande.png", Venti = "/Resources/arte/green_tea_frappuccino_venti.png" });
            Arte.Add(new Arte { Nombre = "Green Tea Latte", Alto = "/Resources/arte/green_tea_latte_alto.png", Grande = "/Resources/arte/green_tea_latte_grande.png", Venti = "/Resources/arte/green_tea_latte_venti.png" });
            Arte.Add(new Arte { Nombre = "Helado Cajeta Latte", Alto = "/Resources/arte/helado_cajeta_latte_alto.png", Grande = "/Resources/arte/helado_cajeta_latte_grande.png", Venti = "/Resources/arte/helado_cajeta_latte_venti.png" });
            Arte.Add(new Arte { Nombre = "Helado Caramel Macchiato", Alto = "/Resources/arte/helado_caramel_macchiato_alto.png", Grande = "/Resources/arte/helado_caramel_macchiato_grande.png", Venti = "/Resources/arte/helado_caramel_macchiato_venti.png" });
            Arte.Add(new Arte { Nombre = "Helado Green Tea Latte", Alto = "/Resources/arte/helado_green_tea_latte_alto.png", Grande = "/Resources/arte/helado_green_tea_latte_grande.png", Venti = "/Resources/arte/helado_green_tea_latte_venti.png" });
            Arte.Add(new Arte { Nombre = "Helado Latte", Alto = "/Resources/arte/helado_latte_alto.png", Grande = "/Resources/arte/helado_latte_grande.png", Venti = "/Resources/arte/helado_latte_venti.png" });
            Arte.Add(new Arte { Nombre = "Helado Mocha", Alto = "/Resources/arte/helado_mocha_alto.png", Grande = "/Resources/arte/helado_mocha_grande.png", Venti = "/Resources/arte/helado_mocha_venti.png" });
            Arte.Add(new Arte { Nombre = "Helado Mocha Blanco", Alto = "/Resources/arte/helado_mocha_blanco_alto.png", Grande = "/Resources/arte/helado_mocha_blanco_grande.png", Venti = "/Resources/arte/helado_mocha_blanco_venti.png" });
            Arte.Add(new Arte { Nombre = "Helado Té Chai Latte", Alto = "/Resources/arte/helado_te_chai_latte_alto.png", Grande = "/Resources/arte/helado_te_chai_latte_grande.png", Venti = "/Resources/arte/helado_te_chai_latte_venti.png" });
            Arte.Add(new Arte { Nombre = "Leche al vapor", Alto = "/Resources/arte/leche_al_vapor_alto.png", Grande = "/Resources/arte/leche_al_vapor_grande.png", Venti = "/Resources/arte/leche_al_vapor_venti.png" });
            Arte.Add(new Arte { Nombre = "Mango Maracuyá Frappuccino", Alto = "/Resources/arte/mango_maracuya_frappuccino_alto.png", Grande = "/Resources/arte/mango_maracuya_frappuccino_grande.png", Venti = "/Resources/arte/mango_maracuya_frappuccino_venti.png" });
            Arte.Add(new Arte { Nombre = "Mocha Blanco Frappuccino", Alto = "/Resources/arte/mocha_blanco_frappuccino_alto.png", Grande = "/Resources/arte/mocha_blanco_frappuccino_grande.png", Venti = "/Resources/arte/mocha_blanco_frappuccino_venti.png" });
            Arte.Add(new Arte { Nombre = "Mocha Frappuccino", Alto = "/Resources/arte/mocha_frappuccino_alto.png", Grande = "/Resources/arte/mocha_frappuccino_grande.png", Venti = "/Resources/arte/mocha_frappuccino_venti.png" });
            Arte.Add(new Arte { Nombre = "Shaken Green Tea", Alto = "/Resources/arte/shaken_green_tea_alto.png", Grande = "/Resources/arte/shaken_green_tea_grande.png", Venti = "/Resources/arte/shaken_green_tea_venti.png" });
            Arte.Add(new Arte { Nombre = "Shaken Lemon Black Tea", Alto = "/Resources/arte/shaken_lemon_black_tea_alto.png", Grande = "/Resources/arte/shaken_lemon_black_tea_grande.png", Venti = "/Resources/arte/shaken_lemon_black_tea_venti.png" });
            Arte.Add(new Arte { Nombre = "Shaken Lemon Passion Tea", Alto = "/Resources/arte/shaken_lemon_passion_tea_alto.png", Grande = "/Resources/arte/shaken_lemon_passion_tea_grande.png", Venti = "/Resources/arte/shaken_lemon_passion_tea_venti.png" });
            Arte.Add(new Arte { Nombre = "Té Chai", Alto = "/Resources/arte/te_chai_alto.png", Grande = "/Resources/arte/te_chai_grande.png", Venti = "/Resources/arte/te_chai_venti.png" });
            Arte.Add(new Arte { Nombre = "Té Chai Latte", Alto = "/Resources/arte/te_chai_latte_alto.png", Grande = "/Resources/arte/te_chai_latte_grande.png", Venti = "/Resources/arte/te_chai_latte_venti.png" });
            Arte.Add(new Arte { Nombre = "Té China Green Tips", Alto = "/Resources/arte/te_china_green_tips_alto.png", Grande = "/Resources/arte/te_china_green_tips_grande.png", Venti = "/Resources/arte/te_china_green_tips_venti.png" });
            Arte.Add(new Arte { Nombre = "Té de Manzanilla", Alto = "/Resources/arte/te_mazanilla_alto.png", Grande = "/Resources/arte/te_mazanilla_grande.png", Venti = "/Resources/arte/te_mazanilla_venti.png" });
            Arte.Add(new Arte { Nombre = "Té de Menta", Alto = "/Resources/arte/te_de_menta_alto.png", Grande = "/Resources/arte/te_de_menta_grande.png", Venti = "/Resources/arte/te_de_menta_venti.png" });
            Arte.Add(new Arte { Nombre = "Té English Breakfast", Alto = "/Resources/arte/te_english_breakfast_alto.png", Grande = "/Resources/arte/te_english_breakfast_grande.png", Venti = "/Resources/arte/te_english_breakfast_venti.png" });
            Arte.Add(new Arte { Nombre = "Té helado", Alto = "/Resources/arte/te_helado_alto.png", Grande = "/Resources/arte/te_helado_grande.png", Venti = "/Resources/arte/te_helado_venti.png" });
            Arte.Add(new Arte { Nombre = "Té Passion Hibiscus", Alto = "/Resources/arte/te_passion_hibiscus_alto.png", Grande = "/Resources/arte/te_passion_hibiscus_grande.png", Venti = "/Resources/arte/te_passion_hibiscus_venti.png" });
            Arte.Add(new Arte { Nombre = "Té Vainilla Rooibos", Alto = "/Resources/arte/te_vainilla_rooibos_alto.png", Grande = "/Resources/arte/te_vainilla_rooibos_grande.png", Venti = "/Resources/arte/te_vainilla_rooibos_venti.png" });
            Arte.Add(new Arte { Nombre = "Vainilla Cappuccino", Alto = "/Resources/arte/vainilla_cappuccino_alto.png", Grande = "/Resources/arte/vainilla_cappuccino_grande.png", Venti = "/Resources/arte/vainilla_cappuccino_venti.png" });
            Arte.Add(new Arte { Nombre = "Vainilla Cream Frappuccino", Alto = "/Resources/arte/vainilla_cream_frappuccino_alto.png", Grande = "/Resources/arte/vainilla_cream_frappuccino_grande.png", Venti = "/Resources/arte/vainilla_cream_frappuccino_venti.png" });
            Arte.Add(new Arte { Nombre = "Vanilla Rooibos Latte", Alto = "/Resources/arte/vainilla_rooibos_latte_alto.png", Grande = "/Resources/arte/vainilla_rooibos_latte_grande.png", Venti = "/Resources/arte/vainilla_rooibos_latte_venti.png" });
            Arte.Add(new Arte { Nombre = "English Breakfast Latte", Alto = "/Resources/arte/english_breakfast_latte_alto.png", Grande = "/Resources/arte/english_breakfast_latte_grande.png", Venti = "/Resources/arte/english_breakfast_latte_venti.png" });
        }

        private void ListLeches()
        {
            Leches = new List<Codigo>();
            Leches.Add(new Codigo { Nombre = "Entera", Clave = "E" });
            Leches.Add(new Codigo { Nombre = "Light", Clave = "L" });
            Leches.Add(new Codigo { Nombre = "Deslactosada", Clave = "D" });
            Leches.Add(new Codigo { Nombre = "Deslactosada Light", Clave = "DL" });
            Leches.Add(new Codigo { Nombre = "Soya", Clave = "SOY" });
            Leches.Add(new Codigo { Nombre = "Half-half", Clave = "B" });
        }

        private void ListPersonalizacion()
        {
            Personalizacion.Add(new Codigo { Nombre = "Crema Batida", Clave = "CB" });
            Personalizacion.Add(new Codigo { Nombre = "Espiral de Mocha", Clave = "M" });
            Personalizacion.Add(new Codigo { Nombre = "Espiral de Caramelo", Clave = "RC" });
            Personalizacion.Add(new Codigo { Nombre = "Espiral de Cajeta", Clave = "K" });
            Personalizacion.Add(new Codigo { Nombre = "Más espuma", Clave = "Dry" });
            Personalizacion.Add(new Codigo { Nombre = "Más leche", Clave = "Wet" });
            Personalizacion.Add(new Codigo { Nombre = "Extra hot", Clave = "XH" });
            Personalizacion.Add(new Codigo { Nombre = "Tibio", Clave = "130°" });
            Personalizacion.Add(new Codigo { Nombre = "Splenda", Clave = "SPL" });
            Personalizacion.Add(new Codigo { Nombre = "Canderel", Clave = "CAN" });
            Personalizacion.Add(new Codigo { Nombre = "Azúcar", Clave = "AZ" });
            Personalizacion.Add(new Codigo { Nombre = "Azúcar Mascabado", Clave = "AM" });
            Personalizacion.Add(new Codigo { Nombre = "Agua", Clave = "AGUA" });
            Personalizacion.Add(new Codigo { Nombre = "Hielo", Clave = "ICE" });
            Personalizacion.Add(new Codigo { Nombre = "Canela en Polvo", Clave = "PCIN" });
            Personalizacion.Add(new Codigo { Nombre = "Vainilla en Polvo", Clave = "PV" });
            Personalizacion.Add(new Codigo { Nombre = "Mocha en Polvo", Clave = "PM" });
            Personalizacion.Add(new Codigo { Nombre = "Chip", Clave = "CHIP" });
        }

        private void ListJarabes()
        {
            Jarabes.Add(new Codigo { Nombre = "Vainilla", Clave = "V" });
            Jarabes.Add(new Codigo { Nombre = "Caramelo", Clave = "CR" });
            Jarabes.Add(new Codigo { Nombre = "Cinnamon Dolce", Clave = "CD" });
            Jarabes.Add(new Codigo { Nombre = "Clásico", Clave = "CL" });
            Jarabes.Add(new Codigo { Nombre = "Avellana", Clave = "H" });
            Jarabes.Add(new Codigo { Nombre = "Menta", Clave = "MENTA" });
            Jarabes.Add(new Codigo { Nombre = "Coco", Clave = "CO" });
            Jarabes.Add(new Codigo { Nombre = "Almendra", Clave = "A" });
            Jarabes.Add(new Codigo { Nombre = "Frambuesa", Clave = "F" });
            Jarabes.Add(new Codigo { Nombre = "Vainilla Sugar Free", Clave = "VSF" });
            Jarabes.Add(new Codigo { Nombre = "Mocha", Clave = "M" });
            Jarabes.Add(new Codigo { Nombre = "Mocha Blanco", Clave = "MB" });
            Jarabes.Add(new Codigo { Nombre = "Salsa Fresa", Clave = "FRESA" });
            Jarabes.Add(new Codigo { Nombre = "Cajeta", Clave = "K" });
        }

        private void ListSinCafe()
        {
            SinCafe.Add(new Codigo { Nombre = "Té helado", Clave = "T" });
            SinCafe.Add(new Codigo { Nombre = "Shaken Lemon Passion Tea", Clave = "LPT" });
            SinCafe.Add(new Codigo { Nombre = "Shaken Lemon Black Tea", Clave = "LBT" });
            SinCafe.Add(new Codigo { Nombre = "Shaken Green Tea", Clave = "LGT" });
            SinCafe.Add(new Codigo { Nombre = "Helado Té Chai Latte", Clave = "CHAI" });
            SinCafe.Add(new Codigo { Nombre = "Chai Cream Frappuccino", Clave = "CRM" });
            SinCafe.Add(new Codigo { Nombre = "Helado Green Tea Latte", Clave = "GTL" });
            SinCafe.Add(new Codigo { Nombre = "Green Tea Frappuccino", Clave = "CRM" });
            SinCafe.Add(new Codigo { Nombre = "Chocolate Cream Frappuccino", Clave = "CRM" });
            SinCafe.Add(new Codigo { Nombre = "Vainilla Cream Frappuccino", Clave = "CRM" });
            SinCafe.Add(new Codigo { Nombre = "Fresa Cream Frappuccino", Clave = "CRM" });
            SinCafe.Add(new Codigo { Nombre = "Mango Maracuyá Frappuccino", Clave = "MM" });
            SinCafe.Add(new Codigo { Nombre = "Frambuesa Grosella Frappuccino", Clave = "FG" });
        }

        private void ListConCafe()
        {
            ConCafe.Add(new Codigo { Nombre = "Café helado", Clave = "IC" });
            ConCafe.Add(new Codigo { Nombre = "Helado Latte", Clave = "L" });
            ConCafe.Add(new Codigo { Nombre = "Helado Cajeta Latte", Clave = "L" });
            ConCafe.Add(new Codigo { Nombre = "Helado Caramel Macchiato", Clave = "CM" });
            ConCafe.Add(new Codigo { Nombre = "Helado Mocha", Clave = "M" });
            ConCafe.Add(new Codigo { Nombre = "Helado Mocha Blanco", Clave = "MB" });
            ConCafe.Add(new Codigo { Nombre = "Caramel Frappuccino", Clave = "CRF" });
            ConCafe.Add(new Codigo { Nombre = "Café Frappuccino", Clave = "CF" });
            ConCafe.Add(new Codigo { Nombre = "Mocha Frappuccino", Clave = "MF" });
            ConCafe.Add(new Codigo { Nombre = "Mocha Blanco Frappuccino", Clave = "MBF" });
            ConCafe.Add(new Codigo { Nombre = "Frapuccino Chip", Clave = "FC" });
        }

        private void ListLeche()
        {
            LecheBebidas.Add(new Codigo { Nombre = "Leche al vapor", Clave = "LV" });
        }

        private void ListChocolates()
        {
            Chocolates.Add(new Codigo { Nombre = "Chocolate", Clave = "CH" });
            Chocolates.Add(new Codigo { Nombre = "Chocolate Blanco", Clave = "CHB" });
        }

        private void ListTesLatte()
        {
            TesLatte.Add(new Codigo { Nombre = "Té Chai Latte", Clave = "CHAI" });
            TesLatte.Add(new Codigo { Nombre = "Green Tea Latte", Clave = "GTL" });
            TesLatte.Add(new Codigo { Nombre = "English Breakfast Latte", Clave = "EBTL" });
            TesLatte.Add(new Codigo { Nombre = "Vanilla Rooibos Latte", Clave = "VRTL" });
        }

        private void ListInfusiones()
        {
            Infusiones.Add(new Codigo { Nombre = "Té Passion Hibiscus", Clave = "T" });
            Infusiones.Add(new Codigo { Nombre = "Té de Menta", Clave = "T" });
            Infusiones.Add(new Codigo { Nombre = "Té de Manzanilla", Clave = "T" });
            Infusiones.Add(new Codigo { Nombre = "Té China Green Tips", Clave = "T" });
            Infusiones.Add(new Codigo { Nombre = "Té English Breakfast", Clave = "T" });
            Infusiones.Add(new Codigo { Nombre = "Té Chai", Clave = "T" });
            Infusiones.Add(new Codigo { Nombre = "Té Vainilla Rooibos", Clave = "T" });
        }

        private void ListEspresso()
        {
            Espresso.Add(new Codigo { Nombre = "Espresso Clásico", Clave = "E" });
            Espresso.Add(new Codigo { Nombre = "Espresso con Panna", Clave = "EP" });
            Espresso.Add(new Codigo { Nombre = "Espresso Macchiato", Clave = "EM" });
            Espresso.Add(new Codigo { Nombre = "Caffè Expresso Americano", Clave = "EA" });
            Espresso.Add(new Codigo { Nombre = "Caffè Latte", Clave = "L" });
            Espresso.Add(new Codigo { Nombre = "Caramel Macchiato", Clave = "CM" });
            Espresso.Add(new Codigo { Nombre = "Capuccino", Clave = "C" });
            Espresso.Add(new Codigo { Nombre = "Vainilla Cappuccino", Clave ="C" });
            Espresso.Add(new Codigo { Nombre = "Caffè Mocha", Clave = "M" });
            Espresso.Add(new Codigo { Nombre = "Caffè Mocha Blanco", Clave = "MB" });
        }

        private void ListFiltrado()
        {
            Filtrado.Add(new Codigo { Nombre = "Cafè del Día", Clave = "COD" });
            Filtrado.Add(new Codigo { Nombre = "Café Misto", Clave = "MIS" });
        }
    }
}
