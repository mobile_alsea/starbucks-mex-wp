﻿using System;
using System.Collections.Generic;
using System.Device.Location;
using System.IO.IsolatedStorage;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Media;
using System.Windows.Navigation;
using System.Windows.Threading;
using Coding4Fun.Phone.Controls;
using Microsoft.Devices;
using Microsoft.Devices.Sensors;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Controls.Maps;
using Microsoft.Phone.Shell;
using Microsoft.Phone.Tasks;
using Microsoft.Xna.Framework;
using Starbucks.UI.Helper;
using Starbucks.UI.Model;
using Starbucks.UI.ViewModel;
using System.Windows.Input;

namespace Starbucks.UI.View.Tiendas
{
    public partial class Mapa : PhoneApplicationPage
    {
        private bool isCompletedMap;
        private bool isPortrait;
        private bool isDataValid;
        private DispatcherTimer timer;
        private Accelerometer accelerometer;
        private Vector3 acceleration;
        private bool search;
        private bool IsFromRA;

        public Mapa()
        {
            InitializeComponent();
            App.TiendasViewModel.CurrentMap = MapaView;
            if (!Accelerometer.IsSupported)
            {
                MessageBox.Show(StarbucksResource.Label_DeviceNoSupport);
            }
            else
            {
                timer = new DispatcherTimer();
                timer.Interval = TimeSpan.FromMilliseconds(30);
                timer.Tick += new EventHandler(timer_Tick);

                if (accelerometer == null)
                {
                    accelerometer = new Accelerometer();
                    accelerometer.TimeBetweenUpdates = TimeSpan.FromSeconds(.5);
                    accelerometer.CurrentValueChanged += new EventHandler<SensorReadingEventArgs<AccelerometerReading>>(accelerometer_CurrentValueChanged);
                }
            }
            CargaAplicationBar();
            Loaded += Mapa_Loaded;
        }

        void Mapa_Loaded(object sender, RoutedEventArgs e)
        {
            IsFromRA = false;
        }

        private void Start()
        {
            accelerometer.Start();
            timer.Start();
        }

        private void Stop()
        {
            accelerometer.Stop();
            timer.Stop();
        }

        private void CargaAplicationBar()
        {
            if (ApplicationBar == null)
                ApplicationBar = new ApplicationBar();
            if (Convert.ToBoolean(App.Settings[Constantes.Switch_RA]))
            {
                var appBarCancelaRA = new ApplicationBarIconButton(new Uri("/Assets/AppBar/actionbar_icon_cancelar.png", UriKind.Relative)) { Text = "desactivar vista" };
                appBarCancelaRA.Click += appBarCancelaRA_Click;
                ApplicationBar.Buttons.Insert(0, appBarCancelaRA);
            }
            else
            {
                var appBarRA = new ApplicationBarIconButton(new Uri("/Assets/AppBar/actionbar_icon_vista.png", UriKind.Relative)) { Text = "vista" };
                appBarRA.Click += appBarRA_Click;
                ApplicationBar.Buttons.Insert(0, appBarRA);
            }
        }

        void appBarCancelaRA_Click(object sender, EventArgs e)
        {
            App.Settings[Constantes.Switch_RA] = false;
            ApplicationBar.Buttons.RemoveAt(0);
            CargaAplicationBar();
            Stop();
        }

        void appBarRA_Click(object sender, EventArgs e)
        {
            App.Settings[Constantes.Switch_RA] = true;
            ApplicationBar.Buttons.RemoveAt(0);
            CargaAplicationBar();
            Start();
        }

        void accelerometer_CurrentValueChanged(object sender, SensorReadingEventArgs<AccelerometerReading> e)
        {
            isDataValid = accelerometer.IsDataValid;
            acceleration = e.SensorReading.Acceleration;
        }

        void timer_Tick(object sender, EventArgs e)
        {
            if (isDataValid)
            {
                if (!isPortrait)
                {
                    if (Convert.ToDouble(acceleration.Y.ToString("0.00")) <= -0.95)
                    {
                        if (App.Settings.Contains("SalirRA"))
                            if (App.Settings["SalirRA"].ToString() == "true")
                            {
                                timer.Stop();
                                App.Settings["SalirRA"] = "false";

                                App.Settings[Constantes.Switch_RA] = false;
                                ApplicationBar.Buttons.RemoveAt(0);
                                CargaAplicationBar();
                                Stop();

                                return;
                            }

                        Stop();
                        acceleration.Y = 0;
                        NavigationService.Navigate(new Uri("/View/RA/ARView.xaml", UriKind.Relative));
                    }
                }
            }
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);

            if (e.NavigationMode == NavigationMode.New)
            {
                if (!App.Settings.Contains(Constantes.MostrarMensaje_RA))
                    Starbucks.UI.Controls.MessageBox.Show(string.Empty, StarbucksResource.Label_RealidadAumentada, "OK", string.Empty);
            }

            if (NavigationContext.QueryString.Keys.Contains("long"))
            {
                search = true;
                var lon = Convert.ToDouble(NavigationContext.QueryString["long"].ToString());
                var lat = Convert.ToDouble(NavigationContext.QueryString["lat"].ToString());
                App.TiendasViewModel.MyLocationSearch = new GeoCoordinate(lat, lon);
                CargaTiendas();
            }
            else
            {
                if (App.ViewModel.GpsDisponible)
                {
                    if (!isCompletedMap)
                        CargaTiendas();
                }
            }
            if (Convert.ToBoolean(App.Settings[Constantes.Switch_RA]))
                Start();
        }

        private void CargaTiendas()
        {
            if (!search)
                PinLocation();

            var list = App.TiendasViewModel.ObtenerListTiendas();
            
            var radio = App.Settings.Contains(Constantes.LocalizaTiendas) ? App.Settings[Constantes.LocalizaTiendas].ToString() : string.Empty;

            if (search)
            {
                list.ForEach(c =>
                {
                    var posicionOrigen = new Posicion(App.TiendasViewModel.MyLocationSearch.Latitude, App.TiendasViewModel.MyLocationSearch.Longitude);
                    var posicionDestion = new Posicion(c.latitud, c.longitud);
                    var distancia = posicionOrigen.DistanciaKm(posicionDestion);
                    if (radio.Equals(string.Empty))
                    {
                        var location = new GeoCoordinate(Convert.ToDouble(c.latitud), Convert.ToDouble(c.longitud));
                        var pin = new Pushpin
                        {
                            Location = location,
                            Template = Resources["PushpinControlTemplateMap"] as ControlTemplate,
                            DataContext = c
                        };
                        pin.Location = location;
                        mapControl.Items.Add(pin);
                    }
                    else
                    {
                        if (Math.Round(distancia) <= Convert.ToInt32(radio))
                        {
                            var location = new GeoCoordinate(Convert.ToDouble(c.latitud), Convert.ToDouble(c.longitud));
                            var pin = new Pushpin
                            {
                                Location = location,
                                Template = Resources["PushpinControlTemplateMap"] as ControlTemplate,
                                DataContext = c
                            };
                            pin.Location = location;
                            mapControl.Items.Add(pin);
                        }
                    }
                });
                isCompletedMap = true;
                App.TiendasViewModel.CurrentMapControl = mapControl;
                MapaView.SetView(App.TiendasViewModel.MyLocationSearch, 16);
            }
            else
            {
                list.ForEach(c =>
                {
                    var posicionOrigen = new Posicion(App.TiendasViewModel.MyLocation.Latitude, App.TiendasViewModel.MyLocation.Longitude);
                    var posicionDestion = new Posicion(c.latitud, c.longitud);
                    var distancia = posicionOrigen.DistanciaKm(posicionDestion);
                    if (radio.Equals(string.Empty))
                    {
                        var location = new GeoCoordinate(Convert.ToDouble(c.latitud), Convert.ToDouble(c.longitud));
                        var pin = new Pushpin
                        {
                            Location = location,
                            Template = Resources["PushpinControlTemplateMap"] as ControlTemplate,
                            DataContext = c
                        };
                        pin.Location = location;
                        mapControl.Items.Add(pin);
                    }
                    else
                    {
                        if (Math.Round(distancia) <= Convert.ToInt32(radio))
                        {
                            var location = new GeoCoordinate(Convert.ToDouble(c.latitud), Convert.ToDouble(c.longitud));
                            var pin = new Pushpin
                            {
                                Location = location,
                                Template = Resources["PushpinControlTemplateMap"] as ControlTemplate,
                                DataContext = c
                            };
                            pin.Location = location;
                            mapControl.Items.Add(pin);
                        }
                    }
                });
                isCompletedMap = true;
                App.TiendasViewModel.CurrentMapControl = mapControl;
                MapaView.SetView(App.TiendasViewModel.MyLocation, 16);
            }
        }

        private void PinLocation()
        {
            var location = new GeoCoordinate(App.TiendasViewModel.MyLocation.Latitude, App.TiendasViewModel.MyLocation.Longitude);
            var pin = new Pushpin
            {
                Location = location,
                Template = Resources["CurrentControlTemplateMap"] as ControlTemplate
            };
            pin.Location = location;
            mapControl.Items.Add(pin);
        }

        private void ApbLista_Click(object sender, EventArgs e)
        {
            NavigationService.Navigate(new Uri("/View/Tiendas/Listado.xaml", UriKind.Relative));
        }

        private void ApbFiltros_Click(object sender, EventArgs e)
        {
            NavigationService.Navigate(new Uri("/View/Tiendas/Filtros.xaml", UriKind.Relative));
        }

        protected override void OnNavigatedFrom(NavigationEventArgs e)
        {
            Stop();
            IsFromRA = false;
        }

        private void btnBuscaTiendas_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            NavigationService.Navigate(new Uri(string.Format("{0}?search={1}", StarbucksResource.View_ResultadosTiendas, txtTiendas.Text), UriKind.Relative));
            txtTiendas.Text = string.Empty;
        }

        private void txtTiendas_KeyUp(object sender, System.Windows.Input.KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                NavigationService.Navigate(new Uri(string.Format("{0}?search={1}", StarbucksResource.View_ResultadosTiendas, txtTiendas.Text), UriKind.Relative));
                txtTiendas.Text = string.Empty;
            }
        }
    }
}