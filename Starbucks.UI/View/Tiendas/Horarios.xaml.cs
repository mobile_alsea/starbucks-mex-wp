﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;

namespace Starbucks.UI.View.Tiendas
{
    public partial class Horarios : PhoneApplicationPage
    {
        public Horarios()
        {
            InitializeComponent();
            DataContext = App.ViewModel.SelectedListHorarios;
            listHorarios.ItemsSource = App.ViewModel.SelectedListHorarios;
        }
    }
}