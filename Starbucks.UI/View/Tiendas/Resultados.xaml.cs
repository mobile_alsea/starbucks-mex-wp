﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Coding4Fun.Phone.Controls;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using Starbucks.UI.Helper;

namespace Starbucks.UI.View.Tiendas
{
    public partial class Resultados : PhoneApplicationPage
    {
        public Resultados()
        {
            InitializeComponent();
            DataContext = App.TiendasViewModel;
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);
            if (NavigationContext.QueryString.Keys.Contains("search"))
            {
                var search = NavigationContext.QueryString["search"].ToString();
                var listResultados = App.TiendasViewModel.ObtenerListTiendas().Select(c => c.direccion).ToList().Where(c => c.ToLower().Contains(search.ToLower())).ToList();
                if (listResultados.Count <= 0)
                {
                    var prompt = new ToastPrompt();
                    prompt.Message = "No existen resultados para su búsqueda.";
                    prompt.Show();
                }
                var tiendas = new List<Model.Tienda>();
                listResultados.ForEach(c =>
                    {
                        foreach (var item in App.TiendasViewModel.ObtenerListTiendas().Where(d => d.direccion.Equals(c)).ToList())
                        {
                            if (!tiendas.Contains(item))
                                tiendas.Add(item);
                        }
                    });
                listTiendas.ItemsSource = tiendas;
            }
        }

        private void listTiendas_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (listTiendas.SelectedIndex > -1 && listTiendas.SelectedItem != null)
            {
                var tiendaContex = listTiendas.SelectedItem as Model.Tienda;
                NavigationService.Navigate(new Uri(string.Format("{0}?long={1}&lat={2}", StarbucksResource.View_Mapa, tiendaContex.longitud, tiendaContex.latitud), UriKind.Relative));
            }
        }
    }
}