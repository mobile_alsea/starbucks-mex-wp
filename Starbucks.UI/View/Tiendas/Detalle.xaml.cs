﻿using System;
using System.Collections.Generic;
using System.Device.Location;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Resources;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using Microsoft.Phone.Tasks;
using Starbucks.UI.Helper;

namespace Starbucks.UI.View.Tiendas
{
    public partial class Detalle : PhoneApplicationPage
    {
        private bool EstatusFavorito;
        private bool isLoad;
        private Random aleatorio;

        public Detalle()
        {
            InitializeComponent();
            var number = new string[] { "1", "2", "3", "4", "5", "6", "7" };
            aleatorio = new Random();
            var indice = aleatorio.Next(number.Length);
            Uri uri = new Uri(string.Format("/Resources/tileMapa/starbucks_WP_mapa_img_0{0}b.png", number[indice]), UriKind.Relative);
            imgDetalle.Source = new BitmapImage(uri);
            DataContext = App.TiendasViewModel.SelectedItemTienda;
            var servicios = App.TiendasViewModel.SelectedItemTienda.servicios;
            servicios.ForEach(c =>
            {
                switch (c)
                {
                    case "Wireless HotSpot":
                        imgWifi.Visibility = Visibility.Visible;
                        break;
                    case "Alimento calentado al Horno":
                        imgHorno.Visibility = Visibility.Visible;
                        break;
                    case "Drive-Thru":
                        imgDrive.Visibility = Visibility.Visible;
                        break;
                    case "Pet friendly":
                        imgPet.Visibility = Visibility.Visible;
                        break;
                    default:
                        break;
                }
            });
            Loaded += Detalle_Loaded;
        }

        void Detalle_Loaded(object sender, RoutedEventArgs e)
        {
            App.TiendasViewModel.EliminaPinActivo();
            if (!isLoad)
            {
                if (ApplicationBar == null)
                    ApplicationBar = new ApplicationBar();
                VerificaFavorito();
                if (!EstatusFavorito)
                {
                    var appBarFavorito = new ApplicationBarIconButton(new Uri("/Assets/AppBar/actionbar_icon_agregarfavoritos.png", UriKind.Relative)) { Text = "Añadir a Tiendas" };
                    appBarFavorito.Click += appBarFavorito_Click;
                    ApplicationBar.Buttons.Add(appBarFavorito);
                }
                else
                {
                    var appBarEliminaFavorito = new ApplicationBarIconButton(new Uri("/Assets/AppBar/actionbar_icon_quitarfavoritos.png", UriKind.Relative)) { Text = "Eliminar de Tiendas" };
                    appBarEliminaFavorito.Click += appBarEliminaFavorito_Click;
                    ApplicationBar.Buttons.Add(appBarEliminaFavorito);
                }
                isLoad = true;
            }
        }

        void appBarEliminaFavorito_Click(object sender, EventArgs e)
        {
            App.FavoritosViewModel.EliminaFavorito<Model.Tienda>(App.TiendasViewModel.SelectedItemTienda, Accion.Tiendas);
            ApplicationBar.Buttons.Clear();
            var appBarFavorito = new ApplicationBarIconButton(new Uri("/Assets/AppBar/actionbar_icon_agregarfavoritos.png", UriKind.Relative)) { Text = "Añadir a Tiendas" };
            appBarFavorito.Click += appBarFavorito_Click;
            ApplicationBar.Buttons.Add(appBarFavorito);
        }

        void appBarFavorito_Click(object sender, EventArgs e)
        {
            App.FavoritosViewModel.AgregaFavorito<Model.Tienda>(App.TiendasViewModel.SelectedItemTienda, Accion.Tiendas);
            ApplicationBar.Buttons.Clear();
            var appBarEliminaFavorito = new ApplicationBarIconButton(new Uri("/Assets/AppBar/actionbar_icon_quitarfavoritos.png", UriKind.Relative)) { Text = "Eliminar de Tiendas" };
            appBarEliminaFavorito.Click += appBarEliminaFavorito_Click;
            ApplicationBar.Buttons.Add(appBarEliminaFavorito);
        }

        private void VerificaFavorito()
        {
            var tienda = App.TiendasViewModel.SelectedItemTienda;
            var item = App.FavoritosViewModel.FavoritoTiendas.Where(c => c.nombre.Equals(tienda.nombre)).SingleOrDefault();
            if (item != null)
                EstatusFavorito = true;
            else
                EstatusFavorito = false;
        }

        private void btnHorarios_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            NavigationService.Navigate(new Uri(StarbucksResource.View_Horarios, UriKind.Relative));
        }

        private void btnDireccion_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            //Implementación con servicio de Bing
            //App.ViewModel.RouteLoaded();
            //App.ViewModel.RouteResolved += ViewModel_RouteResolved;

            Helper.Mapa.ObtenerDireccion();
        }

        //void ViewModel_RouteResolved(object sender, EventArgs e)
        //{
        //    NavigationService.Navigate(new Uri(Constantes.ViewRuta, UriKind.Relative));
        //}

        private void btnInvitaAmigo_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            NavigationService.Navigate(new Uri(StarbucksResource.View_Contactos, UriKind.Relative));
        }
    }
}