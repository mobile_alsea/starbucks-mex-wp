﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;

namespace Starbucks.UI.View.Tiendas
{
    public partial class Filtros : PhoneApplicationPage
    {
        public Filtros()
        {
            InitializeComponent();
            Loaded += Filtros_Loaded;
        }

        void Filtros_Loaded(object sender, RoutedEventArgs e)
        {
            App.ViewModel.SelectedFiltros.ForEach(c => {
                switch (c)
                {
                    case "Wireless HotSpot":
                        chkWireless.IsChecked = true;
                        break;
                    case "Alimento calentado al Horno":
                        chkHorno.IsChecked = true;
                        break;
                    case "Drive-Thru":
                        chkDrive.IsChecked = true;
                        break;
                    case "Pet Friendly":
                        chkPet.IsChecked = true;
                        break;
                    default:
                        break;
                }
            });
        }

        private void chk_Checked(object sender, RoutedEventArgs e)
        {
            var control = sender as CheckBox;
            if (!App.ViewModel.SelectedFiltros.Contains(control.Content.ToString()))
                App.ViewModel.SelectedFiltros.Insert(0, control.Content.ToString());
        }

        private void chk_Unchecked(object sender, RoutedEventArgs e)
        {
            var control = sender as CheckBox;
            App.ViewModel.SelectedFiltros.Remove(control.Content.ToString());
        }
    }
}