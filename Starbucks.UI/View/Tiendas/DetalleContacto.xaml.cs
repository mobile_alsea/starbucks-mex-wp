﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using Starbucks.UI.Helper;

namespace Starbucks.UI.View.Tiendas
{
    public partial class DetalleContacto : PhoneApplicationPage
    {
        public DetalleContacto()
        {
            InitializeComponent();
            DataContext = App.ViewModel.SelectedContact;
            if (App.ViewModel.SelectedContact.EmailAddresses.Count().Equals(0))
                btnEmail.Visibility = Visibility.Collapsed;
        }

        private void btnEmail_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            if (App.ViewModel.SelectedContact.EmailAddresses.Count() > 0)
                Helper.Common.EnviarEmail(StarbucksResource.AsuntoCorreo, string.Format(StarbucksResource.DescripcionCorreo, App.TiendasViewModel.SelectedItemTienda.nombre, App.TiendasViewModel.SelectedItemTienda.direccion), App.ViewModel.SelectedContact.EmailAddresses.First().EmailAddress);
        }

        private void btnSms_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            Helper.Common.EnviaSms(string.Format(StarbucksResource.DescripcionCorreo, App.TiendasViewModel.SelectedItemTienda.nombre, App.TiendasViewModel.SelectedItemTienda.direccion), App.ViewModel.SelectedContact.PhoneNumbers.First().PhoneNumber);
        }

        private void btnPhone_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            Helper.Common.Llamada(App.ViewModel.SelectedContact.DisplayName, App.ViewModel.SelectedContact.PhoneNumbers.First().PhoneNumber);
        }

        private void Image_ImageFailed(object sender, ExceptionRoutedEventArgs e)
        {

        }

        private void Image_BindingValidationError(object sender, ValidationErrorEventArgs e)
        {

        }
    }
}