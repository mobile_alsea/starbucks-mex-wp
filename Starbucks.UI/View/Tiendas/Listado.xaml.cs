﻿using System;
using System.Collections.Generic;
using System.IO.IsolatedStorage;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using Starbucks.UI.Helper;
using Starbucks.UI.Model;

namespace Starbucks.UI.View.Tiendas
{
    public partial class Listado : PhoneApplicationPage
    {
        public Listado()
        {
            InitializeComponent();
            DataContext = App.TiendasViewModel;

            var list = App.TiendasViewModel.ObtenerListTiendas();
            var resList = new List<Tienda>();
            if (App.ViewModel.SelectedFiltros.Count > 0)
            {
                list.ForEach(c =>
                {
                    c.servicios.ForEach(d =>
                    {
                        if (App.ViewModel.SelectedFiltros.Contains(d))
                            resList.Add(c);
                    });
                });
            }
            else
                resList = list;

            resList = resList.Distinct().ToList();

            if (App.ViewModel.GpsDisponible)
            {
                var tiendas = new List<Model.Tienda>();
                var radio = App.Settings.Contains(Constantes.LocalizaTiendas) ? App.Settings[Constantes.LocalizaTiendas].ToString() : string.Empty;

                resList.ForEach(c =>
                {
                    var posicionOrigen = new Posicion(App.TiendasViewModel.MyLocation.Latitude, App.TiendasViewModel.MyLocation.Longitude);
                    var posicionDestion = new Posicion(c.latitud, c.longitud);
                    var distancia = posicionOrigen.DistanciaKm(posicionDestion);
                    if (radio.Equals(string.Empty))
                        tiendas.Add(c);
                    else
                    {
                        if (Math.Round(distancia) <= Convert.ToInt32(radio))
                            tiendas.Add(c);
                    }
                });
                listTiendas.ItemsSource = tiendas;
            }
            else
            {
                listTiendas.ItemsSource = resList;
            }
        }

        private void listTiendas_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (listTiendas.SelectedIndex > -1 && listTiendas.SelectedItem != null)
            {
                var tiendaContex = listTiendas.SelectedItem as Tienda;
                App.TiendasViewModel.SelectedItemTienda = tiendaContex;
                App.ViewModel.SelectedListHorarios = tiendaContex.horario;
                NavigationService.Navigate(new Uri("/View/Tiendas/Detalle.xaml", UriKind.Relative));
                NavigationService.Navigated += NavigationService_Navigated;
            }
        }

        void NavigationService_Navigated(object sender, NavigationEventArgs e)
        {
            NavigationService.Navigated -= NavigationService_Navigated;
            if (listTiendas != null)
                listTiendas.SelectedIndex = -1;
        }
    }
}