﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;

namespace Starbucks.UI.View.Tiendas
{
    public partial class Ruta : PhoneApplicationPage
    {
        public Ruta()
        {
            InitializeComponent();
            DataContext = App.TiendasViewModel;
            Loaded += Ruta_Loaded;
        }

        void Ruta_Loaded(object sender, RoutedEventArgs e)
        {
            mapaRuta.Center = new System.Device.Location.GeoCoordinate(Convert.ToDouble(App.TiendasViewModel.SelectedItemTienda.latitud), Convert.ToDouble(App.TiendasViewModel.SelectedItemTienda.longitud));
            mapaRuta.ZoomLevel = 14;
        }
    }
}