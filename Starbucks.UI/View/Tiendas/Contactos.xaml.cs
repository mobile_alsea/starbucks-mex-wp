﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using Microsoft.Phone.UserData;

namespace Starbucks.UI.View.Tiendas
{
    public partial class Contactos : PhoneApplicationPage
    {
        public Contactos()
        {
            InitializeComponent();
            DataContext = App.ViewModel;
            Loaded += Contactos_Loaded;
        }

        void Contactos_Loaded(object sender, RoutedEventArgs e)
        {
            if (App.ViewModel.ContactsList == null)
            {
                App.ViewModel.LoadContacts();
                App.ViewModel.ContactDataDownloadCompleted += ViewModel_ContactDataDownloadCompleted;
            }
            contactsList.SelectedItem = null;
        }

        void ViewModel_ContactDataDownloadCompleted(object sender, EventArgs e)
        {
            App.ViewModel.ContactDataDownloadCompleted -= ViewModel_ContactDataDownloadCompleted;
        }

        private void contactsList_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (contactsList.SelectedItem != null)
            {
                var model = contactsList.SelectedItem as Contact;
                if (model != null)
                {
                    //NavigationService.Navigate(new Uri(string.Format("{0}?phone={1}", "", model.PhoneNumbers.First().PhoneNumber), UriKind.Relative));
                    App.ViewModel.SelectedContact = model;
                    NavigationService.Navigate(new Uri("/View/Tiendas/DetalleContacto.xaml", UriKind.Relative));
                }
            }
        }
    }
}