﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Navigation;
using Facebook;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using Microsoft.Phone.Tasks;
using Starbucks.UI.Helper;

namespace Starbucks.UI.View.Favoritos
{
    public partial class DetalleBebidas : PhoneApplicationPage
    {
        public DetalleBebidas()
        {
            InitializeComponent();
            DataContext = App.BebidasViewModel.SelectedDetalleConstructor;
            if (App.Settings.Contains("Facebook"))
            {
                var isFacebook = Convert.ToBoolean(App.Settings["Facebook"].ToString());
                if (isFacebook)
                {
                    var apbCompartir = new ApplicationBarIconButton(new Uri("/Assets/AppBar/compartir.png", UriKind.Relative)) { Text = "compartir" };
                    apbCompartir.Click += apbCompartir_Click;
                    ApplicationBar.Buttons.Insert(1, apbCompartir);
                }
                else
                {
                    if (App.Settings.Contains("Twitter"))
                    {
                        var isTwitter = Convert.ToBoolean(App.Settings["Twitter"].ToString());
                        if (isTwitter)
                        {
                            var apbCompartir = new ApplicationBarIconButton(new Uri("/Assets/AppBar/compartir.png", UriKind.Relative)) { Text = "compartir" };
                            apbCompartir.Click += apbCompartir_Click;
                            ApplicationBar.Buttons.Insert(1, apbCompartir);
                        }
                    }
                }
            }
        }

        void apbCompartir_Click(object sender, EventArgs e)
        {
            NavigationService.Navigate(new Uri("/View/Favoritos/Publicar.xaml", UriKind.Relative));
        }

        private void ApbmEditar_Click(object sender, EventArgs e)
        {
            NavigationService.Navigate(new Uri("/View/Favoritos/Editar.xaml", UriKind.Relative));
        }

        private void ApbDetalle_Click(object sender, EventArgs e)
        {
            NavigationService.Navigate(new Uri("/View/Bebidas/DetalleConstructor.xaml", UriKind.Relative));
        }

        private void ApbCorreo_Click(object sender, EventArgs e)
        {
            var emailComposeTask = new EmailComposeTask();
            emailComposeTask.Subject = "Añade esta bebida a tus favoritos.";
            emailComposeTask.Body = string.Format("Acabo de guardar mi bebida {0} de Starbucks Coffee mediante su aplicación móvil.", App.BebidasViewModel.SelectedDetalleConstructor.Nombre);
            emailComposeTask.Show();
        }
    }
}