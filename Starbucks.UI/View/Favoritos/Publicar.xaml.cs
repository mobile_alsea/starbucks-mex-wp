﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using Facebook;
using System.Windows.Media;
using Starbucks.UI.Helper;
using Starbucks.UI.Model;
using Coding4Fun.Phone.Controls;

namespace Starbucks.UI.View.Favoritos
{
    public partial class Publicar : PhoneApplicationPage
    {
        private readonly FacebookClient _fb = new FacebookClient();
        private WebBrowser webControl;
        private ProgressBar progress;

        public Publicar()
        {
            InitializeComponent();
            DataContext = App.BebidasViewModel.SelectedDetalleConstructor;
            if (App.Settings.Contains("Facebook"))
                chkFacebook.IsEnabled = true;

            if (App.Settings.Contains("Twitter"))
                chkTwitter.IsEnabled = true;
        }

        private void PosteaMensaje()
        {
            var fb = new FacebookClient(App.Settings["AccessToken"].ToString());

            fb.PostCompleted += (o, args) =>
            {
                if (args.Error != null)
                {
                    Dispatcher.BeginInvoke(() => MessageBox.Show(args.Error.Message));
                    return;
                }

                Dispatcher.BeginInvoke(() =>
                {
                    var prompt = new ToastPrompt();
                    prompt.Message = "Mensaje de facebook publicado con éxito.";
                    prompt.Show();
                    this.txtMensaje.Text = string.Empty;
                });
            };

            var parameters = new Dictionary<string, object>();
            string msj = string.Empty;
            Dispatcher.BeginInvoke(() =>
            {
                msj = string.Format("{0}, Acabo de guardar mi bebida {1} de Starbucks Coffee mediante su aplicación móvil.", txtMensaje.Text, App.BebidasViewModel.SelectedDetalleConstructor.Nombre);
                parameters["message"] = msj;
                fb.PostAsync("me/feed", parameters);
            });
        }

        private void webControl_Navigated(object sender, NavigationEventArgs e)
        {
            FacebookOAuthResult oauthResult;
            if (!_fb.TryParseOAuthCallbackUrl(e.Uri, out oauthResult))
            {
                return;
            }

            if (oauthResult.IsSuccess)
            {
                var accessToken = oauthResult.AccessToken;
                LoginSucceded(accessToken);
            }
            else
            {
                MessageBox.Show(oauthResult.ErrorDescription);
            }
        }

        private void webControl_Loaded(object sender, RoutedEventArgs e)
        {
            if (!App.Settings.Contains("AccessToken"))
            {
                ApplicationBar.IsVisible = false;
                contentWeb.Visibility = Visibility.Visible;
                var loginUrl = GetFacebookLoginUrl(StarbucksResource.AppId, StarbucksResource.ExtendedPermissions);
                webControl.Navigate(loginUrl);
            }
            else
                contentWeb.Visibility = Visibility.Collapsed;
        }

        private Uri GetFacebookLoginUrl(string appId, string extendedPermissions)
        {
            var parameters = new Dictionary<string, object>();
            parameters["client_id"] = appId;
            parameters["redirect_uri"] = StarbucksResource.redirect_uri;
            parameters["response_type"] = "token";
            parameters["display"] = "touch";

            if (!string.IsNullOrEmpty(extendedPermissions))
                parameters["scope"] = extendedPermissions;

            return _fb.GetLoginUrl(parameters);
        }

        private void LoginSucceded(string accessToken)
        {
            var fb = new FacebookClient(accessToken);

            fb.GetCompleted += (o, e) =>
            {
                if (e.Error != null)
                {
                    Dispatcher.BeginInvoke(() => MessageBox.Show(e.Error.Message));
                    return;
                }

                App.Settings["AccessToken"] = accessToken;
                App.Settings.Save();
                Dispatcher.BeginInvoke(() =>
                {
                    contentWeb.Visibility = Visibility.Collapsed;
                    ApplicationBar.IsVisible = true;
                });
                PosteaMensaje();
            };

            fb.GetAsync("me?fields=id");
        }

        private void webControl_LoadCompleted(object sender, NavigationEventArgs e)
        {
            progress.Visibility = Visibility.Collapsed;
        }

        protected override void OnBackKeyPress(System.ComponentModel.CancelEventArgs e)
        {
            base.OnBackKeyPress(e);
            if (contentWeb.Visibility == Visibility.Visible)
            {
                contentWeb.Visibility = Visibility.Collapsed;
                ApplicationBar.IsVisible = true;
                e.Cancel = true;
            }
            else
                e.Cancel = false;
        }

        void Browser_Navigated(object sender, NavigationEventArgs e)
        {
            if (e.Uri == new Uri("https://mobile.twitter.com/") || e.Uri == new Uri("https://mobile.twitter.com/signup") || e.Uri == new Uri("https://twitter.com/") || e.Uri == new Uri("https://mobile.twitter.com/i/guest"))
            {
                Browser.Visibility = System.Windows.Visibility.Collapsed;
                //MessageBox.Show("Publicado con exito");
                var prompt = new ToastPrompt();
                prompt.Message = "Mensaje de twitter publicado con éxito.";
                prompt.Show();
                this.txtMensaje.Text = string.Empty;
            }
        }

        private void ApbEnviar_Click(object sender, EventArgs e)
        {
            if (chkFacebook.IsEnabled == true && chkFacebook.IsChecked == true)
                PostFacebook();

            if (chkTwitter.IsEnabled == true && chkTwitter.IsChecked == true)
                PostTwitter();
        }

        private void PostFacebook()
        {
            if (string.IsNullOrEmpty(txtMensaje.Text))
            {
                MessageBox.Show("Ingresar el texto a publicar.");
                return;
            }
            if (App.Settings.Contains("AccessToken"))
                PosteaMensaje();
            else
            {
                if (stackWeb.Children.Count > 0)
                    stackWeb.Children.RemoveAt(0);
                webControl = new WebBrowser();
                webControl.Width = 400;
                webControl.Height = 600;
                webControl.IsScriptEnabled = true;
                webControl.Navigated += webControl_Navigated;
                webControl.Loaded += webControl_Loaded;
                webControl.LoadCompleted += webControl_LoadCompleted;
                webControl.NavigationFailed += webControl_NavigationFailed;
                stackWeb.Children.Add(webControl);

                if (contentWeb.Children.Count >= 2)
                    contentWeb.Children.RemoveAt(1);
                progress = new ProgressBar();
                progress.Height = 12;
                progress.VerticalAlignment = VerticalAlignment.Center;
                progress.Background = new SolidColorBrush(Colors.Green);
                progress.Foreground = new SolidColorBrush(Colors.Green);
                progress.IsIndeterminate = true;
                progress.Margin = new Thickness(0, 397, 0, 390);
                contentWeb.Children.Insert(1, progress);
            }
        }

        void webControl_NavigationFailed(object sender, NavigationFailedEventArgs e)
        {
            progress.IsIndeterminate = false;
            progress.Visibility = Visibility.Collapsed;
        }

        private void PostTwitter()
        {
            string tweet = string.Format("{0}. Acabo de guardar mi bebida {1} de Starbucks Coffee mediante su aplicación móvil", txtMensaje.Text, App.BebidasViewModel.SelectedDetalleConstructor.Nombre);
            string txt = string.Format("https://twitter.com/intent/tweet?source=webclient&text={0}", tweet);

            if (tweet.Length > 140)
            {
                MessageBox.Show("Numero de caracteres excedido");
                return;
            }
            
            Browser.Visibility = System.Windows.Visibility.Visible;
            Browser.Navigated += new EventHandler<NavigationEventArgs>(Browser_Navigated);
            Browser.Navigate(new Uri(txt));
        }

        private void txtMensaje_KeyUp(object sender, System.Windows.Input.KeyEventArgs e)
        {
            if (e.Key == System.Windows.Input.Key.Enter)
                this.Focus();
        }
    }
}