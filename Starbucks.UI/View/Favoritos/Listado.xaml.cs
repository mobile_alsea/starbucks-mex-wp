﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Runtime.Serialization;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Xml;
using Microsoft.Phone.Controls;
using Starbucks.UI.Helper;

namespace Starbucks.UI.View.Favoritos
{
    public partial class Listado : PhoneApplicationPage
    {
        private ListBox control;
        private ListBox controlComida;
        private ListBox controlBebida;
        private ListBox controlBebidaAmigos;
        private bool IsFromTile;

        public Listado()
        {
            InitializeComponent();
            DataContext = App.FavoritosViewModel;
            App.FavoritosViewModel.CargaFavoritos();
        }

        private void listCafes_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            control = sender as ListBox;
            if (control.SelectedIndex != -1 && control != null)
            {
                App.CafesViewModel.SelectedItemCafe = control.SelectedItem as Model.Cafe;
                NavigationService.Navigate(new Uri(StarbucksResource.View_DetalleCafes, UriKind.Relative));
                NavigationService.Navigated += NavigationService_Navigated;
            }
        }

        void NavigationService_Navigated(object sender, System.Windows.Navigation.NavigationEventArgs e)
        {
            NavigationService.Navigated -= NavigationService_Navigated;
            if (control != null)
                control.SelectedIndex = -1;
            if (controlComida != null)
                controlComida.SelectedIndex = -1;
            if (controlBebida != null)
                controlBebida.SelectedIndex = -1;
            if (listTiendas != null)
                listTiendas.SelectedIndex = -1;
            if (controlBebidaAmigos != null)
                listBebidaAmigos.SelectedIndex = -1;
        }

        private void listComida_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            controlComida = sender as ListBox;
            if (controlComida.SelectedIndex != -1 && controlComida != null)
            {
                App.AlimentosViewModel.SelectedItemComida = controlComida.SelectedItem as Model.Comida;
                NavigationService.Navigate(new Uri(StarbucksResource.View_DetalleAlimentos, UriKind.Relative));
                NavigationService.Navigated += NavigationService_Navigated;
            }
        }

        private void listBebida_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            controlBebida = sender as ListBox;
            if (controlBebida.SelectedIndex != -1 && controlBebida != null)
            {
                App.BebidasViewModel.SelectedDetalleConstructor = listBebida.SelectedItem as Model.DetalleConstructor;
                NavigationService.Navigate(new Uri("/View/Favoritos/DetalleBebidas.xaml", UriKind.Relative));
                NavigationService.Navigated += NavigationService_Navigated;
            }
        }

        private void listTiendas_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (listTiendas.SelectedIndex > -1 && listTiendas.SelectedItem != null)
            {
                var tiendaContex = listTiendas.SelectedItem as Model.Tienda;
                App.TiendasViewModel.SelectedItemTienda = tiendaContex;
                App.ViewModel.SelectedListHorarios = tiendaContex.horario;
                NavigationService.Navigate(new Uri("/View/Tiendas/Detalle.xaml", UriKind.Relative));
                NavigationService.Navigated += NavigationService_Navigated;
            }
        }

        protected override void OnNavigatedTo(System.Windows.Navigation.NavigationEventArgs e)
        {
            if (NavigationContext.QueryString.Keys.Contains("TitleMisBebidas") || NavigationContext.QueryString.Keys.Contains("TitleMisTiendas") ||
                NavigationContext.QueryString.Keys.Contains("TitleMiCafe") || NavigationContext.QueryString.Keys.Contains("TitleMisAlimentos"))
                IsFromTile = true;
            else
                IsFromTile = false;

            switch (App.SeccionActivaFavoritos)
            {
                case "bebidas":
                    pvtFavoritos.SelectedIndex = 0;
                    break;
                case "bebidasAmigos":
                    pvtFavoritos.SelectedIndex = 1;
                    break;
                case "cafes":
                    pvtFavoritos.SelectedIndex = 2;
                    break;
                case "alimentos":
                    pvtFavoritos.SelectedIndex = 3;
                    break;
                case "tiendas":
                    pvtFavoritos.SelectedIndex = 4;
                    break;
                default:
                    break;
            }

            if (NavigationContext.QueryString.Keys.Contains("TitleMisBebidas"))
                pvtFavoritos.SelectedIndex = 0;
            if (NavigationContext.QueryString.Keys.Contains("TitleMisTiendas"))
                pvtFavoritos.SelectedIndex = 4;
            if (NavigationContext.QueryString.Keys.Contains("TitleMiCafe"))
                pvtFavoritos.SelectedIndex = 2;
            if (NavigationContext.QueryString.Keys.Contains("TitleMisAlimentos"))
                pvtFavoritos.SelectedIndex = 3;
        }

        private void MenuItem_Click(object sender, RoutedEventArgs e)
        {
            var menu = sender as MenuItem;
            var entidad = menu.CommandParameter as Model.DetalleConstructor;
            App.FavoritosViewModel.EliminaFavorito<Model.DetalleConstructor>(entidad, Accion.Constructor);
        }

        private void MenuItemAmigos_Click(object sender, RoutedEventArgs e)
        {
            var menu = sender as MenuItem;
            var entidad = menu.CommandParameter as Model.DetalleConstructor;
            App.FavoritosViewModel.EliminaFavorito<Model.DetalleConstructor>(entidad, Accion.Constructor);
        }

        protected override void OnBackKeyPress(System.ComponentModel.CancelEventArgs e)
        {
            base.OnBackKeyPress(e);
            if (IsFromTile)
            {
                if (App.Settings.Contains("grupoTombstoned"))
                {
                    var grupoTombstoned = App.Settings["grupoTombstoned"] as IList;
                    var listCafes = new List<Model.Cafe>();
                    var listBebidas = new List<Model.Bebida>();
                    var listComidas = new List<Model.Comida>();
                    var listTiendas = new List<Model.Tienda>();

                    (grupoTombstoned[0] as string[]).ToList().ForEach(c => listCafes.Add(Helper.Common.XmlDataDeserialize<Model.Cafe>(c.ToString())));
                    (grupoTombstoned[1] as string[]).ToList().ForEach(c => listBebidas.Add(Helper.Common.XmlDataDeserialize<Model.Bebida>(c.ToString())));
                    (grupoTombstoned[2] as string[]).ToList().ForEach(c => listComidas.Add(Helper.Common.XmlDataDeserialize<Model.Comida>(c.ToString())));
                    (grupoTombstoned[3] as string[]).ToList().ForEach(c => listTiendas.Add(Helper.Common.XmlDataDeserialize<Model.Tienda>(c.ToString())));

                    Data.Parser.Cafes.ListCafes = listCafes;
                    Data.Parser.Bebidas.ListBebidas = listBebidas;
                    Data.Parser.Comidas.ListComidas = listComidas;
                    Data.Parser.Tiendas.ListTiendas = listTiendas;
                }
                //NavigationService.Navigate(new Uri(StarbucksResource.View_Home, UriKind.Relative));
                //e.Cancel = false;
            }
        }

        private void listBebidaAmigos_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            controlBebidaAmigos = sender as ListBox;
            if (controlBebidaAmigos.SelectedIndex != -1 && controlBebidaAmigos != null)
            {
                App.BebidasViewModel.SelectedDetalleConstructor = listBebidaAmigos.SelectedItem as Model.DetalleConstructor;
                App.SeccionActivaFavoritos = "bebidasAmigos";
                NavigationService.Navigate(new Uri("/View/Favoritos/DetalleBebidas.xaml", UriKind.Relative));
                NavigationService.Navigated += NavigationService_Navigated;
            }
        }

        private void pvtFavoritos_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            switch (pvtFavoritos.SelectedIndex)
            {
                case 0:
                    App.SeccionActivaFavoritos = "bebidas";
                    break;
                case 1:
                    App.SeccionActivaFavoritos = "bebidasAmigos";
                    break;
                case 2:
                    App.SeccionActivaFavoritos = "cafes";
                    break;
                case 3:
                    App.SeccionActivaFavoritos = "alimentos";
                    break;
                case 4:
                    App.SeccionActivaFavoritos = "tiendas";
                    break;
                default:
                    break;
            }
        }
    }
}