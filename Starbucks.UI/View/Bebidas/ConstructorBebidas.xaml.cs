﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Microsoft.Phone.Controls;
using Starbucks.UI.Helper.Looping;

namespace Starbucks.UI.View.Bebidas
{
    public partial class ConstructorBebidas : PhoneApplicationPage
    {
        public ConstructorBebidas()
        {
            InitializeComponent();
            DataContext = App.BebidasViewModel.SelectedItemBebida;
            App.BebidasViewModel.SelectedDetalleConstructor = new Model.DetalleConstructor();
            var loopTamanio = new ListLoopingDataSource<string> { Items = App.ViewModel.ListTamanioBebida(), SelectedItem = "Grande" };
            loopTamanio.SelectionChanged += loopTamanio_SelectionChanged;
            this.sTamanio.DataSource = loopTamanio;

            var loopShot = new ListLoopingDataSource<string> { Items = App.ViewModel.ListShots(), SelectedItem = "0" };
            loopShot.SelectionChanged += loopShot_SelectionChanged;
            this.sShots.DataSource = loopShot;

            var loopLeche = new ListLoopingDataSource<string> { Items = App.ViewModel.ListLeche(), SelectedItem = "NO" };
            loopLeche.SelectionChanged += loopLeche_SelectionChanged;
            this.sLeche.DataSource = loopLeche;

            var loopJarabe = new ListLoopingDataSource<string> { Items = App.ViewModel.ListJarabe(), SelectedItem = "NO" };
            loopJarabe.SelectionChanged += loopJarabe_SelectionChanged;
            this.sJarabe.DataSource = loopJarabe;

            var loopJarabeShot = new ListLoopingDataSource<string> { Items = App.ViewModel.ListShots(), SelectedItem = "0" };
            loopJarabeShot.SelectionChanged += loopJarabeShot_SelectionChanged;
            this.sJarabeShots.DataSource = loopJarabeShot;
            brdJarabe.Visibility = System.Windows.Visibility.Visible;

            var loopToping = new ListLoopingDataSource<string> { Items = App.ViewModel.ListToppings(), SelectedItem = "NO" };
            loopToping.SelectionChanged += loopToping_SelectionChanged;
            this.sToppings.DataSource = loopToping;

            var loopsToppingsShot = new ListLoopingDataSource<string> { Items = App.ViewModel.ListShots(), SelectedItem = "0" };
            loopsToppingsShot.SelectionChanged += loopsToppingsShot_SelectionChanged;
            this.sToppingsShots.DataSource = loopsToppingsShot;
            brdToppings.Visibility = System.Windows.Visibility.Visible;

            App.BebidasViewModel.SelectedDetalleConstructor.Tamaño = "Grande";
            App.BebidasViewModel.SelectedDetalleConstructor.Tipo = App.BebidasViewModel.SelectedItemBebida.subcategoria;

            App.BebidasViewModel.SelectedDetalleConstructor.Nombre = App.BebidasViewModel.SelectedItemBebida.nombre;
            App.BebidasViewModel.SelectedDetalleConstructor.Descripcion = App.BebidasViewModel.SelectedItemBebida.descripcion;
            App.BebidasViewModel.SelectedDetalleConstructor.Disponibilidad = App.BebidasViewModel.SelectedItemBebida.disponibilidad;
            App.BebidasViewModel.SelectedDetalleConstructor.Imagen = App.BebidasViewModel.SelectedItemBebida.imagen;

            ctrVaso1.DataContext = App.BebidasViewModel.SelectedItemBebida.Arte.Grande;
            ctrVaso2.DataContext = App.BebidasViewModel.SelectedItemBebida.Arte.Grande;
            ctrVaso3.DataContext = App.BebidasViewModel.SelectedItemBebida.Arte.Grande;
            ctrVaso4.DataContext = App.BebidasViewModel.SelectedItemBebida.Arte.Grande;
            ctrVaso5.DataContext = App.BebidasViewModel.SelectedItemBebida.Arte.Grande;
        }

        void loopsToppingsShot_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var source = sender as ListLoopingDataSource<string>;
            var item = source.SelectedItem.ToString();
            App.BebidasViewModel.SelectedDetalleConstructor.PersonalizadoShot = item;
        }

        void loopToping_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var source = sender as ListLoopingDataSource<string>;
            var item = source.SelectedItem.ToString();
            if (!item.Equals("NO"))
                brdToppings.Visibility = System.Windows.Visibility.Collapsed;
            else
            {
                Dispatcher.BeginInvoke(() =>
                {
                    sToppingsShots.IsExpanded = false;
                });
                brdToppings.Visibility = System.Windows.Visibility.Visible;
            }
            App.BebidasViewModel.SelectedDetalleConstructor.Personalizado = item;
        }

        void loopJarabeShot_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var source = sender as ListLoopingDataSource<string>;
            var item = source.SelectedItem.ToString();
            App.BebidasViewModel.SelectedDetalleConstructor.JarabeShot = item;
        }

        void loopJarabe_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var source = sender as ListLoopingDataSource<string>;
            var item = source.SelectedItem.ToString();
            if (!item.Equals("NO"))
                brdJarabe.Visibility = System.Windows.Visibility.Collapsed;
            else
            {
                Dispatcher.BeginInvoke(() =>
                {
                    sJarabeShots.IsExpanded = false;
                });
                brdJarabe.Visibility = System.Windows.Visibility.Visible;
            }
            App.BebidasViewModel.SelectedDetalleConstructor.Jarabe = item;
        }

        void loopLeche_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var source = sender as ListLoopingDataSource<string>;
            var item = source.SelectedItem.ToString();
            App.BebidasViewModel.SelectedDetalleConstructor.Leche = item;
        }

        void loopShot_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var source = sender as ListLoopingDataSource<string>;
            var item = source.SelectedItem.ToString();
            App.BebidasViewModel.SelectedDetalleConstructor.Shots = item;
        }

        void loopTamanio_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var source = sender as ListLoopingDataSource<string>;
            var item = source.SelectedItem.ToString();
            App.BebidasViewModel.SelectedDetalleConstructor.Tamaño = item;
            switch (item)
            {
                case "Venti":
                    ctrVaso1.DataContext = App.BebidasViewModel.SelectedItemBebida.Arte.Venti;
                    ctrVaso2.DataContext = App.BebidasViewModel.SelectedItemBebida.Arte.Venti;
                    ctrVaso3.DataContext = App.BebidasViewModel.SelectedItemBebida.Arte.Venti;
                    ctrVaso4.DataContext = App.BebidasViewModel.SelectedItemBebida.Arte.Venti;
                    ctrVaso5.DataContext = App.BebidasViewModel.SelectedItemBebida.Arte.Venti;
                    break;
                case "Grande":
                    ctrVaso1.DataContext = App.BebidasViewModel.SelectedItemBebida.Arte.Grande;
                    ctrVaso2.DataContext = App.BebidasViewModel.SelectedItemBebida.Arte.Grande;
                    ctrVaso3.DataContext = App.BebidasViewModel.SelectedItemBebida.Arte.Grande;
                    ctrVaso4.DataContext = App.BebidasViewModel.SelectedItemBebida.Arte.Grande;
                    ctrVaso5.DataContext = App.BebidasViewModel.SelectedItemBebida.Arte.Grande;
                    break;
                case "Alto":
                    ctrVaso1.DataContext = App.BebidasViewModel.SelectedItemBebida.Arte.Alto;
                    ctrVaso2.DataContext = App.BebidasViewModel.SelectedItemBebida.Arte.Alto;
                    ctrVaso3.DataContext = App.BebidasViewModel.SelectedItemBebida.Arte.Alto;
                    ctrVaso4.DataContext = App.BebidasViewModel.SelectedItemBebida.Arte.Alto;
                    ctrVaso5.DataContext = App.BebidasViewModel.SelectedItemBebida.Arte.Alto;
                    break;
                default:
                    break;
            }
        }

        private void ApbInfo_Click(object sender, System.EventArgs e)
        {
            MessageBox.Show(@"ELIGE TU BEBIDA
1.- Decide si quieres una bebida fría o caliente.
2.- Selecciona una bebida de nuestro menú de favoritos y ofertas de la temporada.
3.- Amplia o selecciona un tamaño de tu taza.
HAZLA TUYA
1.- Toca el menú superior para personalizar tu bebida – Puedes elegir toppings, jarabes, leche… y mucho más.
2.- Selecciona Detalle de la bebida para ver el marcado de sus ingredientes.
GUARDA Y COMPARTE
1.- Ponle un nombre y añade tus notas para hacerla perfecta
2.- Compártela con tus amigos por email, Facebook o Twitter.
", "INFORMACIÓN", MessageBoxButton.OK);
        }

        private void ApbGuardar_Click(object sender, System.EventArgs e)
        {
            NavigationService.Navigate(new Uri("/View/Bebidas/Guadar.xaml", UriKind.Relative));
        }

        private void ApbDetalle_Click(object sender, System.EventArgs e)
        {
            //var contexto = DataContext as Model.Bebida;
            NavigationService.Navigate(new Uri("/View/Bebidas/DetalleConstructor.xaml", UriKind.Relative));
        }

        protected override void OnBackKeyPress(System.ComponentModel.CancelEventArgs e)
        {
            base.OnBackKeyPress(e);
            App.BebidasViewModel.SelectedDetalleConstructor = new Model.DetalleConstructor();
        }
    }
}