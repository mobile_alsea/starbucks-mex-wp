﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using Microsoft.Phone.UserData;

namespace Starbucks.UI.View.Bebidas
{
    public partial class Guadar : PhoneApplicationPage
    {
        private bool isLoadContactos;

        public Guadar()
        {
            InitializeComponent();
            DataContext = App.ViewModel;
            Loaded += Guadar_Loaded;
        }

        void Guadar_Loaded(object sender, RoutedEventArgs e)
        {
            
                
                lblNombre.Text = App.BebidasViewModel.SelectedItemBebida.nombre;
            if (App.ViewModel.ContactsList == null)
            {
                App.ViewModel.LoadContacts();
                App.ViewModel.ContactDataDownloadCompleted += ViewModel_ContactDataDownloadCompleted;
            }
            contactsList.SelectedItem = null;
        }

        void ViewModel_ContactDataDownloadCompleted(object sender, EventArgs e)
        {
            App.ViewModel.ContactDataDownloadCompleted -= ViewModel_ContactDataDownloadCompleted;
        }

        private void txtPropietario_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            if (isLoadContactos)
            {
                Contacts.Visibility = Visibility.Collapsed;
                ApplicationBar.IsVisible = true;
                isLoadContactos = false;
            }
            else
            {
                Contacts.Visibility = Visibility.Visible;
                ApplicationBar.IsVisible = false;
                isLoadContactos = true;
            }
        }

        protected override void OnBackKeyPress(System.ComponentModel.CancelEventArgs e)
        {
            if (isLoadContactos)
            {
                Contacts.Visibility = Visibility.Collapsed;
                ApplicationBar.IsVisible = true;
                isLoadContactos = false;
                e.Cancel = true;
            }
            else
                e.Cancel = false;
        }

        private void contactsList_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (contactsList.SelectedItem != null)
            {
                var model = contactsList.SelectedItem as Contact;
                if (model != null)
                {
                    txtPropietario.Text = model.DisplayName;
                    Contacts.Visibility = Visibility.Collapsed;
                    ApplicationBar.IsVisible = true;
                    isLoadContactos = false;
                }
            }
        }

        private void ApbGuardar_Click(object sender, EventArgs e)
        {
            if (App.BebidasViewModel.SelectedDetalleConstructor != null)
            {
                App.BebidasViewModel.SelectedDetalleConstructor.Propietario = txtPropietario.Text;
                App.BebidasViewModel.SelectedDetalleConstructor.Nickname = txtNickname.Text;
                App.BebidasViewModel.SelectedDetalleConstructor.Notas = txtNota.Text;
                App.FavoritosViewModel.AgregaFavorito<Model.DetalleConstructor>(App.BebidasViewModel.SelectedDetalleConstructor, Helper.Accion.Constructor);
            }
            
            NavigationService.RemoveBackEntry();
            NavigationService.RemoveBackEntry();
            NavigationService.GoBack();
        }

        private void txtNickname_KeyUp(object sender, System.Windows.Input.KeyEventArgs e)
        {
            if (e.Key == System.Windows.Input.Key.Enter)
                txtNota.Focus();
        }

        private void txtNota_KeyUp(object sender, System.Windows.Input.KeyEventArgs e)
        {
            if (e.Key == System.Windows.Input.Key.Enter)
                this.Focus();
        }
    }
}