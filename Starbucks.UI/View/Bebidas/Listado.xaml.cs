﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using Starbucks.UI.Helper;

namespace Starbucks.UI.View.Bebidas
{
    public partial class Listado : PhoneApplicationPage
    {
        public Listado()
        {
            InitializeComponent();
            DataContext = App.BebidasViewModel;
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);
            var filtro = NavigationContext.QueryString[Constantes.Filtro];
            lblTitulo.Text += NavigationContext.QueryString[Constantes.Bebida];
            lblNombre.Text = filtro;
            listBebida.ItemsSource = App.BebidasViewModel.ObtenerBebidas(filtro);
        }

        private void listBebida_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (listBebida.SelectedItem != null)
            {
                App.BebidasViewModel.SelectedItemBebida = listBebida.SelectedItem as Model.Bebida;
                NavigationService.Navigate(new Uri(StarbucksResource.View_DetalleBebidas, UriKind.Relative));
            }
        }
    }
}