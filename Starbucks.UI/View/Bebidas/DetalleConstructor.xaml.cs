﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;

namespace Starbucks.UI.View.Bebidas
{
    public partial class DetalleConstructor : PhoneApplicationPage
    {
        public DetalleConstructor()
        {
            InitializeComponent();
            DataContext = App.BebidasViewModel.SelectedDetalleConstructor;
            ctrVaso.DataContext = App.BebidasViewModel.SelectedDetalleConstructor.Arte.Venti;
        }
    }
}