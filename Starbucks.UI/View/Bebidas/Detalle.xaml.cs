﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using Starbucks.UI.Helper;

namespace Starbucks.UI.View.Bebidas
{
    public partial class Detalle : PhoneApplicationPage
    {
        private bool IsVisible;
        private bool EstatusFavorito;

        public Detalle()
        {
            InitializeComponent();
            DataContext = App.BebidasViewModel.SelectedItemBebida;
            Loaded += Detalle_Loaded;
        }

        void Detalle_Loaded(object sender, RoutedEventArgs e)
        {
            if (ApplicationBar == null)
                ApplicationBar = new ApplicationBar();
            ApplicationBar.Buttons.Clear();
            VerificaFavorito();
            if (!EstatusFavorito)
            {
                var appBarFavorito = new ApplicationBarIconButton(new Uri(StarbucksResource.Icon_AgregarFavoritos, UriKind.Relative)) { Text = StarbucksResource.Label_AddMisBebidas };
                appBarFavorito.Click += appBarFavorito_Click;
                ApplicationBar.Buttons.Add(appBarFavorito);

                var appBarConstructor = new ApplicationBarIconButton(new Uri(StarbucksResource.Icon_EditarBebidas, UriKind.Relative)) { Text = StarbucksResource.Label_ConstruirBebida };
                appBarConstructor.Click += appBarConstructor_Click;
                ApplicationBar.Buttons.Add(appBarConstructor);
            }
            else
            {
                var appBarEliminaFavorito = new ApplicationBarIconButton(new Uri(StarbucksResource.Icon_QuitarFavoritos, UriKind.Relative)) { Text = StarbucksResource.Label_EliminarMisBebidas };
                appBarEliminaFavorito.Click += appBarEliminaFavorito_Click;
                ApplicationBar.Buttons.Add(appBarEliminaFavorito);

                var appBarConstructor = new ApplicationBarIconButton(new Uri(StarbucksResource.Icon_EditarBebidas, UriKind.Relative)) { Text = StarbucksResource.Label_ConstruirBebida };
                appBarConstructor.Click += appBarConstructor_Click;
                ApplicationBar.Buttons.Add(appBarConstructor);
            }
        }

        void appBarConstructor_Click(object sender, EventArgs e)
        {
            NavigationService.Navigate(new Uri(StarbucksResource.View_ConstructorBebidas, UriKind.Relative));
        }

        void appBarEliminaFavorito_Click(object sender, EventArgs e)
        {
            var entidad = new Model.DetalleConstructor();
            entidad.Nombre = App.BebidasViewModel.SelectedItemBebida.nombre;
            entidad.Descripcion = App.BebidasViewModel.SelectedItemBebida.descripcion;
            entidad.Imagen = App.BebidasViewModel.SelectedItemBebida.imagen;
            entidad.Disponibilidad = App.BebidasViewModel.SelectedItemBebida.disponibilidad;
            entidad.Tamaño = StarbucksResource.Label_TamanioGrande;
            entidad.Tipo = App.BebidasViewModel.SelectedItemBebida.subcategoria;
            entidad.Propietario = StarbucksResource.Propietario_Yo;
            entidad.IsFavorito = true;

            App.FavoritosViewModel.EliminaFavorito<Model.DetalleConstructor>(entidad, Accion.Constructor);
            ApplicationBar.Buttons.Clear();
            var appBarFavorito = new ApplicationBarIconButton(new Uri(StarbucksResource.Icon_AgregarFavoritos, UriKind.Relative)) { Text = StarbucksResource.Label_AddMisBebidas };
            appBarFavorito.Click += appBarFavorito_Click;
            ApplicationBar.Buttons.Add(appBarFavorito);

            var appBarConstructor = new ApplicationBarIconButton(new Uri(StarbucksResource.Icon_EditarBebidas, UriKind.Relative)) { Text = StarbucksResource.Label_ConstruirBebida };
            appBarConstructor.Click += appBarConstructor_Click;
            ApplicationBar.Buttons.Add(appBarConstructor);
        }

        void appBarFavorito_Click(object sender, EventArgs e)
        {
            var entidad = new Model.DetalleConstructor();
            entidad.Nombre = App.BebidasViewModel.SelectedItemBebida.nombre;
            entidad.Descripcion = App.BebidasViewModel.SelectedItemBebida.descripcion;
            entidad.Imagen = App.BebidasViewModel.SelectedItemBebida.imagen;
            entidad.Disponibilidad = App.BebidasViewModel.SelectedItemBebida.disponibilidad;
            entidad.Tamaño = StarbucksResource.Label_TamanioGrande;
            entidad.Tipo = App.BebidasViewModel.SelectedItemBebida.subcategoria;
            entidad.Propietario = StarbucksResource.Propietario_Yo;
            entidad.IsFavorito = true;

            App.FavoritosViewModel.AgregaFavorito<Model.DetalleConstructor>(entidad, Accion.Constructor);
            ApplicationBar.Buttons.Clear();
            var appBarEliminaFavorito = new ApplicationBarIconButton(new Uri(StarbucksResource.Icon_QuitarFavoritos, UriKind.Relative)) { Text = StarbucksResource.Label_EliminarMisBebidas };
            appBarEliminaFavorito.Click += appBarEliminaFavorito_Click;
            ApplicationBar.Buttons.Add(appBarEliminaFavorito);

            var appBarConstructor = new ApplicationBarIconButton(new Uri(StarbucksResource.Icon_EditarBebidas, UriKind.Relative)) { Text = StarbucksResource.Label_ConstruirBebida };
            appBarConstructor.Click += appBarConstructor_Click;
            ApplicationBar.Buttons.Add(appBarConstructor);
        }

        private void VerificaFavorito()
        {
            var bebida = App.BebidasViewModel.SelectedItemBebida;
            var item = App.FavoritosViewModel.FavoritoConstructor.Where(c => c.Nombre.Equals(bebida.nombre) && c.IsFavorito.Equals(true)).SingleOrDefault();
            if (item != null)
                EstatusFavorito = true;
            else
                EstatusFavorito = false;
        }

        private void imgthumbnail_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            if (!IsVisible)
            {
                IsVisible = true;
                ApplicationBar.IsVisible = false;
                ContentImage.Visibility = Visibility.Visible;
            }
        }

        private void ContentImage_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            IsVisible = false;
            ApplicationBar.IsVisible = true;
            ContentImage.Visibility = Visibility.Collapsed;
        }
    }
}