﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using Starbucks.UI.Helper;

namespace Starbucks.UI.View.Bebidas
{
    public partial class Bebida : PhoneApplicationPage
    {
        private bool IsFromTile;
        public Bebida()
        {
            InitializeComponent();
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);
            if (NavigationContext.QueryString.Keys.Contains("TitleBebidasCalientes") || NavigationContext.QueryString.Keys.Contains("TitleBebidasFrias"))
                IsFromTile = true;
            else
                IsFromTile = false;

            if (this.NavigationContext.QueryString.ContainsKey(Constantes.Desc))
                txtTitulo.Text = NavigationContext.QueryString[Constantes.Desc];
            if (NavigationContext.QueryString[Constantes.Bebida].Equals("Caliente"))
            {
                ViewCaliente.Visibility = Visibility.Visible;
                ViewFrio.Visibility = Visibility.Collapsed;
            }
            else
            {
                ViewCaliente.Visibility = Visibility.Collapsed;
                ViewFrio.Visibility = Visibility.Visible;
            }
        }

        private void btnNavega_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            var url = string.Empty;
            DataTombstoned();
            var button = sender as Button;
            if (NavigationContext.QueryString[Constantes.Bebida].Equals("Caliente"))
                url = StarbucksResource.View_ListadoBebidaCalientes;
            else
                url = StarbucksResource.View_ListadoBebidaFrias;
            url += string.Format("&fitro={0}", button.Content);
            NavigationService.Navigate(new Uri(url, UriKind.Relative));
        }

        protected override void OnBackKeyPress(System.ComponentModel.CancelEventArgs e)
        {
            //DataTombstoned();
            //if (IsFromTile)
            //    NavigationService.Navigate(new Uri(StarbucksResource.View_Home, UriKind.Relative));
            //e.Cancel = false;
        }

        private void DataTombstoned()
        {
            if (IsFromTile)
            {
                if (App.Settings.Contains("grupoTombstoned"))
                {
                    var grupoTombstoned = App.Settings["grupoTombstoned"] as IList;
                    var listCafes = new List<Model.Cafe>();
                    var listBebidas = new List<Model.Bebida>();
                    var listComidas = new List<Model.Comida>();
                    var listTiendas = new List<Model.Tienda>();

                    (grupoTombstoned[0] as string[]).ToList().ForEach(c => listCafes.Add(Helper.Common.XmlDataDeserialize<Model.Cafe>(c.ToString())));
                    (grupoTombstoned[1] as string[]).ToList().ForEach(c => listBebidas.Add(Helper.Common.XmlDataDeserialize<Model.Bebida>(c.ToString())));
                    (grupoTombstoned[2] as string[]).ToList().ForEach(c => listComidas.Add(Helper.Common.XmlDataDeserialize<Model.Comida>(c.ToString())));
                    (grupoTombstoned[3] as string[]).ToList().ForEach(c => listTiendas.Add(Helper.Common.XmlDataDeserialize<Model.Tienda>(c.ToString())));  
                  
                    Data.Parser.Cafes.ListCafes = listCafes;
                    Data.Parser.Bebidas.ListBebidas = listBebidas;
                    Data.Parser.Comidas.ListComidas = listComidas;
                    Data.Parser.Tiendas.ListTiendas = listTiendas;
                }             
            }
        }
    } 
}