﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using Microsoft.Devices;
using Microsoft.Devices.Sensors;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using Microsoft.Xna.Framework;
using Nokia.Framework.Libraries.Foursquare.ResNaviDataModel;
using Nokia.Framework.Libraries.Foursquare.ResNaviWrapper;
using Starbucks.UI.Helper;
using Starbucks.UI.Model;
using Starbucks.UI.ViewModel;
using Color = System.Windows.Media.Color;
using GestureEventArgs = System.Windows.Input.GestureEventArgs;

namespace Starbucks.UI.View.RA
{
    public partial class ARView : PhoneApplicationPage
    {
        #region Inner Data
        private UIElement _curVisibleCanvas;
        private List<UIElement> _canvasList;
        private Motion _motion;
        private bool _isMotionStart;
        private bool _isPromptedAboutMotionNotSupported;
        private PhotoCamera _photoCamera;
        private bool _isCameraIntialized;
        private VideoBrush _backgroundBrush;
        private PhoneApplicationPage _parentPage;
        private bool _isLoaded;
        private bool _marginAdjusted;
        private const int TIMEBETWEENUPDATES = 50; // milliseconds 

        private string ARVIEW_COMMENTS_IS_LOADING = StarbucksResource.ARViewCommentsLoading;
        private string ARVIEW_NOCOMMENTS = StarbucksResource.ARViewNoComments;
        private string ARVIEW_NOT_SUPPORT_MOTION = StarbucksResource.ARViewNotSupportMotion;
        private string ARVIEW_START_MOTION_FAILED = StarbucksResource.ARViewStartMotionFailed;
        private string ARVIEW_STOP_MOTION_FAILED = StarbucksResource.ARViewStopMotionFailed;
        private string ARVIEW_CAMERA_INITIALIZE_FAILED = StarbucksResource.ARViewCameraInitializeFailed;
        private Color _borderDefaultColor;
        #endregion

        public ARView()
        {
            InitializeComponent();
            Initialize();
            OrientationChanged += ARView_OrientationChanged;
        }

        void ARView_OrientationChanged(object sender, OrientationChangedEventArgs e)
        {
            SetVideoOrientation(e.Orientation);
            switch (e.Orientation)
            {
                case PageOrientation.None:
                case PageOrientation.PortraitUp:
                case PageOrientation.PortraitDown:
                    radar.Orientation = Controls.Orientation.Portrait;
                    //myTienda.Orientation = Controls.Orientation.Portrait;
                    break;

                case PageOrientation.LandscapeLeft:
                case PageOrientation.LandscapeRight:
                case PageOrientation.Landscape:
                default:
                    radar.Orientation = Controls.Orientation.Landscape;
                    break;
            }
        }

        /// <summary>
        /// Load PhotoCamera.
        /// </summary>
        public void InitializeCamera()
        {
            if (null == _photoCamera && !_isCameraIntialized)
            {
                _photoCamera = new PhotoCamera();
                _photoCamera.Initialized +=
                    new EventHandler<CameraOperationCompletedEventArgs>(Camera_Initialized);
                _backgroundBrush.SetSource(_photoCamera);
            }
        }

        /// <summary>
        /// Free camera resources.
        /// </summary>
        public void UninitializeCamera()
        {
            if (_isCameraIntialized)
            {
                _photoCamera.Dispose();
                _photoCamera.Initialized -=
                    new EventHandler<CameraOperationCompletedEventArgs>(Camera_Initialized);
                _photoCamera = null;
                _isCameraIntialized = false;
            }
        }

        /// <summary>
        /// When initialize camera complete, occur this method.
        /// </summary>
        /// <param name="sender">event sender</param>
        /// <param name="e">event args</param>
        private void Camera_Initialized(object sender, CameraOperationCompletedEventArgs e)
        {
            if (e.Succeeded)
            {
                _isCameraIntialized = true;
            }
            else
            {
                Dispatcher.BeginInvoke(() =>
                {
                    //MessageBox.Show(ARVIEW_CAMERA_INITIALIZE_FAILED);
                });
            }

        }

        /// <summary>
        /// Tap restaurant label, shows its comments.
        /// </summary>
        /// <param name="sender">event sender</param>
        /// <param name="e">event args</param>
        private void RestaurantItem_Tap(object sender, GestureEventArgs e)
        {
            var brd = sender as Border;
            var ctx = brd.DataContext as RestaurantInfoAdapter;
            var info = ctx.RestaurantInfo;
            var tienda = App.TiendasViewModel.ObtenerListTiendas().Where(c => c.nombre.Equals(info.Name)).FirstOrDefault();
            if (tienda != null)
            {
                App.TiendasViewModel.SelectedItemTienda = tienda;
                App.ViewModel.SelectedListHorarios = tienda.horario;
                NavigationService.Navigate(new Uri("/View/Tiendas/Detalle.xaml", UriKind.Relative));
            }

            //myTienda.SetImage(myTienda.ObtenGradosRotacion);
        }

        /// <summary>
        /// When page loads completed, activate page.
        /// </summary>
        /// <param name="sender">event sender</param>
        /// <param name="e">event args</param>
        private void OnPageLoaded(object sender, RoutedEventArgs e)
        {
            _isLoaded = true;

            try
            {
                radar.Start();
                //myTienda.Start();
                Activated();
                InitializeCamera();
                // Find the page this control is on and listen to its orientation changed events
                if (_parentPage == null)
                {
                    _parentPage =
                        (PhoneApplicationPage)(((PhoneApplicationFrame)Application.Current.RootVisual)).Content;
                }
                _parentPage.OrientationChanged += ParentPageOrientationChanged;
                SetVideoOrientation(_parentPage.Orientation);
            }
            catch
            {
                // Remove warnings on designer
            }
        }

        /// <summary>
        /// The OrientationChanged event of main page
        /// </summary>
        /// <param name="sender">event sender</param>
        /// <param name="e">event args</param>        
        private void ParentPageOrientationChanged(object sender, OrientationChangedEventArgs e)
        {
            SetVideoOrientation(e.Orientation);
            switch (e.Orientation)
            {
                case PageOrientation.None:
                case PageOrientation.PortraitUp:
                case PageOrientation.PortraitDown:
                    radar.Orientation = Controls.Orientation.Portrait;
                    break;

                case PageOrientation.LandscapeLeft:
                case PageOrientation.LandscapeRight:
                case PageOrientation.Landscape:
                default:
                    radar.Orientation = Controls.Orientation.Landscape;
                    break;
            }
        }

        /// <summary>
        /// Sets background video brush parameters based upon page orientation
        /// </summary>
        /// <param name="orientation">orientation</param>
        private void SetVideoOrientation(PageOrientation orientation)
        {
            System.Diagnostics.Debug.WriteLine("Switching to {0}", orientation);
            switch (orientation)
            {
                case PageOrientation.PortraitUp:
                    SetPortraitUp();
                    break;
                case PageOrientation.LandscapeLeft:
                    SetLandscapeLeft();
                    break;
                case PageOrientation.LandscapeRight:
                    SetLandscapeRight();
                    break;
                default:
                    break;
            }
        }

        /// <summary>
        /// Sets background video brush parameters and UI on PortraitUp
        /// </summary>
        private void SetPortraitUp()
        {
            App.RAViewModel.Initialize((int)this.ActualWidth, (int)this.ActualHeight, Vector3.Up);

            _backgroundBrush.Transform = new CompositeTransform { Rotation = 90, CenterX = 240, CenterY = 400 };
            _backgroundBrush.Stretch = Stretch.UniformToFill;
            GridNearbyRestaurant.Height = 800;
            Canvas.SetLeft(radar, 364);
            Canvas.SetTop(radar, 25);
            radar.SetRotate(0);
            if (_marginAdjusted)
            {
                this.Margin = new Thickness(0, -20, 0, 0);
                _marginAdjusted = false;
            }
        }

        /// <summary>
        /// Sets background video brush parameters and UI on LandscapeLeft
        /// </summary>
        private void SetLandscapeLeft()
        {
            App.RAViewModel.Initialize((int)this.ActualWidth, (int)this.ActualHeight, Vector3.Right);

            _backgroundBrush.Transform = null;
            GridNearbyRestaurant.Height = 480;
            Canvas.SetLeft(radar, 684);
            Canvas.SetTop(radar, 40);
            radar.SetRotate(90);
            if (!_marginAdjusted)
            {
                this.Margin = new Thickness(0, 20, 0, 0);
                _marginAdjusted = true;
            }
        }

        /// <summary>
        /// Sets background video brush parameters and UI on LandscapeRight
        /// </summary>
        private void SetLandscapeRight()
        {
            App.RAViewModel.Initialize((int)this.ActualWidth, (int)this.ActualHeight, Vector3.Left);

            _backgroundBrush.Transform = new CompositeTransform { Rotation = 180, CenterX = 400, CenterY = 240 };
            GridNearbyRestaurant.Height = 480;
            Canvas.SetLeft(radar, 684);
            Canvas.SetTop(radar, 40);
            radar.SetRotate(270);
            if (!_marginAdjusted)
            {
                this.Margin = new Thickness(0, 20, 0, 0);
                _marginAdjusted = true;
            }
        }

        /// <summary>
        /// When this view is displayed, load resource.
        /// </summary>
        public void Activated()
        {
            if (_isLoaded)
                StartMotion();
        }

        /// <summary>
        /// Start motion. 
        /// If failed, pop up message box.
        /// </summary>
        private void StartMotion()
        {
            if (Motion.IsSupported)
            {
                if (_motion == null)
                {
                    _motion = new Motion();
                    _motion.TimeBetweenUpdates = TimeSpan.FromMilliseconds(TIMEBETWEENUPDATES);
                    _motion.CurrentValueChanged +=
                        new EventHandler<SensorReadingEventArgs<MotionReading>>(Motion_CurrentValueChanged);
                }

                if (!_isMotionStart)
                {
                    try
                    {
                        _motion.Start();
                        _isMotionStart = true;
                    }
                    catch (Exception /*ex*/)
                    {
                        //MessageBox.Show(ARVIEW_START_MOTION_FAILED);
                    }
                }
            }
            else
            {
                if (!_isPromptedAboutMotionNotSupported)
                {
                    //MessageBox.Show(ARVIEW_NOT_SUPPORT_MOTION);
                    _isPromptedAboutMotionNotSupported = true;
                }

                if (Microsoft.Devices.Environment.DeviceType == Microsoft.Devices.DeviceType.Emulator)
                {
                    foreach (RestaurantInfoAdapter item in App.RAViewModel.NearbyRestaurantItems)
                    {
                        item.Visibility = Visibility.Visible;
                        item.Width = (int)this.ActualWidth;
                    }

                    SetVisibleCanvas(NearbyRestaurantCanvas);
                }
            }
        }

        /// <summary>
        /// Updates restaurants when motion changed.
        /// </summary>
        /// <param name="sender">event sender</param>
        /// <param name="e">event args</param>
        private void Motion_CurrentValueChanged(object sender, SensorReadingEventArgs<MotionReading> e)
        {
            Dispatcher.BeginInvoke(() =>
            {
                if (_motion.IsDataValid)
                {
                    int leftCount = 0;
                    int rightCount = 0;
                    int projectedCount = App.RAViewModel.Project(
                            e.SensorReading.Attitude.RotationMatrix,
                            ref leftCount, ref rightCount);

                    if (projectedCount > 0)
                    {
                        SetVisibleCanvas(ProjectedRestaurantCanvas);
                    }
                    else
                    {
                        SetVisibleCanvas(NearbyRestaurantCanvas);
                    }
                }
            }
            );
        }

        /// <summary>
        /// Determine which canvas will be displayed.
        /// </summary>
        /// <param name="canvas">The canvas</param>
        private void SetVisibleCanvas(UIElement canvas)
        {
            foreach (UIElement item in _canvasList)
            {
                if (item == canvas)
                {
                    item.Visibility = Visibility.Visible;
                    _curVisibleCanvas = canvas;
                }
                else
                {
                    item.Visibility = Visibility.Collapsed;
                }
            }
        }

        /// <summary>
        /// Initialize the variables of the ARView;
        /// </summary>
        private void Initialize()
        {
            _isLoaded = false;
            _isMotionStart = false;
            _isPromptedAboutMotionNotSupported = false;

            _isCameraIntialized = false;
            _borderDefaultColor = Color.FromArgb(255, 01, 01, 01);
            _marginAdjusted = false;

            radar.Distance = ResNaviConfigs.ScanScope;

            InitializeVideoBrush();

            InitializeCanvasList();

            try
            {
                App.RAViewModel.NearbyResLoaded +=
                    new NearbyResLoadedHandler(ARViewModel_NearbyResLoaded);
                App.RAViewModel.NearbyResUpdated(null);
                lstNearbyRestaurants.ItemsSource = App.RAViewModel.NearbyRestaurantItems;
            }
            catch
            {
                // Remove warnings on designer
            }
        }

        /// <summary>
        /// When page unload, deactive page.
        /// </summary>
        /// <param name="sender">event sender</param>
        /// <param name="e">event args</param>
        private void OnPageUnloaded(object sender, RoutedEventArgs e)
        {
            _parentPage.OrientationChanged -= ParentPageOrientationChanged;
            _isLoaded = false;
            radar.Stop();
            Deactived();
            UninitializeCamera();
        }

        /// <summary>
        /// When this view is not displayed, stop motion 
        /// and store restaurants in screen.
        /// </summary>
        public void Deactived()
        {
            StopMotion();
        }

        /// <summary>
        /// Stop motion
        /// </summary>
        private void StopMotion()
        {
            if (_isMotionStart && _motion != null)
            {
                try
                {
                    _motion.Stop();
                    _isMotionStart = false;
                }
                catch (Exception /*ex*/)
                {
                    //MessageBox.Show(ARVIEW_STOP_MOTION_FAILED);
                }
            }
        }

        /// <summary>
        /// When load restuarants complete, occur this method.
        /// </summary>
        private void ARViewModel_NearbyResLoaded()
        {
            Dispatcher.BeginInvoke(() =>
            {
                UpdateProjectedRestaurantView();
            });
        }

        /// <summary>
        /// Update restaurants label.
        /// </summary>
        private void UpdateProjectedRestaurantView()
        {
            radar.SetDeviceLocation(
                App.RAViewModel.CurrentPosition.Longitude,
                App.RAViewModel.CurrentPosition.Latitude
                );
            radar.RemoveVenueLocations();

            this.ProjectedRestaurantCanvas.Children.Clear();
            foreach (RestaurantInfoAdapter item in App.RAViewModel.ProjectedRestaurantItems)
            {
                radar.AddVenueLocation(
                    item.RestaurantInfo.Longitude,
                    item.RestaurantInfo.Latitude);

                TextBlock nameBlock = new TextBlock();
                nameBlock.Width = 180;
                nameBlock.FontSize = 20;
                Color color = Color.FromArgb(255, 125, 237, 17);
                nameBlock.Foreground = new SolidColorBrush(color);
                nameBlock.TextAlignment = TextAlignment.Left;
                nameBlock.TextTrimming = TextTrimming.WordEllipsis;
                nameBlock.Margin = new Thickness(3, 0, 0, 0);

                TextBlock addressBlock = new TextBlock();
                addressBlock.Width = 180;
                addressBlock.FontSize = 14;
                addressBlock.TextAlignment = TextAlignment.Left;
                addressBlock.TextWrapping = TextWrapping.Wrap;
                addressBlock.Margin = new Thickness(3, 0, 0, 0);

                TextBlock distBlock = new TextBlock();
                distBlock.FontSize = 12;
                distBlock.TextAlignment = TextAlignment.Left;
                distBlock.TextWrapping = TextWrapping.Wrap;
                distBlock.Margin = new Thickness(3, 0, 0, 0);

                StackPanel stackPanel = new StackPanel();
                stackPanel.Orientation = System.Windows.Controls.Orientation.Vertical;
                stackPanel.Children.Add(nameBlock);
                stackPanel.Children.Add(addressBlock);
                stackPanel.Children.Add(distBlock);

                Image resIcon = new Image();
                resIcon.Height = 60;
                resIcon.Width = 60;
                resIcon.Margin = new Thickness(8, 4, 4, 4);
                resIcon.Stretch = Stretch.Fill;
                //var uri = new Uri(item.RestaurantInfo.ImageUrl, UriKind.Relative);
                //resIcon.Source = new BitmapImage(uri);

                StackPanel mainStackPanel = new StackPanel();
                mainStackPanel.Orientation = System.Windows.Controls.Orientation.Horizontal;
                mainStackPanel.Children.Add(resIcon);
                mainStackPanel.Children.Add(stackPanel);
                mainStackPanel.HorizontalAlignment = System.Windows.HorizontalAlignment.Center;

                Border border = new Border();
                border.Child = mainStackPanel;
                border.SetValue(Canvas.ZIndexProperty, 2);
                border.Background = new SolidColorBrush(_borderDefaultColor);
                border.Opacity = 0.6;
                border.Tap += new EventHandler<GestureEventArgs>(RestaurantItem_Tap);

                border.DataContext = item;

                Binding nameBinding = new Binding("Name");
                nameBlock.SetBinding(TextBlock.TextProperty, nameBinding);

                Binding addressBinding = new Binding("Address");
                addressBlock.SetBinding(TextBlock.TextProperty, addressBinding);

                Binding distBinding = new Binding("Dist");
                distBlock.SetBinding(TextBlock.TextProperty, distBinding);

                Binding leftBinding = new Binding("Left");
                border.SetBinding(Canvas.LeftProperty, leftBinding);

                Binding topBinding = new Binding("Top");
                border.SetBinding(Canvas.TopProperty, topBinding);

                Binding visibilityBinding = new Binding("Visibility");
                border.SetBinding(Border.VisibilityProperty, visibilityBinding);

                Binding iconUri = new Binding("RestaurantInfo.ImageUrl");
                resIcon.SetBinding(Image.SourceProperty, iconUri);

                border.Width = nameBlock.Width > addressBlock.Width ?
                    nameBlock.Width : addressBlock.Width;
                border.Width += resIcon.Width;
                border.Width += 10;

                border.Tag = item.RestaurantInfo;
                this.ProjectedRestaurantCanvas.Children.Add(border);
            }

        }

        /// <summary>
        /// Initialize the background video brush 
        /// </summary>
        private void InitializeVideoBrush()
        {
            //Init VideoBrush
            _backgroundBrush = new VideoBrush();
            _backgroundBrush.Stretch = Stretch.UniformToFill;
            CanvasScreen.Background = _backgroundBrush;
        }

        /// <summary>
        /// Initialize the canvas list;
        /// </summary>
        private void InitializeCanvasList()
        {
            _canvasList = new List<UIElement>();
            _canvasList.Add(ProjectedRestaurantCanvas);
            _canvasList.Add(NearbyRestaurantCanvas);
            _curVisibleCanvas = null;
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            
            if (App.IsTombstoned)
            {
                UninitializeCamera();
                InitializeCamera();
            }
        }

        protected override void OnBackKeyPress(System.ComponentModel.CancelEventArgs e)
        {
            App.Settings["SalirRA"] = "true";
            base.OnBackKeyPress(e);
        }
    }
}