﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using System.Windows.Media;
using System.IO.IsolatedStorage;
using Starbucks.UI.Data.Parser;
using System.Runtime.Serialization;
using System.IO;

namespace Starbucks.UI.View.Pagos
{
    public partial class Confirmar : PhoneApplicationPage
    {
        IsolatedStorageFile myFile = IsolatedStorageFile.GetUserStoreForApplication();

        public Confirmar()
        {
            InitializeComponent();
            this.Loaded += (sender, args) => this.txtWaterPassword.Focus();
            LoadData("Actualizar.txt");
        }

        private void continuarbtn_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            string pass = txtPassword.Password;
            if (pass == "")
            {
                MessageBox.Show("Introduzca su contraseña");
            }
            else
            {
                if (pass == "prueba")
                {
                    guardar("Actualizar.txt");
                    NavigationService.Navigate(new Uri("/Home.xaml", UriKind.Relative));
                }
                else
                {
                    MessageBox.Show("Contraseña incorrecta");
                }
            }
        }

        private void txtPassword_KeyDown(object sender, System.Windows.Input.KeyEventArgs e)
        {
            string pass = txtPassword.Password;
        }

        private void txtPassword_LostFocus(object sender, RoutedEventArgs e)
        {
            if (string.IsNullOrEmpty(txtPassword.Password))
            {
                txtPassword.Visibility = System.Windows.Visibility.Collapsed;
                txtWaterPassword.Visibility = System.Windows.Visibility.Visible;
            }
        }

        private void txtPassword_GotFocus(object sender, RoutedEventArgs e)
        {
            txtPassword.Visibility = System.Windows.Visibility.Visible;
            txtWaterPassword.Visibility = System.Windows.Visibility.Collapsed;
            txtPassword.Focus();
        }

        private void txtWaterPassword_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            txtPassword.Visibility = System.Windows.Visibility.Visible;
            txtWaterPassword.Visibility = System.Windows.Visibility.Collapsed;
            txtPassword.Focus();
        }

        private void txtWaterPassword_GotFocus(object sender, RoutedEventArgs e)
        {
            txtPassword.Visibility = System.Windows.Visibility.Visible;
            txtWaterPassword.Visibility = System.Windows.Visibility.Collapsed;
            txtPassword.Focus();
        }

        public void LoadData(string sFile)
        {
          
            if (!myFile.FileExists(sFile))
            {
                IsolatedStorageFileStream dataFile = myFile.CreateFile(sFile);
                dataFile.Close();
            }
          
        }
        

        private void guardar(string sFile)
        {
            string sMSG = "actualizarsaldo";

            StreamWriter sw = new StreamWriter(new IsolatedStorageFileStream(sFile, FileMode.Create, myFile));
            sw.WriteLine(sMSG); //Wrting to the file
            sw.Close();
        }

        private void cancelarbtn_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            NavigationService.GoBack();
        }

        public class InfoRecargar
        {
            public string Cadena="STARBUCKS";
            public int IDUsuario;
            public string Token;
            public string TipoDispositivo;
            public string TipoBanco;
            public string TarjetaCadena;
            public string Moneda;
            public double MontoRecarga;
            public string TarjetaBancaria;
            public int  CVV;
            public string FechaVencimiento;
            public string PCFingerPrinting;
            public int GuardarTarjeta;
            public string IDReferencia;
            public List<Cliente> DetalleCliente;
        }

        public class Cliente
        {
            public string PrimerNombre;
           // public string SegundoNombre;
            public string Apellidos;
            public string FechaNacimiento;
            public string Email;
            //public long EdadCuentaCreada;
           // public char EsClientePrevio='N';
           // public string TelefonoCasa;
           // public string TelefonoOficina;
            public string CalleyNumero;
           // public string Ciudad;
          //  public string Estado;
            public string Pais;
            public string CodigoPostal;
           // public string DireccionIP;
        }

        public class RecargaRespone
        {
            public int NoOrden;
            public double NuevoSaldo;
            public int Codigo;
            public string Descripcion;
        }

    }
}