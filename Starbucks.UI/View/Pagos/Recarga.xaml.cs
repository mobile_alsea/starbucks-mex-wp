﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using System.Windows.Media;
using System.Windows.Markup;
using System.IO.IsolatedStorage;
using System.IO;
using System.Windows.Media.Imaging;
using Newtonsoft.Json;
using Starbucks.DAL;
using Starbucks.Entities;
using Starbucks.Utilities;
using System.Windows.Resources;
using System.Diagnostics;
using System.Windows.Input;

namespace Starbucks.UI.View.Pagos
{
    public partial class Recarga : PhoneApplicationPage
    {
        int recarga = 0;
        int recargaCantidadAutomatica = 0;
        int diames = 0;
        int autorizarAutomatica = 0;
        public int[] otro;
        public int[] dias;
        public int[] otroRecarga;
        string tarjetadigitos = "0";
        string tipoTarjeta;

        string urlTarjetaR;
        string saldoTarjetaR;
        string actualizadoTarjetaR;
        
        IsolatedStorageFile myFile = IsolatedStorageFile.GetUserStoreForApplication();

        public class Cantidad
        {
            public string Numero
            {
                get;
                set;
            }
        }


        public DatosRecarga DatosRecarga = (App.Current as App).DatosRecarga;

        WSRewardsViewModel wsRewardsClient = new WSRewardsViewModel();

        public Recarga()
        {
            

            otro = new int[19];
            otroRecarga = new int[15];
            dias = new int[30];
            urlTarjetaR = "";
            saldoTarjetaR = "";
            actualizadoTarjetaR = "";
            InitializeComponent();
            llenardias();
            llenarcantidades();
            llenarcantidadesAutomatica();
            panelAutomatico.Visibility = Visibility.Collapsed;
            continuarbtn.Margin = new Thickness(-13, 150, 296, 0);
            //LoadData("Info.txt");
            DataTemplate itemTmp = (DataTemplate)XamlReader.Load(
                @"<DataTemplate xmlns=""http://schemas.microsoft.com/client/2007"">
                      <StackPanel  Orientation=""Vertical"">
                    <TextBlock Text=""{Binding Numero}"" Margin=""0 -5 0 0"" HorizontalAlignment=""Center"" VerticalAlignment=""Center""/>
                </StackPanel>
                    </DataTemplate>");
            cantidadlist.ItemTemplate = itemTmp;
            cantidadlist2.ItemTemplate = itemTmp;

            DataTemplate itemTmpRecarga2 = (DataTemplate)XamlReader.Load(
               @"<DataTemplate xmlns=""http://schemas.microsoft.com/client/2007"">
                <StackPanel Orientation=""Horizontal"" HorizontalAlignment=""Center"" VerticalAlignment=""Center"" >
                    <TextBlock Text=""recarga por un día del mes""/>
                </StackPanel>
                    </DataTemplate>");
            diasmeslist.ItemTemplate = itemTmpRecarga2;
            diasmeslist.BorderBrush = new SolidColorBrush(Color.FromArgb(255, 113, 173, 42));
            diasmeslist.Foreground = new SolidColorBrush(Color.FromArgb(255, 113, 173, 42));

            wsSmartClient.FinishConsultaMedioPago += SmartClient_FinishConsultaMedioPago;
            wsSmartClient.FinishBajaMedioPago += SmartClient_FinishBajaMedioPago;
            wsRewardsClient.FinishConsultaTarjetas += RewardsClient_FinishConsultaTarjetas;
            CargaMedioPago();


            

        }

        /// <summary>
        /// 
        /// </summary>
        WSSmartSBXViewModel wsSmartClient = new WSSmartSBXViewModel();
        /// <summary>
        /// 
        /// </summary>
        /// 
        private void CargaMedioPago()
        {
            ContentPanelProgress.Visibility = System.Windows.Visibility.Visible;
            wsSmartClient.SyncConsultaMediosPagoWSService((int)CadenaEnum.STARBUCKS, (int)TipoDispositivoEnum.WindowsMobile);

            
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void SmartClient_FinishConsultaMedioPago(object sender, FinishSmartServiceArgs<ConsultaMedioPagoResponse> e)
        {
            ExecuteSafelyDispatcher(() =>
            {
                ContentPanelProgress.Visibility = System.Windows.Visibility.Collapsed;
            });

            if (e.IsTimeOut) {
                ExecuteSafelyDispatcher(() =>
                {
                    System.Windows.MessageBox.Show("No fue posible contactar el sistema de recargas, reintenta más tarde.","Aviso",MessageBoxButton.OK);
                    NavigationService.GoBack();
                    return;
                });
            }

            if (!e.Success)
            {
                ExecuteSafelyDispatcher(() =>
               {
                   System.Windows.MessageBox.Show("No fue posible contactar el sistema de recargas, reintenta más tarde.", "Aviso", MessageBoxButton.OK);
                   NavigationService.GoBack();
                   return;
               });

            }
            else
            {
                if (e.Data.Codigo == 0)
                {
                    ExecuteSafelyDispatcher(() =>
                    {
                        tarjetadigitos = e.Data.TarjetaBancaria.Substring(e.Data.TarjetaBancaria.Length - 4);
                        tipoTarjeta = e.Data.TipoBanco;

                        tarjetapanel.Visibility = System.Windows.Visibility.Visible;
                        tarjetaimg.Source = new BitmapImage(TipoBancoUtil.GetLogo(e.Data.TipoBanco));
                        tipoblock.Text = string.Format("Tarjeta Terminación : {0}", tarjetadigitos);
                    });
                }

            }

            ExecuteSafelyDispatcher(() => continuarbtn.IsEnabled = true);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void SmartClient_FinishBajaMedioPago(object sender, FinishSmartServiceArgs<BajaMedioPagoResponse> e)
        {
            if (e.IsTimeOut) 
            {
                Dispatcher.BeginInvoke(() => System.Windows.MessageBox.Show("No fue posible contactar el sistema de recargas, reintenta más tarde.", "Aviso", MessageBoxButton.OK));
                return;
            }
            if (!e.Success)
            {
                Dispatcher.BeginInvoke(() => System.Windows.MessageBox.Show(e.Message));
                return;
            }

            Dispatcher.BeginInvoke(() =>
            {
                if (e.Data.Codigo == 0) 
                {
                    tarjetapanel.Visibility = System.Windows.Visibility.Collapsed;
                    tarjetaimg.Source = new BitmapImage(TipoBancoUtil.GetLogo("MasterCard"));
                    tipoblock.Text = "tarjeta no disponible";
                    tarjetadigitos = string.Empty;
                }
                else if(e.Data.Codigo == 11)
                {
                    ContentPanelProgress.Visibility = System.Windows.Visibility.Visible;
                    continuarbtn.IsEnabled = false;
                    wsRewardsClient.SyncConsultaTarjetasWSService();
                    System.Windows.MessageBox.Show(e.Data.Descripcion);
                }
                //else muestra el mensaje completo.
            });
        }

        protected void RewardsClient_FinishConsultaTarjetas(object sender, FinishSmartServiceArgs<ConsultaTarjetasResponse> e)
        {
            ExecuteSafelyDispatcher(() =>
            {
                ContentPanelProgress.Visibility = System.Windows.Visibility.Collapsed;
                continuarbtn.IsEnabled = true;
            });

            if (e.IsTimeOut)
            {
                ExecuteSafelyDispatcher(() =>
                {
                    ContentPanelProgress.Visibility = System.Windows.Visibility.Visible;
                    continuarbtn.IsEnabled = false;
                    wsRewardsClient.SyncConsultaTarjetasWSService();
                });
            }

            if (!e.Success)
            {
                ExecuteSafelyDispatcher(() =>
                {
                    System.Windows.MessageBox.Show("No fue posible contactar el sistema de recargas, reintenta más tarde.", "Aviso", MessageBoxButton.OK);
                    NavigationService.GoBack();
                });

            }

            Dispatcher.BeginInvoke(() =>
            {
                if (e.Data.codigo == 0)
                {
                    System.Windows.MessageBox.Show("Tu tarjeta Starbucks Rewards se actualizo correctamente.");
                    IsolatedStorageSettings.ApplicationSettings["idTarjeta"] = Convert.ToInt64(e.Data.tarjetas[0].idTarjeta);
                    IsolatedStorageSettings.ApplicationSettings["uriTarjeta"] = e.Data.tarjetas[0].imagen.grande;
                    IsolatedStorageSettings.ApplicationSettings["uriTarjetaMov"] = e.Data.tarjetas[0].imagen.mediana;
                    IsolatedStorageSettings.ApplicationSettings.Save();
                    NavigationService.GoBack();
                }
                else 
                {
                    System.Windows.MessageBox.Show(e.Data.descripcion);
                }
                //else muestra el mensaje completo.
            });

        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void StackPanel_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            if (MessageBox.Show("¿Está seguro que quiere eliminar la tarjeta de crédito como medio de pago?", "Aviso", MessageBoxButton.OKCancel) == MessageBoxResult.OK)
            {
                wsSmartClient.SyncBajaMedioPagoWSService((int)CadenaEnum.STARBUCKS, (int)TipoDispositivoEnum.WindowsMobile);
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="expresion"></param>
        /// <param name="message"></param>
        private void IsValidRequired(bool expresion, string message)
        {
            if (expresion)
            {
                throw new MissingFieldException(message);
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void continuarbtn_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            try
            {
                IsValidRequired(recarga == 0, "Seleccione una cantidad para la recarga");
                IsValidRequired(autorizarAutomatica == 1 && recargaCantidadAutomatica == 0 && diames == 0, "No haz seleccionado monto mínimo o día para realizar la recarga automática");

                App current = (App.Current as App);
                current.cantidadRecarga = recarga;
                current.cantidadRecargaAutomatica = recargaCantidadAutomatica;
                current.diaRecarga = diames;
                current.numeroTarjeta = "000000000000" + tarjetadigitos;
                current.tipoTarjeta = tipoTarjeta;


                DatosRecarga.Saldo = "";
                DatosRecarga.FechaSaldo = "";
                DatosRecarga.MontoRecarga = current.cantidadRecarga;
                DatosRecarga.MontoRecargaAutomatica = current.cantidadRecargaAutomatica;
                DatosRecarga.DiaRecarga = current.diaRecarga;
                DatosRecarga.DigitosTarjeta = tarjetadigitos;
                DatosRecarga.TipoTarjeta = current.tipoTarjeta;
                DatosRecarga.IsNewMedioPago = tarjetadigitos.Length != 4;
                (App.Current as App).DatosRecarga = DatosRecarga;


                
                
                    string url = tarjetadigitos.Length == 4 ? "/View/Pagos/Transaccion.xaml" : "/View/Pagos/Altas.xaml";
                    NavigationService.Navigate(new Uri(url, UriKind.Relative));
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        public void llenardias()
        {
            List<Cantidad> source = new List<Cantidad>();
            var i = 0;
            for (i = 0; i <= 28; i++)
            {
                source.Add(new Cantidad() { Numero = "" + i });
                dias[i] = i;
            }
            source.Add(new Cantidad() { Numero = "último" });
            dias[i] = 100;

            diasmeslist.ItemsSource = source;
        }

        public void llenarcantidades()
        {
            List<Cantidad> source = new List<Cantidad>();
            source.Add(new Cantidad() { Numero = "otro" });
            for (int i = 0; i <= 18; i++)
            {
                int resultado = (i + 1) * 100;
                source.Add(new Cantidad() { Numero = "$" + resultado });
                otro[i] = resultado;
            }
            cantidadlist.ItemsSource = source;
        }

        public void llenarcantidadesAutomatica()
        {
            List<Cantidad> source = new List<Cantidad>();
            source.Add(new Cantidad() { Numero = "otro" });
            for (int i = 0; i <= 14; i++)
            {
                int resultado = i * 10;
                source.Add(new Cantidad() { Numero = "$" + resultado });
                otroRecarga[i] = resultado;
            }
            cantidadlist2.ItemsSource = source;
        }

        public void colorListasVerde()
        {
            cantidadlist.BorderBrush = new SolidColorBrush(Color.FromArgb(255, 113, 173, 42));
            cantidadlist.Foreground = new SolidColorBrush(Color.FromArgb(255, 113, 173, 42));
            boton1.BorderBrush = new SolidColorBrush(Color.FromArgb(255, 113, 173, 42));
            boton1.Foreground = new SolidColorBrush(Color.FromArgb(255, 113, 173, 42));
            boton2.BorderBrush = new SolidColorBrush(Color.FromArgb(255, 113, 173, 42));
            boton2.Foreground = new SolidColorBrush(Color.FromArgb(255, 113, 173, 42));
            boton3.BorderBrush = new SolidColorBrush(Color.FromArgb(255, 113, 173, 42));
            boton3.Foreground = new SolidColorBrush(Color.FromArgb(255, 113, 173, 42));
        }

        public void colorListasVerde2()
        {
            cantidadlist2.BorderBrush = new SolidColorBrush(Color.FromArgb(255, 113, 173, 42));
            cantidadlist2.Foreground = new SolidColorBrush(Color.FromArgb(255, 113, 173, 42));
            boton4.BorderBrush = new SolidColorBrush(Color.FromArgb(255, 113, 173, 42));
            boton4.Foreground = new SolidColorBrush(Color.FromArgb(255, 113, 173, 42));
            boton5.BorderBrush = new SolidColorBrush(Color.FromArgb(255, 113, 173, 42));
            boton5.Foreground = new SolidColorBrush(Color.FromArgb(255, 113, 173, 42));
            boton6.BorderBrush = new SolidColorBrush(Color.FromArgb(255, 113, 173, 42));
            boton6.Foreground = new SolidColorBrush(Color.FromArgb(255, 113, 173, 42));
            diasmeslist.BorderBrush = new SolidColorBrush(Color.FromArgb(255, 113, 173, 42));
            diasmeslist.Foreground = new SolidColorBrush(Color.FromArgb(255, 113, 173, 42));
        }

        private void cantidadlist_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            colorListasVerde();
            cantidadlist.BorderBrush = new SolidColorBrush(Colors.White);
            cantidadlist.Foreground = new SolidColorBrush(Colors.White);
            DataTemplate itemTmp = (DataTemplate)XamlReader.Load(
             @"<DataTemplate xmlns=""http://schemas.microsoft.com/client/2007"">
                      <StackPanel  Orientation=""Vertical"">
                    <TextBlock Text=""{Binding Numero}"" Margin=""0 0 0 0"" HorizontalAlignment=""Center"" VerticalAlignment=""Center""/>
                </StackPanel>
                    </DataTemplate>");
            cantidadlist.ItemTemplate = itemTmp;
        }

        private void cantidadlist2_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            colorListasVerde2();
            cantidadlist2.BorderBrush = new SolidColorBrush(Colors.White);
            cantidadlist2.Foreground = new SolidColorBrush(Colors.White);
            DataTemplate itemTmp = (DataTemplate)XamlReader.Load(
                @"<DataTemplate xmlns=""http://schemas.microsoft.com/client/2007"">
                      <StackPanel  Orientation=""Vertical"">
                    <TextBlock Text=""{Binding Numero}"" Margin=""0 0 0 0"" HorizontalAlignment=""Center"" VerticalAlignment=""Center""/>
                </StackPanel>
                    </DataTemplate>");
            cantidadlist2.ItemTemplate = itemTmp;
        }

        private void Button_Tap_1(object sender, System.Windows.Input.GestureEventArgs e)
        {
            recarga = 150;
            colorListasVerde();
            boton1.BorderBrush = new SolidColorBrush(Colors.White);
            boton1.Foreground = new SolidColorBrush(Colors.White);
        }

        private void Button_Tap_2(object sender, System.Windows.Input.GestureEventArgs e)
        {
            recarga = 150;
            colorListasVerde();
            boton2.BorderBrush = new SolidColorBrush(Colors.White);
            boton2.Foreground = new SolidColorBrush(Colors.White);
        }

        private void Button_Tap_3(object sender, System.Windows.Input.GestureEventArgs e)
        {
            recarga = 500;
            colorListasVerde();
            boton3.BorderBrush = new SolidColorBrush(Colors.White);
            boton3.Foreground = new SolidColorBrush(Colors.White);
        }

        private void Button_Tap_4(object sender, System.Windows.Input.GestureEventArgs e)
        {
            recargaCantidadAutomatica = 50;
            colorListasVerde2();
            boton4.BorderBrush = new SolidColorBrush(Colors.White);
            boton4.Foreground = new SolidColorBrush(Colors.White);
            diames = 0;
        }

        private void Button_Tap_5(object sender, System.Windows.Input.GestureEventArgs e)
        {
            recargaCantidadAutomatica = 70;
            colorListasVerde2();
            boton5.BorderBrush = new SolidColorBrush(Colors.White);
            boton5.Foreground = new SolidColorBrush(Colors.White);
            diames = 0;
        }

        private void Button_Tap_6(object sender, System.Windows.Input.GestureEventArgs e)
        {
            recargaCantidadAutomatica = 100;
            colorListasVerde2();
            boton6.BorderBrush = new SolidColorBrush(Colors.White);
            boton6.Foreground = new SolidColorBrush(Colors.White);
            diames = 0;
        }

        private void switchRecarga_Checked(object sender, RoutedEventArgs e)
        {
            panelAutomatico.Visibility = Visibility.Visible;
            continuarbtn.Margin = new Thickness(-13, 0, 296, 0);
            autorizarAutomatica = 1;
        }

        private void switchRecarga_Unchecked(object sender, RoutedEventArgs e)
        {
            panelAutomatico.Visibility = Visibility.Collapsed;
            continuarbtn.Margin = new Thickness(-13, 150, 296, 0);
            autorizarAutomatica = 0;
            recargaCantidadAutomatica = 0;
            colorListasVerde2();
            diames = 0;
        }

        private void cantidadlist2_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                int s = cantidadlist2.SelectedIndex;
                
                if (s > 0)
                {
                    colorListasVerde2();
                    DataTemplate itemTmp = (DataTemplate)XamlReader.Load(
        @"<DataTemplate xmlns=""http://schemas.microsoft.com/client/2007"">
                      <StackPanel  Orientation=""Vertical"">
                    <TextBlock Text=""{Binding Numero}"" Margin=""0 -5 0 0"" HorizontalAlignment=""Center"" VerticalAlignment=""Center""/>
                </StackPanel>
                    </DataTemplate>");
                    cantidadlist2.ItemTemplate = itemTmp;
                    recargaCantidadAutomatica = otroRecarga[cantidadlist2.SelectedIndex-1];
                    cantidadlist2.BorderBrush = new SolidColorBrush(Colors.White);
                    cantidadlist2.Foreground = new SolidColorBrush(Colors.White);
                    diames = 0;
                }
            }
            catch
            {
            }
        }

        private void cantidadlist_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                int s = cantidadlist.SelectedIndex;
                if (s > 0)
                {
                    colorListasVerde();
                    DataTemplate itemTmp = (DataTemplate)XamlReader.Load(
        @"<DataTemplate xmlns=""http://schemas.microsoft.com/client/2007"">
                      <StackPanel  Orientation=""Vertical"">
                    <TextBlock Text=""{Binding Numero}"" Margin=""0 -5 0 0"" HorizontalAlignment=""Center"" VerticalAlignment=""Center"" />
                </StackPanel>
                    </DataTemplate>");
                    cantidadlist.ItemTemplate = itemTmp;
                    recarga = otro[cantidadlist.SelectedIndex-1];
                    cantidadlist.BorderBrush = new SolidColorBrush(Colors.White);
                    cantidadlist.Foreground = new SolidColorBrush(Colors.White);
                }
            }
            catch
            {
            }
        }

        private void diasmeslist_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            try
            {
                
            }
            catch
            {
            }
        }

        private void diasmeslist_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                
                int s = diasmeslist.SelectedIndex;
                if (s > 0)
                {
                    recargaCantidadAutomatica = 0;
                    diames = dias[diasmeslist.SelectedIndex];
                    DataTemplate itemTmpRecarga1 = (DataTemplate)XamlReader.Load(
                    @"<DataTemplate xmlns=""http://schemas.microsoft.com/client/2007"">
                <StackPanel Orientation=""Horizontal"" HorizontalAlignment=""Center"" VerticalAlignment=""Center"" >
                    <TextBlock Text=""recarga el día "" Margin=""0 0 0 0""/>
                    <TextBlock Text=""{Binding Numero}"" Margin=""0 0 0 0"" HorizontalAlignment=""Center"" VerticalAlignment=""Center""/>
                    <TextBlock Text="" de cada mes""/>
                </StackPanel>
                    </DataTemplate>");
                    diasmeslist.ItemTemplate = itemTmpRecarga1;

                    colorListasVerde2();
                    diasmeslist.BorderBrush = new SolidColorBrush(Colors.White);
                    diasmeslist.Foreground = new SolidColorBrush(Colors.White);
                }
            }
            catch
            {
            }
        }

        public void LoadData(string sFile)
        {
            Saldo.Text = saldoTarjetaR;
            FechaSaldo.Text = actualizadoTarjetaR;
            string uritarjetalog = urlTarjetaR;

            Uri uri = new Uri(uritarjetalog, UriKind.Absolute);
            tarjetaGrandeImg.Source = new BitmapImage(uri);

            //myFile.DeleteFile(sFile);
            if (!myFile.FileExists(sFile))
            {
                tarjetapanel.Visibility = Visibility.Collapsed;
                IsolatedStorageFileStream dataFile = myFile.CreateFile(sFile);
                dataFile.Close();
            }
            else
            {
                //Reading and loading data
                StreamReader reader = new StreamReader(new IsolatedStorageFileStream(sFile, FileMode.Open, myFile));
                string rawData = reader.ReadToEnd();
                reader.Close();

                string[] sep = new string[] { "\r\n" }; //Splittng it with new line
                string[] arrData = rawData.Split(sep, StringSplitOptions.RemoveEmptyEntries);
                try
                {

                    if (arrData[0].Length < 4)
                    {
                        tarjetapanel.Visibility = Visibility.Collapsed;
                    }
                    else
                    {
                        tarjetadigitos = arrData[0];
                        tipoblock.Text = arrData[1].ToLower() + " terminación " + tarjetadigitos;
                        (App.Current as App).tipoTarjeta = arrData[1];
                        (App.Current as App).numeroTarjeta = "000000000000" + tarjetadigitos;
                        tarjetapanel.Visibility = Visibility.Visible;

                        if ((App.Current as App).tipoTarjeta == "MASTERCARD")
                        {
                            BitmapImage image = new BitmapImage(new Uri("/Resources/mastercardlogo.png", UriKind.Relative));
                            tarjetaimg.Source = image;
                        }
                        if ((App.Current as App).tipoTarjeta == "VISA")
                        {
                            BitmapImage image = new BitmapImage(new Uri("/Resources/visalogo.png", UriKind.Relative));
                            tarjetaimg.Source = image;
                        }
                        if ((App.Current as App).tipoTarjeta == "AMEX")
                        {
                            BitmapImage image = new BitmapImage(new Uri("/Resources/amexlogo.png", UriKind.Relative));
                            tarjetaimg.Source = image;
                        }
                    }
                }
                catch
                {
                }
            }
        }

        public void programarRecarga()
        {
            System.Uri targetUri = new System.Uri("http://201.155.112.175:4090/Servicio.svc/JSON/ActualizaProgramaRecarga");
            HttpWebRequest request = (HttpWebRequest)HttpWebRequest.Create(targetUri);
            request.Method = "POST";
            request.ContentType = "application/json.";
            request.BeginGetRequestStream(new AsyncCallback(ReadWebRequestStreamCallback), request);
        }



        private void ReadWebRequestStreamCallback(IAsyncResult callbackResult)
        {
            HttpWebRequest myRequest = (HttpWebRequest)callbackResult.AsyncState;
            Stream postStream = myRequest.EndGetRequestStream(callbackResult);

            //string postData = "contrasenia=" + hashlog + "&udId=" + deviceID + "&email=" + usernamelog + "";
            //byte[] byteArray = Encoding.UTF8.GetBytes(postData);
            //postStream.Write(byteArray, 0, byteArray.Length);
            //postStream.Close();

            myRequest.BeginGetResponse(new AsyncCallback(GetResponsetStreamCallback), myRequest);
        }

        private void GetResponsetStreamCallback(IAsyncResult callbackResult)
        {

            HttpWebRequest request = (HttpWebRequest)callbackResult.AsyncState;
            HttpWebResponse response = (HttpWebResponse)request.EndGetResponse(callbackResult);
            using (StreamReader httpWebStreamReader = new StreamReader(response.GetResponseStream()))
            {
                string results = httpWebStreamReader.ReadToEnd();
                Dispatcher.BeginInvoke(() =>
                {
                    RecargaRespone myObjects = JsonConvert.DeserializeObject<RecargaRespone>(results) as RecargaRespone;
                    if (myObjects.Codigo == 0)
                    {
                        return;
                    }
                    else
                    {
                        System.Windows.MessageBox.Show("No se pudo programar la recarga automática, intente de nuevo");
                    }
                });

            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="action"></param>
        private void ExecuteSafelyDispatcher(Action action)
        {
            if (Deployment.Current.Dispatcher.CheckAccess())
            {
                action();
            }
            else
            {
                Deployment.Current.Dispatcher.BeginInvoke(() =>
                {
                    action();
                });
            }
        }
        public class ActualizarProgramaRecarga
        {
            public string Cadena = "STARBUCKS";
            public int IDUsuario = Convert.ToInt16(IsolatedStorageSettings.ApplicationSettings["idusuario"]);
            public string Token = IsolatedStorageSettings.ApplicationSettings["token"].ToString();
            public string TipoDispositivo = "Movil";
            public string TipoBanco;
            public string TarjetaCadena;
            public int TipoRecarga;
            public string Moneda;
            public double MontoRecarga;
            public double LimiteMinimoSaldo;
            public int DiaRecarga;
            public string TarjetaBancaria;
            public int CVV;
            public string FechaVencimiento;
            public List<Cliente> DetalleCliente;
            public string PCFingerPrinting;

        }

        public class Cliente
        {
            public string PrimerNombre;
            // public string SegundoNombre;
            public string Apellidos;
            public string FechaNacimiento;
            public string Email;
            //public long EdadCuentaCreada;
            // public char EsClientePrevio='N';
            // public string TelefonoCasa;
            // public string TelefonoOficina;
            public string CalleyNumero;
            // public string Ciudad;
            //  public string Estado;
            public string Pais;
            public string CodigoPostal;
            // public string DireccionIP;
        }

        public class RecargaRespone
        {
            public int Codigo;
            public string Descripcion;
        }

        protected override void OnNavigatedTo(System.Windows.Navigation.NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);
            urlTarjetaR = NavigationContext.QueryString["imagenURL"];
            saldoTarjetaR = NavigationContext.QueryString["saldo"];
            actualizadoTarjetaR = NavigationContext.QueryString["actualizado"];
            LoadData("Info.txt");
        }
    }
}