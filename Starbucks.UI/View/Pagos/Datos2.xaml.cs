﻿using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using Starbucks.UI.Data.Parser;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.IO.IsolatedStorage;
using System.Linq;
using System.Net;
using System.Runtime.Serialization;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Info;
using JeffWilcox.Utilities.Silverlight;
using Newtonsoft.Json;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Collections.ObjectModel;
using Starbucks.Entities;
using Starbucks.DAL;

namespace Starbucks.UI.View.Pagos
{
    public partial class Datos2 : PhoneApplicationPage
    {
        bool avanzar = false;
        int idPreregistro;
        string nombre;
        string correo;
        string contrasenia;
        string apellidos;
        string fechaNacimiento;
        string deviceID;
        string colonia;
        string codigopostal;
        string direccion;
        int suscripcion = 1;
        int estadoID = 0;
        int municipioID = 0;
        string hashlog;
        string estadoName;
        string ciudadName;

        public DatosRecarga DatosRecarga = (App.Current as App).DatosRecarga;
        IsolatedStorageFile myFile = IsolatedStorageFile.GetUserStoreForApplication();
        WSRewardsViewModel wsRewardstClient = new WSRewardsViewModel();

        long idusuariolog;
        string tokenlog;
        long idTarjetalog;
        string uritarjetalog;
        string uritarjetamov;
        string actualizadotext;
        string saldoText;

        protected override void OnNavigatedTo(System.Windows.Navigation.NavigationEventArgs e)
        {
            idPreregistro = Convert.ToInt16(NavigationContext.QueryString["idPreregistro"]);
            nombre = NavigationContext.QueryString["nombre"];
            correo = NavigationContext.QueryString["email"];
            contrasenia = NavigationContext.QueryString["contrasenia"];
            apellidos = NavigationContext.QueryString["apellidos"];
            fechaNacimiento = NavigationContext.QueryString["fechaNacimiento"];
        }

        public Datos2()
        {
            //codigopostaltxt.MaxLength = 5;
            InitializeComponent();
            codigopostaltxt.MaxLength = 5;
            Starbucks.DAL.DataBaseViewModel db = new Starbucks.DAL.DataBaseViewModel();
            List<Starbucks.DAL.Model.Estados> estadosDb =  db.GetEntidades();
            this.estadolist.ItemsSource = new ObservableCollection<Starbucks.DAL.Model.Estados>(estadosDb);
            DatosRecarga = (App.Current as App).DatosRecarga;

            wsRewardstClient.FinishRegistroUsuario += wsSmartClient_FinishRegistroUsuario;
            ciudadlist.Visibility = System.Windows.Visibility.Collapsed;
            ciudadlbl.Visibility = System.Windows.Visibility.Collapsed;
            progress.Visibility = System.Windows.Visibility.Collapsed;
        }


        private void estadolist_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            ciudadlist.Visibility = System.Windows.Visibility.Visible;
            ciudadlbl.Visibility = System.Windows.Visibility.Visible;
        }

        private void ciudadlist_Tap(object sender, SelectionChangedEventArgs e)
        {
            Starbucks.DAL.Model.Municipios municipio = ciudadlist.SelectedItem as Starbucks.DAL.Model.Municipios;
            municipioID = Convert.ToInt32(municipio.ID_Municipio);
            ciudadName = municipio.Descrpcion;
            avanzar = true;
        }

        private void estadolist_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            Starbucks.DAL.Model.Estados estado = estadolist.SelectedItem as Starbucks.DAL.Model.Estados;
            estadoID = estado.ID_Estado;
            estadoName = estado.Descripcion;

            Starbucks.DAL.DataBaseViewModel db = new Starbucks.DAL.DataBaseViewModel();
            List<Starbucks.DAL.Model.Municipios> municipiosDB = db.GetMunicipiosByIDEstado(estado.ID_Estado);
            ciudadlist.ItemsSource = municipiosDB;
            ciudadlist.Visibility = System.Windows.Visibility.Visible;
            ciudadlbl.Visibility = System.Windows.Visibility.Visible;
        }

        private void ReadWebRequestStreamCallback(IAsyncResult callbackResult)
        {
            HttpWebRequest myRequest = (HttpWebRequest)callbackResult.AsyncState;
            Stream postStream = myRequest.EndGetRequestStream(callbackResult);
            string DeviceUniqueId = Starbucks.Utilities.ConfigurationSettings.DeviceUniqueId;

            string postData = "idPreregistro=" + idPreregistro + "&udId=" + DeviceUniqueId + "&nombre=" + nombre + "&apellidos=" + apellidos + "&email=" + correo + "&contrasenia=" + hashlog + "&fechaNacimiento=" + fechaNacimiento + "&colonia=" + colonia + "&calleyNumero=" + direccion + "&ciudad=" + municipioID + "&estado=" + estadoID + "&pais=139&codigoPostal=" + codigopostal + "&suscripcion=" + suscripcion + "";
            //   postData = "idPreregistro=" + idPreregistro + "&udId=123456789&nombre=" + nombre + "&apellidos=" + apellidos + "&email=" + correo + "&contrasenia=" + hashlog + "&fechaNacimiento=" + fechaNacimiento + "&colonia=" + colonia + "&calleyNumero=" + direccion + "&ciudad=1&estado=" + estado + "&pais=139&codigoPostal=" + codigopostal + "&suscripcion=" + suscripcion + "";
            //string postData = "idPreregistro=98&udId=F4F381B3D27F4CA1B0A7FB62B7BD6B37&nombre=Juan&apellidos=Perez&email=jperez@mail.com&contrasenia=5286E1DC378D7F403A5A48967AF0BB61&fechaNacimiento=27/01/1970&colonia=Springfield&calleyNumero=Avenida siempre viva 742&ciudad=256&estado=9&pais=139&codigoPostal=04915&suscripcion=true";

            byte[] byteArray = Encoding.UTF8.GetBytes(postData);
            postStream.Write(byteArray, 0, byteArray.Length);
            postStream.Close();

            myRequest.BeginGetResponse(new AsyncCallback(GetResponsetStreamCallback), myRequest);
        }

        void GetResponsetStreamCallback(IAsyncResult callbackResult)
        {

            HttpWebRequest request = (HttpWebRequest)callbackResult.AsyncState;
            try{
                HttpWebResponse response = (HttpWebResponse)request.EndGetResponse(callbackResult);
                using (StreamReader httpWebStreamReader = new StreamReader(response.GetResponseStream()))
                {
                    string results = httpWebStreamReader.ReadToEnd();
                    Dispatcher.BeginInvoke(() =>
                    {
                        InfoRegistroUsuario myObjects = JsonConvert.DeserializeObject<InfoRegistroUsuario>(results) as InfoRegistroUsuario;
                        if (myObjects.codigo == 0)
                        {
                            MessageBox.Show("Registro realizado con éxito");
                            NavigationService.Navigate(new Uri("/Home.xaml", UriKind.Relative));
                            return;
                        }
                        else
                        {
                            MessageBox.Show("El registro no se a podido realizar: " + myObjects.descripcion);
                        }
                    });

                }
            }
            catch (IOException e){
                Console.WriteLine("IOException source: {0}", e.Message);
            }
            
        }

        private void continuarbtn_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            if (direcciontxt.Text == "" || coloniatxt.Text == "" || codigopostaltxt.Text == "")
            {
                MessageBox.Show("Es necesario llenar todos los datos");
                return;
            }
            if (!avanzar)
            {
                MessageBox.Show("Es necesario elegir su estado y ciudad");
                return;
            }

            colonia = coloniatxt.Text;
            direccion = direcciontxt.Text;
            codigopostal = codigopostaltxt.Text;

            hashlog = MD5CryptoServiceProvider.GetMd5String(contrasenia);
            string DeviceUniqueId = Starbucks.Utilities.ConfigurationSettings.DeviceUniqueId;

            string postData = "idPreregistro=" + idPreregistro + "&udId=" + DeviceUniqueId + "&nombre=" + nombre + "&apellidos=" + apellidos + "&email=" + correo + "&contrasenia=" + hashlog + "&fechaNacimiento=" + fechaNacimiento + "&colonia=" + colonia + "&calleyNumero=" + direccion + "&ciudad=" + municipioID + "&estado=" + estadoID + "&pais=139&codigoPostal=" + codigopostal + "&suscripcion=" + suscripcion + "";

            progress.Visibility = System.Windows.Visibility.Visible;
            /*System.Uri targetUri = new System.Uri("http://54.219.33.208:8080/Starbucks-WSRewards/wsrewards/registroUsuario");
            HttpWebRequest request = (HttpWebRequest)HttpWebRequest.Create(targetUri);
            request.Method = "POST";
            request.ContentType = "application/x-www-form-urlencoded";
            request.BeginGetRequestStream(new AsyncCallback(ReadWebRequestStreamCallback), request);*/
            wsRewardstClient.SyncRegistroUsuarioWSService(postData);
        }

        private void switchRecarga_Checked(object sender, RoutedEventArgs e)
        {
            suscripcion = 1;
        }

        private void switchRecarga_Unchecked(object sender, RoutedEventArgs e)
        {
            suscripcion = 0;
        }

        protected void wsSmartClient_FinishRegistroUsuario(object sender, FinishSmartServiceArgs<RegistroUsuarioResponse> e)
        {
            ExecuteSafelyDispatcher(() => progress.Visibility = System.Windows.Visibility.Collapsed);

            if (e.IsTimeOut)
            {
                return;
            }

            if (!e.Success)
            {
                ExecuteSafelyDispatcher(() => System.Windows.MessageBox.Show(e.Message));
                return;
            }

            if (e.Data.codigo == 0)
            {
                ExecuteSafelyDispatcher(() =>
                {
                    //Correcto
                    double inputValue = Math.Round(e.Data.tarjetas[0].saldo, 2);
                    saldoText = "$" + String.Format("{0:0.00}", inputValue);
                    DateTime now = DateTime.Now;
                    actualizadotext = "Actualizado el " + now;
                    idusuariolog = e.Data.idUsuario;
                    tokenlog = e.Data.token;
                    idTarjetalog = Convert.ToInt64(e.Data.tarjetas[0].idTarjeta);
                    uritarjetamov = e.Data.tarjetas[0].imagen.mediana;
                    uritarjetalog = e.Data.tarjetas[0].imagen.grande;

                    DatosCliente datosCliente = new DatosCliente();
                    datosCliente.apellidos = apellidos;
                    datosCliente.codigoPostal = codigopostal;
                    datosCliente.calleyNumero = direccion;
                    datosCliente.email = correo;
                    datosCliente.primerNombre = nombre;
                    datosCliente.esClientePrevio = "Y";
                    datosCliente.fechaNacimiento = fechaNacimiento;
                    datosCliente.estado = estadoName;
                    datosCliente.ciudad = ciudadName;
                    datosCliente.pais = "México";

                    DatosRecarga.TarjetaCadena = e.Data.tarjetas[0].numeroTarjeta;
                    DatosRecarga.DatosCliente = datosCliente;

                    guardar("Sesion.txt");
                });
            }
            else 
            {
                ExecuteSafelyDispatcher(() =>
                {
                    //Correcto
                    System.Windows.MessageBox.Show(e.Data.descripcion, "Aviso", MessageBoxButton.OK);
                });
            }
        }

        private void ExecuteSafelyDispatcher(Action action)
        {
            if (Deployment.Current.Dispatcher.CheckAccess())
            {
                action();
            }
            else
            {
                Deployment.Current.Dispatcher.BeginInvoke(() =>
                {
                    action();
                });
            }
        }

        private void guardar(string sFile)
        {
            string sMSG = "";
            if (sFile == "Sesion.txt")
            {
                sMSG = "sesionabierta";

                IsolatedStorageSettings.ApplicationSettings["idusuario"] = idusuariolog;
                IsolatedStorageSettings.ApplicationSettings["token"] = tokenlog;
                IsolatedStorageSettings.ApplicationSettings["idTarjeta"] = idTarjetalog;
                IsolatedStorageSettings.ApplicationSettings["uriTarjeta"] = uritarjetalog;
                IsolatedStorageSettings.ApplicationSettings["uriTarjetaMov"] = uritarjetamov;
                IsolatedStorageSettings.ApplicationSettings["saldotemporal"] = saldoText;
                IsolatedStorageSettings.ApplicationSettings["fechaactualizado"] = actualizadotext;
                IsolatedStorageSettings.ApplicationSettings["DatosRecarga"] = DatosRecarga;

                (App.Current as App).DatosRecarga = DatosRecarga;
                IsolatedStorageSettings.ApplicationSettings.Save();
            }
            if (sFile == "Actualizar.txt") sMSG = "desactivar";
            StreamWriter sw = new StreamWriter(new IsolatedStorageFileStream(sFile, FileMode.Create, myFile));
            sw.WriteLine(sMSG); //Wrting to the file
            sw.Close();

            MessageBox.Show("Registro realizado con éxito");
            NavigationService.Navigate(new Uri("/Home.xaml", UriKind.Relative));
        }
    }

    public class InfoRegistroUsuario
    {
        public int codigo;
        public string descripcion;
        public List<string> datosFaltantes;
        public string token;
        public List<Tarjetas> tarjetas;
        public long idUsuario;
    }

    public class Tarjetas
    {
        public long idTarjeta;
        public string numeroTarjeta;
        public string nivelLealtad;
        public Imagen imagen;
        public string alias;
        public double saldo;
        public bool esPrincipal;
        public string fechaActivacion;

    }

    public class Imagen
    {
        public string grande;
        public string icon;
        public string mediana;
        public string chica;
    }

}