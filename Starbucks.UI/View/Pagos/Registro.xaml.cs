﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using System.IO.IsolatedStorage;
using System.Runtime.Serialization.Json;
using System.IO;
using Microsoft.Phone.Info;
using System.Runtime.Serialization;
using Newtonsoft.Json;
using System.Text;
using System.Windows.Input;
using Starbucks.DAL;
using Starbucks.Entities;

namespace Starbucks.UI.View.Pagos
{
    public partial class Registro : PhoneApplicationPage
    {
        string replacedString;
        string codigo;
        WSRewardsViewModel wsRewardstClient = new WSRewardsViewModel();

        public Registro()
        {
            InitializeComponent();
            this.Loaded += (sender, args) => this.numerotxt.Focus();
            numerotxt.MaxLength = 19;
            codigotxt.MaxLength = 8;
            progress.Visibility = System.Windows.Visibility.Collapsed;
            wsRewardstClient.FinishValidaTarjeta += wsRewardsClient_FinishValidaTarjeta;
        }

        protected void wsRewardsClient_FinishValidaTarjeta(object sender, FinishSmartServiceArgs<ValidaTarjetaResponse> e) 
        {
            if (e.IsTimeOut)
            {
                ExecuteSafelyDispatcher(() =>
                {
                    System.Windows.MessageBox.Show("No se pudo conectar con el servidor, reintente mas tarde.", "Aviso", MessageBoxButton.OK);
                });
                return;
            }

            if (!e.Success)
            {
                ExecuteSafelyDispatcher(() =>
                {
                    System.Windows.MessageBox.Show(e.Message);
                });

                return;
            }

            if (e.Data.Codigo == 0)
            {

                ExecuteSafelyDispatcher(() =>
                {
                    //Correcto
                    NavigationService.Navigate(new Uri("/View/Pagos/Datos1.xaml?idPreregistro=" + e.Data.IdPreregistro.ToString() + "", UriKind.Relative));
                    return;
                });
            }
            else
            {
                ExecuteSafelyDispatcher(() =>
                {
                    System.Windows.MessageBox.Show("Tarjeta Rewards invalida: " + e.Data.Descripcion);
                });
            }
        }


        private void ExecuteSafelyDispatcher(Action action)
        {
            if (Deployment.Current.Dispatcher.CheckAccess())
            {
                action();
            }
            else
            {
                Deployment.Current.Dispatcher.BeginInvoke(() =>
                {
                    action();
                });
            }
        }


        private void continuarbtn_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            progress.Visibility = System.Windows.Visibility.Visible;
            codigo = codigotxt.Text;
            if (numerotxt.Text.Length < 19 || codigotxt.Text.Length < 8) MessageBox.Show("Los datos de tu tarjeta no son correctos");
            else
            {
                string numero = numerotxt.Text;
                replacedString = numero.Replace(" ", "");
                wsRewardstClient.SyncValidarTarjetaWSService(replacedString, codigo);
            }
        }

        private void numerotxt_TextChanged(object sender, KeyEventArgs e)
        {
            string numero = GetNewCardNumber(numerotxt.Text, e);
            if (numero.Length <= numerotxt.MaxLength)
            {
                numerotxt.Text = numero;
                numerotxt.SelectionStart = numerotxt.Text.Length;
            }
        }

        private string GetNewCardNumber(string oldPasscode, KeyEventArgs keyEventArgs)
        {
            string newPasscode = string.Empty;
            switch (keyEventArgs.Key)
            {
                case Key.D0:
                case Key.D1:
                case Key.D2:
                case Key.D3:
                case Key.D4:
                case Key.D5:
                case Key.D6:
                case Key.D7:
                case Key.D8:
                case Key.D9:
                    if (oldPasscode.Length < numerotxt.MaxLength)
                    {
                        if (oldPasscode.Length == 4 || oldPasscode.Length == 9 || oldPasscode.Length == 14)
                        {
                            newPasscode = oldPasscode + " ";
                        }
                        else
                        {
                            newPasscode = oldPasscode;
                        }
                    }
                    else
                    {
                        newPasscode = oldPasscode;
                    }
                    break;
                case Key.Back:
                    if (oldPasscode.Length > 0)
                    {
                        if (oldPasscode.Length == 4 || oldPasscode.Length == 9 || oldPasscode.Length == 14)
                        {
                            newPasscode = oldPasscode.Remove(oldPasscode.Length - 1);
                        }
                        else
                        {
                            newPasscode = oldPasscode;
                        }
                    }
                    //oldPasscode.Substring(0, oldPasscode.Length - 1);
                    break;
                default:
                    //others
                    newPasscode = oldPasscode;
                    break;
            }
            return newPasscode;
        }

        public class InfoValidaTarjeta
        {
            public int codigo;
            public string descripcion;
            public int idPreregistro;
        }
    }
}