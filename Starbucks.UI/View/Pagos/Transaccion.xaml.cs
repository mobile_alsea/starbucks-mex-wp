﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using System.Windows.Media.Imaging;
using System.IO;
using System.Text;
using Starbucks.Utilities;
using Starbucks.Entities;
using Starbucks.UI.Controls;
using System.IO.IsolatedStorage;
using Starbucks.DAL;

namespace Starbucks.UI.View.Pagos
{
    public partial class Transaccion : PhoneApplicationPage
    {
        /// <summary>
        /// 
        /// </summary>
        WSSmartSBXViewModel wsSmartClient = new WSSmartSBXViewModel();
        /// <summary>
        /// 
        /// </summary>
        public DatosRecarga DatosRecarga { get; set; }
        /// <summary>
        /// Ejecuta la Transaccion considerando 4 flujos diferentes
        /// </summary>
        public Transaccion()
        {
            InitializeComponent();

            DatosRecarga = (App.Current as App).DatosRecarga;

            wsSmartClient.FinishRecarga += wsSmartClient_FinishRecarga;
            wsSmartClient.FinishActualizaProgramaRecarga += wsSmartClient_FinishActualizaProgramaRecarga;
            wsSmartClient.FinishEstatusRecarga += wsSmartClient_FinishEstatusRecarga;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="e"></param>
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            MontoRecarga.Text = string.Format("${0}.00", DatosRecarga.MontoRecarga);
            autorecargapanel.Visibility = DatosRecarga.IsRecargaAutomatica ? Visibility.Visible : Visibility.Collapsed;
            if (DatosRecarga.TipoRecarga == TipoRecargaEnum.MontoMinimo)
            {
                LabelRecargaAutomatica.Text = "Autorecarga en saldo de:";
                MontoRecargaAutomatica.Text = string.Format("${0}.00", DatosRecarga.MontoRecargaAutomatica);
            }
            if (DatosRecarga.TipoRecarga == TipoRecargaEnum.DiaFijo)
            {
                LabelRecargaAutomatica.Text = string.Format("Recarga el día {0} de cada mes.", DatosRecarga.DiaRecarga == 100 ? "Ultimo" : DatosRecarga.DiaRecarga.ToString());
                MontoRecargaAutomatica.Text = string.Empty;
            }
            BitmapImage image = new BitmapImage(TipoBancoUtil.GetLogo(DatosRecarga.TipoTarjeta));
            TarjetaImagen.Source = image;
            TarjetaEtiqueta.Text = string.Format("Tarjeta terminación : {0}", DatosRecarga.DigitosTarjeta);

            Saldo.Text = IsolatedStorageSettings.ApplicationSettings["saldotemporal"].ToString();
            FechaSaldo.Text = IsolatedStorageSettings.ApplicationSettings["fechaactualizado"].ToString();
            string uritarjetalog = IsolatedStorageSettings.ApplicationSettings["uriTarjeta"].ToString();

            Uri uri = new Uri(uritarjetalog, UriKind.Absolute);
            tarjetaGrandeImg.Source = new BitmapImage(uri);

            base.OnNavigatedTo(e);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void wsSmartClient_FinishRecarga(object sender, FinishSmartServiceArgs<RecargaResponse> e)
        {
            IsTransaccionProcess = false;
            ExecuteSafelyDispatcher(() =>
                {
                    ContentPanelProgress.Visibility = System.Windows.Visibility.Collapsed;
                    continuarbtn.IsEnabled = true;
                });

            if (e.IsTimeOut)
            {
                ExecuteSafelyDispatcher(() =>
                {
                    //Aqui va ConsultaEstatusRecarga
                    ContentPanelProgress.Visibility = System.Windows.Visibility.Visible;
                    continuarbtn.IsEnabled = false;
                    IsTransaccionProcess = true;

                    wsSmartClient.SyncEstatusRecargaWSService
                    (
                        (int)CadenaEnum.STARBUCKS,
                        (int)TipoDispositivoEnum.WindowsMobile,
                        DatosRecarga.TarjetaCadena
                    );
                });


                return;
            }

            if (!e.Success)
            {
                Dispatcher.BeginInvoke(() => System.Windows.MessageBox.Show(e.Message));
                return;
            }

            string message = e.Data.Codigo == 0 ? "Tu Recarga se realizo correctamente." : e.Data.Descripcion;
            Dispatcher.BeginInvoke(() => System.Windows.MessageBox.Show(message));

            if (e.Data.Codigo == 0)
            {

                ExecuteSafelyDispatcher(() =>
                {
                    DatosRecarga.CVV = string.Empty;
                    (App.Current as App).DatosRecarga = DatosRecarga;
                    NavigationService.Navigate(new Uri("/Home.xaml?goto=3", UriKind.Relative));
                });
            }

        }

        protected void wsSmartClient_FinishEstatusRecarga(object sender, FinishSmartServiceArgs<EstatusRecargaResponse> e)
        {
            IsTransaccionProcess = false;
            ExecuteSafelyDispatcher(() =>
            {
                ContentPanelProgress.Visibility = System.Windows.Visibility.Collapsed;
                continuarbtn.IsEnabled = true;
            });

            if (e.IsTimeOut)
            {
                Dispatcher.BeginInvoke(() => System.Windows.MessageBox.Show("Se interrumpio la comunicación"));

                return;
            }

            if (!e.Success)
            {
                Dispatcher.BeginInvoke(() => System.Windows.MessageBox.Show(e.Message));
                return;
            }

            string message = e.Data.Descripcion;
            Dispatcher.BeginInvoke(() => System.Windows.MessageBox.Show(message));
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void wsSmartClient_FinishActualizaProgramaRecarga(object sender, FinishSmartServiceArgs<ActualizaProgramaRecargaResponse> e)
        {
            IsTransaccionProcess = false;
            ExecuteSafelyDispatcher(() =>
            {
                ContentPanelProgress.Visibility = System.Windows.Visibility.Collapsed;
                continuarbtn.IsEnabled = true;
            });

            if (e.IsTimeOut)
            {
                ExecuteSafelyDispatcher(() =>
                {
                    //Aqui va ConsultaEstatusRecarga
                    ContentPanelProgress.Visibility = System.Windows.Visibility.Visible;
                    continuarbtn.IsEnabled = false;
                    IsTransaccionProcess = true;

                    wsSmartClient.SyncEstatusRecargaWSService
                    (
                        (int)CadenaEnum.STARBUCKS,
                        (int)TipoDispositivoEnum.WindowsMobile,
                        DatosRecarga.TarjetaCadena
                    );
                });
               

                return;
            }

            if (!e.Success)
            {
                Dispatcher.BeginInvoke(() => System.Windows.MessageBox.Show(e.Message));
                return;
            }

            string message = e.Data.Codigo == 0 ? "Tu Recarga se realizo correctamente." : e.Data.Descripcion;
            Dispatcher.BeginInvoke(() => System.Windows.MessageBox.Show(message));

            if (e.Data.Codigo == 0)
            {
                ExecuteSafelyDispatcher(() =>
                {
                    DatosRecarga.CVV = string.Empty;
                    (App.Current as App).DatosRecarga = DatosRecarga;
                    NavigationService.Navigate(new Uri("/Home.xaml?goto=3", UriKind.Relative));
                });
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void continuarbtn_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            if (string.IsNullOrEmpty(DatosRecarga.CVV))
            {
                SolicitaCVV();
            }
            else
            {
                EnviaTransaccion();
            }

        }
        /// <summary>
        /// 
        /// </summary>
        private void SolicitaCVV()
        {
            ConfirmPassword confirm = new ConfirmPassword();
            confirm.setImageCVV(DatosRecarga.TipoTarjeta);
            CustomMessageBox messageBox = new CustomMessageBox()
            {
                //ContentTemplate = (DataTemplate)this.Resources["PivotContentTemplate"],
                Caption = "Aviso",
                Message = "Ingresa el código de verificación de tu tarjeta",
                Content = confirm,
                LeftButtonContent = "cancelar",
                RightButtonContent = "hacer recarga",
                IsFullScreen = false
            };
            messageBox.Dismissing += (o, eventArgs) =>
            {
                if (eventArgs.Result == CustomMessageBoxResult.RightButton)
                {
                    string cvvString = confirm.Password;
                    if (cvvString.Length == 3 || cvvString.Length == 4)
                    {
                        if (DatosRecarga.TipoTarjeta == "American Express" && cvvString.Length == 4)
                        {
                            DatosRecarga.CVV = cvvString;
                            eventArgs.Cancel = string.IsNullOrEmpty(DatosRecarga.CVV);
                            if (eventArgs.Cancel == false)
                            {
                                EnviaTransaccion();
                            }
                        }
                        else
                        {
                            if (cvvString.Length == 3 && DatosRecarga.TipoTarjeta == "MasterCard" || DatosRecarga.TipoTarjeta == "Visa")
                            {
                                DatosRecarga.CVV = cvvString;
                                eventArgs.Cancel = string.IsNullOrEmpty(DatosRecarga.CVV);
                                if (eventArgs.Cancel == false)
                                {
                                    EnviaTransaccion();
                                }
                            }
                            else {
                                System.Windows.MessageBox.Show("Por favor introduzca un codigo de seguridad válido");
                            }
                        }
                    }
                    else {
                        System.Windows.MessageBox.Show("Por favor introduzca un codigo de seguridad válido");
                    }
                }
            };
            messageBox.Show();
        }

        public bool IsTransaccionProcess { get; set; }

        protected override void OnBackKeyPress(System.ComponentModel.CancelEventArgs e)
        {
            e.Cancel = IsTransaccionProcess;
            if (e.Cancel)
            {
                System.Windows.MessageBox.Show("Por favor espere a que termine de procesar la transacción");
            }

            base.OnBackKeyPress(e);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="cvv"></param>
        private void EnviaTransaccion()
        {
            ContentPanelProgress.Visibility = System.Windows.Visibility.Visible;
            continuarbtn.IsEnabled = false;
            IsTransaccionProcess = true;
            
            string  CVV = DatosRecarga.CVV ;
            //DatosRecarga.CVV = string.Empty;

            if (DatosRecarga.IsRecargaAutomatica)
            {
                wsSmartClient.SyncActualizaProgramaRecargaWSService
                (
                    (int)CadenaEnum.STARBUCKS,
                    (int)TipoDispositivoEnum.WindowsMobile,
                    DatosRecarga.TipoTarjeta,
                    "7777076235647207",
                    (int)DatosRecarga.TipoRecarga,
                    "MXN",
                    DatosRecarga.MontoRecarga,
                    DatosRecarga.MontoRecargaAutomatica,
                    DatosRecarga.DiaRecarga,
                    DatosRecarga.Tarjeta,
                    string.Empty,
                    CVV,
                    DatosRecarga.FechaVigencia,
                    DatosRecarga.DatosCliente,
                    IsolatedStorageSettings.ApplicationSettings["PCHuella"].ToString()
                );
            }
            else
            {
                wsSmartClient.SyncRecargaWSService
                (
                    (int)CadenaEnum.STARBUCKS,
                    (int)TipoDispositivoEnum.WindowsMobile,
                    DatosRecarga.TipoTarjeta,
                    "7777076235647207",
                    "MXN",
                    DatosRecarga.MontoRecarga,
                    DatosRecarga.Tarjeta,
                    string.Empty,
                    DatosRecarga.CVV,
                    DatosRecarga.FechaVigencia,
                    DatosRecarga.DatosCliente,
                    IsolatedStorageSettings.ApplicationSettings["PCHuella"].ToString(),
                    DatosRecarga.GuardaTarjeta
                );
            }
           
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="action"></param>
        private void ExecuteSafelyDispatcher(Action action)
        {
            if (Deployment.Current.Dispatcher.CheckAccess())
            {
                action();
            }
            else
            {
                Deployment.Current.Dispatcher.BeginInvoke(() =>
                {
                    action();
                });
            }
        }
    }
}