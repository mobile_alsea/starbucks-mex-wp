﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using System.IO.IsolatedStorage;
using System.IO;
using System.Windows.Media.Imaging;
using Starbucks.Entities;
using Starbucks.Utilities;
using System.Text.RegularExpressions;
using System.Windows.Input;

namespace Starbucks.UI.View.Pagos
{
    public partial class Altas : PhoneApplicationPage
    {
        IsolatedStorageFile myFile = IsolatedStorageFile.GetUserStoreForApplication();

        int validarTarjeta = 0;
        int validarAno = 0;
        int validarMes = 0;
        string tipoTarjeta;
        string mes;
        int ano;
        int[] anoarreglo; 
        string _enteredPasscode = "";
        string _passwordChar = "*";

        /// <summary>
        /// 
        /// </summary>
        public DatosRecarga DatosRecarga { get; set; }

        public Altas()
        {
            InitializeComponent();
            DatosRecarga = (App.Current as App).DatosRecarga;


            numertotxt.MaxLength = 16;
            seguridadtxt.MaxLength = 4;
            anoarreglo = new int[51];
            tarjetalist.SetValue(Microsoft.Phone.Controls.ListPicker.ItemCountThresholdProperty, 0);
            llenarmeses();
            llenaranos();
            llenartarjeta();
        }

        public class Cantidad
        {
            public string Numero
            {
                get;
                set;
            }
        }

        public void guardarInformacion()
        {
            (App.Current as App).nombre = nombretxt.Text;
            (App.Current as App).numeroTarjeta = numertotxt.Text;
            (App.Current as App).tipoTarjeta = tipoTarjeta;
            (App.Current as App).codigoSeguridad = seguridadtxt.Text;
            (App.Current as App).anoTajerta = ano;
            (App.Current as App).mesTarjeta = mes;
        }

        public void llenartarjeta()
        {
            List<Cantidad> source = new List<Cantidad>();
            source.Add(new Cantidad() { Numero = "Selecciona tu tarjeta" });
            source.Add(new Cantidad() { Numero = "MASTERCARD" });
            source.Add(new Cantidad() { Numero = "VISA" });
            source.Add(new Cantidad() { Numero = "AMERICAN EXPRESS" });
            tarjetalist.ItemsSource = source;
        }

        public void llenarmeses()
        {
            List<Cantidad> source = new List<Cantidad>();
            source.Add(new Cantidad() { Numero = "MM" });
            source.Add(new Cantidad() { Numero = "01" });
            source.Add(new Cantidad() { Numero = "02" });
            source.Add(new Cantidad() { Numero = "03" });
            source.Add(new Cantidad() { Numero = "04" });
            source.Add(new Cantidad() { Numero = "05" });
            source.Add(new Cantidad() { Numero = "06" });
            source.Add(new Cantidad() { Numero = "07" });
            source.Add(new Cantidad() { Numero = "08" });
            source.Add(new Cantidad() { Numero = "09" });
            source.Add(new Cantidad() { Numero = "10" });
            source.Add(new Cantidad() { Numero = "11" });
            source.Add(new Cantidad() { Numero = "12" });
            meslist.ItemsSource = source;
        }

        public void llenaranos()
        {
            List<Cantidad> source = new List<Cantidad>();
            source.Add(new Cantidad() { Numero = "AAAA" });

            int anio = DateTime.Now.Year;

            for (int i = 0; i <= 50; i++)
            {
                source.Add(new Cantidad() { Numero = "" + (anio + i) });
                anoarreglo[i] = anio + i;
            }
            anoslist.ItemsSource = source;
        }

        private void continuarbtn_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            
            try
            {
                IsValidRequired(nombretxt.Text.Length < 5, "Introduzca un nombre válido");
                IsValidRequired(numertotxt.Text.Length != 19 && numertotxt.Text.Length != 18, "Introduzca un número de tarjeta válido");
                IsValidRequired(_enteredPasscode.Length == 0 || _enteredPasscode.Length > 5, "Introduzca un codigo de seguridad válido");

                IsValidRequired(validarAno == 0, "Selecciona un año");
                IsValidRequired(validarMes == 0, "Selecciona un mes");
                IsValidRequired(validarTarjeta == 0, "Selecciona un tipo de tarjeta");

                var tarjetadigitos = numertotxt.Text.Substring(numertotxt.Text.Length - 4);

                DatosRecarga.NombreTarjeta = nombretxt.Text;
                DatosRecarga.Tarjeta = numertotxt.Text.Replace(" ", String.Empty);
                DatosRecarga.CVV = _enteredPasscode;
                DatosRecarga.ValidaAnio = validarAno;
                DatosRecarga.ValidaMes = validarMes;
                DatosRecarga.FechaVigencia = string.Format("{0}/{1}", mes, ano);
                DatosRecarga.TipoTarjeta = tipoTarjeta;
                DatosRecarga.DigitosTarjeta = tarjetadigitos;
                DatosRecarga.GuardaTarjeta = switchRecarga.IsChecked.Value;
                
                (App.Current as App).DatosRecarga = DatosRecarga;
                NavigationService.Navigate(new Uri("/View/Pagos/Transaccion.xaml", UriKind.Relative));
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="expresion"></param>
        /// <param name="message"></param>
        private void IsValidRequired(bool expresion, string message)
        {
            if (expresion)
            {
                throw new MissingFieldException(message);
            }
        }

        private void numerotxt_TextChanged(object sender, KeyEventArgs e)
        {
            string numero = GetNewCardNumber(numertotxt.Text, e);
            if (numero.Length <= numertotxt.MaxLength)
            {
                numertotxt.Text = numero;
                numertotxt.SelectionStart = numertotxt.Text.Length;
            }
        }

        private string GetNewCardNumber(string oldPasscode, KeyEventArgs keyEventArgs)
        {
            string newPasscode = string.Empty;
            switch (keyEventArgs.Key)
            {
                case Key.D0:
                case Key.D1:
                case Key.D2:
                case Key.D3:
                case Key.D4:
                case Key.D5:
                case Key.D6:
                case Key.D7:
                case Key.D8:
                case Key.D9:
                    if (oldPasscode.Length < numertotxt.MaxLength)
                    {
                        if (oldPasscode.Length == 4 || oldPasscode.Length == 9 || oldPasscode.Length == 14)
                        {
                            newPasscode = oldPasscode + " ";
                        }
                        else{
                            newPasscode = oldPasscode;
                        }
                    }
                    else
                    {
                        newPasscode = oldPasscode;
                    }
                    break;
                case Key.Back:
                    if (oldPasscode.Length > 0)
                    {
                        if (oldPasscode.Length == 4 || oldPasscode.Length == 9 || oldPasscode.Length == 14)
                        {
                            newPasscode = oldPasscode.Remove(oldPasscode.Length - 1);
                        }
                        else
                        {
                            newPasscode = oldPasscode;
                        }
                    }
                        //oldPasscode.Substring(0, oldPasscode.Length - 1);
                    break;
                default:
                    //others
                    newPasscode = oldPasscode;
                    break;
            }
            return newPasscode;
        }

        private void PasswordTextBox_KeyUp(object sender, KeyEventArgs e)
        {
            //modify new passcode according to entered key
            _enteredPasscode = GetNewPasscode(_enteredPasscode, e);
            if (_enteredPasscode.Length <= seguridadtxt.MaxLength) {
                
                //replace text by *
                seguridadtxt.Text = Regex.Replace(_enteredPasscode, @".", _passwordChar);

                //take cursor to end of string
                seguridadtxt.SelectionStart = seguridadtxt.Text.Length;
            }
            
        }

        private string GetNewPasscode(string oldPasscode, KeyEventArgs keyEventArgs)
        {
            string newPasscode = string.Empty;
            switch (keyEventArgs.Key)
            {
                case Key.D0:
                case Key.D1:
                case Key.D2:
                case Key.D3:
                case Key.D4:
                case Key.D5:
                case Key.D6:
                case Key.D7:
                case Key.D8:
                case Key.D9:
                    if (oldPasscode.Length < seguridadtxt.MaxLength)
                    {
                        newPasscode = oldPasscode + (keyEventArgs.PlatformKeyCode - 48);
                    }
                    else
                    {
                        newPasscode = oldPasscode;
                    }
                    break;
                case Key.Back:
                    if (oldPasscode.Length > 0)
                        newPasscode = oldPasscode.Remove(oldPasscode.Length - 1);//oldPasscode.Substring(0, oldPasscode.Length - 1);
                    break;
                default:
                    //others
                    newPasscode = oldPasscode;
                    break;
            }
            return newPasscode;
        }

        private void tarjetalist_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                int s = tarjetalist.SelectedIndex;
                switch (s)
                {
                    case 0: validarTarjeta = 0;
                        break;
                    case 1: validarTarjeta = 1;
                        tipoTarjeta = TipoBancoUtil.MASTERCARD;
                        BitmapImage image = new BitmapImage(new Uri("/Resources/cvv.png", UriKind.Relative));
                        cvvimage.Source = image;
                        numertotxt.MaxLength = 16+3;
                        seguridadtxt.MaxLength = 3;
                        break;
                    case 2: validarTarjeta = 1;
                        tipoTarjeta = TipoBancoUtil.VISA;
                        BitmapImage image2 = new BitmapImage(new Uri("/Resources/cvv.png", UriKind.Relative));
                        cvvimage.Source = image2;
                        numertotxt.MaxLength = 16+3;
                        seguridadtxt.MaxLength = 3;
                        break;
                    case 3: validarTarjeta = 1;
                        tipoTarjeta = TipoBancoUtil.AMEX;
                        BitmapImage image3 = new BitmapImage(new Uri("/Resources/cvvamex.png", UriKind.Relative));
                        cvvimage.Source = image3;
                        numertotxt.MaxLength = 15+3;
                        seguridadtxt.MaxLength = 4;
                        break;
                }
            }
            catch
            {
            }
        }

        private void meslist_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            int s = meslist.SelectedIndex;
            if (s == 0)
            {
                validarMes = 0;
            }
            else
            {
                validarMes = 1;
                if (s < 11) mes = "0" + s;
                else mes = "" + s;
            }
        }

        private void anoslist_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            int s = anoslist.SelectedIndex;
            if (s == 0)
            {
                validarAno = 0;
            }
            else
            {
                validarAno = 1;
                string anioString = anoarreglo[anoslist.SelectedIndex - 1].ToString();
                int anio2digitos = Convert.ToInt32(anioString.Substring(2,2));
                ano = anio2digitos;
            }
        }

        private void guardar(string sFile)
        {
            string sMSG = "0";
            string sMSG2 = "0";
            if (switchRecarga.IsChecked == true)
            {
                sMSG = "" + (App.Current as App).numeroTarjeta[12] + (App.Current as App).numeroTarjeta[13] + (App.Current as App).numeroTarjeta[14] + (App.Current as App).numeroTarjeta[15];
                sMSG2 = (App.Current as App).tipoTarjeta;
                StreamWriter sw = new StreamWriter(new IsolatedStorageFileStream(sFile, FileMode.Create, myFile));
                sw.WriteLine(sMSG); //Wrting to the file
                sw.Close();
                StreamWriter sw2 = new StreamWriter(new IsolatedStorageFileStream(sFile, FileMode.Append, myFile));
                sw2.WriteLine(sMSG2); //Wrting to the file
                sw2.Close();
            }
            else
            {
                StreamWriter sw = new StreamWriter(new IsolatedStorageFileStream(sFile, FileMode.Create, myFile));
                sw.WriteLine(sMSG); //Wrting to the file
                sw.Close();
            }
        }

        private void numertotxt_GotFocus(object sender, RoutedEventArgs e)
        {
            pagopanel.Margin = new Thickness(25, -237, 0, 0);
        }

        private void numertotxt_LostFocus(object sender, RoutedEventArgs e)
        {
            pagopanel.Margin = new Thickness(25, 0, 0, 0);
        }

        private void seguridadtxt_GotFocus(object sender, RoutedEventArgs e)
        {
            pagopanel.Margin = new Thickness(25, -237, 0, 0);
        }

        private void seguridadtxt_LostFocus(object sender, RoutedEventArgs e)
        {
            pagopanel.Margin = new Thickness(25, 0, 0, 0);
        }

    }
}