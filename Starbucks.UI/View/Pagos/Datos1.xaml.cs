﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using JeffWilcox.Utilities.Silverlight;
using System.Text;
using System.Text.RegularExpressions;

namespace Starbucks.UI.View.Pagos
{
    public partial class Datos1 : PhoneApplicationPage
    {
        string idPreregistro;
        string hashContrasenia;

        public Datos1()
        {
            InitializeComponent();
        }

        protected override void OnNavigatedTo(System.Windows.Navigation.NavigationEventArgs e)
        {

           idPreregistro = NavigationContext.QueryString["idPreregistro"];
        }

        private void continuarbtn_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            if (nombretxt.Text == "" || apellidotxt.Text == "" || correotxt.Text == "")
            {
                MessageBox.Show("Es necesario llenar todos los campos");
                return;
            }
            if (txtPassword.Password == "" && txtPasswordConfirmar.Password == "")
            {
                MessageBox.Show("Escriba su contraseña");
                return;
            }
            if (txtPassword.Password != txtPasswordConfirmar.Password)
            {
                MessageBox.Show("Las contraseñas no coinciden");
                return;
            }

            if (!IsValidEmail(correotxt.Text))
            {
                MessageBox.Show("El correo tiene un formato invalido.");
                return;
            }

            hashContrasenia = MD5CryptoServiceProvider.GetMd5String(txtPassword.Password);
            var date = fechapicker.Value.Value.Date;
            string fecha = date.ToShortDateString();
            NavigationService.Navigate(new Uri("/View/Pagos/Datos2.xaml?idPreregistro=" + idPreregistro + "&nombre=" + nombretxt.Text + "&apellidos=" + apellidotxt.Text + "&email=" + correotxt.Text + "&contrasenia=" + hashContrasenia + "&fechaNacimiento=" + fecha + "", UriKind.Relative));
        }

        private bool IsValidEmail(string strIn)
        {
            return Regex.IsMatch(strIn,
                   @"^(?("")("".+?""@)|(([0-9a-zA-Z]((\.(?!\.))|[-!#\$%&'\*\+/=\?\^`\{\}\|~\w])*)(?<=[0-9a-zA-Z])@))" +
                   @"(?(\[)(\[(\d{1,3}\.){3}\d{1,3}\])|(([0-9a-zA-Z][-\w]*[0-9a-zA-Z]\.)+[a-zA-Z]{2,6}))$");
        }

        private void txtWaterPassword_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            txtPassword.Visibility = System.Windows.Visibility.Visible;
            txtWaterPassword.Visibility = System.Windows.Visibility.Collapsed;
            txtPassword.Focus();
        }

        private void txtWaterPassword_GotFocus(object sender, RoutedEventArgs e)
        {
            txtPassword.Visibility = System.Windows.Visibility.Visible;
            txtWaterPassword.Visibility = System.Windows.Visibility.Collapsed;
            txtPassword.Focus();
        }

        private void txtPassword_LostFocus(object sender, RoutedEventArgs e)
        {
            if (string.IsNullOrEmpty(txtPassword.Password))
            {
                txtPassword.Visibility = System.Windows.Visibility.Collapsed;
                txtWaterPassword.Visibility = System.Windows.Visibility.Visible;
            }
            string pass = txtPassword.Password;
        }

        private void txtWaterPasswordConfirmar_GotFocus(object sender, RoutedEventArgs e)
        {
            txtPasswordConfirmar.Visibility = System.Windows.Visibility.Visible;
            txtWaterPasswordConfirmar.Visibility = System.Windows.Visibility.Collapsed;
            txtPasswordConfirmar.Focus();
        }

        private void txtWaterPasswordConfirmar_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            txtPasswordConfirmar.Visibility = System.Windows.Visibility.Visible;
            txtWaterPasswordConfirmar.Visibility = System.Windows.Visibility.Collapsed;
            txtPasswordConfirmar.Focus();
        }

        private void txtPasswordConfirmar_LostFocus(object sender, RoutedEventArgs e)
        {
            if (string.IsNullOrEmpty(txtPasswordConfirmar.Password))
            {
                txtPasswordConfirmar.Visibility = System.Windows.Visibility.Collapsed;
                txtWaterPasswordConfirmar.Visibility = System.Windows.Visibility.Visible;
            }
            string pass = txtPasswordConfirmar.Password;
        }
    }
}