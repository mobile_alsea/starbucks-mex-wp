﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;

namespace Starbucks.UI.View
{
    public partial class Template : PhoneApplicationPage
    {
        public Template()
        {
            InitializeComponent();
        }

        private void txtPassword_KeyDown(object sender, System.Windows.Input.KeyEventArgs e)
        {
            string pass = txtPassword.Password;
        }

        private void txtPassword_LostFocus(object sender, RoutedEventArgs e)
        {
            if (string.IsNullOrEmpty(txtPassword.Password))
            {
                txtPassword.Visibility = System.Windows.Visibility.Collapsed;
                txtWaterPassword.Visibility = System.Windows.Visibility.Visible;
            }
        }

        private void txtPassword_GotFocus(object sender, RoutedEventArgs e)
        {
            txtPassword.Visibility = System.Windows.Visibility.Visible;
            txtWaterPassword.Visibility = System.Windows.Visibility.Collapsed;
            txtPassword.Focus();
        }

        private void txtWaterPassword_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            txtPassword.Visibility = System.Windows.Visibility.Visible;
            txtWaterPassword.Visibility = System.Windows.Visibility.Collapsed;
            txtPassword.Focus();
        }

        private void txtWaterPassword_GotFocus(object sender, RoutedEventArgs e)
        {
            txtPassword.Visibility = System.Windows.Visibility.Visible;
            txtWaterPassword.Visibility = System.Windows.Visibility.Collapsed;
            txtPassword.Focus();
        }
    }
}