﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using Microsoft.Phone.Tasks;
using Starbucks.UI.Helper;

namespace Starbucks.UI.View.Configuracion
{
    public partial class AcercaDe : PhoneApplicationPage
    {
        public AcercaDe()
        {
            InitializeComponent();
        }

        private void btnAvisoPrivacidad_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            var webBrowserTask = new WebBrowserTask();
            webBrowserTask.Uri = new Uri(StarbucksResource.Url_AvisoPrivacidad, UriKind.Absolute);
            webBrowserTask.Show();
        }

        private void btnTerminosCondiciones_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            var webBrowserTask = new WebBrowserTask();
            webBrowserTask.Uri = new Uri(StarbucksResource.Url_TerminosCondiciones, UriKind.Absolute);
            webBrowserTask.Show();
        }

        private void TextBlock_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            //PhoneCallTask task = new PhoneCallTask();
            //task.PhoneNumber = "018002880888";
            //task.DisplayName = "Startbucks Coffee";
            //task.Show();
        }

        private void Image_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            PhoneCallTask task = new PhoneCallTask();
            task.PhoneNumber = "018002880888";
            task.DisplayName = "Startbucks Coffee";
            task.Show();
        }

        private void TextBlock_Tap_1(object sender, System.Windows.Input.GestureEventArgs e)
        {
            //var x = new EmailComposeTask()
            //{
            //    To = "tuopinion@starbucks.com.mx",
            //    Body = "Tu opinión es importante para nosotros y por eso queremos escuchar todas tus recomendaciones y sugerencias para que esta aplicación sea mejor. Atentamente",
            //    Subject = "Comentarios Starbucks Windows Phone"
            //};
            //x.Show();
        }

        private void Image_Tap_1(object sender, System.Windows.Input.GestureEventArgs e)
        {
            var x = new EmailComposeTask()
            {
                To = "tuopinion@starbucks.com.mx",
                Body = "Tu opinión es importante para nosotros y por eso queremos escuchar todas tus recomendaciones y sugerencias para que esta aplicación sea mejor. Atentamente",
                Subject = "Comentarios Starbucks Windows Phone"
            };
            x.Show();
        }

        private void Button_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            PhoneCallTask task = new PhoneCallTask();
            task.PhoneNumber = "018002880888";
            task.DisplayName = "Startbucks Coffee";
            task.Show();
        }

        private void PhoneApplicationPage_Loaded(object sender, RoutedEventArgs e)
        {

        }

        private void btnMail_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            var x = new EmailComposeTask()
            {
                To = "tuopinion@starbucks.com.mx",
                Body = "Tu opinión es importante para nosotros y por eso queremos escuchar todas tus recomendaciones y sugerencias para que esta aplicación sea mejor. Atentamente",
                Subject = "Comentarios Starbucks Windows Phone"
            };
            x.Show();
        }
    }
}