﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using Starbucks.UI.Helper;
using System.IO.IsolatedStorage;
using System.IO;

namespace Starbucks.UI.View.Configuracion
{
    public partial class Configura : PhoneApplicationPage
    {
        private int intChangePin;
        private int intEliminaPin;
        IsolatedStorageFile myFile = IsolatedStorageFile.GetUserStoreForApplication();

        public Configura()
        {
            InitializeComponent();
            LoadData("Sesion.txt");
            intChangePin = 0;
            intEliminaPin = 0;
            if (App.Settings.Contains("Facebook"))
            {
                var isFacebook = Convert.ToBoolean(App.Settings["Facebook"].ToString());
                if (isFacebook)
                    switchFaceBook.IsChecked = true;
                else
                    switchFaceBook.IsChecked = false;
            }

            if (App.Settings.Contains("Twitter"))
            {
                var isTwitter = Convert.ToBoolean(App.Settings["Twitter"].ToString());
                if (isTwitter)
                    switchTwitter.IsChecked = true;
                else
                    switchTwitter.IsChecked = false;
            }

            if (App.Settings.Contains("GPS"))
            {
                var isGPS = Convert.ToBoolean(App.Settings["GPS"].ToString());
                if (isGPS)
                    switchGPS.IsChecked = true;
                else
                    switchGPS.IsChecked = false;
            }
        }

        private void btnLocalizaTiendas_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            NavigationService.Navigate(new Uri(StarbucksResource.View_LocalizaTiendas, UriKind.Relative));
        }

        private void btnCodigoAcceso_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            //NavigationService.Navigate(new Uri(StarbucksResource.View_CodigoAcceso, UriKind.Relative));
            LayoutRoot2.Visibility = Visibility.Visible;
        }

        private void btnCambiaCodigoAcceso_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            intChangePin++;
            ProteccionC.Visibility = System.Windows.Visibility.Visible;
        }

        private void btnEliminaCodigoAcceso_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            intEliminaPin++;
            ProteccionC.Visibility = System.Windows.Visibility.Visible;
        }

        private void switchFaceBook_Checked(object sender, RoutedEventArgs e)
        {
            switchFaceBook.Content = "On";
            App.Settings["Facebook"] = true;
            App.Settings.Save();
        }

        private void switchFaceBook_Unchecked(object sender, RoutedEventArgs e)
        {
            switchFaceBook.Content = "Off";
            App.Settings["Facebook"] = false;
            App.Settings.Remove("AccessToken");
            App.Settings.Save();
        }

        private void switchTwitter_Checked(object sender, RoutedEventArgs e)
        {
            switchTwitter.Content = "On";
            App.Settings["Twitter"] = true;
            App.Settings.Save();
        }

        private void switchTwitter_Unchecked(object sender, RoutedEventArgs e)
        {
            switchTwitter.Content = "Off";
            App.Settings["Twitter"] = false;
            App.Settings.Save();
        }

        private void switchGPS_Checked(object sender, RoutedEventArgs e)
        {
            switchGPS.Content = "On";
            App.Settings["GPS"] = true;
            App.Settings.Save();
        }

        private void switchGPS_Unchecked(object sender, RoutedEventArgs e)
        {
            switchGPS.Content = "Off";
            App.Settings["GPS"] = false;
            App.Settings.Save();
        }

        private void btnSalir_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            cerrarSesion();
        }

        private void cerrarSesion()
        {

            MessageBoxResult m = MessageBox.Show("¿Estás seguro que quieres cerrar la sesión de tu cuenta de Starbucks Rewards?", "", MessageBoxButton.OKCancel);
            if (m == MessageBoxResult.OK)
            {
                guardarCerrarSesion("Sesion.txt");
            }
        }

        private void guardarCerrarSesion(string sFile)
        {
            btnCerrarSesion.Visibility = System.Windows.Visibility.Collapsed;
            string sMSG = "";
            if (sFile == "Sesion.txt") sMSG = "sesioncerrada";
            StreamWriter sw = new StreamWriter(new IsolatedStorageFileStream(sFile, FileMode.Create, myFile));
            sw.WriteLine(sMSG); //Wrting to the file
            sw.Close();
        }    

        public void LoadData(string sFile)
        {
            //Existe PIN
            if (App.Settings.Contains("CodigoAcceso"))
            {
                CambiarPIN.Visibility = System.Windows.Visibility.Visible;
                EliminarPIN.Visibility = System.Windows.Visibility.Visible;
                CrearPIN.Visibility = System.Windows.Visibility.Collapsed;
            }
            else {
                CambiarPIN.Visibility = System.Windows.Visibility.Collapsed;
                EliminarPIN.Visibility = System.Windows.Visibility.Collapsed;
                CrearPIN.Visibility = System.Windows.Visibility.Visible;
            }
            //myFile.DeleteFile(sFile);
            if (!myFile.FileExists(sFile))
            {
                IsolatedStorageFileStream dataFile = myFile.CreateFile(sFile);
                dataFile.Close();
            }
            else
            {
                //Reading and loading data
                StreamReader reader = new StreamReader(new IsolatedStorageFileStream(sFile, FileMode.Open, myFile));
                string rawData = reader.ReadToEnd();
                reader.Close();

                string[] sep = new string[] { "\r\n" }; //Splittng it with new line
                string[] arrData = rawData.Split(sep, StringSplitOptions.RemoveEmptyEntries);
                try
                {
                    if (arrData[0] == "sesionabierta")
                    {
                        btnCerrarSesion.Visibility = System.Windows.Visibility.Visible;
                    }
                    else
                    {
                        btnCerrarSesion.Visibility = System.Windows.Visibility.Collapsed;
                    }
                }
                catch
                {
                }
            }
        }

        private void TextBox_GotFocus(object sender, System.Windows.RoutedEventArgs e)
        {
            ApplicationBar.IsVisible = true;
        }

        private void TextBox_LostFocus(object sender, System.Windows.RoutedEventArgs e)
        {
            ApplicationBar.IsVisible = false;
        }

        private void ApbAceptar_Click(object sender, EventArgs e)
        {
            this.Focus();
            var codigo = App.Settings["CodigoAcceso"].ToString();
            if (codigo != null)
            {
                if (codigo.Equals(txtCodigoC.Password))
                {
                    ProteccionC.Visibility = Visibility.Collapsed;
                    txtCodigoC.Password = "";
                    //YLP
                    if (intChangePin == 1)
                    {
                        intChangePin = 0;
                        LayoutRoot2.Visibility = Visibility.Visible;
                    }
                    if (intEliminaPin == 1)
                    {
                        intEliminaPin = 0;
                        App.Settings.Remove("CodigoAcceso");
                        MessageBox.Show("Código eliminado.");
                        LoadData("Sesion.txt");
                    }
                    //END
                }
                else
                    System.Windows.MessageBox.Show("Código incorrecto.");
            }
        }

        private void Tap_GuardarPIN(object sender, System.Windows.Input.GestureEventArgs e)
        {
            if (VerificaCodigo())
            {
                if (txtCodigo.Password == "")
                {
                    MessageBox.Show("Debe capturar el código.");
                    return;
                }
                App.Settings["CodigoAcceso"] = txtCodigo.Password;
                LimpiaCampos();
                this.Focus();
                MessageBox.Show("Código guardado.");
                LayoutRoot2.Visibility = Visibility.Collapsed;
                LoadData("Sesion.txt");
            }
            else
                MessageBox.Show("Los códigos no coinciden.");

            
        }

        private void LimpiaCampos()
        {
            txtCodigo.Password = string.Empty;
            txtRepetir.Password = string.Empty;
        }

        private bool VerificaCodigo()
        {
            return txtCodigo.Password.Equals(txtRepetir.Password) ? true : false;
        }

        private void Tap_CancelaPIN(object sender, System.Windows.Input.GestureEventArgs e)
        {
            LayoutRoot2.Visibility = Visibility.Collapsed;
        }

        private void txtCodigo_KeyUp(object sender, System.Windows.Input.KeyEventArgs e)
        {
            if (e.Key == System.Windows.Input.Key.Enter)
                txtRepetir.Focus();
        }

        private void txtRepetir_KeyUp(object sender, System.Windows.Input.KeyEventArgs e)
        {
            if (e.Key == System.Windows.Input.Key.Enter)
                this.Focus();
        }
    }
}