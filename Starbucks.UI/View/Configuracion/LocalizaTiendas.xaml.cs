﻿using System;
using System.Collections.Generic;
using System.IO.IsolatedStorage;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Controls.Primitives;
using Microsoft.Phone.Shell;
using Starbucks.UI.Helper;
using Starbucks.UI.Helper.Looping;

namespace Starbucks.UI.View.Configuracion
{
    public partial class LocalizaTiendas : PhoneApplicationPage
    {
        private string resultado = string.Empty;
        private ListLoopingDataSource<string> selectorSource;

        public LocalizaTiendas()
        {
            InitializeComponent();
            var seleccion = string.Empty;
            if (App.Settings.Contains(Constantes.LocalizaTiendas))
                seleccion = App.Settings[Constantes.LocalizaTiendas].ToString().Equals(string.Empty) ? "NO" : App.Settings[Constantes.LocalizaTiendas].ToString();
            selectorSource = new ListLoopingDataSource<string> { Items = App.ViewModel.ListKilometros(), SelectedItem = seleccion };
            this.selector.DataSource = selectorSource;
            selectorSource.SelectionChanged +=selectorSource_SelectionChanged;
        }

        void selectorSource_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var source = sender as ListLoopingDataSource<string>;
            resultado = source.SelectedItem.ToString();
        }

        private void ApbGuardar_Click(object sender, EventArgs e)
        {
            GuardaDistancia(resultado);
        }

        private void GuardaDistancia(string value)
        {
            if (value.Equals("NO"))
                App.Settings[Constantes.LocalizaTiendas] = string.Empty;            
            else
                App.Settings[Constantes.LocalizaTiendas] = value;
            MessageBox.Show("Se guardo correctamente.");
        }
    }
}