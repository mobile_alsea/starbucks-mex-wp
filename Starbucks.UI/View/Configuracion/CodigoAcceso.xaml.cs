﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;

namespace Starbucks.UI.View.Configuracion
{
    public partial class CodigoAcceso : PhoneApplicationPage
    {
        public CodigoAcceso()
        {
            InitializeComponent();
        }

        private void ApbGuarda_Click(object sender, EventArgs e)
        {
            if (VerificaCodigo())
            {
                if (txtCodigo.Password == "")
                {
                    MessageBox.Show("Debe capturar el código.");
                    return;
                }
                App.Settings["CodigoAcceso"] = txtCodigo.Password;
                LimpiaCampos();
                this.Focus();
                MessageBox.Show("Código guardado.");
                NavigationService.GoBack();
            }
            else
                MessageBox.Show("Los códigos no coinciden.");
        }

        private void LimpiaCampos()
        {
            txtCodigo.Password = string.Empty;
            txtRepetir.Password = string.Empty;
        }

        private bool VerificaCodigo()
        {
            return txtCodigo.Password.Equals(txtRepetir.Password) ? true : false;
        }

        private void ApbReset_Click(object sender, EventArgs e)
        {
            App.Settings.Remove("CodigoAcceso");
            MessageBox.Show("Código reseteado.");
        }

        private void txtCodigo_KeyUp(object sender, System.Windows.Input.KeyEventArgs e)
        {
            if (e.Key == System.Windows.Input.Key.Enter)
                txtRepetir.Focus();
        }

        private void txtRepetir_KeyUp(object sender, System.Windows.Input.KeyEventArgs e)
        {
            if (e.Key == System.Windows.Input.Key.Enter)
                this.Focus();
        }
    }
}