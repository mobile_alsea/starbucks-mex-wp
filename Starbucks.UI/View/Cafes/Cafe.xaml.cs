﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Microsoft.Phone.Controls;
using Starbucks.UI.Helper;

namespace Starbucks.UI.View.Cafes
{
    public partial class Cafe : PhoneApplicationPage
    {
        private bool IsLoader;
        private ListBox control;

        public Cafe()
        {
            InitializeComponent();
            Loaded += Cafe_Loaded;
        }

        void Cafe_Loaded(object sender, RoutedEventArgs e)
        {
            if (!IsLoader)
                CreaSecciones();
        }

        private void CreaSecciones()
        {
            var categorias = App.CafesViewModel.ObtenerPerfilesCafe();
            categorias.ForEach(c =>
            {
                var newPivotItem = new PivotItem();
                newPivotItem.Header = c;
                Grid newGrid = new Grid();
                var newListBox = new ListBox();
                newListBox.ItemTemplate = createDataTemplate();
                newListBox.ItemsSource = App.CafesViewModel.ObtenerCafe(c);
                newListBox.SelectionChanged += newListBox_SelectionChanged;
                newGrid.Children.Add(newListBox);
                newPivotItem.Content = newGrid;
                PivotMain.Items.Add(newPivotItem);
            });
            IsLoader = true;
        }

        void newListBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            control = sender as ListBox;
            if (control.SelectedIndex != -1 && control != null)
            {
                App.CafesViewModel.SelectedItemCafe = control.SelectedItem as Model.Cafe;
                NavigationService.Navigate(new Uri(StarbucksResource.View_DetalleCafes, UriKind.Relative));
                NavigationService.Navigated += NavigationService_Navigated;
            }
        }

        void NavigationService_Navigated(object sender, System.Windows.Navigation.NavigationEventArgs e)
        {
            NavigationService.Navigated -= NavigationService_Navigated;
            if (control != null)
                control.SelectedIndex = -1;
        }

        public DataTemplate createDataTemplate()
        { return (DataTemplate)XamlReader.Load(Constantes.TemplateCafes); }
    }
}