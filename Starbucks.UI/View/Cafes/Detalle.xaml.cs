﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using Starbucks.UI.Helper;

namespace Starbucks.UI.View.Cafes
{
    public partial class Detalle : PhoneApplicationPage
    {
        private bool IsVisible;
        private bool EstatusFavorito;

        public Detalle()
        {
            InitializeComponent();
            DataContext = App.CafesViewModel.SelectedItemCafe;
            Loaded += Detalle_Loaded;
        }

        void Detalle_Loaded(object sender, RoutedEventArgs e)
        {
            if (ApplicationBar == null)
                ApplicationBar = new ApplicationBar();
            VerificaFavorito();
            if (!EstatusFavorito)
            {
                var appBarFavorito = new ApplicationBarIconButton(new Uri("/Assets/AppBar/actionbar_icon_agregarfavoritos.png", UriKind.Relative)) { Text = "Añadir a cafes" };
                appBarFavorito.Click += appBarFavorito_Click;
                ApplicationBar.Buttons.Add(appBarFavorito);
            }
            else
            {
                var appBarEliminaFavorito = new ApplicationBarIconButton(new Uri("/Assets/AppBar/actionbar_icon_quitarfavoritos.png", UriKind.Relative)) { Text = "Eliminar de cafes" };
                appBarEliminaFavorito.Click += appBarEliminaFavorito_Click;
                ApplicationBar.Buttons.Add(appBarEliminaFavorito);
            }
        }

        void appBarEliminaFavorito_Click(object sender, EventArgs e)
        {
            App.FavoritosViewModel.EliminaFavorito<Model.Cafe>(App.CafesViewModel.SelectedItemCafe, Accion.Cafe);
            ApplicationBar.Buttons.Clear();
            var appBarFavorito = new ApplicationBarIconButton(new Uri("/Assets/AppBar/actionbar_icon_agregarfavoritos.png", UriKind.Relative)) { Text = "Añadir a cafes" };
            appBarFavorito.Click += appBarFavorito_Click;
            ApplicationBar.Buttons.Add(appBarFavorito);
        }

        void appBarFavorito_Click(object sender, EventArgs e)
        {
            App.FavoritosViewModel.AgregaFavorito<Model.Cafe>(App.CafesViewModel.SelectedItemCafe, Accion.Cafe);
            ApplicationBar.Buttons.Clear();
            var appBarEliminaFavorito = new ApplicationBarIconButton(new Uri("/Assets/AppBar/actionbar_icon_quitarfavoritos.png", UriKind.Relative)) { Text = "Eliminar de cafes" };
            appBarEliminaFavorito.Click += appBarEliminaFavorito_Click;
            ApplicationBar.Buttons.Add(appBarEliminaFavorito);
        }

        private void VerificaFavorito()
        {
            var cafe = App.CafesViewModel.SelectedItemCafe;
            if (cafe != null)
            {
                var item = App.FavoritosViewModel.FavoritoCafes.Where(c => c.nombre.Equals(cafe.nombre)).SingleOrDefault();
                if (item != null)
                    EstatusFavorito = true;
                else
                    EstatusFavorito = false;
            }
        }

        private void imgthumbnail_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            if (!IsVisible)
            {
                IsVisible = true;
                ApplicationBar.IsVisible = false;
                ContentImage.Visibility = Visibility.Visible;
            }
        }

        private void ContentImage_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            IsVisible = false;
            ApplicationBar.IsVisible = true;
            ContentImage.Visibility = Visibility.Collapsed;
        }
    }
}