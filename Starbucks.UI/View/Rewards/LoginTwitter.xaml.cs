﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Microsoft.Phone.Controls;

namespace Starbucks.UI.View.Rewards
{
    public partial class LoginTwitter : PhoneApplicationPage
    {
        private WebClient webClient;
        public LoginTwitter()
        {
            InitializeComponent();
            Loaded += LoginTwitter_Loaded;
            webClient = new WebClient();
            wbLogin.LoadCompleted += wbLogin_LoadCompleted;
            wbLogin.Navigated += new EventHandler<System.Windows.Navigation.NavigationEventArgs>(wbLogin_Navigated);
        }

        void wbLogin_Navigated(object sender, System.Windows.Navigation.NavigationEventArgs e)
        {
            string html = this.wbLogin.SaveToString();
            if (html.Contains("<SCRIPT type=text/javascript>"))
            {
                NavigationService.GoBack();
            }

        }
        void wbLogin_LoadCompleted(object sender, System.Windows.Navigation.NavigationEventArgs e)
        {
            //progressBrowser.Visibility = Visibility.Collapsed;
            //progressBrowser.IsEnabled = false;
            var html = this.wbLogin.SaveToString();
            if (html.Contains("ssuccess"))
            {
                NavigationService.GoBack();
            }

        }
        void webClient_DownloadStringCompleted(object sender, DownloadStringCompletedEventArgs args)
        {
            if (args.Error == null)
            {
                var res = args.Result;
                if (args.Result.Contains("succeess"))
                {
                    try
                    {
                        NavigationService.GoBack();
                    }
                    catch (Exception)
                    {

                        throw;
                    }
                    webClient.DownloadStringCompleted -= webClient_DownloadStringCompleted;
                }
                else
                {
                    //string url = "https://twitter.com/intent/session?return_to=%2Fintent%2Ftweet%3Fsource%3Dwebclient%26text%3DTitulo%2BMensaje&source=webclient&text=Titulo+Mensaje";
                    wbLogin.NavigateToString(res);
                    //wbLogin.Navigate(new Uri(url));
                }
            }

        }

        void LoginTwitter_Loaded(object sender, RoutedEventArgs e)
        {
            //string url = "https://twitter.com/intent/session?return_to=%2Fintent%2Ftweet%3Fsource%3Dwebclient%26text%3DTitulo%2BMensaje&source=webclient&text=Titulo+Mensaje";
            webClient.DownloadStringCompleted += webClient_DownloadStringCompleted;
            webClient.DownloadStringAsync(new Uri(Starbucks.UI.ViewModel.Principal.Current().itemTwitter.getParameters(), UriKind.Absolute));
            //webClient.Headers["User-Agent"] = "Mozilla/5.0 (iPhone; U; CPU like Mac OS X; en) AppleWebKit/420+ (KHTML, like Gecko) Version/3.0 Mobile/1A543 Safari/419.3";
        }
    }
}