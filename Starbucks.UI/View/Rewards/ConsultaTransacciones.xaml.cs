﻿using System;
using System.Collections.Generic;
using System.IO.IsolatedStorage;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using Starbucks.DAL;
using Starbucks.Entities;
using System.Windows.Data;

namespace Starbucks.UI.View.Rewards
{
    public class TextConverterStringFormat : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            double inputValue = Math.Round(double.Parse(value.ToString()), 2);
            return "$" + String.Format("{0:0.00}", inputValue);
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return value;
        }
    }

    public partial class ConsultaTransacciones : PhoneApplicationPage
    {
        WSRewardsViewModel cliente = new WSRewardsViewModel();
        public ConsultaTransacciones()
        {
            InitializeComponent();

            cliente.FinishConsultaTransaccionesPorTarjeta += FinishConsultaTransaccionesTarjeta;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="e"></param>
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {            
            SaldoText.Text = NavigationContext.QueryString["saldo"];
            FechaText.Text = NavigationContext.QueryString["fecha"].Replace("Actualizado el", "");
            string uritarjetalog = IsolatedStorageSettings.ApplicationSettings["uriTarjeta"].ToString();

            Uri uri = new Uri(uritarjetalog, UriKind.Absolute);
            tarjetaRewards.Source = new BitmapImage(uri);


            cliente.SyncConsultaTransaccionesPorTarjeta(Convert.ToInt64(IsolatedStorageSettings.ApplicationSettings["idTarjeta"]));
            base.OnNavigatedTo(e);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void FinishConsultaTransaccionesTarjeta(object sender, FinishSmartServiceArgs<ConsultaTransaccionesPorTarjetaResponse> e)
        {
            Dispatcher.BeginInvoke(() =>
           {
               ContentPanelProgress.Visibility = System.Windows.Visibility.Collapsed;

               if (!e.Success)
               {
                   MessageBox.Show(e.Message);
                   return;
               }
               List<Movimiento> movimientos = e.Data.Movimientos;
               MovimientosLongListSelector.ItemsSource = movimientos;
           });
        }


    }


}