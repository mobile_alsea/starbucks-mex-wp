﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using System.IO;
using System.Text;
using Starbucks.UI.Helper;

namespace Starbucks.UI.View.Rewards
{
    public partial class RecuperaPassword : PhoneApplicationPage
    {
        private string Usuario;
        private string Dominio;
        private string Tarjeta;
        private string Codigo;
        private event EventHandler LoadCodeCompled;

        public RecuperaPassword()
        {
            InitializeComponent();
            this.LoadCodeCompled += RecuperaPassword_LoadCodeCompled;
        }

        void RecuperaPassword_LoadCodeCompled(object sender, EventArgs e)
        {
            ApplicationBar.IsVisible = false;
            progress.Visibility = Visibility.Collapsed;
            txtPassword.Text = Codigo;
            lblEmail.Text = txtUsuario.Text;
            pnlInfo.Visibility = Visibility.Visible;
            txtPassword.Focus();
        }

        private void GetRequestStreamCallback(IAsyncResult asynchronousResult)
        {
            try
            {
                HttpWebRequest request = (HttpWebRequest)asynchronousResult.AsyncState;
                using (var postStream = request.EndGetRequestStream(asynchronousResult))
                {
                    string postData = string.Format("correoe={0}%40{1}&card={2}", Usuario, Dominio, Tarjeta);
                    byte[] byteArray = Encoding.UTF8.GetBytes(postData);
                    postStream.Write(byteArray, 0, postData.Length);
                    postStream.Close();
                }
                request.BeginGetResponse(new AsyncCallback(GetResponseCallback), request);
            }
            catch
            {
                IntenteloMasTarde();
            }
        }

        private void GetResponseCallback(IAsyncResult asynchronousResult)
        {
            try
            {
                HttpWebRequest request = (HttpWebRequest)asynchronousResult.AsyncState;
                HttpWebResponse response = (HttpWebResponse)request.EndGetResponse(asynchronousResult);
                var streamResponse = response.GetResponseStream();
                var streamRead = new StreamReader(streamResponse);
                string responseString = streamRead.ReadToEnd();
                if (ObtenCodigo(responseString))
                {
                    Dispatcher.BeginInvoke(() =>
                    {
                        if (LoadCodeCompled != null)
                            LoadCodeCompled(null, null);
                    });
                }
                else
                    Dispatcher.BeginInvoke(() => { progress.Visibility = Visibility.Collapsed; });
            }
            catch
            {
                IntenteloMasTarde();
            }
        }

        private bool ObtenCodigo(string cadena)
        {
            cadena = cadena.Replace("\t", string.Empty);
            cadena = cadena.Replace("\n", string.Empty);
            cadena = cadena.Replace("\r", string.Empty);
            try
            {
                var res = cadena.Substring(cadena.IndexOf("Nuevo password") + 23, 30);
                var codigo = res.Substring(0, res.IndexOf("</td>"));
                Codigo = codigo;
                return true;
            }
            catch
            {
                this.LoadCodeCompled -= RecuperaPassword_LoadCodeCompled;
                //El usuario no esta registrado en el programa
                if (cadena.Contains("El usuario no esta registrado en el programa"))
                    Dispatcher.BeginInvoke(() => MessageBox.Show("El usuario no esta registrado en el programa."));
                else
                    Dispatcher.BeginInvoke(() => MessageBox.Show("La tarjeta no coincide."));
                return false;
            }
        }

        private void ApbGenerarPassword_Click(object sender, EventArgs e)
        {
            Dispatcher.BeginInvoke(() => { this.Focus(); });

            if (string.IsNullOrEmpty(txtUsuario.Text) || string.IsNullOrEmpty(txtTarjeta.Text))
            {
                MessageBox.Show("Los campos son requeridos.");
                return;
            }

            if (!txtUsuario.Text.Contains('@'))
            {
                MessageBox.Show("El correo electrónico es invalido.");
                return;
            }

            if (!txtTarjeta.Text.Length.Equals(16))
            {
                MessageBox.Show("Número de tarjeta invalido.");
                return;
            }

            var infoEmail = txtUsuario.Text.Split('@');
            Usuario = infoEmail[0];
            Dominio = infoEmail[1];
            Tarjeta = txtTarjeta.Text;

            Dispatcher.BeginInvoke(() => { progress.Visibility = Visibility.Visible; });
            try
            {
                var httpWebRequest = (HttpWebRequest)WebRequest.Create(StarbucksResource.Liga_SolicitaPassword);
                httpWebRequest.ContentType = "application/x-www-form-urlencoded";
                httpWebRequest.Method = "POST";
                httpWebRequest.BeginGetRequestStream(new AsyncCallback(GetRequestStreamCallback), httpWebRequest);
            }
            catch
            {
                IntenteloMasTarde();
            }
        }

        private void IntenteloMasTarde()
        {
            Dispatcher.BeginInvoke(() =>
                {
                    progress.Visibility = Visibility.Collapsed;
                    MessageBox.Show("Intentelo más tarde");
                });
        }

        private void txtUsuario_KeyUp(object sender, System.Windows.Input.KeyEventArgs e)
        {
            if (e.Key == System.Windows.Input.Key.Enter)
                txtTarjeta.Focus();
        }
    }
}