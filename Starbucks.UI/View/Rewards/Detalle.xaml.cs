﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using Starbucks.UI.Controls;
using System.Collections;
using System.Windows.Threading;
using System.Windows.Interactivity;
using Spritehand.PhysicsBehaviors;
using Spritehand.FarseerHelper;

namespace Starbucks.UI.View.Rewards
{
    public partial class Detalle : PhoneApplicationPage
    {
        private ApplicationBarIconButton appBarBuscar;
        private int[] arr_x = new int[] { 260, 276, 292, 308, 324, 340 };
        private int[] arr_y = new int[] { 50, 85, 120, 155, 190 };
        private DispatcherTimer timer = null;
        int[] PosicionesEstrellas = new int[] { 80 };
        public PhysicsControllerMain _physicsController = null;
        int tmp = 0;
        int startCount;

        public Detalle()
        {
            InitializeComponent();
        }

        //Aqui modifico y seteo los datos
        private void PhoneApplicationPage_Loaded(object sender, RoutedEventArgs e)
        {
           // DataContext = (Model.Usuario)App.Settings["Logged"];
            //var tiene = ((Model.Usuario)App.Settings["Logged"]).Estrellas;
            //var faltantes = ((Model.Usuario)App.Settings["Logged"]).EstrellasFaltantes;
            //var suma = (tiene + faltantes);

            var tiene = 15;
            var faltantes = 5;
            var suma = (tiene + faltantes);

            progressDetalle.Maximum = suma;
            progressDetalle.Value = tiene;

            string set = "Green";

            //switch (((Model.Usuario)App.Settings["Logged"]).Nivel)
            switch (set)
            {
                case "Welcome":
                    progressDetalle.Foreground = new SolidColorBrush(new Color()
                    {
                        A = 255 /*Opacity*/,
                        R = 31 /*Red*/,
                        G = 27 /*Green*/,
                        B = 28 /*Blue*/
                    });
                    break;
                case "Green":
                    progressDetalle.Foreground = new SolidColorBrush(new Color()
                    {
                        A = 255 /*Opacity*/,
                        R = 113 /*Red*/,
                        G = 173 /*Green*/,
                        B = 42 /*Blue*/
                    });
                    break;
                case "Gold":
                    progressDetalle.Foreground = new SolidColorBrush(new Color()
                    {
                        A = 255 /*Opacity*/,
                        R = 173 /*Red*/,
                        G = 118 /*Green*/,
                        B = 0 /*Blue*/
                    });
                    break;
                default:
                    break;
            }

            _physicsController = containerStart.GetValue(PhysicsControllerMain.PhysicsControllerProperty) as PhysicsControllerMain;

            timer = new DispatcherTimer();
            timer.Interval = TimeSpan.FromSeconds(1);
            timer.Tick += timer_Tick;
            timer.Start();
        }

        void timer_Tick(object sender, EventArgs e)
        {
            int count = 0;
            string nivel = "Green";
            int tiene = 15;

            //if (App.RewardsViewModel.UsuarioActivo.Nivel.Equals("Green"))
            if (nivel.Equals("Green"))
            {
                //if (App.RewardsViewModel.UsuarioActivo.Estrellas > 10)
                    if (tiene > 10)
                    count = 10;
                else
                   // count = App.RewardsViewModel.UsuarioActivo.Estrellas;
                    count = tiene;
            }
            else
            {
                //if (App.RewardsViewModel.UsuarioActivo.Estrellas > 10)
                if (tiene > 10)
                    count = 10;
                else
                    //count = App.RewardsViewModel.UsuarioActivo.Estrellas;
                    count = tiene;
            }

            if (startCount < count)
            {
                if (tmp + 1 == count)
                {
                    tmp = 0;
                    var estrella = new Start();
                    int x = PosicionesEstrellas[0]; int y = 150;
                    Canvas.SetLeft(estrella, x);
                    Canvas.SetTop(estrella, y);
                    var newPos = new TranslateTransform();
                    newPos.X = x; newPos.Y = y;
                    estrella.RenderTransform = newPos;
                    AddPhysicsBody(estrella, false);
                    startCount++;
                    return;
                }
                var estrella1 = new Start();
                int x1 = PosicionesEstrellas[0]; int y1 = 0;
                Canvas.SetLeft(estrella1, x1);
                Canvas.SetTop(estrella1, y1);
                var newPos1 = new TranslateTransform();
                newPos1.X = x1; newPos1.Y = y1;
                estrella1.RenderTransform = newPos1;
                AddPhysicsBody(estrella1, false);
                startCount++;
                tmp++;
            }
            else
            {
                startCount = 0;
                timer.Stop();
                _physicsController.PhysicsObjects.Clear();

                //if (App.RewardsViewModel.UsuarioActivo.Nivel.Equals("Green"))
                    if (nivel.Equals("Green"))
                {
                   // if (App.RewardsViewModel.UsuarioActivo.Estrellas > 10)
                     if (tiene > 10)
                    {
                        var sb = this.Resources["sbStart"] as Storyboard;
                        sb.Begin();
                    }
                }
                else
                {
                  //  if (App.RewardsViewModel.UsuarioActivo.Estrellas > 10)
                    if (tiene > 10)
                    {
                        var sb = this.Resources["sbStart"] as Storyboard;
                        sb.Begin();
                    }
                }
            }
        }

        private void AddPhysicsBody(FrameworkElement element, bool isStatic)
        {
            var behaviorCollection = Interaction.GetBehaviors(element);
            behaviorCollection.Add(new PhysicsObjectBehavior()
            {
                IsStatic = isStatic
            });

            var physicsObject = element.GetValue(PhysicsObjectMain.PhysicsObjectProperty) as PhysicsObjectMain;
            _physicsController.AddPhysicsBody(physicsObject);
        }

        void appBarBuscar_Click(object sender, EventArgs e)
        {
            startCount = 0;
            timer.Stop();
            _physicsController.PhysicsObjects.Clear();
            NavigationService.Navigate(new Uri("/View/Rewards/Beneficios.xaml", UriKind.Relative));
        }

        private void PVRewards_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (PVRewards.SelectedIndex == 1)
            {
                ApplicationBar = null;

                if (ApplicationBar == null)
                {
                    ApplicationBar = new ApplicationBar();
                    ApplicationBar.BackgroundColor = Color.FromArgb(Convert.ToByte("FF", 16), Convert.ToByte("14", 16), Convert.ToByte("49", 16), Convert.ToByte("0F", 16));//"#FF14490F"
                }
                appBarBuscar = new ApplicationBarIconButton(new Uri("/Assets/AppBar/actionbar_icon_editarbebidas.png", UriKind.Relative)) { Text = "Beneficios" };
                appBarBuscar.Click += new EventHandler(appBarBuscar_Click);
                ApplicationBar.Buttons.Add(appBarBuscar);

            }
            else
                ApplicationBar = null;
        }

        protected override void OnBackKeyPress(System.ComponentModel.CancelEventArgs e)
        {
            startCount = 0;
            timer.Stop();
            _physicsController.PhysicsObjects.Clear();
            if (App.RewardsViewModel.IsBeneficios)
            {
                NavigationService.RemoveBackEntry();
                App.RewardsViewModel.IsBeneficios = false;
            }
            e.Cancel = false;
        }
    }
}