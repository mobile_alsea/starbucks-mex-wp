﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Microsoft.Phone.Controls;

namespace Starbucks.UI.View.Rewards
{
    public partial class Beneficios : PhoneApplicationPage
    {
        public Beneficios()
        {
            InitializeComponent();
        }

        protected override void OnBackKeyPress(System.ComponentModel.CancelEventArgs e)
        {
            if (App.Settings.Contains("Logged"))
            {
                try
                {
                    if (App.Settings.Contains("Logged"))
                    {
                        if (((Model.Usuario)App.Settings["Logged"]).Mensaje != "" || ((Model.Usuario)App.Settings["Logged"]) != null)
                        {
                            var listStack = NavigationService.BackStack;
                            while (!listStack.ToList()[0].Source.ToString().Equals("/Home.xaml"))
                            {
                                NavigationService.RemoveBackEntry();
                            }

                            App.RewardsViewModel.IsBeneficios = true;
                            NavigationService.Navigate(new Uri("/View/Rewards/Detalle.xaml", UriKind.Relative));
                            e.Cancel = false;
                        }
                    }
                }
                catch (Exception)
                {
                    e.Cancel = false;
                }
            }
            else
                e.Cancel = false;
        }
    }
}