﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO.IsolatedStorage;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using Nokia.Framework.Libraries.GeoLocation;
using Starbucks.Entities;
using Starbucks.UI.Controller;
using Starbucks.UI.Data;
using Starbucks.UI.Helper;
using Starbucks.UI.Resources;
using Starbucks.UI.ViewModel;

namespace Starbucks
{
    public partial class App : Application
    {

        private static RewardsViewModel rewardsViewModel = null;
        private static HomeViewModel viewModel = null;
        private static AlimentosViewModel alimentosViewModel = null;
        private static BebidasViewModel bebidasViewModel = null;
        private static CafesViewModel cafesViewModel = null;
        private static TiendasViewModel tiendasViewModel = null;
        private static FavoritosViewModel favoritosViewModel = null;
        private static LocationService _LocationService;
        private static SplashViewModel splashViewModel = null;
        private static CodigosBebidas codigoBebidas = null;
        private static ARViewModel arViewModel = null;
        private static IsolatedStorageSettings settings;

        //datos recarga
        public int cantidadRecarga { get; set; }
        public int cantidadRecargaAutomatica { get; set; }
        public int diaRecarga { get; set; }

        //datos tarjeta
        public string alias { get; set; }
        public string nombre { get; set; }
        public string numeroTarjeta { get; set; }
        public string tipoTarjeta { get; set; }
        public string codigoSeguridad { get; set; }
        public int anoTajerta { get; set; }
        public string mesTarjeta { get; set; }

        public static string SeccionActivaFavoritos { get; set; }

        public static bool IsTombstoned { get; set; }

        public static string BingApiKey = "Auwug_hdWw3iMtrk8-w7b90PDjBjxuIGb7JM06u0WkHMKzpXeqHd5vovETKZmiaH";

        public TwiiterTimeLinePost itemTwitter { get; set; }

        public static RewardsViewModel RewardsViewModel
        {
            get
            {
                if (rewardsViewModel == null)
                    rewardsViewModel = new RewardsViewModel();
                return rewardsViewModel;
            }
        }

        public static ARViewModel RAViewModel
        {
            get
            {
                if (arViewModel == null)
                    arViewModel = new ARViewModel();
                return arViewModel;
            }
        }

        public static HomeViewModel ViewModel
        {
            get
            {
                if (viewModel == null)
                    viewModel = new HomeViewModel();
                return viewModel;
            }
        }

        public static AlimentosViewModel AlimentosViewModel
        {
            get
            {
                if (alimentosViewModel == null)
                    alimentosViewModel = new AlimentosViewModel();
                return alimentosViewModel;
            }
        }

        public static BebidasViewModel BebidasViewModel
        {
            get
            {
                if (bebidasViewModel == null)
                    bebidasViewModel = new BebidasViewModel();
                return bebidasViewModel;
            }
        }

        public static CafesViewModel CafesViewModel
        {
            get
            {
                if (cafesViewModel == null)
                    cafesViewModel = new CafesViewModel();
                return cafesViewModel;
            }
        }

        public static TiendasViewModel TiendasViewModel
        {
            get
            {
                if (tiendasViewModel == null)
                    tiendasViewModel = new TiendasViewModel();
                return tiendasViewModel;
            }
        }

        public static FavoritosViewModel FavoritosViewModel
        {
            get
            {
                if (favoritosViewModel == null)
                    favoritosViewModel = new FavoritosViewModel();
                return favoritosViewModel;
            }
        }

        internal static LocationService LocationService
        {
            get
            {
                if (null == _LocationService)
                    _LocationService = new LocationService(MapResource.BingApiKey, CultureInfo.CurrentUICulture);
                return _LocationService;
            }
        }

        internal static SplashViewModel SplashViewModel
        {
            get
            {
                if (null == splashViewModel)
                    splashViewModel = new SplashViewModel();
                return splashViewModel;
            }
        }

        public static IsolatedStorageSettings Settings
        {
            get
            {
                if (settings == null)
                    settings = IsolatedStorageSettings.ApplicationSettings;
                return settings;
            }
        }

        /// <summary>
        /// Provides easy access to the root frame of the Phone Application.
        /// </summary>
        /// <returns>The root frame of the Phone Application.</returns>
        public PhoneApplicationFrame RootFrame { get; private set; }

        /// <summary>
        /// Constructor for the Application object.
        /// </summary>
        public App()
        {

            if (!IsolatedStorageSettings.ApplicationSettings.Contains("DatosRecarga"))
            {
                IsolatedStorageSettings.ApplicationSettings["DatosRecarga"] = new DatosRecarga();
                IsolatedStorageSettings.ApplicationSettings.Save();
            }
            DatosRecarga = IsolatedStorageSettings.ApplicationSettings["DatosRecarga"] as DatosRecarga;

            codigoBebidas = new CodigosBebidas();

            // Global handler for uncaught exceptions. 
            UnhandledException += Application_UnhandledException;

            // Standard Silverlight initialization
            InitializeComponent();

            // Phone-specific initialization
            InitializePhoneApplication();

            // Show graphics profiling information while debugging.
            if (System.Diagnostics.Debugger.IsAttached)
            {
                // Display the current frame rate counters.
                Application.Current.Host.Settings.EnableFrameRateCounter = true;
                MetroGridHelper.IsVisible = true;
                // Show the areas of the app that are being redrawn in each frame.
                //Application.Current.Host.Settings.EnableRedrawRegions = true;

                // Enable non-production analysis visualization mode, 
                // which shows areas of a page that are handed off to GPU with a colored overlay.
                //Application.Current.Host.Settings.EnableCacheVisualization = true;

                // Disable the application idle detection by setting the UserIdleDetectionMode property of the
                // application's PhoneApplicationService object to Disabled.
                // Caution:- Use this under debug mode only. Application that disables user idle detection will continue to run
                // and consume battery power when the user is not using the phone.
                PhoneApplicationService.Current.UserIdleDetectionMode = IdleDetectionMode.Disabled;
            }

        }

        // Code to execute when the application is launching (eg, from Start)
        // This code will not execute when the application is reactivated
        private void Application_Launching(object sender, LaunchingEventArgs e)
        {
            if (!App.Settings.Contains(Constantes.LocalizaTiendas))
                App.Settings[Constantes.LocalizaTiendas] = Constantes.Radio_Cinco;
            if (!App.Settings.Contains(Constantes.Switch_RA))
                App.Settings[Constantes.Switch_RA] = false;
        }

        // Code to execute when the application is activated (brought to foreground)
        // This code will not execute when the application is first launched
        private void Application_Activated(object sender, ActivatedEventArgs e)
        {
            if (e.IsApplicationInstancePreserved)
                IsTombstoned = true;
            else
                IsTombstoned = false;
        }

        // Code to execute when the application is deactivated (sent to background)
        // This code will not execute when the application is closing
        private void Application_Deactivated(object sender, DeactivatedEventArgs e)
        {
        }

        // Code to execute when the application is closing (eg, user hit Back)
        // This code will not execute when the application is deactivated
        private void Application_Closing(object sender, ClosingEventArgs e)
        {
        }

        // Code to execute if a navigation fails
        private void RootFrame_NavigationFailed(object sender, NavigationFailedEventArgs e)
        {
            if (System.Diagnostics.Debugger.IsAttached)
            {
                // A navigation has failed; break into the debugger
                System.Diagnostics.Debugger.Break();
            }
        }

        // Code to execute on Unhandled Exceptions
        private void Application_UnhandledException(object sender, ApplicationUnhandledExceptionEventArgs e)
        {
            if (System.Diagnostics.Debugger.IsAttached)
            {
                // An unhandled exception has occurred; break into the debugger
                System.Diagnostics.Debugger.Break();
            }
        }

        #region Phone application initialization

        // Avoid double-initialization
        private bool phoneApplicationInitialized = false;

        // Do not add any additional code to this method
        private void InitializePhoneApplication()
        {
            if (phoneApplicationInitialized)
                return;

            // Create the frame but don't set it as RootVisual yet; this allows the splash
            // screen to remain active until the application is ready to render.
            RootFrame = new PhoneApplicationFrame();
            RootFrame.Navigated += CompleteInitializePhoneApplication;

            // Handle navigation failures
            RootFrame.NavigationFailed += RootFrame_NavigationFailed;

            // Ensure we don't initialize again
            phoneApplicationInitialized = true;
        }

        // Do not add any additional code to this method
        private void CompleteInitializePhoneApplication(object sender, NavigationEventArgs e)
        {
            // Set the root visual to allow the application to render
            if (RootVisual != RootFrame)
                RootVisual = RootFrame;

            // Remove this handler since it is no longer needed
            RootFrame.Navigated -= CompleteInitializePhoneApplication;
        }

        #endregion

        public static void Quit()
        {
            App.Settings.Save();
            new Microsoft.Xna.Framework.Game().Exit();
        }

        public Entities.DatosRecarga DatosRecarga { get; set; }
    }
}