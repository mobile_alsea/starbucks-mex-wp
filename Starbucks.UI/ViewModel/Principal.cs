﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace Starbucks.UI.ViewModel
{
    public class Principal
    {
        public static App Current() { return Application.Current as App; }
    }
}
