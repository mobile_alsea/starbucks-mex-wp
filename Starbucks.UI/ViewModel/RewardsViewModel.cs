﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Starbucks.UI.Model;
using System.Collections.Generic;
using Starbucks.UI.Data;
using Starbucks.UI.Helper;
using Starbucks.UI;
using System.Collections;
using System.IO.IsolatedStorage;
using System.Runtime.Serialization;
using System.ServiceModel.Channels;
using System.ServiceModel;

namespace Starbucks.UI.ViewModel
{
    public class RewardsViewModel : ViewModelBase
    {
        public delegate void LoadComplete(string pantalla, IList args);
        public event LoadComplete LoadCompletedAll;

        public bool IsBeneficios { get; set; }
        public Model.Usuario UsuarioActivo { get; set; }
        public string Usuario { get; set; }
        public string Contra { get; set; }

        public RewardsViewModel()
        {
            
        }

        public void ObtenerDetalleRewards(string usuario, string contra)
        {
            try
            {
                this.Usuario = usuario;
                this.Contra = contra;

                string config = "http://api.starbucks.mx:8080/replicador/services/AlseaValueImpWS.AlseaValueImpWSHttpSoap11Endpoint/";
                RewardsService.AlseaValueImpWSPortTypeClient client = new RewardsService.AlseaValueImpWSPortTypeClient("AlseaValueImpWSHttpSoap11Endpoint");
                client.Endpoint.Address = new EndpointAddress(config);

                //Create wsse security object
                Security security = new Security();
                UsernameToken usernameToken = new UsernameToken { Password = "bob", Username = "bobPW" };
                security.UsernameToken = usernameToken;

                //Serialize object to xml
                XmlObjectSerializer xmlObjectSerializer = new DataContractSerializer(typeof(Security), "Security", "");

                //Create address header with security header
                AddressHeader addressHeader = AddressHeader.CreateAddressHeader("Security", "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd", security, xmlObjectSerializer);

                //Create new endpoint with addressHeader that contains security header
                EndpointAddress endpoint = new EndpointAddress(new Uri(client.Endpoint.Address.Uri.AbsoluteUri), new[] { addressHeader });

                //Set new endpoint
                client.Endpoint.Address = endpoint;

                client.ConsultaDatosAsync(this.Usuario, this.Contra, "12345678912345678912345");
                client.ConsultaDatosCompleted += new EventHandler<RewardsService.ConsultaDatosCompletedEventArgs>(client_ConsultaDatosCompleted);
            }
            catch (Exception ex)
            {
                Log.EscribeLog(ex.Message);
            }
        }

        void client_ConsultaDatosCompleted(object sender, RewardsService.ConsultaDatosCompletedEventArgs e)
        {
            try
            {
                if (e.Result.errorMessage != "" && e.Result.errorMessage != null)
                {
                    UsuarioActivo = new Usuario()
                    {
                        Mensaje = e.Result.errorMessage == null ? "" : e.Result.errorMessage
                    };
                    if (LoadCompletedAll != null)
                        LoadCompletedAll("Ingreso", null);
                }


                UsuarioActivo = new Usuario()
                {
                    Mensaje = e.Result.errorMessage == null ? "" : e.Result.errorMessage,
                    Cumple = e.Result.birthDate,
                    FechaRegistro = e.Result.startDate,
                    Saldo = double.Parse(e.Result.saldo),
                    Estrellas = int.Parse(e.Result.estrellas1),
                    NumeroTarjeta = e.Result.cardNumber,
                    Nivel = ObtieneNivel(int.Parse(e.Result.estrellas1)),
                    EstrellasFaltantes = ObtieneEstrellasFaltanatesSiguienteNivel(int.Parse(e.Result.estrellas1)),
                    NivelSiguiente = ObtieneSiguienteNivel(int.Parse(e.Result.estrellas1)),
                    EstrellasGold = ObtieneEstrellas(int.Parse(e.Result.estrellas1)),
                    EstrellasSiguienteNivel = ObtieneEstrellasSiguienteNivel(int.Parse(e.Result.estrellas1)),
                    Pass = this.Contra,
                    Mail = this.Usuario

                };

                UsuarioActivo.mensajeEstrellasSiguienteNivel1 = UsuarioActivo.Estrellas > 0 ? "Necesitas " + UsuarioActivo.EstrellasFaltantes + " estrellas más para el nivel " + UsuarioActivo.NivelSiguiente : "Necesitas " + UsuarioActivo.EstrellasFaltantes + " estrella más para el nivel " + UsuarioActivo.NivelSiguiente; ;
                UsuarioActivo.mensajeEstrellasSiguienteNivel2 = "Estrellas para conseguir el " + UsuarioActivo.NivelSiguiente + " level: " + UsuarioActivo.EstrellasFaltantes;
                UsuarioActivo.mensajemiembroDesde = "Miembro desde " + UsuarioActivo.FechaRegistro;
                UsuarioActivo.mensajeNivel = UsuarioActivo.Nivel + " level";
                if (UsuarioActivo.Nivel.Equals("Gold"))
                    UsuarioActivo.mensajeTienesVSFaltan = (15 - UsuarioActivo.EstrellasGold) + " / " + 15;
                else
                    UsuarioActivo.mensajeTienesVSFaltan = UsuarioActivo.Estrellas + " / " + UsuarioActivo.EstrellasSiguienteNivel;

                //MensajesGold
                if (UsuarioActivo.Nivel.Equals("Gold"))
                {
                    UsuarioActivo.mensajeEstrellasSiguienteNivel1 = string.Format("Necesitas {0} estrellas más para una bebida gratis." , UsuarioActivo.EstrellasFaltantes);
                    UsuarioActivo.mensajeEstrellasSiguienteNivel2 = string.Format("Estrellas para conseguir una bebida gratis: {0}", UsuarioActivo.EstrellasFaltantes);
                }

                App.Settings["Logged"] = UsuarioActivo;
                App.Settings.Save();
                
                if (LoadCompletedAll != null)
                    LoadCompletedAll("Ingreso", null);
            }
            catch
            {
                if (LoadCompletedAll != null)
                    LoadCompletedAll("Ingreso", null);
            }
            
        }

        void cliente_GetUserCompleted(object sender, ServiceReference.GetUserCompletedEventArgs e)
        {
            try
            {

                UsuarioActivo = new Usuario()
                {
                    Mensaje = e.Result.GetUserResult.errorMessage,
                    Cumple = e.Result.GetUserResult.birthDate.ToShortDateString(),
                    FechaRegistro = e.Result.GetUserResult.startDate.ToShortDateString(),
                    Saldo = e.Result.GetUserResult.saldo,
                    Estrellas = e.Result.GetUserResult.estrellas1,
                    NumeroTarjeta = e.Result.GetUserResult.cardNumber,
                    Nivel = ObtieneNivel(e.Result.GetUserResult.estrellas1),
                    EstrellasFaltantes = ObtieneEstrellasFaltanatesSiguienteNivel(e.Result.GetUserResult.estrellas1),
                    NivelSiguiente = ObtieneSiguienteNivel(e.Result.GetUserResult.estrellas1),
                    EstrellasGold = ObtieneEstrellas(e.Result.GetUserResult.estrellas1),
                    EstrellasSiguienteNivel = ObtieneEstrellasSiguienteNivel(e.Result.GetUserResult.estrellas1),
                    Pass = this.Contra,
                    Mail = this.Usuario

                };

                UsuarioActivo.mensajeEstrellasSiguienteNivel1 = UsuarioActivo.Estrellas > 0 ? "Necesitas " + UsuarioActivo.EstrellasFaltantes + " estrellas más para el nivel " + UsuarioActivo.NivelSiguiente : "Necesitas " + UsuarioActivo.EstrellasFaltantes + " estrella más para el nivel " + UsuarioActivo.NivelSiguiente; ;
                UsuarioActivo.mensajeEstrellasSiguienteNivel2 = "Estrellas para conseguir el " + UsuarioActivo.NivelSiguiente + " level: " + UsuarioActivo.EstrellasFaltantes;
                UsuarioActivo.mensajemiembroDesde = "Miembro desde " + UsuarioActivo.FechaRegistro;
                UsuarioActivo.mensajeNivel = UsuarioActivo.Nivel + " level";
                if (UsuarioActivo.Nivel.Equals("Gold"))
                    UsuarioActivo.mensajeTienesVSFaltan = (15 - UsuarioActivo.EstrellasGold) + " / " + 15;
                else
                    UsuarioActivo.mensajeTienesVSFaltan = UsuarioActivo.Estrellas + " / " + UsuarioActivo.EstrellasSiguienteNivel;

                //MensajesGold
                if (UsuarioActivo.Nivel.Equals("Gold"))
                {
                    UsuarioActivo.mensajeEstrellasSiguienteNivel1 = string.Format("Necesitas {0} estrellas más para una bebida gratis.", UsuarioActivo.EstrellasFaltantes);
                    UsuarioActivo.mensajeEstrellasSiguienteNivel2 = string.Format("Estrellas para conseguir una bebida gratis: {0}", UsuarioActivo.EstrellasFaltantes);
                }

                App.Settings["Logged"] = UsuarioActivo;
                App.Settings.Save();

                if (LoadCompletedAll != null)
                    LoadCompletedAll("Ingreso", null);

            }
            catch (Exception)
            {
                if (LoadCompletedAll != null)
                    LoadCompletedAll("Ingreso", null);
            }
        }

        string ObtieneNivel(int estrellas)
        {
            string result = "";
            if (estrellas < int.Parse(StarbucksResource.MaxNivel1))
                result = StarbucksResource.Nivel1;
            if (estrellas >= (int.Parse(StarbucksResource.MinNivel12)) && (estrellas < int.Parse(StarbucksResource.MaxNivel2)))
                result = StarbucksResource.Nivel2;
            if (estrellas >= int.Parse(StarbucksResource.MinNivel3))
                result = StarbucksResource.Nivel3;

            return result;
        }

        string ObtieneSiguienteNivel(int estrellas)
        {
            string result = "";
            if (estrellas < int.Parse(StarbucksResource.MaxNivel1))
                result = StarbucksResource.Nivel2;
            if (estrellas >= (int.Parse(StarbucksResource.MinNivel12)) && (estrellas < int.Parse(StarbucksResource.MaxNivel2)))
                result = StarbucksResource.Nivel3;
            if (estrellas >= int.Parse(StarbucksResource.MinNivel3))
                result = StarbucksResource.Nivel1;

            return result;
        }

        int ObtieneEstrellas(int estrellas)
        {
            int result = 0;
            result = 15 - (((estrellas - 30) % 15));
            return result;
        }

        int ObtieneEstrellasFaltanatesSiguienteNivel(int estrellas)
        {
            int result = 0;

            if (estrellas < int.Parse(StarbucksResource.MaxNivel1))
                result = int.Parse(StarbucksResource.MaxNivel1) - estrellas;
            if (estrellas >= (int.Parse(StarbucksResource.MinNivel12)) && (estrellas < int.Parse(StarbucksResource.MaxNivel2)))
                result = int.Parse(StarbucksResource.MaxNivel2) - estrellas;
            if (estrellas >= int.Parse(StarbucksResource.MinNivel3))
                result = ObtieneEstrellas(estrellas);

            return result;
        }


        int ObtieneEstrellasSiguienteNivel(int estrellas)
        {
            int result = 0;

            if (estrellas < int.Parse(StarbucksResource.MaxNivel1))
                result = int.Parse(StarbucksResource.MaxNivel1);
            else if (estrellas >= (int.Parse(StarbucksResource.MinNivel12)) && (estrellas < int.Parse(StarbucksResource.MaxNivel2)))
                result = int.Parse(StarbucksResource.MaxNivel2);
            else
                result = estrellas + ObtieneEstrellas(estrellas);

            return result;
        }



    }

    [ICSharpCode.SharpZipLib.Silverlight.Serialization.Serializable]
    [DataContract(Namespace = "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd")] // This object serialize specific namespace
    public class Security
    {
        [DataMember] // This object serialize without namespace
        public UsernameToken UsernameToken;
    }


    public class UsernameToken : System.Xml.Serialization.IXmlSerializable
    {

        public string Username { get; set; }
        public string Password { get; set; }

        public System.Xml.Schema.XmlSchema GetSchema() { return null; }

        public void ReadXml(System.Xml.XmlReader reader)
        {
            reader.MoveToContent();

            Username = reader.ReadElementContentAsString("Username", "");
            reader.ReadStartElement();

            Password = reader.ReadElementContentAsString("Password", "");
            reader.ReadEndElement();
        }

        public void WriteXml(System.Xml.XmlWriter writer)
        {
            writer.WriteElementString("Username", Username);
            writer.WriteElementString("Password", Password);
        }
    }
}
