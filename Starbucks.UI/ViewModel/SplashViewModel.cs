﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Starbucks.UI.Data;
using Starbucks.UI.Helper;
using Starbucks.UI.Model;
using Starbucks.UI.Data.Service;

namespace Starbucks.UI.ViewModel
{
    public class SplashViewModel : ViewModelBase
    {
        public event EventHandler LoadCompleted;
        public event EventHandler DownloadError;

        public SplashViewModel()
        {
            //var _serviceRequestBebidas = new ServiceRequest();
            //_serviceRequestBebidas.BeginWebRequest<Bebida>(StarbucksResource.UrlBebidas, RequestBebidas);
            var _serviceRequestVersiones = new ServiceRequest();
            _serviceRequestVersiones.BeginWebRequest<Starbucks.UI.Model.Version>(StarbucksResource.UrlVersiones, RequestVersiones);
        }

        public void Reintentar()
        {
            BorrarDatos();
            var _serviceRequestBebidas = new ServiceRequest();
            _serviceRequestBebidas.BeginWebRequest<Bebida>(StarbucksResource.UrlBebidas, RequestBebidas);
        }

        private void BorrarDatos()
        {
            Data.Parser.Bebidas.ListBebidas = null;
            Data.Parser.Cafes.ListCafes = null;
            Data.Parser.Comidas.ListComidas = null;
            Data.Parser.Tiendas.ListTiendas = null;
            Data.Parser.Versiones.ListVersiones = null;
        }

        private void RequestVersiones(ServiceEventArgs<List<Starbucks.UI.Model.Version>> response)
        {
            if (!response.TimeOut)
            {
                if (!response.ErrorInMakingRequest)
                {
                    if (!response.ErrorParser)
                    {
                        if (null != response.Data)
                        {
                            Data.Parser.Versiones.ListVersiones = response.Data;
                            for (int i = 0; i < Data.Parser.Versiones.ListVersiones.Count;i++ )
                            {
                                if (Data.Parser.Versiones.ListVersiones.ElementAt(i).nombre.Equals("cafes"))
                                    Data.Parser.Cafes.version = Convert.ToInt32(Data.Parser.Versiones.ListVersiones.ElementAt(i).version);
                                else if (Data.Parser.Versiones.ListVersiones.ElementAt(i).nombre.Equals("comidas"))
                                    Data.Parser.Comidas.version = Convert.ToInt32(Data.Parser.Versiones.ListVersiones.ElementAt(i).version);
                                else if (Data.Parser.Versiones.ListVersiones.ElementAt(i).nombre.Equals("bebidas"))
                                    Data.Parser.Bebidas.version = Convert.ToInt32(Data.Parser.Versiones.ListVersiones.ElementAt(i).version);
                                else if (Data.Parser.Versiones.ListVersiones.ElementAt(i).nombre.Equals("tiendas"))
                                    Data.Parser.Tiendas.version = Convert.ToInt32(Data.Parser.Versiones.ListVersiones.ElementAt(i).version);
                            }
                            var _serviceRequestBebidas = new ServiceRequest();
                            _serviceRequestBebidas.BeginWebRequest<Bebida>(StarbucksResource.UrlBebidas, RequestBebidas);
                        }
                    }
                    else
                    {
                        DownloadError(ErrorData.ErrorInParser, null);
                    }
                }
                else
                {
                    DownloadError(ErrorData.ErrorInRequest, null);
                }
            }
            else
            {
                DownloadError(ErrorData.TimeOut, null);
            }
        }

        private void RequestBebidas(ServiceEventArgs<List<Bebida>> response)
        {
            if (!response.TimeOut)
            {
                if (!response.ErrorInMakingRequest)
                {
                    if (!response.ErrorParser)
                    {
                        if (null != response.Data)
                        {
                            Data.Parser.Bebidas.ListBebidas = response.Data;
                            var _serviceRequestCafes = new ServiceRequest();
                            _serviceRequestCafes.BeginWebRequest<Cafe>(StarbucksResource.UrlCafes, RequestCafes);
                        }
                    }
                    else
                    {
                        DownloadError(ErrorData.ErrorInParser, null);
                    }
                }
                else
                {
                    DownloadError(ErrorData.ErrorInRequest, null);
                }
            }
            else
            {
                DownloadError(ErrorData.TimeOut, null);
            }
        }

        private void RequestCafes(ServiceEventArgs<List<Cafe>> response)
        {
            if (!response.TimeOut)
            {
                if (!response.ErrorInMakingRequest)
                {
                    if (!response.ErrorParser)
                    {
                        if (null != response.Data)
                        {
                            Data.Parser.Cafes.ListCafes = response.Data;
                            var _serviceRequestComidas = new ServiceRequest();
                            _serviceRequestComidas.BeginWebRequest<Comida>(StarbucksResource.UrlComidas, RequestComidas);
                        }
                    }
                    else
                    {
                        DownloadError(ErrorData.ErrorInParser, null);
                    }
                }
                else
                {
                    DownloadError(ErrorData.ErrorInRequest, null);
                }
            }
            else
            {
                DownloadError(ErrorData.TimeOut, null);
            }
        }

        private void RequestComidas(ServiceEventArgs<List<Comida>> response)
        {
            if (!response.TimeOut)
            {
                if (!response.ErrorInMakingRequest)
                {
                    if (!response.ErrorParser)
                    {
                        if (null != response.Data)
                        {
                            Data.Parser.Comidas.ListComidas = response.Data;
                            var _serviceRequestTiendas = new ServiceRequest();
                            _serviceRequestTiendas.BeginWebRequest<Tienda>(StarbucksResource.UrlTiendas, RequestTiendas);
                        }
                    }
                    else
                    {
                        DownloadError(ErrorData.ErrorInParser, null);
                    }
                }
                else
                {
                    DownloadError(ErrorData.ErrorInRequest, null);
                }
            }
            else
            {
                DownloadError(ErrorData.TimeOut, null);
            }
        }

        private void RequestTiendas(ServiceEventArgs<List<Tienda>> response)
        {
            if (!response.TimeOut)
            {
                if (!response.ErrorInMakingRequest)
                {
                    if (!response.ErrorParser)
                    {
                        if (null != response.Data)
                        {
                            Data.Parser.Tiendas.ListTiendas = response.Data;
                            LoadCompleted(null, null);
                        }
                    }
                    else
                    {
                        DownloadError(ErrorData.ErrorInParser, null);
                    }
                }
                else
                {
                    DownloadError(ErrorData.ErrorInRequest, null);
                }
            }
            else
            {
                DownloadError(ErrorData.TimeOut, null);
            }
        }

        public void ActualizaEstrellas()
        {
            if (App.Settings.Contains("Logged"))
            {
                if (App.Settings["Logged"] != null)
                {
                    App.RewardsViewModel.ObtenerDetalleRewards(((Model.Usuario)App.Settings["Logged"]).Mail, ((Model.Usuario)App.Settings["Logged"]).Pass);
                }
            }
        }
    }
}
