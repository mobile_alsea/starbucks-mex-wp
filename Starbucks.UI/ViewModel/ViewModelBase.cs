﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using Starbucks.UI.Data;

namespace Starbucks.UI.ViewModel {
      public class ViewModelBase : INotifyPropertyChanged {

          public ViewModelBase()
          {
              
          }

          #region INotifyPropertyChanged Members

          public event PropertyChangedEventHandler PropertyChanged;
          protected void NotifyPropertyChanged(string property)
          {
              PropertyChangedEventHandler handler = PropertyChanged;
              if (handler != null)
              {
                  handler(this, new PropertyChangedEventArgs(property));
              }
          }

          #endregion
      }
}
