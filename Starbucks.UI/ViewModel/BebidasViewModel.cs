﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Starbucks.UI.Data.Parser;

namespace Starbucks.UI.ViewModel
{
    public class BebidasViewModel : ViewModelBase
    {
        public Model.Bebida SelectedItemBebida { get; set; }
        public Model.DetalleConstructor SelectedDetalleConstructor { get; set; }

        public BebidasViewModel()
        {
            SelectedDetalleConstructor = new Model.DetalleConstructor();
        }

        public List<Model.Bebida> ObtenerBebidas(string subcategoria)
        {
            return Bebidas.ObtenerListaBebidas(subcategoria);
        }

        public List<Model.Bebida> ObtenerTodasLasBebidas()
        {
            return Bebidas.ObtenerTodasLasBebidas();
        }
    }
}
