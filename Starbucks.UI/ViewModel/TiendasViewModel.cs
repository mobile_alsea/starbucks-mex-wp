﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Device.Location;
using System.Linq;
using System.Text;
using Microsoft.Phone.Controls.Maps;
using Microsoft.Phone.Controls.Maps.Platform;
using Starbucks.UI.Data.Parser;
using Utils.WP7.Bing.BingRoute;

namespace Starbucks.UI.ViewModel
{
    public class TiendasViewModel : ViewModelBase
    {
        public event EventHandler RouteResolved;

        public Model.Tienda SelectedItemTienda { get; set; }

        public GeoCoordinate MyLocation { get; set; }

        public GeoCoordinate MyLocationSearch { get; set; }

        public Map CurrentMap { get; set; }
        public MapItemsControl CurrentMapControl { get; set; }
        public Pushpin PinActivo { get; set; }
        public bool IsLoadPin { get; set; }
        public bool IsCambioPin { get; set; }

        private Location startPoint;
        public Location StartPoint
        {
            get { return startPoint; }
            set
            {
                startPoint = value;
                NotifyPropertyChanged("StartPoint");
            }
        }

        private Location endPoint;
        public Location EndPoint
        {
            get { return endPoint; }
            set
            {
                endPoint = value;
                NotifyPropertyChanged("EndPoint");
            }
        }

        private ObservableCollection<Location> routePoints;
        public ObservableCollection<Location> RoutePoints
        {
            get { return routePoints; }
            set
            {
                routePoints = value;
                NotifyPropertyChanged("RoutePoints");
            }
        }

        private ObservableCollection<ItineraryItem> itinerary;
        public ObservableCollection<ItineraryItem> Itinerary
        {
            get
            {
                return itinerary;
            }
            set
            {
                itinerary = value;
                NotifyPropertyChanged("Itinerary");
            }
        }

        public TiendasViewModel()
        {
            
        }

        public List<Model.Tienda> ObtenerListTiendas()
        {
            return Tiendas.ObtenerListaTiendas();
        }

        public void RouteLoaded()
        {
            var fromWaypoint = new Waypoint();
            fromWaypoint.Description = "From";
            fromWaypoint.Location = new Location();
            fromWaypoint.Location.Latitude = MyLocation.Latitude;
            fromWaypoint.Location.Longitude = MyLocation.Longitude;

            var toWaypoint = new Waypoint();
            toWaypoint.Description = "To";
            toWaypoint.Location = new Location();
            toWaypoint.Location.Latitude = Convert.ToDouble(SelectedItemTienda.latitud);
            toWaypoint.Location.Longitude = Convert.ToDouble(SelectedItemTienda.longitud);

            var routeRequest = new RouteRequest();
            routeRequest.Credentials = new Credentials();
            routeRequest.Credentials.ApplicationId = App.BingApiKey;
            routeRequest.Waypoints = new System.Collections.ObjectModel.ObservableCollection<Waypoint>();
            routeRequest.Waypoints.Add(fromWaypoint);
            routeRequest.Waypoints.Add(toWaypoint);
            routeRequest.Options = new RouteOptions();
            routeRequest.Options.RoutePathType = RoutePathType.Points;
            routeRequest.UserProfile = new Utils.WP7.Bing.BingRoute.UserProfile();
            routeRequest.UserProfile.DistanceUnit = Utils.WP7.Bing.BingRoute.DistanceUnit.Kilometer;

            var routeClient = new RouteServiceClient("BasicHttpBinding_IRouteService");
            routeClient.CalculateRouteCompleted += new EventHandler<CalculateRouteCompletedEventArgs>(OnRouteComplete);
            routeClient.CalculateRouteAsync(routeRequest);
        }

        private void OnRouteComplete(object sender, CalculateRouteCompletedEventArgs e)
        {
            if (e.Result != null && e.Result.Result != null && e.Result.Result.Legs != null & e.Result.Result.Legs.Any())
            {
                var result = e.Result.Result;
                var legs = result.Legs.FirstOrDefault();

                StartPoint = legs.ActualStart;
                EndPoint = legs.ActualEnd;
                RoutePoints = result.RoutePath.Points;
                Itinerary = legs.Itinerary;
                RaiseRouteResolved();
            }
        }

        private void RaiseRouteResolved()
        {
            if (RouteResolved != null)
                RouteResolved(this, EventArgs.Empty);
        }

        public void AgregaPinActivo()
        {
            if (CurrentMapControl != null)
                CurrentMapControl.Items.Add(PinActivo);
        }

        public void EliminaPinActivo()
        {
            if (CurrentMapControl != null)
            {
                CurrentMapControl.Items.Remove(PinActivo);
                IsLoadPin = false;
            }
        }
    }
}
