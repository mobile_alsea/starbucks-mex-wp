﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Device.Location;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using Microsoft.Xna.Framework;
using Nokia.Framework.Libraries.AugmentedReality;
using Nokia.Framework.Libraries.Foursquare.ResNaviDataModel;
using Starbucks.UI.Helper;
using Starbucks.UI.Model;

namespace Starbucks.UI.ViewModel
{
    #region delegate declaration
    /// <summary>
    /// Represents the method that will handle the CommentsLoaded
    /// </summary>
    /// <param name="count">the count of Comments</param>
    public delegate void CommentsLoadedHandler(int count);

    /// <summary>
    /// Represents the method that will handle the NearbyResLoaded
    /// </summary>
    public delegate void NearbyResLoadedHandler();

    /// <summary>
    /// Represents the method that will handle the CurrentRestaurantInfoUpdated
    /// </summary>
    /// <param name="resInfo">the Info of Restaurant</param>
    public delegate void CurrentRestaurantInfoUpdatedHanler(RestaurantInfo resInfo);
    #endregion

    public class ARViewModel
    {
        #region events

        /// <summary>
        /// Occurs when the NearbyRes Load Completed
        /// </summary>
        public event NearbyResLoadedHandler NearbyResLoaded;

        /// <summary>
        /// Occurs when the Current Restaurant Info Updated
        /// </summary>
        public event CurrentRestaurantInfoUpdatedHanler CurRestaurantInfoUpdated;
        #endregion

        #region Inner Data
        AREngine _arEngine;
        RectRender _rectArea;
        private int _width;
        private int _height;
        const int RESTAURANTINFOWIDTH = 250;
        const int RESTAURANTINFOHEIGHT = 45;
        #endregion

        #region Constructor
        /// <summary>
        /// Constructor.
        /// </summary>
        public ARViewModel()
        {
            _arEngine = new AREngine();
            _rectArea = new RectRender();
            ProjectedRestaurantItems = new ObservableCollection<RestaurantInfoAdapter>();
            NearbyRestaurantItems = new ObservableCollection<RestaurantInfoAdapter>();
            RestaurantsInScreen = new List<RestaurantInfo>();
        }
        #endregion

        public GeoCoordinate CurrentPosition;

        /// <summary>
        /// gets Restaurants InScreen
        /// </summary>
        public List<RestaurantInfo> RestaurantsInScreen { get; private set; }

        /// <summary>
        /// gets Projected Restaurant Items
        /// </summary>
        public ObservableCollection<RestaurantInfoAdapter> ProjectedRestaurantItems { get; private set; }

        /// <summary>
        /// gets Nearby Restaurant Items
        /// </summary>
        public ObservableCollection<RestaurantInfoAdapter> NearbyRestaurantItems { get; private set; }

        /// <summary>
        /// Get the positions of restuarants in screen.
        /// </summary>
        /// <param name="rotationMatrix">Rotation Matrix</param>
        /// <param name="leftCout">The count of restaurants in device left</param>
        /// <param name="rightCount">The count of restaurants in device right</param>
        /// <returns>
        /// The count of restuarants in device screen.
        /// </returns>
        public int Project(Matrix rotationMatrix, ref int leftCout, ref int rightCount)
        {
            int projectedCount = 0;

            Vector3 projected = new Vector3();
            int index = 0;

            _rectArea.ClearUsedRect();
            RestaurantsInScreen.Clear();

            foreach (RestaurantInfoAdapter item in ProjectedRestaurantItems)
            {
                item.Visibility = Visibility.Collapsed;
            }

            foreach (RestaurantInfoAdapter item in NearbyRestaurantItems)
            {
                projected = _arEngine.Project(item.CoordinationPoint, rotationMatrix);
                if (_arEngine.IsWithinScreen(projected))
                {
                    ProjectedRestaurantItems[index].Visibility = Visibility.Visible;
                    RestaurantsInScreen.Add(ProjectedRestaurantItems[index].RestaurantInfo);
                    projectedCount++;

                    ProjectedRestaurantItems[index].Left = (int)projected.X;
                    ProjectedRestaurantItems[index].Top = (int)projected.Y;

                    TextBlock addressBlock = new TextBlock();
                    addressBlock.Width = 180;
                    addressBlock.TextWrapping = TextWrapping.Wrap;
                    addressBlock.FontSize = 16;
                    addressBlock.Text = item.Address;

                    StackPanel stackPanel = new StackPanel();
                    stackPanel.Children.Add(addressBlock);

                    stackPanel.Measure(new Size(double.PositiveInfinity, double.PositiveInfinity));
                    Rectangle validRect = _rectArea.GetValidRectangle(
                        new Rectangle(ProjectedRestaurantItems[index].Left,
                                      ProjectedRestaurantItems[index].Top,
                                      RESTAURANTINFOWIDTH,
                                      RESTAURANTINFOHEIGHT + (int)stackPanel.DesiredSize.Height));

                    if (validRect.IsEmpty)
                    {
                        ProjectedRestaurantItems[index].Visibility = Visibility.Collapsed;
                    }
                    else
                    {
                        ProjectedRestaurantItems[index].Left = validRect.Left;
                        ProjectedRestaurantItems[index].Top = validRect.Top;
                        _rectArea.AddUsedRect(validRect);
                    }

                }
                else
                {
                    if (projected.Z >= 1)
                    {
                        if (projected.X > 0)
                        {
                            item.HorizontalAlignment = HorizontalAlignment.Left;
                            leftCout++;
                        }
                        else
                        {
                            item.HorizontalAlignment = HorizontalAlignment.Right;
                            rightCount++;
                        }
                    }
                    else
                    {
                        if (projected.X < 0)
                        {
                            item.HorizontalAlignment = HorizontalAlignment.Left;
                            leftCout++;
                        }
                        else
                        {
                            item.HorizontalAlignment = HorizontalAlignment.Right;
                            rightCount++;
                        }
                    }

                    item.Width = _width;
                    item.Left = (int)projected.X;
                    item.Top = (int)projected.Y;
                }


                index++;
            }

            return projectedCount;
        }

        /// <summary>
        /// Initialize AREngine.
        /// </summary>
        /// <param name="width">The width of viewport</param>
        /// <param name="height">The height of viewport</param>
        /// <param name="cameraUpVector">the camera Up Vector</param>
        public void Initialize(int width, int height, Vector3 cameraUpVector)
        {
            const int distance = 1000;
            _arEngine.Initialize(width, height, cameraUpVector, distance);
            _rectArea.SetRectArea(0, 0, width, height);
            _width = width;
            _height = height;
        }

        /// <summary>
        /// Loads nearby restaurants.
        /// </summary>
        /// <param name="resList">The list storing nearby restuarants</param>
        public void NearbyResUpdated(List<RestaurantInfo> resList)
        {
            if (Microsoft.Devices.Environment.DeviceType == Microsoft.Devices.DeviceType.Emulator)
            {
                NearbyRestaurantItems.Clear();
                foreach (RestaurantInfo item in resList)
                {
                    AddNearbyRestaurant(item);
                }
            }
            else
            {
                ProjectedRestaurantItems.Clear();
                NearbyRestaurantItems.Clear();

                if (App.RAViewModel.CurrentPosition != null)
                {
                    var dataTiendas = from c in App.TiendasViewModel.ObtenerListTiendas()
                                      select new RestaurantInfo
                                      {
                                          Name = c.nombre,
                                          Address = c.direccion,
                                          City = c.ciudad,
                                          Province = c.provincia,
                                          Latitude = c.latitud,
                                          Longitude = c.longitud,
                                          ImageUrl = c.EstatusTiendaTile,
                                          Coordinate = new System.Device.Location.GeoCoordinate(c.latitud, c.longitud)
                                      };
                    var list = new List<RestaurantInfo>();

                    foreach (var item in dataTiendas)
                    {
                        var Origen = new Posicion(App.RAViewModel.CurrentPosition.Latitude, App.RAViewModel.CurrentPosition.Longitude);
                        var Destino = new Posicion(Convert.ToDouble(item.Latitude), Convert.ToDouble(item.Longitude));
                        var km = Origen.DistanciaKm(Destino);
                        item.Dist = (Math.Round(km, 2) * 1000).ToString();
                        if (Math.Round(km) <= 1.5)
                            list.Add(item);
                    }

                    foreach (RestaurantInfo item in list)
                    {
                        Vector3 xnaCoordinationPoint = AREngine.ConvertToXnaCoordinate(item.Coordinate,
                            App.RAViewModel.CurrentPosition, _arEngine.CameraPosition);

                        ProjectedRestaurantItems.Add(new RestaurantInfoAdapter(item, xnaCoordinationPoint));
                        NearbyRestaurantItems.Add(new RestaurantInfoAdapter(item, xnaCoordinationPoint));
                    }

                    if (NearbyResLoaded != null)
                    {
                        NearbyResLoaded();
                    }
                }
            }
        }

        /// <summary>
        /// Stores tapped restaurant.
        /// </summary>
        /// <param name="resInfo">Restaurant Information</param>
        private void CurrentRestaurantInfoUpdated(RestaurantInfo resInfo)
        {
            if (null != CurRestaurantInfoUpdated)
            {
                CurRestaurantInfoUpdated(resInfo);
            }
        }

        /// <summary>
        /// Calculates restaurant position in XNA coordinate system(3D).
        /// </summary>
        /// <param name="restaurantInfo">Restaurant information</param>
        private void AddNearbyRestaurant(RestaurantInfo restaurantInfo)
        {
            Vector3 xnaCoordinationPoint = AREngine.ConvertToXnaCoordinate(restaurantInfo.Coordinate,
                    App.RAViewModel.CurrentPosition, _arEngine.CameraPosition);
            NearbyRestaurantItems.Add(new RestaurantInfoAdapter(restaurantInfo, xnaCoordinationPoint));
        }
    }
}
