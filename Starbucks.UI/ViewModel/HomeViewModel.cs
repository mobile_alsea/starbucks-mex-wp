﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Device.Location;
using System.IO.IsolatedStorage;
using System.Linq;
using System.Text;
using Microsoft.Phone.Controls.Maps;
using Microsoft.Phone.Controls.Maps.Platform;
using Microsoft.Phone.UserData;
using Starbucks.UI.Data.Parser;
using Starbucks.UI.Helper;
using Starbucks.UI.Model;
using Utils.WP7.Bing.BingRoute;

namespace Starbucks.UI.ViewModel
{
    public class HomeViewModel : ViewModelBase
    {
        #region ### Atributos ###
        private ObservableCollection<Group<Contact>> _contactsList = null;
        #endregion

        #region ### Metodos ###
        public ObservableCollection<Group<Contact>> ContactsList
        {
            get
            {
                return _contactsList;
            }

            set
            {
                if (_contactsList == value)
                {
                    return;
                }
                _contactsList = value;
                NotifyPropertyChanged("ContactsList");
            }
        }
        public List<string> SelectedFiltros { get; set; }
        public List<Dia> SelectedListHorarios { get; set; }
        public Contact SelectedContact { get; set; }
        public TileFlipBebida TileFlipBebidaSelected { get; set; }
        public TileFlipCafe TileFlipCafeSelected { get; set; }
        public TileFlipAlimentos TileFlipAlimentosSelected { get; set; }
        public TileFlipTiendas TileFlipTiendaSelected { get; set; }
        public bool GpsDisponible { get; set; }
        #endregion

        #region ### Constructor ###
        public HomeViewModel()
        {
            SelectedFiltros = new List<string>();
        }
        #endregion

        #region ### Eventos ###
        public event EventHandler ContactDataDownloadCompleted;
        #endregion

        #region ### Funsiones ###
        

        public void LoadContacts()
        {
            var cons = new Contacts();
            EventHandler<ContactsSearchEventArgs> handler = null;
            handler = (s, e) =>
            {
                ContactsList = CreateGroupedOC(new ObservableCollection<Contact>(e.Results.Where(c => c.PhoneNumbers.Any(a => a.Kind == PhoneNumberKind.Mobile))));
                if (ContactDataDownloadCompleted != null)
                    ContactDataDownloadCompleted(this, EventArgs.Empty);
                cons.SearchCompleted -= handler;
            };
            cons.SearchCompleted += handler;
            cons.SearchAsync(String.Empty, FilterKind.None, "Contacts");
        }

        public static ObservableCollection<Group<Contact>> CreateGroupedOC(ObservableCollection<Contact> InitialContactsList)
        {
            var GroupedContacts = new ObservableCollection<Group<Contact>>();
            var SortedList = (from con in InitialContactsList
                              orderby con.DisplayName
                              select con).ToList();

            string Alpha = "#abcdefghijklmnopqrstuvwxyz";
            foreach (char c in Alpha)
            {
                Group<Contact> thisGOC = new Group<Contact>(c.ToString());
                var SubsetOfCons = (from con in SortedList
                                    where con.DisplayName.Substring(0, 1).ToLower() == c.ToString()
                                    select con).ToList();
                if (SubsetOfCons.Count > 0)
                {
                    SubsetOfCons.ForEach(d => thisGOC.Add(d));
                    GroupedContacts.Add(thisGOC);
                }
            }
            return GroupedContacts;
        }

        public List<string> ListKilometros()
        {
            return new List<string> { "NO", "1", "2", "5", "10", "20", "50", "100", "1000" };
        }

        public List<string> ListTamanioBebida()
        {
            return new List<string> { "Venti", "Grande", "Alto" };
        }

        public List<string> ListShots()
        {
            return new List<string> { "0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12" };
        }

        public List<string> ListLeche()
        {
            return new List<string> { "NO", "Entera", "Light", "Deslactosada", "Soya", "Half-half", "Deslactosada Light" };
        }

        public List<string> ListJarabe()
        {
            return new List<string> { "NO", "Vainilla", "Caramelo", "Cinnamon Dolce", "Clásico", "Avellana", "Menta", "Coco", "Almendra", "Frambuesa", "Vainilla Sugar Free", "Mocha", "Mocha Blanco" };
        }

        public List<string> ListToppings()
        {
            return new List<string> { "NO", "Crema Batida", "Espiral de Mocha", "Espiral de Caramelo", "Espiral de Cajeta", "Más Leche", "Splenda", "Canderel", "Azúcar", "Azúcar Mascabado", "Agua", "Hielo", "Canela en polvo", "Vainilla en polvo", "Mocha en polvo", "Chip" };
        }
        #endregion
    }
}
