﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Starbucks.UI.Data.Parser;

namespace Starbucks.UI.ViewModel
{
    public class CafesViewModel : ViewModelBase
    {
        public Model.Cafe SelectedItemCafe { get; set; }

        public CafesViewModel()
        {
            
        }

        public List<Model.Cafe> ObtenerListCafes()
        {
            return Cafes.ListCafes;
        }

        public List<string> ObtenerPerfilesCafe()
        {
            return Cafes.ObtenerPerfiles();
        }

        public List<Model.Cafe> ObtenerCafe(string perfil)
        {
            return Cafes.ObtenerListaCafes(perfil);
        }
    }
}
