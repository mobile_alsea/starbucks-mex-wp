﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Starbucks.UI.Data.Parser;
using Starbucks.UI.View.Alimentos;

namespace Starbucks.UI.ViewModel
{
    public class AlimentosViewModel : ViewModelBase
    {
        public AlimentosViewModel()
        {

        }

        public Model.Comida SelectedItemComida { get; set; }
        
        public List<Model.Comida> ObtenerComidas(string categoria)
        {
            return Comidas.ObtenerListaComida(categoria);
        }

        public List<Model.Comida> ObtenerTodasLasComidas()
        {
            return Comidas.ListComidas;
        }

        public List<string> ObtenerCategoriasComida()
        {
            return Comidas.ObtenerCategorias();
        }
    }
}
