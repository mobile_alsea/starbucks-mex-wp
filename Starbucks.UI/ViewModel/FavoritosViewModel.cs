﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO.IsolatedStorage;
using System.Linq;
using System.Text;
using Starbucks.UI.Data.Parser;
using Starbucks.UI.Helper;

namespace Starbucks.UI.ViewModel
{
    public class FavoritosViewModel : ViewModelBase
    {
        private ObservableCollection<Model.Cafe> _favoritoCafes;
        private ObservableCollection<Model.Comida> _favoritoComidas;
        private ObservableCollection<Model.Bebida> _favoritoBebidas;
        private ObservableCollection<Model.Tienda> _favoritoTienda;
        private ObservableCollection<Model.DetalleConstructor> _favoritoConstructor;
        private ObservableCollection<Model.DetalleConstructor> _favoritoConstructorAmigo;

        public ObservableCollection<Model.Tienda> FavoritoTiendas
        {
            get
            {
                return _favoritoTienda;
            }
            set
            {
                if (value != _favoritoTienda)
                {
                    _favoritoTienda = value;
                    NotifyPropertyChanged("FavoritoTiendas");
                }
            }
        }

        public ObservableCollection<Model.Bebida> FavoritoBebidas
        {
            get
            {
                return _favoritoBebidas;
            }
            set
            {
                if (value != _favoritoBebidas)
                {
                    _favoritoBebidas = value;
                    NotifyPropertyChanged("FavoritoBebidas");
                }
            }
        }

        public ObservableCollection<Model.Cafe> FavoritoCafes
        {
            get
            {
                return _favoritoCafes;
            }
            set
            {
                if (value != _favoritoCafes)
                {
                    _favoritoCafes = value;
                    NotifyPropertyChanged("FavoritoCafes");
                }
            }
        }

        public ObservableCollection<Model.Comida> FavoritoComidas
        {
            get
            {
                return _favoritoComidas;
            }
            set
            {
                if (value != _favoritoComidas)
                {
                    _favoritoComidas = value;
                    NotifyPropertyChanged("FavoritoComidas");
                }
            }
        }

        public ObservableCollection<Model.DetalleConstructor> FavoritoConstructor
        {
            get
            {
                return _favoritoConstructor;
            }
            set
            {
                if (value != _favoritoConstructor)
                {
                    _favoritoConstructor = value;
                    NotifyPropertyChanged("FavoritoConstructor");
                }
            }
        }

        public ObservableCollection<Model.DetalleConstructor> FavoritoConstructorAmigo
        {
            get
            {
                return _favoritoConstructorAmigo;
            }
            set
            {
                if (value != _favoritoConstructorAmigo)
                {
                    _favoritoConstructorAmigo = value;
                    NotifyPropertyChanged("FavoritoConstructorAmigo");
                }
            }
        }

        public FavoritosViewModel()
        {
            FavoritoCafes = new ObservableCollection<Model.Cafe>();
            FavoritoComidas = new ObservableCollection<Model.Comida>();
            FavoritoBebidas = new ObservableCollection<Model.Bebida>();
            FavoritoTiendas = new ObservableCollection<Model.Tienda>();
            FavoritoConstructor = new ObservableCollection<Model.DetalleConstructor>();
            FavoritoConstructorAmigo = new ObservableCollection<Model.DetalleConstructor>();
        }

        public void CargaFavoritos()
        {
            if (App.Settings.Contains(Constantes.ListFavoritoCafes))
            {
                var listCafe = ((ObservableCollection<Model.Cafe>)App.Settings[Constantes.ListFavoritoCafes]);
                FavoritoCafes = listCafe;
            }

            if (App.Settings.Contains(Constantes.ListFavoritoComidas))
            {
                var listComida = ((ObservableCollection<Model.Comida>)App.Settings[Constantes.ListFavoritoComidas]);
                FavoritoComidas = listComida;
            }

            if (App.Settings.Contains(Constantes.ListFavoritoConstructor))
            {
                var listBebidas = ((ObservableCollection<Model.DetalleConstructor>)App.Settings[Constantes.ListFavoritoConstructor]);
                FavoritoConstructor = listBebidas;
            }

            if (App.Settings.Contains(Constantes.ListFavoritoConstructorAmigos))
            {
                var listBebidasAmigos = ((ObservableCollection<Model.DetalleConstructor>)App.Settings[Constantes.ListFavoritoConstructorAmigos]);
                FavoritoConstructorAmigo = listBebidasAmigos;
            }

            if (App.Settings.Contains(Constantes.ListFavoritoTiendas))
            {
                var listTiendas = ((ObservableCollection<Model.Tienda>)App.Settings[Constantes.ListFavoritoTiendas]);
                FavoritoTiendas = listTiendas;
            }
        }
        
        public void AgregaFavorito<T>(T value, Accion accion)
        {
            switch (accion)
            {
                case Accion.Cafe:
                    var resCafe = value as Model.Cafe;
                    var entidadCafe = Cafes.ListCafes.Where(c => c.nombre.Equals(resCafe.nombre)).SingleOrDefault();
                    if (entidadCafe != null)
                    {
                        entidadCafe.isFavorito = true;
                        if (App.ViewModel.TileFlipCafeSelected != null)
                            App.ViewModel.TileFlipCafeSelected.isFavorito = true;
                    }
                    if (App.Settings.Contains(Constantes.ListFavoritoCafes))
                    {
                        var list = ((ObservableCollection<Model.Cafe>)App.Settings[Constantes.ListFavoritoCafes]);
                        if (!FavoritoCafes.Contains(resCafe))
                        {
                            list.Insert(0, resCafe);
                            FavoritoCafes = list;
                        }
                    }
                    else
                    {
                        App.Settings[Constantes.ListFavoritoCafes] = new ObservableCollection<Model.Cafe>();
                        ((ObservableCollection<T>)App.Settings[Constantes.ListFavoritoCafes]).Insert(0, value);
                        FavoritoCafes = ((ObservableCollection<Model.Cafe>)App.Settings[Constantes.ListFavoritoCafes]);
                    }
                    break;
                case Accion.Comida:
                    var resComida = value as Model.Comida;
                    var entidadComida = Comidas.ListComidas.Where(c => c.nombre.Equals(resComida.nombre)).SingleOrDefault();
                    if (entidadComida != null)
                    {
                        entidadComida.isFavorito = true;
                        if (App.ViewModel.TileFlipAlimentosSelected != null)
                            App.ViewModel.TileFlipAlimentosSelected.isFavorito = true;
                    }
                    if (App.Settings.Contains(Constantes.ListFavoritoComidas))
                    {
                        var list = ((ObservableCollection<Model.Comida>)App.Settings[Constantes.ListFavoritoComidas]);
                        if (!FavoritoComidas.Contains(resComida))
                        {
                            list.Insert(0, resComida);
                            FavoritoComidas = list;
                        }
                    }
                    else
                    {
                        App.Settings[Constantes.ListFavoritoComidas] = new ObservableCollection<Model.Comida>();
                        ((ObservableCollection<T>)App.Settings[Constantes.ListFavoritoComidas]).Insert(0, value);
                        FavoritoComidas = ((ObservableCollection<Model.Comida>)App.Settings[Constantes.ListFavoritoComidas]);
                    }
                    break;
                case Accion.Bebidas:
                    var resBebida = value as Model.Bebida;
                    var entidadBebida = Bebidas.ListBebidas.Where(c => c.nombre.Equals(resBebida.nombre)).SingleOrDefault();
                    if (entidadBebida != null)
                    {
                        entidadBebida.isFavorito = true;
                        if (App.ViewModel.TileFlipBebidaSelected != null)
                            App.ViewModel.TileFlipBebidaSelected.isFavorito = true;
                    }
                    if (App.Settings.Contains(Constantes.ListFavoritoBebidas))
                    {
                        var list = ((ObservableCollection<Model.Bebida>)App.Settings[Constantes.ListFavoritoBebidas]);
                        if (!FavoritoBebidas.Contains(resBebida))
                        {
                            list.Insert(0, resBebida);
                            FavoritoBebidas = list;
                        }
                    }
                    else
                    {
                        App.Settings[Constantes.ListFavoritoBebidas] = new ObservableCollection<Model.Bebida>();
                        ((ObservableCollection<T>)App.Settings[Constantes.ListFavoritoBebidas]).Insert(0, value);
                        FavoritoBebidas = ((ObservableCollection<Model.Bebida>)App.Settings[Constantes.ListFavoritoBebidas]);
                    }
                    break;
                case Accion.Tiendas:
                    var resTienda = value as Model.Tienda;
                    var entidadTienda = Tiendas.ListTiendas.Where(c => c.nombre.Equals(resTienda.nombre)).SingleOrDefault();
                    if (entidadTienda != null)
                    {
                        entidadTienda.isFavorito = true;
                        if (App.ViewModel.TileFlipTiendaSelected != null)
                            App.ViewModel.TileFlipTiendaSelected.isFavorito = true;
                    }
                    if (App.Settings.Contains(Constantes.ListFavoritoTiendas))
                    {
                        var list = ((ObservableCollection<Model.Tienda>)App.Settings[Constantes.ListFavoritoTiendas]);
                        if (!FavoritoTiendas.Contains(resTienda))
                        {
                            list.Insert(0, resTienda);
                            FavoritoTiendas = list;
                        }
                    }
                    else
                    {
                        App.Settings[Constantes.ListFavoritoTiendas] = new ObservableCollection<Model.Tienda>();
                        ((ObservableCollection<T>)App.Settings[Constantes.ListFavoritoTiendas]).Insert(0, value);
                        FavoritoTiendas = ((ObservableCollection<Model.Tienda>)App.Settings[Constantes.ListFavoritoTiendas]);
                    }
                    break;
                case Accion.Constructor:
                    var resConstructor = value as Model.DetalleConstructor;

                    if (resConstructor.IsFavorito)
                    {
                        var entidadBebidaConst = Bebidas.ListBebidas.Where(c => c.nombre.Equals(resConstructor.Nombre)).SingleOrDefault();
                        if (entidadBebidaConst != null)
                        {
                            entidadBebidaConst.isFavorito = true;
                            if (App.ViewModel.TileFlipBebidaSelected != null)
                                App.ViewModel.TileFlipBebidaSelected.isFavorito = true;
                        }
                    }

                    if (resConstructor.Propietario.Equals("Yo"))
                    {
                        if (App.Settings.Contains(Constantes.ListFavoritoConstructor))
                        {
                            var list = ((ObservableCollection<Model.DetalleConstructor>)App.Settings[Constantes.ListFavoritoConstructor]);
                            resConstructor.Id = list.Count.Equals(0) ? 1 : list.Select(c => c.Id).Max() + 1;

                            if (!FavoritoConstructor.Contains(resConstructor))
                            {
                                list.Insert(0, resConstructor);
                                FavoritoConstructor = list;
                            }
                        }
                        else
                        {
                            resConstructor.Id = 1;
                            App.Settings[Constantes.ListFavoritoConstructor] = new ObservableCollection<Model.DetalleConstructor>();
                            ((ObservableCollection<Model.DetalleConstructor>)App.Settings[Constantes.ListFavoritoConstructor]).Insert(0, resConstructor);
                            FavoritoConstructor = ((ObservableCollection<Model.DetalleConstructor>)App.Settings[Constantes.ListFavoritoConstructor]);
                        }
                    }
                    else
                    {
                        if (App.Settings.Contains(Constantes.ListFavoritoConstructorAmigos))
                        {
                            var list = ((ObservableCollection<Model.DetalleConstructor>)App.Settings[Constantes.ListFavoritoConstructorAmigos]);
                            resConstructor.Id = list.Count.Equals(0) ? 1 : list.Select(c => c.Id).Max() + 1;
                            if (!FavoritoConstructorAmigo.Contains(resConstructor))
                            {
                                list.Insert(0, resConstructor);
                                FavoritoConstructorAmigo = list;
                            }
                        }
                        else
                        {
                            App.Settings[Constantes.ListFavoritoConstructorAmigos] = new ObservableCollection<Model.DetalleConstructor>();
                            ((ObservableCollection<T>)App.Settings[Constantes.ListFavoritoConstructorAmigos]).Insert(0, value);
                            FavoritoConstructorAmigo = ((ObservableCollection<Model.DetalleConstructor>)App.Settings[Constantes.ListFavoritoConstructorAmigos]);
                        }
                    }
                    break;
                default:
                    break;
            }
        }

        public void EliminaFavorito<T>(T value, Accion accion)
        {
            switch (accion)
            {
                case Accion.Cafe:
                    var resCafe = value as Model.Cafe;
                    var entidadCafes = Cafes.ListCafes.Where(c => c.nombre.Equals(resCafe.nombre)).SingleOrDefault();
                    if (entidadCafes != null)
                    {
                        entidadCafes.isFavorito = false;
                        if (App.ViewModel.TileFlipCafeSelected != null)
                            App.ViewModel.TileFlipCafeSelected.isFavorito = false;
                    }
                    if (App.Settings.Contains(Constantes.ListFavoritoCafes))
                    {
                        var item = FavoritoCafes.Where(c => c.nombre.Equals(resCafe.nombre)).SingleOrDefault();
                        if (item != null)
                            FavoritoCafes.Remove(item);
                    }
                    break;
                case Accion.Comida:
                    var resComida = value as Model.Comida;
                    var entidadComida = Comidas.ListComidas.Where(c => c.nombre.Equals(resComida.nombre)).SingleOrDefault();
                    if (entidadComida != null)
                    {
                        entidadComida.isFavorito = false;
                        if (App.ViewModel.TileFlipAlimentosSelected != null)
                            App.ViewModel.TileFlipAlimentosSelected.isFavorito = false;
                    }
                    if (App.Settings.Contains(Constantes.ListFavoritoComidas))
                    {
                        var item = FavoritoComidas.Where(c => c.nombre.Equals(resComida.nombre)).SingleOrDefault();
                        if (item != null)
                            FavoritoComidas.Remove(item);
                    }
                    break;
                case Accion.Bebidas:
                    var resBebida = value as Model.Bebida;
                    var entidadBebida = Bebidas.ListBebidas.Where(c => c.nombre.Equals(resBebida.nombre)).SingleOrDefault();
                    if (entidadBebida != null)
                    {
                        entidadBebida.isFavorito = false;
                        if (App.ViewModel.TileFlipBebidaSelected != null)
                            App.ViewModel.TileFlipBebidaSelected.isFavorito = false;
                    }
                    if (App.Settings.Contains(Constantes.ListFavoritoBebidas))
                    {
                        var item = FavoritoBebidas.Where(c => c.nombre.Equals(resBebida.nombre)).SingleOrDefault();
                        if (item != null)
                            FavoritoBebidas.Remove(item);
                    }
                    break;
                case Accion.Tiendas:
                    var resTienda = value as Model.Tienda;
                    var entidadTienda = Tiendas.ListTiendas.Where(c => c.nombre.Equals(resTienda.nombre)).SingleOrDefault();
                    if (entidadTienda != null)
                    {
                        entidadTienda.isFavorito = false;
                        if (App.ViewModel.TileFlipTiendaSelected != null)
                            App.ViewModel.TileFlipTiendaSelected.isFavorito = false;
                    }
                    if (App.Settings.Contains(Constantes.ListFavoritoTiendas))
                    {
                        var item = FavoritoTiendas.Where(c => c.nombre.Equals(resTienda.nombre)).SingleOrDefault();
                        if (item != null)
                            FavoritoTiendas.Remove(item);
                    }
                    break;
                case Accion.Constructor:
                    var resConstructor = value as Model.DetalleConstructor;
                    if (resConstructor.IsFavorito)
                    {
                        var entidadConstructor = Bebidas.ListBebidas.Where(c => c.nombre.Equals(resConstructor.Nombre)).SingleOrDefault();
                        if (entidadConstructor != null)
                        {
                            entidadConstructor.isFavorito = false;
                            if (App.ViewModel.TileFlipBebidaSelected != null)
                                App.ViewModel.TileFlipBebidaSelected.isFavorito = false;
                        }
                    }

                    if (resConstructor.Propietario.Equals("Yo"))
                    {
                        if (App.Settings.Contains(Constantes.ListFavoritoConstructor))
                        {
                            if (resConstructor.Id <= 0)
                            {
                                var id = FavoritoConstructor.Where(c => c.Nombre.Equals(resConstructor.Nombre) && c.IsFavorito.Equals(true)).SingleOrDefault();
                                if (id != null)
                                {
                                    var item = FavoritoConstructor.Where(c => c.Id.Equals(id.Id)).SingleOrDefault();
                                    if (item != null)
                                        FavoritoConstructor.Remove(item);
                                }
                            }
                            else
                            {
                                var item = FavoritoConstructor.Where(c => c.Id.Equals(resConstructor.Id)).SingleOrDefault();
                                if (item != null)
                                    FavoritoConstructor.Remove(item);
                            }
                        }
                    }
                    else
                    {
                        if (App.Settings.Contains(Constantes.ListFavoritoConstructorAmigos))
                        {
                            if (resConstructor.Id <= 0)
                            {
                                var item = FavoritoConstructorAmigo.Where(c => c.Id.Equals(resConstructor.Id)).SingleOrDefault();
                                if (item != null)
                                    FavoritoConstructorAmigo.Remove(item);
                            }
                            else
                            {
                                var item = FavoritoConstructorAmigo.Where(c => c.Id.Equals(resConstructor.Id)).SingleOrDefault();
                                if (item != null)
                                    FavoritoConstructorAmigo.Remove(item);
                            }
                        }
                    }
                    break;
                default:
                    break;
            }
        }
    }
}
