﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Starbucks.UI.Helper
{
    public enum Accion
    {
        Cafe = 1,
        Comida = 2,
        Bebidas = 3,
        Tiendas = 4,
        Constructor = 5,
        Usuario = 6,
        Version = 7
    }
}
