﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using XNARectangle = Microsoft.Xna.Framework;

namespace Starbucks.UI.Helper
{
    /// <summary>
    /// Tools Class
    /// </summary>
    public class RectRender
    {
        #region internal data
        private List<XNARectangle.Rectangle> _usedRectList;
        private XNARectangle.Rectangle _wholeArea;
        private const int RECT_GAP = 4;
        #endregion

        #region constructor
        /// <summary>
        /// Constructor.
        /// </summary>
        public RectRender()
        {
            _usedRectList = new List<XNARectangle.Rectangle>();
        }

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="left">The x-coordinate of left edge of the region</param>
        /// <param name="top">The y-coordinate of top edge of the region</param>
        /// <param name="width">The width of the region</param>
        /// <param name="height">The height of the region</param>
        public RectRender(int left, int top, int width, int height)
        {
            _usedRectList = new List<XNARectangle.Rectangle>();
            _wholeArea = new XNARectangle.Rectangle(left, top, width, height);
        }
        #endregion

        #region public methods
        /// <summary>
        /// Get the valid(unused) region.
        /// </summary>
        /// <param name="rect">The rectangle to test</param>
        /// <returns>
        /// Returns a third Rectangle structure that doesn't intersect  
        /// with any other Rectangle structure. If there is no intersection,
        /// an empty Rectangle is returned.
        /// </returns>
        public XNARectangle.Rectangle GetValidRectangle(XNARectangle.Rectangle rect)
        {
            XNARectangle.Rectangle intersectRect = Intersect(rect);

            if (intersectRect.IsEmpty)
            {
                return rect;
            }
            else
            {
                XNARectangle.Rectangle tmpRect = rect;
                tmpRect.Y = intersectRect.Bottom + RECT_GAP;//Down

                if (Intersect(tmpRect).IsEmpty)
                {
                    return tmpRect;
                }
                else
                {
                    tmpRect = rect;
                    tmpRect.Y = intersectRect.Top - RECT_GAP;//Up
                    if (Intersect(tmpRect).IsEmpty)
                    {
                        return tmpRect;
                    }
                    else
                    {
                        tmpRect = rect;
                        tmpRect.X = intersectRect.X - RECT_GAP - tmpRect.Width;//Left
                        if (Intersect(tmpRect).IsEmpty)
                        {
                            return tmpRect;
                        }
                        else
                        {
                            tmpRect = rect;
                            tmpRect.X = intersectRect.Right + RECT_GAP;//Right
                            if (Intersect(tmpRect).IsEmpty)
                            {
                                return tmpRect;
                            }
                        }
                    }
                }
            }

            return new XNARectangle.Rectangle();
        }

        /// <summary>
        /// Set the whole valid region.
        /// </summary>
        /// <param name="left">The x-coordinate of left edge of the region</param>
        /// <param name="top">The y-coordinate of top edge of the region</param>
        /// <param name="width">The width of the region</param>
        /// <param name="height">The height of the region</param>
        /// <returns>
        /// No return
        /// </returns>
        public void SetRectArea(int left, int top, int width, int height)
        {
            _wholeArea = new XNARectangle.Rectangle(left, top, width, height);
        }

        /// <summary>
        /// Add the used rectangle in this region.
        /// </summary>
        /// <param name="rect">The used rectangle</param>
        /// <returns>
        /// No return
        /// </returns>
        public void AddUsedRect(XNARectangle.Rectangle rect)
        {
            _usedRectList.Add(rect);
        }

        /// <summary>
        /// Removes all used rectangles in this region.
        /// </summary>
        /// <returns>
        /// No return
        /// </returns>
        public void ClearUsedRect()
        {
            _usedRectList.Clear();
        }
        #endregion

        #region private methods
        /// <summary>
        /// Determines if this rectangle intersects with rect.
        /// </summary>
        /// <param name="rect">The rectangle to test</param>
        /// <returns>
        /// Returns a third Rectangle structure that represents the intersection
        /// with the Rectangle structure to test. If there is no intersection,
        /// an empty Rectangle is returned.
        /// </returns>
        private XNARectangle.Rectangle Intersect(XNARectangle.Rectangle rect)
        {
            XNARectangle.Rectangle intersectRect = new XNARectangle.Rectangle();

            foreach (XNARectangle.Rectangle item in _usedRectList)
            {
                intersectRect = XNARectangle.Rectangle.Intersect(item, rect);
                if (!intersectRect.IsEmpty)//Intersect
                {
                    return item;
                }
            }

            return intersectRect;
        }

        #endregion

    }
}
