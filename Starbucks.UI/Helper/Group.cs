﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;

namespace Starbucks.UI.Helper
{
    public class Group<T> : ObservableCollection<T>
    {
        public string Title { get; set; }

        public bool HasItems { get { return Count != 0; } private set { } }

        public Group(string name)
        {
            this.Title = name;
        }
    }
}
