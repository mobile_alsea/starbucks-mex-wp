﻿using System;
using System.Collections.Generic;
using System.Device.Location;
using System.Linq;
using System.Text;
using Microsoft.Phone.Tasks;

namespace Starbucks.UI.Helper
{
    public class Mapa
    {
        public static void ObtenerDireccion()
        {
            var bingMapsDirectionsTask = new BingMapsDirectionsTask();
            var spaceNeedleLocation = new GeoCoordinate(Convert.ToDouble(App.TiendasViewModel.SelectedItemTienda.latitud), Convert.ToDouble(App.TiendasViewModel.SelectedItemTienda.longitud));
            var spaceNeedleLML = new LabeledMapLocation(App.TiendasViewModel.SelectedItemTienda.nombre, spaceNeedleLocation);
            bingMapsDirectionsTask.Start = new LabeledMapLocation("yo", App.TiendasViewModel.MyLocation); 
            bingMapsDirectionsTask.End = spaceNeedleLML;
            bingMapsDirectionsTask.Show();
        }
    }
}
