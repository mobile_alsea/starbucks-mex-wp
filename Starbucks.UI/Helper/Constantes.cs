﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Starbucks.UI.Helper
{
    public class Constantes
    {
        public static string VistaTwitter = "/View/Rewards/LoginTwitter.xaml";

        public const string TemplateAlimentos = @"<DataTemplate xmlns=""http://schemas.microsoft.com/client/2007"">
                    <Grid Width=""480"" Height=""120"">
                        <Rectangle Fill=""{StaticResource PhoneInactiveBrush}"" HorizontalAlignment=""Left"" Width=""100"" Height=""100"" VerticalAlignment=""Top"" Margin=""12,10,0,0"" />
                        <Image Height=""100"" Margin=""12,10,368,10"" Source=""{Binding imagen}"" />
                        <StackPanel HorizontalAlignment=""Left"" Height=""71"" VerticalAlignment=""Top"" Width=""355"" Margin=""125,0,0,0"">
                            <TextBlock TextWrapping=""Wrap"" FontSize=""33.333"" Text=""{Binding nombre}"" Foreground=""#FF71AD2A""/>
                        </StackPanel>
                        <TextBlock HorizontalAlignment=""Left"" TextWrapping=""Wrap"" VerticalAlignment=""Top"" Margin=""125,78,0,0"" FontSize=""24"" Text=""{Binding disponibilidad}"" Foreground=""#FFFFFFF1""/>
                    </Grid>
                </DataTemplate>";
        public const string Desc = "desc";
        public const string Bebida = "bebida";
        public const string Filtro = "fitro";
        public const string TemplateCafes = @"<DataTemplate xmlns=""http://schemas.microsoft.com/client/2007"">
                    <Grid Width=""480"" Height=""120"">
                        <Rectangle Fill=""{StaticResource PhoneInactiveBrush}"" HorizontalAlignment=""Left"" Width=""100"" Height=""100"" VerticalAlignment=""Top"" Margin=""12,10,0,0"" />
                        <Image Height=""100"" Margin=""12,10,368,10"" Source=""{Binding imagenprincipal}"" Stretch=""UniformToFill""/>
                        <StackPanel HorizontalAlignment=""Left"" Height=""71"" VerticalAlignment=""Top"" Width=""355"" Margin=""125,0,0,0"">
                            <TextBlock TextWrapping=""Wrap"" FontSize=""33.333"" Text=""{Binding nombre}"" Foreground=""#FF71AD2A""/>
                        </StackPanel>
                        <TextBlock HorizontalAlignment=""Left"" TextWrapping=""Wrap"" VerticalAlignment=""Top"" Margin=""125,78,0,0"" FontSize=""24"" Text=""{Binding saboresDesc }"" Foreground=""#FFFFFFF1""/>
                    </Grid>
                </DataTemplate>";
        public const string ListFavoritoCafes = "listFavoritoCafes";
        public const string ListFavoritoComidas = "listFavoritoComidas";
        public const string ListFavoritoBebidas = "listFavoritoBebidas";
        public const string ListFavoritoTiendas = "listFavoritoTiendas";
        public const string ListFavoritoConstructor = "listFavoritoConstructor";
        public const string ListFavoritoConstructorAmigos = "listFavoritoConstructorAmigos";
        public const string LocalizaTiendas = "localizaTiendas";
        public const string Radio_Cinco = "5";
        public const string Switch_RA = "switch_RA";
        public const string MostrarMensaje_RA = "Mensaje_RA";
        public const string MostrarMensaje_GPS = "Mensaje_GPS";
        public const string TombstonedBebidas = "TombstonedBebidas";
        public const string TombstonedCafes = "TombstonedCafes";
        public const string TombstonedComidas = "TombstonedComidas";
        public const string TombstonedTiendas = "TombstonedTiendas";
    }
}
