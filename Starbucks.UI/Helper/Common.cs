﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using Microsoft.Phone.Tasks;

namespace Starbucks.UI.Helper
{
    public class Common
    {
        public static void EnviarEmail(string asunto, string mensaje, string para)
        {
            var emailComposeTask = new EmailComposeTask();
            emailComposeTask.Subject = asunto;
            emailComposeTask.Body = mensaje;
            emailComposeTask.To = para;
            emailComposeTask.Show();
        }

        public static void EnviaSms(string mensaje, string numero)
        {
            var smsComposeTask = new SmsComposeTask();
            smsComposeTask.To = numero;
            smsComposeTask.Body = mensaje;
            smsComposeTask.Show();
        }

        public static void Llamada(string nombre, string numero)
        {
            var phoneCallTask = new PhoneCallTask();
            phoneCallTask.PhoneNumber = numero;
            phoneCallTask.DisplayName = nombre;
            phoneCallTask.Show();
        }

        public static string XmlDataSerializer<T>(T source) where T : class, new()
        {
            try
            {
                MemoryStream ms = new MemoryStream();
                DataContractSerializer ser =
                    new DataContractSerializer(typeof(T));
                ser.WriteObject(ms, source);
                string xmlResult = Encoding.UTF8.GetString(ms.ToArray(), 0, (int)ms.Length);
                return xmlResult;
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine("<<< Error:[Deserialize] [{0}] >>> : {1}", ex.StackTrace, ex.Message);
                throw (ex);
            }
        }

        public static T XmlDataDeserialize<T>(string source) where T : class, new()
        {
            try
            {
                MemoryStream ms = new MemoryStream();
                byte[] buffer = Encoding.UTF8.GetBytes(source);
                ms.Write(buffer, 0, buffer.Length);
                ms.Position = 0;
                DataContractSerializer ser = new DataContractSerializer(typeof(T));
                T deserializedPerson =
                    (T)ser.ReadObject(ms);
                return deserializedPerson;
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine("<<< Error:[Deserialize] [{0}] >>> : {1}", ex.StackTrace, ex.Message);
                throw (ex);
            }
        }
    }
}
