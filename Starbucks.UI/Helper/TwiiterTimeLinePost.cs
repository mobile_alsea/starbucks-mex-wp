﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace Starbucks.UI.Helper
{
    public class TwiiterTimeLinePost
    {
        private string sharerUrl { get; set; }
        public string titulo { get; set; }
        public string message { get; set; }
        public TwiiterTimeLinePost()
        {
            
            //sharerUrl = "http://twitter.com/home?status=";
            sharerUrl = "https://twitter.com/intent/tweet?text=";

        }

        public string getParameters()
        {
            string regreso;
            regreso = sharerUrl + titulo + " " + message;

            return regreso;
        }
    }
}
