﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Data;
using Microsoft.Phone.UserData;

namespace Starbucks.UI.Helper.Converter
{
    public class PhoneNumbersToMobileConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            var phones = value as IEnumerable<ContactPhoneNumber>;
            return phones != null ? phones.First(p => p.Kind == PhoneNumberKind.Mobile).PhoneNumber : null;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
