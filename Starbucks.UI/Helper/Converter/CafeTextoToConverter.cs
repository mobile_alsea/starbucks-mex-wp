﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Data;

namespace Starbucks.UI.Helper.Converter
{
    public class CafeTextoToConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            var perfil = value.ToString();
            var color = string.Empty;
            switch (perfil)
            {
                case "Dark":
                    color = "#FF62072F";
                    break;
                case "Temporada":
                    color = "#FF6CB33F";
                    break;
                case "Medium":
                    color = "#FF9D4A0D";
                    break;
                case "Blonde":
                    color = "#FFE7A613";
                    break;
                default:
                    break;
            }
            return color;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
