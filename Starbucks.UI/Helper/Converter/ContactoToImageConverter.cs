﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Data;
using System.Windows.Media.Imaging;
using Microsoft.Phone.UserData;

namespace Starbucks.UI.Helper.Converter
{
    public class ContactoToImageConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (!(value is Contact)) return null;
            var contact = value as Contact;
            var source = new BitmapImage();
            var picture = contact.GetPicture();
            if (picture != null)
                source.SetSource(picture);
            else
                source.UriSource = new Uri("/Resources/arte/dummie.png", UriKind.Relative);
            return source;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
