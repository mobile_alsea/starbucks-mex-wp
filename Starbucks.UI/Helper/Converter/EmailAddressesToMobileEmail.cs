﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Data;
using Microsoft.Phone.UserData;

namespace Starbucks.UI.Helper.Converter
{
    public class EmailAddressesToMobileEmail : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            var emails = value as IEnumerable<ContactEmailAddress>;
            return emails != null ? emails.First(p => p.Kind == EmailAddressKind.Personal).EmailAddress : null;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
