﻿#pragma checksum "C:\Users\lvazquez\Desktop\V2_PT\Starbucks.UI\View\Rewards\RecuperaPassword.xaml" "{406ea660-64cf-4c82-b6f0-42d48172a799}" "4D6880E012EE5E38C3A6EFBE0E818923"
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.18449
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using System;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Automation.Peers;
using System.Windows.Automation.Provider;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Interop;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Resources;
using System.Windows.Shapes;
using System.Windows.Threading;


namespace Starbucks.UI.View.Rewards {
    
    
    public partial class RecuperaPassword : Microsoft.Phone.Controls.PhoneApplicationPage {
        
        internal System.Windows.Controls.Grid LayoutRoot;
        
        internal System.Windows.Controls.ProgressBar progress;
        
        internal System.Windows.Controls.TextBlock txtTitulo;
        
        internal System.Windows.Controls.Grid ContentPanel;
        
        internal System.Windows.Controls.TextBlock lblTitulo;
        
        internal System.Windows.Controls.WatermarkTextBox txtUsuario;
        
        internal System.Windows.Controls.WatermarkTextBox txtTarjeta;
        
        internal System.Windows.Controls.Grid pnlInfo;
        
        internal System.Windows.Controls.TextBox txtPassword;
        
        internal System.Windows.Controls.TextBlock lblEmail;
        
        internal Microsoft.Phone.Shell.ApplicationBarIconButton ApbGenerarPassword;
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Windows.Application.LoadComponent(this, new System.Uri("/Starbucks.UI;component/View/Rewards/RecuperaPassword.xaml", System.UriKind.Relative));
            this.LayoutRoot = ((System.Windows.Controls.Grid)(this.FindName("LayoutRoot")));
            this.progress = ((System.Windows.Controls.ProgressBar)(this.FindName("progress")));
            this.txtTitulo = ((System.Windows.Controls.TextBlock)(this.FindName("txtTitulo")));
            this.ContentPanel = ((System.Windows.Controls.Grid)(this.FindName("ContentPanel")));
            this.lblTitulo = ((System.Windows.Controls.TextBlock)(this.FindName("lblTitulo")));
            this.txtUsuario = ((System.Windows.Controls.WatermarkTextBox)(this.FindName("txtUsuario")));
            this.txtTarjeta = ((System.Windows.Controls.WatermarkTextBox)(this.FindName("txtTarjeta")));
            this.pnlInfo = ((System.Windows.Controls.Grid)(this.FindName("pnlInfo")));
            this.txtPassword = ((System.Windows.Controls.TextBox)(this.FindName("txtPassword")));
            this.lblEmail = ((System.Windows.Controls.TextBlock)(this.FindName("lblEmail")));
            this.ApbGenerarPassword = ((Microsoft.Phone.Shell.ApplicationBarIconButton)(this.FindName("ApbGenerarPassword")));
        }
    }
}

