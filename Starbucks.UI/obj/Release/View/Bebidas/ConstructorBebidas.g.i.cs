﻿#pragma checksum "C:\Users\lvazquez\Desktop\V2_PT\Starbucks.UI\View\Bebidas\ConstructorBebidas.xaml" "{406ea660-64cf-4c82-b6f0-42d48172a799}" "E21C1C7784B6FFEB3EB989C2FB5980B3"
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.18449
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using Microsoft.Phone.Controls;
using Microsoft.Phone.Controls.Primitives;
using Microsoft.Phone.Shell;
using Starbucks.UI;
using System;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Automation.Peers;
using System.Windows.Automation.Provider;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Interop;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Resources;
using System.Windows.Shapes;
using System.Windows.Threading;


namespace Starbucks.UI.View.Bebidas {
    
    
    public partial class ConstructorBebidas : Microsoft.Phone.Controls.PhoneApplicationPage {
        
        internal Microsoft.Phone.Shell.ApplicationBarIconButton ApbInfo;
        
        internal Microsoft.Phone.Shell.ApplicationBarIconButton ApbGuardar;
        
        internal Microsoft.Phone.Shell.ApplicationBarIconButton ApbDetalle;
        
        internal System.Windows.Controls.Grid LayoutRoot;
        
        internal Microsoft.Phone.Controls.Primitives.LoopingSelector sTamanio;
        
        internal Starbucks.UI.VasoControl ctrVaso1;
        
        internal Microsoft.Phone.Controls.Primitives.LoopingSelector sShots;
        
        internal Starbucks.UI.VasoControl ctrVaso2;
        
        internal Microsoft.Phone.Controls.Primitives.LoopingSelector sLeche;
        
        internal Starbucks.UI.VasoControl ctrVaso3;
        
        internal Microsoft.Phone.Controls.Primitives.LoopingSelector sJarabe;
        
        internal Microsoft.Phone.Controls.Primitives.LoopingSelector sJarabeShots;
        
        internal System.Windows.Controls.Border brdJarabe;
        
        internal Starbucks.UI.VasoControl ctrVaso4;
        
        internal Microsoft.Phone.Controls.Primitives.LoopingSelector sToppings;
        
        internal Microsoft.Phone.Controls.Primitives.LoopingSelector sToppingsShots;
        
        internal System.Windows.Controls.Border brdToppings;
        
        internal Starbucks.UI.VasoControl ctrVaso5;
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Windows.Application.LoadComponent(this, new System.Uri("/Starbucks.UI;component/View/Bebidas/ConstructorBebidas.xaml", System.UriKind.Relative));
            this.ApbInfo = ((Microsoft.Phone.Shell.ApplicationBarIconButton)(this.FindName("ApbInfo")));
            this.ApbGuardar = ((Microsoft.Phone.Shell.ApplicationBarIconButton)(this.FindName("ApbGuardar")));
            this.ApbDetalle = ((Microsoft.Phone.Shell.ApplicationBarIconButton)(this.FindName("ApbDetalle")));
            this.LayoutRoot = ((System.Windows.Controls.Grid)(this.FindName("LayoutRoot")));
            this.sTamanio = ((Microsoft.Phone.Controls.Primitives.LoopingSelector)(this.FindName("sTamanio")));
            this.ctrVaso1 = ((Starbucks.UI.VasoControl)(this.FindName("ctrVaso1")));
            this.sShots = ((Microsoft.Phone.Controls.Primitives.LoopingSelector)(this.FindName("sShots")));
            this.ctrVaso2 = ((Starbucks.UI.VasoControl)(this.FindName("ctrVaso2")));
            this.sLeche = ((Microsoft.Phone.Controls.Primitives.LoopingSelector)(this.FindName("sLeche")));
            this.ctrVaso3 = ((Starbucks.UI.VasoControl)(this.FindName("ctrVaso3")));
            this.sJarabe = ((Microsoft.Phone.Controls.Primitives.LoopingSelector)(this.FindName("sJarabe")));
            this.sJarabeShots = ((Microsoft.Phone.Controls.Primitives.LoopingSelector)(this.FindName("sJarabeShots")));
            this.brdJarabe = ((System.Windows.Controls.Border)(this.FindName("brdJarabe")));
            this.ctrVaso4 = ((Starbucks.UI.VasoControl)(this.FindName("ctrVaso4")));
            this.sToppings = ((Microsoft.Phone.Controls.Primitives.LoopingSelector)(this.FindName("sToppings")));
            this.sToppingsShots = ((Microsoft.Phone.Controls.Primitives.LoopingSelector)(this.FindName("sToppingsShots")));
            this.brdToppings = ((System.Windows.Controls.Border)(this.FindName("brdToppings")));
            this.ctrVaso5 = ((Starbucks.UI.VasoControl)(this.FindName("ctrVaso5")));
        }
    }
}

