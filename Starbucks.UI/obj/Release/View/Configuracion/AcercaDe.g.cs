﻿#pragma checksum "C:\Users\lvazquez\Desktop\V2_PT\Starbucks.UI\View\Configuracion\AcercaDe.xaml" "{406ea660-64cf-4c82-b6f0-42d48172a799}" "50FD1EB120B18C9ADBFA9E26CD5FC100"
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.18449
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using Microsoft.Phone.Controls;
using System;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Automation.Peers;
using System.Windows.Automation.Provider;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Interop;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Resources;
using System.Windows.Shapes;
using System.Windows.Threading;


namespace Starbucks.UI.View.Configuracion {
    
    
    public partial class AcercaDe : Microsoft.Phone.Controls.PhoneApplicationPage {
        
        internal System.Windows.Controls.Grid LayoutRoot;
        
        internal System.Windows.Controls.Grid ContentPanel;
        
        internal System.Windows.Controls.TextBlock txtTitulo;
        
        internal System.Windows.Controls.Button btnAvisoPrivacidad;
        
        internal System.Windows.Controls.Button btnTerminosCondiciones;
        
        internal System.Windows.Controls.Button btnMail;
        
        internal System.Windows.Controls.TextBlock lblVersion;
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Windows.Application.LoadComponent(this, new System.Uri("/Starbucks.UI;component/View/Configuracion/AcercaDe.xaml", System.UriKind.Relative));
            this.LayoutRoot = ((System.Windows.Controls.Grid)(this.FindName("LayoutRoot")));
            this.ContentPanel = ((System.Windows.Controls.Grid)(this.FindName("ContentPanel")));
            this.txtTitulo = ((System.Windows.Controls.TextBlock)(this.FindName("txtTitulo")));
            this.btnAvisoPrivacidad = ((System.Windows.Controls.Button)(this.FindName("btnAvisoPrivacidad")));
            this.btnTerminosCondiciones = ((System.Windows.Controls.Button)(this.FindName("btnTerminosCondiciones")));
            this.btnMail = ((System.Windows.Controls.Button)(this.FindName("btnMail")));
            this.lblVersion = ((System.Windows.Controls.TextBlock)(this.FindName("lblVersion")));
        }
    }
}

