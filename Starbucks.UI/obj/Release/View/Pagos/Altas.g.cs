﻿#pragma checksum "C:\Users\lvazquez\Desktop\V2_PT\Starbucks.UI\View\Pagos\Altas.xaml" "{406ea660-64cf-4c82-b6f0-42d48172a799}" "7D2FE40E096EEA8203911ED753365478"
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.18449
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using Microsoft.Phone.Controls;
using System;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Automation.Peers;
using System.Windows.Automation.Provider;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Interop;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Resources;
using System.Windows.Shapes;
using System.Windows.Threading;


namespace Starbucks.UI.View.Pagos {
    
    
    public partial class Altas : Microsoft.Phone.Controls.PhoneApplicationPage {
        
        internal System.Windows.Controls.Grid LayoutRoot;
        
        internal System.Windows.DataTemplate PickerFullModeItemTemplate;
        
        internal System.Windows.DataTemplate PickerItemTemplate;
        
        internal System.Windows.Controls.StackPanel pagopanel;
        
        internal Microsoft.Phone.Controls.ListPicker tarjetalist;
        
        internal System.Windows.Controls.WatermarkTextBox nombretxt;
        
        internal System.Windows.Controls.WatermarkTextBox numertotxt;
        
        internal System.Windows.Controls.WatermarkTextBox seguridadtxt;
        
        internal System.Windows.Controls.Image cvvimage;
        
        internal Microsoft.Phone.Controls.ListPicker meslist;
        
        internal Microsoft.Phone.Controls.ListPicker anoslist;
        
        internal Microsoft.Phone.Controls.ToggleSwitch switchRecarga;
        
        internal System.Windows.Controls.Button continuarbtn;
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Windows.Application.LoadComponent(this, new System.Uri("/Starbucks.UI;component/View/Pagos/Altas.xaml", System.UriKind.Relative));
            this.LayoutRoot = ((System.Windows.Controls.Grid)(this.FindName("LayoutRoot")));
            this.PickerFullModeItemTemplate = ((System.Windows.DataTemplate)(this.FindName("PickerFullModeItemTemplate")));
            this.PickerItemTemplate = ((System.Windows.DataTemplate)(this.FindName("PickerItemTemplate")));
            this.pagopanel = ((System.Windows.Controls.StackPanel)(this.FindName("pagopanel")));
            this.tarjetalist = ((Microsoft.Phone.Controls.ListPicker)(this.FindName("tarjetalist")));
            this.nombretxt = ((System.Windows.Controls.WatermarkTextBox)(this.FindName("nombretxt")));
            this.numertotxt = ((System.Windows.Controls.WatermarkTextBox)(this.FindName("numertotxt")));
            this.seguridadtxt = ((System.Windows.Controls.WatermarkTextBox)(this.FindName("seguridadtxt")));
            this.cvvimage = ((System.Windows.Controls.Image)(this.FindName("cvvimage")));
            this.meslist = ((Microsoft.Phone.Controls.ListPicker)(this.FindName("meslist")));
            this.anoslist = ((Microsoft.Phone.Controls.ListPicker)(this.FindName("anoslist")));
            this.switchRecarga = ((Microsoft.Phone.Controls.ToggleSwitch)(this.FindName("switchRecarga")));
            this.continuarbtn = ((System.Windows.Controls.Button)(this.FindName("continuarbtn")));
        }
    }
}

