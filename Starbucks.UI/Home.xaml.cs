﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Device.Location;
using System.IO;
using System.IO.IsolatedStorage;
using System.Linq;
using System.Net;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Interactivity;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Resources;
using System.Windows.Threading;
using Coding4Fun.Phone.Controls;
using JeffWilcox.Utilities.Silverlight;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Controls.Maps;
using Microsoft.Phone.Info;
using Microsoft.Phone.Shell;
using Neodynamic.WP.Barcode;
using Newtonsoft.Json;
using Spritehand.FarseerHelper;
using Spritehand.PhysicsBehaviors;
using Starbucks.DAL;
using Starbucks.Entities;
using Starbucks.UI.Controls;
using Starbucks.UI.Data.Parser;
using Starbucks.UI.Helper;
using Starbucks.UI.Model;
using ZXing;
using ZXing.Common; 
using ZXing.PDF417;
using System.Diagnostics;
using Microsoft.Phone.Notification;
using System.Security.Cryptography;
using Starbucks.Utilities;
using System.Device.Location;


namespace Starbucks.UI
{
    public partial class Home : PhoneApplicationPage
    {
        IsolatedStorageFile myFile = IsolatedStorageFile.GetUserStoreForApplication();
        WSRewardsViewModel wsRewardstClient = new WSRewardsViewModel();
        private bool isLoad;
        private string[] randomNumeric = new string[] { "1", "2", "3", "4", "5" };
        private string[] randomNumericCafeAlimento = new string[] { "1", "2", "3", "4" };
        private string[] randomNumericTiendas = new string[] { "1", "2", "3", "4", "5" };
        private int currentTile;
        private int currentTileCafes;
        private int currentTileComida;
        private int currentTileTienda;

        private int currentImage1;
        private int currentImage2;
        private int currentImage3;
        private int currentImage4;
        private int currentImage5;
        private bool _front1;
        private bool _front2;
        private bool _front3;
        private bool _front4;
        private bool _front5;

        private int currentCafeImage1;
        private int currentCafeImage3;
        private int currentCafeImage4;
        private int currentCafeImage5;
        private bool _frontCafe1;
        private bool _frontCafe3;
        private bool _frontCafe4;
        private bool _frontCafe5;

        private int currentComidaImage2;
        private int currentComidaImage3;
        private int currentComidaImage4;
        private int currentComidaImage5;
        private bool _frontComida2;
        private bool _frontComida3;
        private bool _frontComida4;
        private bool _frontComida5;

        private int currentTiendaImage1;
        private int currentTiendaImage2;
        private int currentTiendaImage3;
        private int currentTiendaImage4;
        private int currentTiendaImage5;
        private bool _frontTienda1;
        private bool _frontTienda2;
        private bool _frontTienda3;
        private bool _frontTienda4;
        private bool _frontTienda5;

        private DispatcherTimer _splashTimer;
        private DispatcherTimer _splashTimerCafes;
        private DispatcherTimer _splashTimerComida;
        private DispatcherTimer _splashTimerTiendas;
        private List<Model.Bebida> listBebidas = null;
        private List<Model.Cafe> listCafes = null;
        private List<Model.Comida> listComida = null;
        private List<Model.Tienda> listTiendas = null;
        private List<Model.Tienda> listTiendasCercanas = null;
        private List<Model.Tienda> listMuestraTiendas = null;
        private List<int> listaInicial = null;
        private List<int> listaInicialCafes = null;
        private List<int> listaInicialComida = null;
        private List<int> listaInicialTienda = null;
        private GeoCoordinateWatcher watcher;
        private GeoCoordinate myLocation;

        string usernamelog;
        string passwordlog;
        string hashlog;
        long idusuariolog;
        string tokenlog;
        long idTarjetalog;
        string uritarjetalog;
        string uritarjetamov;
        bool recargar = false;

        public delegate void LoadComplete(string pantalla, IList args);

        private int[] arr_x = new int[] { 260, 276, 292, 308, 324, 340 };
        private int[] arr_y = new int[] { 50, 85, 120, 155, 190 };

        private int isPDF417;
        //int[] PosicionesEstrellas = new int[] { 300 }

        public DatosRecarga DatosRecarga = (App.Current as App).DatosRecarga;

        HttpNotificationChannel pushChannel;
        string channelName = "ChannelStarbucks";

        private IGeoPositionWatcher<GeoCoordinate> _watcher;
        string _wacherLat;
        string _wacherLong;

        public Home()
        {
            InitializeComponent();


            var rs = Application.GetResourceStream(new Uri("DeviceFingerPrint.html", UriKind.Relative));
            //var rs = Application.GetResourceStream(new Uri("/Help/Help.html", UriKind.Relative));
            StreamReader sr = new StreamReader(rs.Stream);
            string htmlText = sr.ReadToEnd();
            sr.Close();
            WebBrowserX.NavigateToString(htmlText);
            


            pushChannel = HttpNotificationChannel.Find(channelName);

            // If the channel was not found, then create a new connection to the push service.
            if (pushChannel == null)
            {
                pushChannel = new HttpNotificationChannel(channelName);

                // Register for all the events before attempting to open the channel.
                pushChannel.ChannelUriUpdated += new EventHandler<NotificationChannelUriEventArgs>(PushChannel_ChannelUriUpdated);
                pushChannel.ErrorOccurred += new EventHandler<NotificationChannelErrorEventArgs>(PushChannel_ErrorOccurred);

                pushChannel.Open();

                // Bind this new channel for Tile events.
                pushChannel.BindToShellTile();


            }
            else
            {
                // The channel was already open, so just register for all the events.
                pushChannel.ChannelUriUpdated += new EventHandler<NotificationChannelUriEventArgs>(PushChannel_ChannelUriUpdated);
                pushChannel.ErrorOccurred += new EventHandler<NotificationChannelErrorEventArgs>(PushChannel_ErrorOccurred);
                // Display the URI for testing purposes. Normally, the URI would be passed back to your web service at this point.
                //System.Diagnostics.Debug.WriteLine(pushChannel.ChannelUri.ToString());
                //System.Windows.MessageBox.Show(String.Format("Channel Uri is {0}",
                //    pushChannel.ChannelUri.ToString()));

            }

            ApplicationBar.IsVisible = false;
            _splashTimer = new DispatcherTimer();
            _splashTimerCafes = new DispatcherTimer();
            _splashTimerComida = new DispatcherTimer();
            _splashTimerTiendas = new DispatcherTimer();
            listBebidas = new List<Model.Bebida>();
            listCafes = new List<Model.Cafe>();
            listComida = new List<Model.Comida>();
            listTiendas = new List<Model.Tienda>();
            listMuestraTiendas = new List<Model.Tienda>();
            listTiendasCercanas = new List<Model.Tienda>();
            listBebidas = App.BebidasViewModel.ObtenerTodasLasBebidas();
            listCafes = App.CafesViewModel.ObtenerListCafes();
            listComida = App.AlimentosViewModel.ObtenerTodasLasComidas();
            listTiendas = App.TiendasViewModel.ObtenerListTiendas();
            listaInicial = new List<int>();
            listaInicialCafes = new List<int>();
            listaInicialComida = new List<int>();
            listaInicialTienda = new List<int>();
            isPDF417 = 0;

            


            _watcher = new GeoCoordinateWatcher();
            _watcher.PositionChanged += _watcher_PositionChanged;
            _watcher.Start();
            var aleatorio = new Random();

            int timeAnimation = 100 * 1;
            startTimer.Duration = new Duration(new TimeSpan(0, 0, 0, 0, timeAnimation));
            startTimer.Completed += new EventHandler(myStoryboard_Completed);

            startTimer1.Duration = new Duration(new TimeSpan(0, 0, 0, 0, timeAnimation));
            startTimer1.Completed += new EventHandler(myStoryboard_Completed2);

            startTimer2.Duration = new Duration(new TimeSpan(0, 0, 0, 0, timeAnimation));
            startTimer2.Completed += new EventHandler(myStoryboard_Completed3);

            startTimer3.Duration = new Duration(new TimeSpan(0, 0, 0, 0, timeAnimation));
            startTimer3.Completed += new EventHandler(myStoryboard_Completed4);

            wsRewardstClient.FinishConsultaEstatusRewards += wsSmartClient_FinishConsultaEstatusRewards;
            wsRewardstClient.FinishLogin += wsRewardsClient_FinishLogin;
            wsRewardstClient.FinishConsultaSaldoTarjeta += wsRewardsClient_FinishConsultaSaldoTarjeta;

            try
            {

                // ############################  Aleatorios de Bebidas ############################
                var res = aleatorio.Next(listBebidas.Count - 1);
                var item1 = new Model.TileFlipBebida { Second = listBebidas[res], Nombre = listBebidas[res].nombre, PlecaTiles = StarbucksResource.Tile_PlecaVerde, UriImageSecond = listBebidas[res].imagen, isFavorito = listBebidas[res].isFavorito };
                tile1.DataContext = item1;
                currentImage1 = res;
                listaInicial.Add(res);

                while (listaInicial.Contains(res))
                    res = aleatorio.Next(randomNumeric.Length);
                var item2 = new Model.TileFlipBebida { Second = listBebidas[res], Nombre = listBebidas[res].nombre, PlecaTiles = StarbucksResource.Tile_PlecaVerde, UriImageSecond = listBebidas[res].imagen, isFavorito = listBebidas[res].isFavorito };
                tile2.DataContext = item2;
                currentImage2 = res;
                listaInicial.Add(res);

                while (listaInicial.Contains(res))
                    res = aleatorio.Next(randomNumeric.Length);
                var item3 = new Model.TileFlipBebida { Second = listBebidas[res], Nombre = listBebidas[res].nombre, PlecaTiles = StarbucksResource.Tile_PlecaVerde, UriImageSecond = listBebidas[res].imagen, isFavorito = listBebidas[res].isFavorito };
                tile3.DataContext = item3;
                currentImage3 = res;
                listaInicial.Add(res);

                while (listaInicial.Contains(res))
                    res = aleatorio.Next(randomNumeric.Length);
                var item4 = new Model.TileFlipBebida { Second = listBebidas[res], Nombre = listBebidas[res].nombre, PlecaTiles = StarbucksResource.Tile_PlecaVerde, UriImageSecond = listBebidas[res].imagen, isFavorito = listBebidas[res].isFavorito };
                tile4.DataContext = item4;
                currentImage4 = res;
                listaInicial.Add(res);

                while (listaInicial.Contains(res))
                    res = aleatorio.Next(randomNumeric.Length);
                var item5 = new Model.TileFlipBebida { Second = listBebidas[res], Nombre = listBebidas[res].nombre, PlecaTiles = StarbucksResource.Tile_PlecaVerde, UriImageSecond = listBebidas[res].imagen, isFavorito = listBebidas[res].isFavorito };
                tile5.DataContext = item5;
                currentImage5 = res;
                listaInicial.Add(res);
                _front1 = true;
                _front2 = true;
                _front3 = true;
                _front4 = true;
                _front5 = true;
                // ############################  Aleatorios de Cafes ############################
                var resCafe = aleatorio.Next(randomNumericCafeAlimento.Length);
                var itemCafe1 = new Model.TileFlipCafe { Second = listCafes[resCafe], Nombre = listCafes[resCafe].nombre, Perfil = listCafes[resCafe].perfil, UriImageSecond = listCafes[resCafe].imagen, isFavorito = listCafes[resCafe].isFavorito };
                tileCafe1.DataContext = itemCafe1;
                currentCafeImage1 = resCafe;
                listaInicialCafes.Add(resCafe);

                while (listaInicialCafes.Contains(resCafe))
                    resCafe = aleatorio.Next(randomNumericCafeAlimento.Length);
                var itemCafe3 = new Model.TileFlipCafe { Second = listCafes[resCafe], Nombre = listCafes[resCafe].nombre, Perfil = listCafes[resCafe].perfil, UriImageSecond = listCafes[resCafe].imagen, isFavorito = listCafes[resCafe].isFavorito };
                tileCafe3.DataContext = itemCafe3;
                currentCafeImage3 = resCafe;
                listaInicialCafes.Add(resCafe);

                while (listaInicialCafes.Contains(resCafe))
                    resCafe = aleatorio.Next(randomNumericCafeAlimento.Length);
                var itemCafe4 = new Model.TileFlipCafe { Second = listCafes[resCafe], Nombre = listCafes[resCafe].nombre, Perfil = listCafes[resCafe].perfil, UriImageSecond = listCafes[resCafe].imagen, isFavorito = listCafes[resCafe].isFavorito };
                tileCafe4.DataContext = itemCafe4;
                currentCafeImage4 = resCafe;
                listaInicialCafes.Add(resCafe);

                while (listaInicialCafes.Contains(resCafe))
                    resCafe = aleatorio.Next(randomNumericCafeAlimento.Length);
                var itemCafe5 = new Model.TileFlipCafe { Second = listCafes[resCafe], Nombre = listCafes[resCafe].nombre, Perfil = listCafes[resCafe].perfil, UriImageSecond = listCafes[resCafe].imagen, isFavorito = listCafes[resCafe].isFavorito };
                tileCafe5.DataContext = itemCafe5;
                currentCafeImage5 = resCafe;
                listaInicialCafes.Add(resCafe);
                _frontCafe1 = true;
                _frontCafe3 = true;
                _frontCafe4 = true;
                _frontCafe5 = true;
                // ############################  Aleatorios de Alimentos ############################
                var resComida = aleatorio.Next(randomNumericCafeAlimento.Length);

                while (listaInicialComida.Contains(resComida))
                    resComida = aleatorio.Next(randomNumericCafeAlimento.Length);
                var itemComida2 = new Model.TileFlipAlimentos { Second = listComida[resComida], Nombre = listComida[resComida].nombre, PlecaTiles = StarbucksResource.Tile_PlecaVerde, UriImageSecond = listComida[resComida].imagen, isFavorito = listComida[resComida].isFavorito };
                tileComida2.DataContext = itemComida2;
                currentComidaImage2 = resComida;
                listaInicialComida.Add(resComida);

                while (listaInicialComida.Contains(resComida))
                    resComida = aleatorio.Next(randomNumericCafeAlimento.Length);
                var itemComida3 = new Model.TileFlipAlimentos { Second = listComida[resComida], Nombre = listComida[resComida].nombre, PlecaTiles = StarbucksResource.Tile_PlecaVerde, UriImageSecond = listComida[resComida].imagen, isFavorito = listComida[resComida].isFavorito };
                tileComida3.DataContext = itemComida3;
                currentComidaImage3 = resComida;
                listaInicialComida.Add(resComida);

                while (listaInicialComida.Contains(resComida))
                    resComida = aleatorio.Next(randomNumericCafeAlimento.Length);
                var itemComida4 = new Model.TileFlipAlimentos { Second = listComida[resComida], Nombre = listComida[resComida].nombre, PlecaTiles = StarbucksResource.Tile_PlecaVerde, UriImageSecond = listComida[resComida].imagen, isFavorito = listComida[resComida].isFavorito };
                tileComida4.DataContext = itemComida4;
                currentComidaImage4 = resComida;
                listaInicialComida.Add(resComida);

                while (listaInicialComida.Contains(resComida))
                    resComida = aleatorio.Next(randomNumericCafeAlimento.Length);
                var itemComida5 = new Model.TileFlipAlimentos { Second = listComida[resComida], Nombre = listComida[resComida].nombre, PlecaTiles = StarbucksResource.Tile_PlecaVerde, UriImageSecond = listComida[resComida].imagen, isFavorito = listComida[resComida].isFavorito };
                tileComida5.DataContext = itemComida5;
                currentComidaImage5 = resComida;
                listaInicialComida.Add(resComida);
                _frontComida2 = true;
                _frontComida3 = true;
                _frontComida4 = true;
                _frontComida5 = true;

                // ############################  Aleatorios de Tiendas ############################
                var aleatorioTileTiendas = aleatorio.Next(randomNumericTiendas.Length);
                var resTienda = aleatorio.Next(listTiendas.Count - 1);
                var itemTienda1 = new Model.TileFlipTiendas { Second = listTiendas[resTienda], Nombre = listTiendas[resTienda].nombre, Horarios = listTiendas[resTienda].horario, UriImageSecond = string.Format("{0}{1}.png", StarbucksResource.TileMapa, randomNumericTiendas[aleatorioTileTiendas]), isFavorito = listTiendas[resTienda].isFavorito };
                tileTienda1.DataContext = itemTienda1;
                currentTiendaImage1 = resTienda;
                listaInicialTienda.Add(resTienda);

                aleatorioTileTiendas = aleatorio.Next(randomNumericTiendas.Length);
                while (listaInicialTienda.Contains(resTienda))
                    resTienda = aleatorio.Next(listTiendas.Count - 1);
                var itemTienda2 = new Model.TileFlipTiendas { Second = listTiendas[resTienda], Nombre = listTiendas[resTienda].nombre, Horarios = listTiendas[resTienda].horario, UriImageSecond = string.Format("{0}{1}.png", StarbucksResource.TileMapa, randomNumericTiendas[aleatorioTileTiendas]), isFavorito = listTiendas[resTienda].isFavorito };
                tileTienda2.DataContext = itemTienda2;
                currentTiendaImage2 = resTienda;
                listaInicialTienda.Add(resTienda);

                aleatorioTileTiendas = aleatorio.Next(randomNumericTiendas.Length);
                while (listaInicialTienda.Contains(resTienda))
                    resTienda = aleatorio.Next(listTiendas.Count - 1);
                var itemTienda3 = new Model.TileFlipTiendas { Second = listTiendas[resTienda], Nombre = listTiendas[resTienda].nombre, Horarios = listTiendas[resTienda].horario, UriImageSecond = string.Format("{0}{1}.png", StarbucksResource.TileMapa, randomNumericTiendas[aleatorioTileTiendas]), isFavorito = listTiendas[resTienda].isFavorito };
                tileTienda3.DataContext = itemTienda3;
                currentTiendaImage3 = resTienda;
                listaInicialTienda.Add(resTienda);

                aleatorioTileTiendas = aleatorio.Next(randomNumericTiendas.Length);
                while (listaInicialTienda.Contains(resTienda))
                    resTienda = aleatorio.Next(listTiendas.Count - 1);
                var itemTienda4 = new Model.TileFlipTiendas { Second = listTiendas[resTienda], Nombre = listTiendas[resTienda].nombre, Horarios = listTiendas[resTienda].horario, UriImageSecond = string.Format("{0}{1}.png", StarbucksResource.TileMapa, randomNumericTiendas[aleatorioTileTiendas]), isFavorito = listTiendas[resTienda].isFavorito };
                tileTienda4.DataContext = itemTienda4;
                currentTiendaImage4 = resTienda;
                listaInicialTienda.Add(resTienda);

                aleatorioTileTiendas = aleatorio.Next(randomNumericTiendas.Length);
                while (listaInicialTienda.Contains(resTienda))
                    resTienda = aleatorio.Next(listTiendas.Count - 1);
                var itemTienda5 = new Model.TileFlipTiendas { Second = listTiendas[resTienda], Nombre = listTiendas[resTienda].nombre, Horarios = listTiendas[resTienda].horario, UriImageSecond = string.Format("{0}{1}.png", StarbucksResource.TileMapa, randomNumericTiendas[aleatorioTileTiendas]), isFavorito = listTiendas[resTienda].isFavorito };
                tileTienda5.DataContext = itemTienda5;
                currentTiendaImage5 = resTienda;
                listaInicialTienda.Add(resTienda);
                _frontTienda1 = true;
                _frontTienda2 = true;
                _frontTienda3 = true;
                _frontTienda4 = true;
                _frontTienda5 = true;

                _splashTimer.Interval = TimeSpan.FromSeconds(4);
                _splashTimer.Tick += new EventHandler(_splashTimer_Tick);
                _splashTimer.Start();

                _splashTimerCafes.Interval = TimeSpan.FromSeconds(4);
                _splashTimerCafes.Tick += _splashTimerCafes_Tick;
                _splashTimerCafes.Start();

                _splashTimerComida.Interval = TimeSpan.FromSeconds(5);
                _splashTimerComida.Tick += _splashTimerComida_Tick;
                _splashTimerComida.Start();

                
                Loaded += Home_Loaded;


                //// Load HTML document as a string

                

            }
            catch
            {
            }
            
            wsSmartClient.FinishConsultaProgramaRecarga += SmartClient_FinishConsultaProgramaRecarga;
            
        }

        void _watcher_PositionChanged(object sender, GeoPositionChangedEventArgs<GeoCoordinate> e)
        {
            _wacherLat = e.Position.Location.Latitude.ToString();
            _wacherLong = e.Position.Location.Longitude. ToString();
        }

        void PushChannel_ChannelUriUpdated(object sender, NotificationChannelUriEventArgs e)
        {
            Dispatcher.BeginInvoke(() =>
            {
                // Display the new URI for testing purposes. Normally, the URI would be passed back to your web service at this point.
                //System.Diagnostics.Debug.WriteLine(e.ChannelUri.ToString());
                //System.Windows.MessageBox.Show(String.Format("Channel Uri is {0}", e.ChannelUri.ToString() ));

                string uri = e.ChannelUri.ToString();
                
                if (!IsolatedStorageSettings.ApplicationSettings.Contains("CHANNEL_URI"))
                {
                    IsolatedStorageSettings.ApplicationSettings["CHANNEL_URI"] = uri;
                    IsolatedStorageSettings.ApplicationSettings.Save();

                    //Llamar Registro de notificaciones
                    string url = IsolatedStorageSettings.ApplicationSettings["URL_NOTIFICATIONS"].ToString() + "registerNotificationsData";
                    HttpWebRequest webRequest = (HttpWebRequest)WebRequest.Create(url);
                    webRequest.Method = "POST";
                    webRequest.ContentType = "application/json";
                    webRequest.Headers["Programa"] = "ALS_SBX";
                    webRequest.BeginGetRequestStream(new AsyncCallback(GetRequestStreamCallback), webRequest);    
                }
            });
        }

        void PushChannel_ErrorOccurred(object sender, NotificationChannelErrorEventArgs e)
        {
            // Error handling logic for your particular application would be here.
            Dispatcher.BeginInvoke(() =>
                System.Windows.MessageBox.Show(String.Format("A push notification {0} error occurred.  {1} ({2}) {3}",
                    e.ErrorType, e.Message, e.ErrorCode, e.ErrorAdditionalData))
                    );
        }



        void GetRequestStreamCallback(IAsyncResult asynchronousResult)
        {
            HttpWebRequest webRequest = (HttpWebRequest)asynchronousResult.AsyncState;
            Stream postStream = webRequest.EndGetRequestStream(asynchronousResult);
            string postData = "{\"data\":{{\"os\":\"WP\",\"udid\":\"" + ConfigurationSettings.DeviceUniqueId + "\",\"token\":\"" + IsolatedStorageSettings.ApplicationSettings["CHANNEL_URI"].ToString() + "\"}}";
                
            byte[] byteArray = Encoding.UTF8.GetBytes(postData);
            postStream.Write(byteArray, 0, byteArray.Length);
            postStream.Close();
            webRequest.BeginGetResponse(new AsyncCallback(GetResponseCallback), webRequest);
        }

        void GetResponseCallback(IAsyncResult asynchronousResult)
        {
            try
            {
                HttpWebRequest webRequest = (HttpWebRequest)asynchronousResult.AsyncState;
                HttpWebResponse response;

                response = (HttpWebResponse)webRequest.EndGetResponse(asynchronousResult);
                Stream streamResponse = response.GetResponseStream();
                StreamReader streamReader = new StreamReader(streamResponse);
                var Response = streamReader.ReadToEnd();
                streamResponse.Close();
                streamReader.Close();
                response.Close();

                var obj = JsonConvert.DeserializeObject<RegisterNotificationsResult>(Response) as RegisterNotificationsResult;
                
                Deployment.Current.Dispatcher.BeginInvoke(() =>
                {
                    IsolatedStorageSettings.ApplicationSettings["idTokenNotificacion"] = obj.idTokenNotificacion;
                    IsolatedStorageSettings.ApplicationSettings.Save();

                    //Comienzo con Registrar Dispositivo
                    string url = IsolatedStorageSettings.ApplicationSettings["URL_NOTIFICATIONS"].ToString() + "registerDeviceData";
                    HttpWebRequest webRequestNext = (HttpWebRequest)WebRequest.Create(url);
                    webRequestNext.Method = "POST";
                    webRequestNext.ContentType = "application/json";
                    webRequestNext.Headers["Programa"] = "ALS_SBX";
                    webRequestNext.BeginGetRequestStream(new AsyncCallback(GetRequestStreamCallbackRegisterDeviceData), webRequestNext); 
                });
            }
            catch (WebException ex)
            {
                // Error treatment
                System.Diagnostics.Debug.WriteLine(ex.Message);
            }
        }

        void GetRequestStreamCallbackRegisterDeviceData(IAsyncResult asynchronousResult)
        {
            var phone = PhoneNameResolver.Resolve(DeviceStatus.DeviceManufacturer, DeviceStatus.DeviceName);
            HttpWebRequest webRequest = (HttpWebRequest)asynchronousResult.AsyncState;
            Stream postStream = webRequest.EndGetRequestStream(asynchronousResult);
            string postData = "{\"data\":{{\"lat\":\"" + _wacherLat + "\",\"lon\":\"" + _wacherLong + "\",\"modelo\":\"" + phone.FullCanonicalName + "\",\"os\":\"WP\",\"os_version\":\"" + System.Environment.OSVersion.Version + "\",\"udid\":\"" + ConfigurationSettings.DeviceUniqueId + "\",\"version_app\":\"1.10\"}}";

            byte[] byteArray = Encoding.UTF8.GetBytes(postData);
            postStream.Write(byteArray, 0, byteArray.Length);
            postStream.Close();
            webRequest.BeginGetResponse(new AsyncCallback(GetResponseCallbackRegisterDeviceData), webRequest);
        }

        void GetResponseCallbackRegisterDeviceData(IAsyncResult asynchronousResult)
        {
            try
            {
                HttpWebRequest webRequest = (HttpWebRequest)asynchronousResult.AsyncState;
                HttpWebResponse response;

                response = (HttpWebResponse)webRequest.EndGetResponse(asynchronousResult);
                Stream streamResponse = response.GetResponseStream();
                StreamReader streamReader = new StreamReader(streamResponse);
                var Response = streamReader.ReadToEnd();
                streamResponse.Close();
                streamReader.Close();
                response.Close();

                var obj = JsonConvert.DeserializeObject<CustomResult>(Response) as CustomResult;

                Deployment.Current.Dispatcher.BeginInvoke(() =>
                {
                    System.Diagnostics.Debug.WriteLine(obj.message);
                });
            }
            catch (WebException ex)
            {
                // Error treatment
                System.Diagnostics.Debug.WriteLine(ex.Message);
            }
        }

        void _splashTimerTiendas_Tick(object sender, EventArgs e)
        {
            var aleatorio = new Random();
            var res = aleatorio.Next(randomNumeric.Length);

            var aleatorioTileTiendas = aleatorio.Next(randomNumericTiendas.Length);
            var aleatorioImagenTienda = new Random();
            var resImagenTienda = aleatorioImagenTienda.Next(listMuestraTiendas.Count - 1);

            if (currentTileTienda != res)
            {
                switch (res)
                {
                    case 1:
                        while (listaInicialTienda.Contains(resImagenTienda))
                            resImagenTienda = aleatorioImagenTienda.Next(listMuestraTiendas.Count - 1);
                        currentTileTienda = res;
                        currentTiendaImage1 = resImagenTienda;
                        RecargaListaContainerTienda();
                        var item = new Model.TileFlipTiendas();
                        if (_frontTienda1)
                        {
                            item.First = listMuestraTiendas[resImagenTienda];
                            item.Nombre = listMuestraTiendas[resImagenTienda].nombre;
                            item.UriImageFirst = string.Format("{0}{1}.png", StarbucksResource.TileMapa, randomNumericTiendas[aleatorioTileTiendas]);
                            item.Horarios = listTiendas[resImagenTienda].horario;
                            item.isFavorito = listMuestraTiendas[resImagenTienda].isFavorito;
                            _frontTienda1 = false;
                        }
                        else
                        {
                            item.Second = listMuestraTiendas[resImagenTienda];
                            item.Nombre = listMuestraTiendas[resImagenTienda].nombre;
                            item.UriImageSecond = string.Format("{0}{1}.png", StarbucksResource.TileMapa, randomNumericTiendas[aleatorioTileTiendas]);
                            item.Horarios = listTiendas[resImagenTienda].horario;
                            item.isFavorito = listMuestraTiendas[resImagenTienda].isFavorito;
                            _frontTienda1 = true;
                        }
                        tileTienda1.DataContext = item;
                        tileTienda1.Flip();
                        break;
                    case 2:
                        while (listaInicialTienda.Contains(resImagenTienda))
                            resImagenTienda = aleatorioImagenTienda.Next(listMuestraTiendas.Count - 1);
                        currentTileTienda = res;
                        currentTiendaImage2 = resImagenTienda;
                        RecargaListaContainerTienda();
                        var item2 = new Model.TileFlipTiendas();
                        if (_frontTienda2)
                        {
                            item2.First = listMuestraTiendas[resImagenTienda];
                            item2.Nombre = listMuestraTiendas[resImagenTienda].nombre;
                            item2.UriImageFirst = string.Format("{0}{1}.png", StarbucksResource.TileMapa, randomNumericTiendas[aleatorioTileTiendas]);
                            item2.Horarios = listTiendas[resImagenTienda].horario;
                            item2.isFavorito = listMuestraTiendas[resImagenTienda].isFavorito;
                            _frontTienda2 = false;
                        }
                        else
                        {
                            item2.Second = listMuestraTiendas[resImagenTienda];
                            item2.Nombre = listMuestraTiendas[resImagenTienda].nombre;
                            item2.UriImageSecond = string.Format("{0}{1}.png", StarbucksResource.TileMapa, randomNumericTiendas[aleatorioTileTiendas]);
                            item2.Horarios = listTiendas[resImagenTienda].horario;
                            item2.isFavorito = listMuestraTiendas[resImagenTienda].isFavorito;
                            _frontTienda2 = true;
                        }
                        tileTienda2.DataContext = item2;
                        tileTienda2.Flip();
                        break;
                    case 3:
                        while (listaInicialTienda.Contains(resImagenTienda))
                            resImagenTienda = aleatorioImagenTienda.Next(listMuestraTiendas.Count - 1);
                        currentTileTienda = res;
                        currentTiendaImage3 = resImagenTienda;
                        RecargaListaContainerTienda();
                        var item3 = new Model.TileFlipTiendas();
                        if (_frontTienda3)
                        {
                            item3.First = listMuestraTiendas[resImagenTienda];
                            item3.Nombre = listMuestraTiendas[resImagenTienda].nombre;
                            item3.UriImageFirst = string.Format("{0}{1}.png", StarbucksResource.TileMapa, randomNumericTiendas[aleatorioTileTiendas]);
                            item3.Horarios = listTiendas[resImagenTienda].horario;
                            item3.isFavorito = listMuestraTiendas[resImagenTienda].isFavorito;
                            _frontTienda3 = false;
                        }
                        else
                        {
                            item3.Second = listMuestraTiendas[resImagenTienda];
                            item3.Nombre = listMuestraTiendas[resImagenTienda].nombre;
                            item3.UriImageSecond = string.Format("{0}{1}.png", StarbucksResource.TileMapa, randomNumericTiendas[aleatorioTileTiendas]);
                            item3.Horarios = listTiendas[resImagenTienda].horario;
                            item3.isFavorito = listMuestraTiendas[resImagenTienda].isFavorito;
                            _frontTienda3 = true;
                        }
                        tileTienda3.DataContext = item3;
                        tileTienda3.Flip();
                        break;
                    case 4:
                        while (listaInicialTienda.Contains(resImagenTienda))
                            resImagenTienda = aleatorioImagenTienda.Next(listMuestraTiendas.Count - 1);
                        currentTileTienda = res;
                        currentTiendaImage4 = resImagenTienda;
                        RecargaListaContainerTienda();
                        var item4 = new Model.TileFlipTiendas();
                        if (_frontTienda4)
                        {
                            item4.First = listMuestraTiendas[resImagenTienda];
                            item4.Nombre = listMuestraTiendas[resImagenTienda].nombre;
                            item4.UriImageFirst = string.Format("{0}{1}.png", StarbucksResource.TileMapa, randomNumericTiendas[aleatorioTileTiendas]);
                            item4.Horarios = listTiendas[resImagenTienda].horario;
                            item4.isFavorito = listMuestraTiendas[resImagenTienda].isFavorito;
                            _frontTienda4 = false;
                        }
                        else
                        {
                            item4.Second = listMuestraTiendas[resImagenTienda];
                            item4.Nombre = listMuestraTiendas[resImagenTienda].nombre;
                            item4.UriImageSecond = string.Format("{0}{1}.png", StarbucksResource.TileMapa, randomNumericTiendas[aleatorioTileTiendas]);
                            item4.Horarios = listTiendas[resImagenTienda].horario;
                            item4.isFavorito = listMuestraTiendas[resImagenTienda].isFavorito;
                            _frontTienda4 = true;
                        }
                        tileTienda4.DataContext = item4;
                        tileTienda4.Flip();
                        break;
                    case 5:
                        while (listaInicialTienda.Contains(resImagenTienda))
                            resImagenTienda = aleatorioImagenTienda.Next(listMuestraTiendas.Count - 1);
                        currentTileTienda = res;
                        currentTiendaImage5 = resImagenTienda;
                        RecargaListaContainerTienda();
                        var item5 = new Model.TileFlipTiendas();
                        if (_frontTienda5)
                        {
                            item5.First = listMuestraTiendas[resImagenTienda];
                            item5.Nombre = listMuestraTiendas[resImagenTienda].nombre;
                            item5.UriImageFirst = string.Format("{0}{1}.png", StarbucksResource.TileMapa, randomNumericTiendas[aleatorioTileTiendas]);
                            item5.Horarios = listTiendas[resImagenTienda].horario;
                            item5.isFavorito = listMuestraTiendas[resImagenTienda].isFavorito;
                            _frontTienda5 = false;
                        }
                        else
                        {
                            item5.Second = listMuestraTiendas[resImagenTienda];
                            item5.Nombre = listMuestraTiendas[resImagenTienda].nombre;
                            item5.UriImageSecond = string.Format("{0}{1}.png", StarbucksResource.TileMapa, randomNumericTiendas[aleatorioTileTiendas]);
                            item5.Horarios = listTiendas[resImagenTienda].horario;
                            item5.isFavorito = listMuestraTiendas[resImagenTienda].isFavorito;
                            _frontTienda5 = true;
                        }
                        tileTienda5.DataContext = item5;
                        tileTienda5.Flip();
                        break;
                    default:
                        break;
                }
            }
        }

        private bool IsLoadMensajeGPS = false;
        void Home_Loaded(object sender, RoutedEventArgs e)
        {
            //if (App.Settings.Contains("CodigoAcceso"))
            //    return;
            
            
            if (!App.Settings.Contains(Constantes.MostrarMensaje_GPS))
            {
                if (!IsLoadMensajeGPS)
                {
                    Starbucks.UI.Controls.MessageBoxGeneric.Show(StarbucksResource.Label_GPS_Off, "UBICACION", "OK", string.Empty);
                    IsLoadMensajeGPS = true;
                }
            }

            if (App.Settings.Contains("GPS"))
            {
                if (Convert.ToBoolean(App.Settings["GPS"]) == true)
                {
                    if (!isLoad)
                    {
                        watcher = new GeoCoordinateWatcher(GeoPositionAccuracy.High)
                        {
                            MovementThreshold = 1
                        };

                        watcher.StatusChanged += watcher_StatusChanged;
                        watcher.Start();
                        _splashTimerTiendas.Tick -= _splashTimerTiendas_Tick;
                        _splashTimerTiendas.Stop();
                    }
                }
                else
                {
                    if (watcher != null)
                    {
                        App.ViewModel.GpsDisponible = false;
                        watcher.Stop();
                        watcher = null;
                        isLoad = false;
                        _splashTimerTiendas.Tick -= _splashTimerTiendas_Tick;
                        _splashTimerTiendas.Stop();
                        CargaTiendasCercanas();
                    }
                    else
                    {
                        _splashTimerTiendas.Tick -= _splashTimerTiendas_Tick;
                        _splashTimerTiendas.Stop();
                        CargaTiendasCercanas();
                    }
                }
            }

            //if (App.Settings.Contains("Logged") && App.Settings["Logged"] != null)
            //{
            //    Vaso_out.Visibility = System.Windows.Visibility.Collapsed;
            //    Vaso.Visibility = System.Windows.Visibility.Visible;
            //    containerLog.DataContext = (Model.Usuario)App.Settings["Logged"];
            //    PathEstrella.Visibility = System.Windows.Visibility.Visible;
            //    txtEstrellasAlmacenadas.Visibility = System.Windows.Visibility.Visible;
            //}
            mitarjeta.Visibility = Visibility.Collapsed;
            LoadData("Sesion.txt");
            LoadData("Actualizar.txt");
/*
            */
        }

        private void watcher_StatusChanged(object sender, GeoPositionStatusChangedEventArgs e)
        {
            switch (e.Status)
            {
                case GeoPositionStatus.Disabled:
                    isLoad = true;
                    App.ViewModel.GpsDisponible = false;
                    CargaTiendasCercanas();
                    break;
                case GeoPositionStatus.NoData:
                    // data unavailable
                    break;
                case GeoPositionStatus.Ready:
                    isLoad = true;
                    var epl = watcher.Position.Location;
                    myLocation = epl;
                    App.ViewModel.GpsDisponible = true;
                    App.TiendasViewModel.MyLocation = epl;
                    App.RAViewModel.CurrentPosition = epl;
                    CargaTiendasCercanas();
                    break;
            }
        }

        private void CargaTiendasCercanas()
        {
            var radio = App.Settings.Contains(Constantes.LocalizaTiendas) ? App.Settings[Constantes.LocalizaTiendas].ToString() : string.Empty;
            if (App.ViewModel.GpsDisponible)
            {
                listTiendas.ForEach(c =>
                {
                    var posicionOrigen = new Posicion(App.TiendasViewModel.MyLocation.Latitude, App.TiendasViewModel.MyLocation.Longitude);
                    var posicionDestion = new Posicion(c.latitud, c.longitud);
                    var distancia = posicionOrigen.DistanciaKm(posicionDestion);

                    if (radio.Equals(string.Empty))
                        listTiendasCercanas.Add(c);
                    else
                    {
                        if (Math.Round(distancia) <= 5)
                            listTiendasCercanas.Add(c);
                    }
                });
                MuestraTilesTiendas(listTiendasCercanas);
            }
            else
                MuestraTilesTiendas(listTiendas);
        }

        private void MuestraTilesTiendas(List<Model.Tienda> listaTiendas)
        {
            listMuestraTiendas = listaTiendas;
            if (listaTiendas.Count >= 7)
            {
                _splashTimerTiendas.Interval = TimeSpan.FromSeconds(4);
                _splashTimerTiendas.Tick += _splashTimerTiendas_Tick;
                _splashTimerTiendas.Start();
            }
        }

        void _splashTimerComida_Tick(object sender, EventArgs e)
        {
            var aleatorio = new Random();
            var res = aleatorio.Next(randomNumericCafeAlimento.Length);
            var aleatorioImagenComida = new Random();
            var resImagenComida = aleatorioImagenComida.Next(listComida.Count - 1);

            if (currentTileComida != res)
            {
                switch (res)
                {
                    case 1:
                        while (listaInicialComida.Contains(resImagenComida))
                            resImagenComida = aleatorioImagenComida.Next(listComida.Count - 1);
                        currentTileComida = res;
                        currentComidaImage2 = resImagenComida;
                        RecargaListaContainerCafes();
                        var item2 = new Model.TileFlipAlimentos();
                        if (_frontComida2)
                        {
                            item2.First = listComida[resImagenComida];
                            item2.Nombre = listComida[resImagenComida].nombre;
                            item2.PlecaTiles = StarbucksResource.Tile_PlecaVerde;
                            item2.UriImageFirst = listComida[resImagenComida].imagen;
                            item2.isFavorito = listComida[resImagenComida].isFavorito;
                            _frontComida2 = false;
                        }
                        else
                        {
                            item2.Second = listComida[resImagenComida];
                            item2.Nombre = listComida[resImagenComida].nombre;
                            item2.PlecaTiles = StarbucksResource.Tile_PlecaVerde;
                            item2.UriImageSecond = listComida[resImagenComida].imagen;
                            item2.isFavorito = listComida[resImagenComida].isFavorito;
                            _frontComida2 = true;
                        }
                        tileComida2.DataContext = item2;
                        tileComida2.Flip();
                        break;
                    case 2:
                        while (listaInicialComida.Contains(resImagenComida))
                            resImagenComida = aleatorioImagenComida.Next(listCafes.Count - 1);
                        currentTileComida = res;
                        currentComidaImage3 = resImagenComida;
                        RecargaListaContainerComida();
                        var item3 = new Model.TileFlipAlimentos();
                        if (_frontComida3)
                        {
                            item3.First = listComida[resImagenComida];
                            item3.Nombre = listComida[resImagenComida].nombre;
                            item3.PlecaTiles = StarbucksResource.Tile_PlecaVerde;
                            item3.UriImageFirst = listComida[resImagenComida].imagen;
                            item3.isFavorito = listComida[resImagenComida].isFavorito;
                            _frontComida3 = false;
                        }
                        else
                        {
                            item3.Second = listComida[resImagenComida];
                            item3.Nombre = listComida[resImagenComida].nombre;
                            item3.PlecaTiles = StarbucksResource.Tile_PlecaVerde;
                            item3.UriImageSecond = listComida[resImagenComida].imagen;
                            item3.isFavorito = listComida[resImagenComida].isFavorito;
                            _frontComida3 = true;
                        }
                        tileComida3.DataContext = item3;
                        tileComida3.Flip();
                        break;
                    case 3:
                        while (listaInicialComida.Contains(resImagenComida))
                            resImagenComida = aleatorioImagenComida.Next(listCafes.Count - 1);
                        currentTileComida = res;
                        currentComidaImage4 = resImagenComida;
                        RecargaListaContainerComida();
                        var item4 = new Model.TileFlipAlimentos();
                        if (_frontComida4)
                        {
                            item4.First = listComida[resImagenComida];
                            item4.Nombre = listComida[resImagenComida].nombre;
                            item4.PlecaTiles = StarbucksResource.Tile_PlecaVerde;
                            item4.UriImageFirst = listComida[resImagenComida].imagen;
                            item4.isFavorito = listComida[resImagenComida].isFavorito;
                            _frontComida4 = false;
                        }
                        else
                        {
                            item4.Second = listComida[resImagenComida];
                            item4.Nombre = listComida[resImagenComida].nombre;
                            item4.PlecaTiles = StarbucksResource.Tile_PlecaVerde;
                            item4.UriImageSecond = listComida[resImagenComida].imagen;
                            item4.isFavorito = listComida[resImagenComida].isFavorito;
                            _frontComida4 = true;
                        }
                        tileComida4.DataContext = item4;
                        tileComida4.Flip();
                        break;
                    case 4:
                        while (listaInicialComida.Contains(resImagenComida))
                            resImagenComida = aleatorioImagenComida.Next(listComida.Count - 1);
                        currentTileComida = res;
                        currentComidaImage5 = resImagenComida;
                        RecargaListaContainerComida();
                        var item5 = new Model.TileFlipAlimentos();
                        if (_frontComida5)
                        {
                            item5.First = listComida[resImagenComida];
                            item5.Nombre = listComida[resImagenComida].nombre;
                            item5.PlecaTiles = StarbucksResource.Tile_PlecaVerde;
                            item5.UriImageFirst = listComida[resImagenComida].imagen;
                            item5.isFavorito = listComida[resImagenComida].isFavorito;
                            _frontComida5 = false;
                        }
                        else
                        {
                            item5.Second = listComida[resImagenComida];
                            item5.Nombre = listComida[resImagenComida].nombre;
                            item5.PlecaTiles = StarbucksResource.Tile_PlecaVerde;
                            item5.UriImageSecond = listComida[resImagenComida].imagen;
                            item5.isFavorito = listComida[resImagenComida].isFavorito;
                            _frontComida5 = true;
                        }
                        tileComida5.DataContext = item5;
                        tileComida5.Flip();
                        break;
                    default:
                        break;
                }
            }
        }

        void _splashTimerCafes_Tick(object sender, EventArgs e)
        {
            var aleatorio = new Random();
            var res = aleatorio.Next(randomNumericCafeAlimento.Length);
            var aleatorioImagenCafes = new Random();
            var resImagenCafes = aleatorioImagenCafes.Next(listCafes.Count - 1);

            if (currentTileCafes != res)
            {
                switch (res)
                {
                    case 1:
                        while (listaInicialCafes.Contains(resImagenCafes))
                            resImagenCafes = aleatorioImagenCafes.Next(listCafes.Count - 1);
                        currentTileCafes = res;
                        currentCafeImage1 = resImagenCafes;
                        RecargaListaContainerCafes();
                        var item = new Model.TileFlipCafe();
                        if (_frontCafe1)
                        {
                            item.First = listCafes[resImagenCafes];
                            item.Nombre = listCafes[resImagenCafes].nombre;
                            item.Perfil = listCafes[resImagenCafes].perfil;
                            item.UriImageFirst = listCafes[resImagenCafes].imagen;
                            item.isFavorito = listCafes[resImagenCafes].isFavorito;
                            _frontCafe1 = false;
                        }
                        else
                        {
                            item.Second = listCafes[resImagenCafes];
                            item.Nombre = listCafes[resImagenCafes].nombre;
                            item.Perfil = listCafes[resImagenCafes].perfil;
                            item.UriImageSecond = listCafes[resImagenCafes].imagen;
                            item.isFavorito = listCafes[resImagenCafes].isFavorito;
                            _frontCafe1 = true;
                        }
                        tileCafe1.DataContext = item;
                        tileCafe1.Flip();
                        break;
                    case 2:
                        while (listaInicialCafes.Contains(resImagenCafes))
                            resImagenCafes = aleatorioImagenCafes.Next(listCafes.Count - 1);
                        currentTileCafes = res;
                        currentCafeImage3 = resImagenCafes;
                        RecargaListaContainerCafes();
                        var item3 = new Model.TileFlipCafe();
                        if (_frontCafe3)
                        {
                            item3.First = listCafes[resImagenCafes];
                            item3.Nombre = listCafes[resImagenCafes].nombre;
                            item3.Perfil = listCafes[resImagenCafes].perfil;
                            item3.UriImageFirst = listCafes[resImagenCafes].imagen;
                            item3.isFavorito = listCafes[resImagenCafes].isFavorito;
                            _frontCafe3 = false;
                        }
                        else
                        {
                            item3.Second = listCafes[resImagenCafes];
                            item3.Nombre = listCafes[resImagenCafes].nombre;
                            item3.Perfil = listCafes[resImagenCafes].perfil;
                            item3.UriImageSecond = listCafes[resImagenCafes].imagen;
                            item3.isFavorito = listCafes[resImagenCafes].isFavorito;
                            _frontCafe3 = true;
                        }
                        tileCafe3.DataContext = item3;
                        tileCafe3.Flip();
                        break;
                    case 3:
                        while (listaInicialCafes.Contains(resImagenCafes))
                            resImagenCafes = aleatorioImagenCafes.Next(listCafes.Count - 1);
                        currentTileCafes = res;
                        currentCafeImage4 = resImagenCafes;
                        RecargaListaContainerCafes();
                        var item4 = new Model.TileFlipCafe();
                        if (_frontCafe4)
                        {
                            item4.First = listCafes[resImagenCafes];
                            item4.Nombre = listCafes[resImagenCafes].nombre;
                            item4.Perfil = listCafes[resImagenCafes].perfil;
                            item4.UriImageFirst = listCafes[resImagenCafes].imagen;
                            item4.isFavorito = listCafes[resImagenCafes].isFavorito;
                            _frontCafe4 = false;
                        }
                        else
                        {
                            item4.Second = listCafes[resImagenCafes];
                            item4.Nombre = listCafes[resImagenCafes].nombre;
                            item4.Perfil = listCafes[resImagenCafes].perfil;
                            item4.UriImageSecond = listCafes[resImagenCafes].imagen;
                            item4.isFavorito = listCafes[resImagenCafes].isFavorito;
                            _frontCafe4 = true;
                        }
                        tileCafe4.DataContext = item4;
                        tileCafe4.Flip();
                        break;
                    case 4:
                        while (listaInicialCafes.Contains(resImagenCafes))
                            resImagenCafes = aleatorioImagenCafes.Next(listCafes.Count - 1);
                        currentTileCafes = res;
                        currentCafeImage5 = resImagenCafes;
                        RecargaListaContainerCafes();
                        var item5 = new Model.TileFlipCafe();
                        if (_frontCafe5)
                        {
                            item5.First = listCafes[resImagenCafes];
                            item5.Nombre = listCafes[resImagenCafes].nombre;
                            item5.Perfil = listCafes[resImagenCafes].perfil;
                            item5.UriImageFirst = listCafes[resImagenCafes].imagen;
                            item5.isFavorito = listCafes[resImagenCafes].isFavorito;
                            _frontCafe5 = false;
                        }
                        else
                        {
                            item5.Second = listCafes[resImagenCafes];
                            item5.Nombre = listCafes[resImagenCafes].nombre;
                            item5.Perfil = listCafes[resImagenCafes].perfil;
                            item5.UriImageSecond = listCafes[resImagenCafes].imagen;
                            item5.isFavorito = listCafes[resImagenCafes].isFavorito;
                            _frontCafe5 = true;
                        }
                        tileCafe5.DataContext = item5;
                        tileCafe5.Flip();
                        break;
                    default:
                        break;
                }
            }
        }

        void _splashTimer_Tick(object sender, EventArgs e)
        {
            var aleatorio = new Random();
            var res = aleatorio.Next(randomNumeric.Length);

            var aleatorioImagen = new Random();
            var resImagen = aleatorioImagen.Next(listBebidas.Count - 1);

            if (currentTile != res)
            {
                switch (res)
                {
                    case 1:
                        while (listaInicial.Contains(resImagen))
                            resImagen = aleatorioImagen.Next(listBebidas.Count - 1);
                        currentTile = res;
                        currentImage1 = resImagen;
                        RecargaListaContainer();
                        var item = new Model.TileFlipBebida();
                        if (_front1)
                        {
                            item.First = listBebidas[resImagen];
                            item.Nombre = listBebidas[resImagen].nombre;
                            item.PlecaTiles = StarbucksResource.Tile_PlecaVerde;
                            item.UriImageFirst = listBebidas[resImagen].imagen;
                            item.isFavorito = listBebidas[resImagen].isFavorito;
                            _front1 = false;
                        }
                        else
                        {
                            item.Second = listBebidas[resImagen];
                            item.Nombre = listBebidas[resImagen].nombre;
                            item.PlecaTiles = StarbucksResource.Tile_PlecaVerde;
                            item.UriImageSecond = listBebidas[resImagen].imagen;
                            item.isFavorito = listBebidas[resImagen].isFavorito;
                            _front1 = true;
                        }
                        tile1.DataContext = item;
                        tile1.Flip();
                        break;
                    case 2:
                        while (listaInicial.Contains(resImagen))
                            resImagen = aleatorioImagen.Next(listBebidas.Count - 1);
                        currentTile = res;
                        currentImage2 = resImagen;
                        RecargaListaContainer();
                        var item2 = new Model.TileFlipBebida();
                        if (_front2)
                        {
                            item2.First = listBebidas[resImagen];
                            item2.Nombre = listBebidas[resImagen].nombre;
                            item2.PlecaTiles = StarbucksResource.Tile_PlecaVerde;
                            item2.UriImageFirst = listBebidas[resImagen].imagen;
                            item2.isFavorito = listBebidas[resImagen].isFavorito;
                            _front2 = false;
                        }
                        else
                        {
                            item2.Second = listBebidas[resImagen];
                            item2.Nombre = listBebidas[resImagen].nombre;
                            item2.PlecaTiles = StarbucksResource.Tile_PlecaVerde;
                            item2.UriImageSecond = listBebidas[resImagen].imagen;
                            item2.isFavorito = listBebidas[resImagen].isFavorito;
                            _front2 = true;
                        }
                        tile2.DataContext = item2;
                        tile2.Flip();
                        break;
                    case 3:
                        while (listaInicial.Contains(resImagen))
                            resImagen = aleatorioImagen.Next(listBebidas.Count - 1);
                        currentTile = res;
                        currentImage3 = resImagen;
                        RecargaListaContainer();
                        var item3 = new Model.TileFlipBebida();
                        if (_front3)
                        {
                            item3.First = listBebidas[resImagen];
                            item3.Nombre = listBebidas[resImagen].nombre;
                            item3.PlecaTiles = StarbucksResource.Tile_PlecaVerde;
                            item3.UriImageFirst = listBebidas[resImagen].imagen;
                            item3.isFavorito = listBebidas[resImagen].isFavorito;
                            _front3 = false;
                        }
                        else
                        {
                            item3.Second = listBebidas[resImagen];
                            item3.Nombre = listBebidas[resImagen].nombre;
                            item3.PlecaTiles = StarbucksResource.Tile_PlecaVerde;
                            item3.UriImageSecond = listBebidas[resImagen].imagen;
                            item3.isFavorito = listBebidas[resImagen].isFavorito;
                            _front3 = true;
                        }
                        tile3.DataContext = item3;
                        tile3.Flip();
                        break;
                    case 4:
                        while (listaInicial.Contains(resImagen))
                            resImagen = aleatorioImagen.Next(listBebidas.Count - 1);
                        currentTile = res;
                        currentImage4 = resImagen;
                        RecargaListaContainer();
                        var item4 = new Model.TileFlipBebida();
                        if (_front4)
                        {
                            item4.First = listBebidas[resImagen];
                            item4.Nombre = listBebidas[resImagen].nombre;
                            item4.PlecaTiles = StarbucksResource.Tile_PlecaVerde;
                            item4.UriImageFirst = listBebidas[resImagen].imagen;
                            item4.isFavorito = listBebidas[resImagen].isFavorito;
                            _front4 = false;
                        }
                        else
                        {
                            item4.Second = listBebidas[resImagen];
                            item4.Nombre = listBebidas[resImagen].nombre;
                            item4.PlecaTiles = StarbucksResource.Tile_PlecaVerde;
                            item4.UriImageSecond = listBebidas[resImagen].imagen;
                            item4.isFavorito = listBebidas[resImagen].isFavorito;
                            _front4 = true;
                        }
                        tile4.DataContext = item4;
                        tile4.Flip();
                        break;
                    case 5:
                        while (listaInicial.Contains(resImagen))
                            resImagen = aleatorioImagen.Next(listBebidas.Count - 1);
                        currentTile = res;
                        currentImage5 = resImagen;
                        RecargaListaContainer();
                        var item5 = new Model.TileFlipBebida();
                        if (_front5)
                        {
                            item5.First = listBebidas[resImagen];
                            item5.Nombre = listBebidas[resImagen].nombre;
                            item5.PlecaTiles = StarbucksResource.Tile_PlecaVerde;
                            item5.UriImageFirst = listBebidas[resImagen].imagen;
                            item5.isFavorito = listBebidas[resImagen].isFavorito;
                            _front5 = false;
                        }
                        else
                        {
                            item5.Second = listBebidas[resImagen];
                            item5.Nombre = listBebidas[resImagen].nombre;
                            item5.PlecaTiles = StarbucksResource.Tile_PlecaVerde;
                            item5.UriImageSecond = listBebidas[resImagen].imagen;
                            item5.isFavorito = listBebidas[resImagen].isFavorito;
                            _front5 = true;
                        }
                        tile5.DataContext = item5;
                        tile5.Flip();
                        break;
                    default:
                        break;
                }
            }
        }

        private void RecargaListaContainer()
        {
            listaInicial.Clear();
            listaInicial.Add(currentImage1);
            listaInicial.Add(currentImage2);
            listaInicial.Add(currentImage3);
            listaInicial.Add(currentImage4);
            listaInicial.Add(currentImage5);
        }

        private void RecargaListaContainerCafes()
        {
            listaInicialCafes.Clear();
            listaInicialCafes.Add(currentCafeImage1);
            listaInicialCafes.Add(currentCafeImage3);
            listaInicialCafes.Add(currentCafeImage4);
            listaInicialCafes.Add(currentCafeImage5);
        }

        private void RecargaListaContainerComida()
        {
            listaInicialComida.Clear();
            listaInicialComida.Add(currentComidaImage2);
            listaInicialComida.Add(currentComidaImage3);
            listaInicialComida.Add(currentComidaImage4);
            listaInicialComida.Add(currentComidaImage5);
        }

        private void RecargaListaContainerTienda()
        {
            listaInicialTienda.Clear();
            listaInicialTienda.Add(currentTiendaImage1);
            listaInicialTienda.Add(currentTiendaImage2);
            listaInicialTienda.Add(currentTiendaImage3);
            listaInicialTienda.Add(currentTiendaImage4);
            listaInicialTienda.Add(currentTiendaImage5);
        }

        private void btnBebidasCalientes_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            NavigationService.Navigate(new Uri(StarbucksResource.View_BebidasCalientes, UriKind.Relative));
        }

        private void btnBebidasFrias_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            NavigationService.Navigate(new Uri(StarbucksResource.View_BebidasFrias, UriKind.Relative));
        }

        private void btnPerfil_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            NavigationService.Navigate(new Uri(StarbucksResource.View_Configuracion, UriKind.Relative));
        }

        private void btnAcercaDe_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            NavigationService.Navigate(new Uri(StarbucksResource.View_AcercaDe, UriKind.Relative));
        }

        private void Rectangle_Tap_3(object sender, System.Windows.Input.GestureEventArgs e)
        {
            NavigationService.Navigate(new Uri(StarbucksResource.View_ConstructorBebidas, UriKind.Relative));
        }

        private void btnVeneficios_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            try
            {
                //if (App.Settings.Contains("Logged"))
                //{
                //    if (((Model.Usuario)App.Settings["Logged"]).Mensaje != "" || ((Model.Usuario)App.Settings["Logged"]) != null)
                NavigationService.Navigate(new Uri("/View/Rewards/Beneficios.xaml", UriKind.Relative));
                //}
                //else
                //{
                //    App.Settings["Logged"] = null;
                //    App.Settings.Save();
                //    NavigationService.Navigate(new Uri(StarbucksResource.View_RewardBeneficios, UriKind.Relative));
                //    PathEstrella.Visibility = System.Windows.Visibility.Collapsed;
                //    txtEstrellasAlmacenadas.Visibility = System.Windows.Visibility.Collapsed;
                //}
            }
            catch (Exception)
            {
                //App.Settings["Logged"] = null;
                //App.Settings.Save();
                //NavigationService.Navigate(new Uri(StarbucksResource.View_RewardBeneficios, UriKind.Relative));
                //PathEstrella.Visibility = System.Windows.Visibility.Collapsed;
                //txtEstrellasAlmacenadas.Visibility = System.Windows.Visibility.Collapsed;
            }
        }

        private void StartTiles()
        {
            _splashTimer.Start();
            _splashTimerCafes.Start();
            _splashTimerComida.Start();
            _splashTimerTiendas.Start();
        }

        private void StopTiles()
        {
            _splashTimer.Stop();
            _splashTimerCafes.Stop();
            _splashTimerComida.Stop();
            _splashTimerTiendas.Stop();
        }

        private void mostrarVaso()
        {
            Vaso_out.Visibility = System.Windows.Visibility.Collapsed;
            Vaso.Visibility = System.Windows.Visibility.Visible;
            PathEstrella.Visibility = System.Windows.Visibility.Visible;
            txtEstrellasAlmacenadas.Visibility = System.Windows.Visibility.Visible;
            NavigationService.Navigate(new Uri("/View/Rewards/Detalle.xaml", UriKind.Relative));
        }

        private void cerrarSesion()
        {
            guardarCerrarSesion("Sesion.txt");
            mitarjeta.Visibility = Visibility.Collapsed;
            grvEntrar.Visibility = Visibility.Visible;
            grvSalir.Visibility = Visibility.Collapsed;
            this.PanoramaHome.DefaultItem = this.PanoramaHome.DefaultItem;
            PanoramaHome.DefaultItem = PanoramaHome.Items[3];
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            string strItemIndex;
            if (NavigationContext.QueryString.TryGetValue("goto", out strItemIndex))
            {
                this.PanoramaHome.DefaultItem = this.PanoramaHome.DefaultItem;
                PanoramaHome.DefaultItem = PanoramaHome.Items[Convert.ToInt32(strItemIndex)];
            }

            base.OnNavigatedTo(e);
        }
            

        private void btnEntrar_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            usernamelog = txtUsuario.Text;
            passwordlog = txtPassword.Password;
            hashlog = MD5CryptoServiceProvider.GetMd5String(passwordlog);

            if (usernamelog == "" || passwordlog == "")
            {
                System.Windows.MessageBox.Show("Los campos son obligatorios");
                return;
            }

            progress2.Visibility = System.Windows.Visibility.Visible;
            btnEntrar.IsEnabled = false;
            wsRewardstClient.SyncLoginWSService(usernamelog, hashlog);
        }

        protected void wsRewardsClient_FinishLogin(object sender, FinishSmartServiceArgs<LoginResponse> e)
        {
            ExecuteSafelyDispatcher(() =>
            {
                progress2.Visibility = System.Windows.Visibility.Collapsed;
                btnEntrar.IsEnabled = true;
            });

            if (e.IsTimeOut)
            {
                ExecuteSafelyDispatcher(() => {
                    System.Windows.MessageBox.Show("No se pudo conectar con el servidor, reintente mas tarde.", "Aviso", MessageBoxButton.OK);
                });
                return;
            }

            if (!e.Success)
            {
                ExecuteSafelyDispatcher(() => { 
                    System.Windows.MessageBox.Show(e.Message);
                });
                
                return;
            }

            if (e.Data.codigo == 0)
            {

                ExecuteSafelyDispatcher(() =>
                {
                    mitarjeta.Visibility = Visibility.Visible;
                    grvEntrar.Visibility = Visibility.Collapsed;
                    grvSalir.Visibility = Visibility.Visible;
                    this.PanoramaHome.DefaultItem = this.PanoramaHome.DefaultItem;
                    PanoramaHome.DefaultItem = PanoramaHome.Items[3];
                    Uri uri = new Uri(e.Data.tarjetas[0].imagen.grande, UriKind.Absolute);
                    tarjetaGrandeImg.Source = new BitmapImage(uri);
                    double inputValue = Math.Round(e.Data.tarjetas[0].saldo, 2);
                    saldotext.Text = "$" + String.Format("{0:0.00}", inputValue);
                    DateTime now = DateTime.Now;
                    actualizadotext.Text = "Actualizado el " + now;
                    cardnumber.Text = "Tu número de tarjeta es: "+e.Data.tarjetas[0].idTarjeta.ToString();
                    idusuariolog = e.Data.idUsuario;
                    tokenlog = e.Data.token;
                    idTarjetalog = Convert.ToInt64(e.Data.tarjetas[0].idTarjeta);
                    uritarjetamov = e.Data.tarjetas[0].imagen.mediana;
                    uritarjetalog = e.Data.tarjetas[0].imagen.grande;

                    DatosRecarga.TarjetaCadena = e.Data.tarjetas[0].numeroTarjeta;
                    DatosRecarga.DatosCliente = e.Data.datosCliente;
                    DatosRecarga.DatosCliente.esClientePrevio = "Y";

                    guardar("Sesion.txt");
                    refreshRewards();
                    txtUsuario.Text = "";
                    txtPassword.Password = "";
                    btnEntrar.IsEnabled = true;
                });
            }
            else { 
                ExecuteSafelyDispatcher(() =>
                {
                    System.Windows.MessageBox.Show(e.Data.descripcion, "Aviso", MessageBoxButton.OK);
                });
            }
        }

        void RewardsViewModel_LoadCompletedAll(string pantalla, IList args)
        {
            //try
            //{
            //    switch (pantalla)
            //    {
            //        case "Ingreso":
            //            if (App.RewardsViewModel.UsuarioActivo.Mensaje == "")
            //            {
            //                App.RewardsViewModel.LoadCompletedAll -= RewardsViewModel_LoadCompletedAll;
            //                if (App.RewardsViewModel.UsuarioActivo == null)
            //                    throw new Exception();


            //                grvEntrar.Visibility = System.Windows.Visibility.Collapsed;
            //                grvSalir.Visibility = System.Windows.Visibility.Visible;
            //                //containerLog.DataContext = (Model.Usuario)App.Settings["Logged"];
            //                btnEntrar.IsEnabled = true;
            //                Dispatcher.BeginInvoke(() =>
            //                {
            //                    progress2.Visibility = System.Windows.Visibility.Collapsed;
            //                });
            //                Vaso_out.Visibility = System.Windows.Visibility.Collapsed;
            //                Vaso.Visibility = System.Windows.Visibility.Visible;
            //                containerLog.DataContext = (Model.Usuario)App.Settings["Logged"];
            //                PathEstrella.Visibility = System.Windows.Visibility.Visible;
            //                txtEstrellasAlmacenadas.Visibility = System.Windows.Visibility.Visible;
            //                NavigationService.Navigate(new Uri("/View/Rewards/Detalle.xaml", UriKind.Relative));
            //            }
            //            else
            //            {
            //                App.RewardsViewModel.LoadCompletedAll -= RewardsViewModel_LoadCompletedAll;
            //                System.Windows.MessageBox.Show(App.RewardsViewModel.UsuarioActivo.Mensaje);
            //                progress2.Visibility = System.Windows.Visibility.Collapsed;
            //                btnEntrar.IsEnabled = true;
            //            }
            //            break;
            //    }
            //}
            //catch
            //{
            //    App.RewardsViewModel.LoadCompletedAll -= RewardsViewModel_LoadCompletedAll;
            //    progress2.Visibility = System.Windows.Visibility.Collapsed;
            //}
        }

        void RewardsViewModel_LoadCompleted(object sender, EventArgs e)
        {
            NavigationService.Navigate(new Uri(StarbucksResource.View_RewardDetalle, UriKind.Relative));
        }

        private void btnMisBebidas_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            App.SeccionActivaFavoritos = "bebidas";
            NavigationService.Navigate(new Uri(StarbucksResource.View_FavoritosBebidas, UriKind.Relative));
        }

        private void btnBuscaTiendas_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            NavigationService.Navigate(new Uri(string.Format("{0}?search={1}", StarbucksResource.View_ResultadosTiendas, txtTiendas.Text), UriKind.Relative));
            txtTiendas.Text = string.Empty;
        }

        private void txtTiendas_KeyUp(object sender, System.Windows.Input.KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                NavigationService.Navigate(new Uri(string.Format("{0}?search={1}", StarbucksResource.View_ResultadosTiendas, txtTiendas.Text), UriKind.Relative));
                txtTiendas.Text = string.Empty;
            }
        }

        protected override void OnBackKeyPress(System.ComponentModel.CancelEventArgs e)
        { App.Quit(); }

        private void btnMisCafes_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            App.SeccionActivaFavoritos = "cafes";
            NavigationService.Navigate(new Uri(StarbucksResource.View_FavoritosCafes, UriKind.Relative));
        }

        private void btnMisAlimentos_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            App.SeccionActivaFavoritos = "alimentos";
            NavigationService.Navigate(new Uri(StarbucksResource.View_FavoritosAlimentos, UriKind.Relative));
        }

        private void btnOlvidoContrasenia_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            NavigationService.Navigate(new Uri(StarbucksResource.View_RewardPassword, UriKind.Relative));
        }

        private void btnMisTiendas_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            App.SeccionActivaFavoritos = "tiendas";
            NavigationService.Navigate(new Uri(StarbucksResource.View_FavoritosTiendas, UriKind.Relative));
        }

        private void btnMiCafe_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            NavigationService.Navigate(new Uri(StarbucksResource.View_Cafes, UriKind.Relative));
        }

        private void btnMiAlimento_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            NavigationService.Navigate(new Uri(StarbucksResource.View_Alimentos, UriKind.Relative));
        }

        private void ApbAceptar_Click(object sender, EventArgs e)
        {
            this.Focus();
            var codigo = App.Settings["CodigoAcceso"].ToString();
            if (codigo != null)
            {
                if (codigo.Equals(txtCodigo.Password))
                {
                    Proteccion.Visibility = Visibility.Collapsed;
                    //YLP
                    if (!App.Settings.Contains(Constantes.MostrarMensaje_GPS))
                    {
                        if (!IsLoadMensajeGPS)
                        {
                            Starbucks.UI.Controls.MessageBoxGeneric.Show(StarbucksResource.Label_GPS_Off, "Ubicación", "OK", string.Empty);
                            IsLoadMensajeGPS = true;
                        }
                    }

                    if (App.Settings.Contains("GPS"))
                    {
                        if (Convert.ToBoolean(App.Settings["GPS"]) == true)
                        {
                            if (!isLoad)
                            {
                                watcher = new GeoCoordinateWatcher(GeoPositionAccuracy.High)
                                {
                                    MovementThreshold = 1
                                };

                                watcher.StatusChanged += watcher_StatusChanged;
                                watcher.Start();
                                _splashTimerTiendas.Tick -= _splashTimerTiendas_Tick;
                                _splashTimerTiendas.Stop();
                            }
                        }
                        else
                        {
                            if (watcher != null)
                            {
                                App.ViewModel.GpsDisponible = false;
                                watcher.Stop();
                                watcher = null;
                                isLoad = false;
                                _splashTimerTiendas.Tick -= _splashTimerTiendas_Tick;
                                _splashTimerTiendas.Stop();
                                CargaTiendasCercanas();
                            }
                            else
                            {
                                _splashTimerTiendas.Tick -= _splashTimerTiendas_Tick;
                                _splashTimerTiendas.Stop();
                                CargaTiendasCercanas();
                            }
                        }
                    }

                    //if (App.Settings.Contains("Logged") && App.Settings["Logged"] != null)
                    //{
                    //    Vaso_out.Visibility = System.Windows.Visibility.Collapsed;
                    //    Vaso.Visibility = System.Windows.Visibility.Visible;
                    //    containerLog.DataContext = (Model.Usuario)App.Settings["Logged"];
                    //    PathEstrella.Visibility = System.Windows.Visibility.Visible;
                    //    txtEstrellasAlmacenadas.Visibility = System.Windows.Visibility.Visible;

                    //}

                    if (isPDF417 == 1)
                    {
                        txtCodigo.Password = string.Empty;
                        isPDF417 = 0;
                        if (myFile.FileExists("Sesion.txt"))
                        {

                            {
                                idTarjetalog = Convert.ToInt64(IsolatedStorageSettings.ApplicationSettings["idTarjeta"]);
                                setImagePdf417();
                            }
                        }

                        startTimer.Begin();

                    }
                    //END
                }
                else
                    System.Windows.MessageBox.Show("Código incorrecto.");
            }
        }

        private void TextBox_GotFocus(object sender, System.Windows.RoutedEventArgs e)
        {
            ApplicationBar.IsVisible = true;
        }

        private void TextBox_LostFocus(object sender, System.Windows.RoutedEventArgs e)
        {
            ApplicationBar.IsVisible = false;
        }

        private void MenuItem_Click(object sender, RoutedEventArgs e)
        {
            ShellTile TileToFind = ShellTile.ActiveTiles.FirstOrDefault(x => x.NavigationUri.ToString().Contains("TitleMisBebidas=FromTile"));
            if (TileToFind == null)
            {
                StandardTileData NewTileData = new StandardTileData
                {
                    BackgroundImage = new Uri("/Resources/PintoStart/pintostart_bebidas.png", UriKind.Relative)
                };

                ShellTile.Create(new Uri(string.Format("{0}{1}", StarbucksResource.View_FavoritosBebidas, "&TitleMisBebidas=FromTile"), UriKind.Relative), NewTileData);
                GuardaInfoEnTombstoned();
            }
        }

        private void MenuItemBebidasCalientes_Click(object sender, RoutedEventArgs e)
        {
            ShellTile TileToFind = ShellTile.ActiveTiles.FirstOrDefault(x => x.NavigationUri.ToString().Contains("TitleBebidasCalientes=FromTile"));
            if (TileToFind == null)
            {
                StandardTileData NewTileData = new StandardTileData
                {
                    BackgroundImage = new Uri("/Resources/PintoStart/pintostart_bebidas_calientes_new.png", UriKind.Relative)
                };

                ShellTile.Create(new Uri(string.Format("{0}{1}", StarbucksResource.View_BebidasCalientes, "&TitleBebidasCalientes=FromTile"), UriKind.Relative), NewTileData);
                GuardaInfoEnTombstoned();
            }
        }

        private void MenuItemBebidasFrias_Click(object sender, RoutedEventArgs e)
        {
            ShellTile TileToFind = ShellTile.ActiveTiles.FirstOrDefault(x => x.NavigationUri.ToString().Contains("TitleBebidasFrias=FromTile"));
            if (TileToFind == null)
            {
                StandardTileData NewTileData = new StandardTileData
                {
                    BackgroundImage = new Uri("/Resources/PintoStart/pintostart_bebidas_frias_new.png", UriKind.Relative)
                };

                ShellTile.Create(new Uri(string.Format("{0}{1}", StarbucksResource.View_BebidasFrias, "&TitleBebidasFrias=FromTile"), UriKind.Relative), NewTileData);
                GuardaInfoEnTombstoned();
            }
        }

        private void MenuItemMiCafe_Click(object sender, RoutedEventArgs e)
        {
            ShellTile TileToFind = ShellTile.ActiveTiles.FirstOrDefault(x => x.NavigationUri.ToString().Contains("TitleMiCafe=FromTile"));
            if (TileToFind == null)
            {
                StandardTileData NewTileData = new StandardTileData
                {
                    BackgroundImage = new Uri("/Resources/PintoStart/pintostart_cafe.png", UriKind.Relative)
                };

                ShellTile.Create(new Uri(string.Format("{0}{1}", StarbucksResource.View_FavoritosCafes, "&TitleMiCafe=FromTile"), UriKind.Relative), NewTileData);
                GuardaInfoEnTombstoned();
            }
        }

        private void MenuItemMisAlimentos_Click(object sender, RoutedEventArgs e)
        {
            ShellTile TileToFind = ShellTile.ActiveTiles.FirstOrDefault(x => x.NavigationUri.ToString().Contains("TitleMisAlimentos=FromTile"));
            if (TileToFind == null)
            {
                StandardTileData NewTileData = new StandardTileData
                {
                    BackgroundImage = new Uri("/Resources/PintoStart/pintostart_alimentos.png", UriKind.Relative)
                };

                ShellTile.Create(new Uri(string.Format("{0}{1}", StarbucksResource.View_FavoritosAlimentos, "&TitleMisAlimentos=FromTile"), UriKind.Relative), NewTileData);
                GuardaInfoEnTombstoned();
            }
        }

        private void MenuItemMisTiendas_Click(object sender, RoutedEventArgs e)
        {
            ShellTile TileToFind = ShellTile.ActiveTiles.FirstOrDefault(x => x.NavigationUri.ToString().Contains("TitleMisTiendas=FromTile"));
            if (TileToFind == null)
            {
                StandardTileData NewTileData = new StandardTileData
                {
                    BackgroundImage = new Uri("/Resources/PintoStart/pintostart_tiendas.png", UriKind.Relative)
                };

                ShellTile.Create(new Uri(string.Format("{0}{1}", StarbucksResource.View_FavoritosTiendas, "&TitleMisTiendas=FromTile"), UriKind.Relative), NewTileData);
                GuardaInfoEnTombstoned();
            }
        }

        private void GuardaInfoEnTombstoned()
        {
            if (!App.Settings.Contains("grupoTombstoned"))
            {
                var grupoTombstoned = new List<IList<string>>();
                var listCafes = new List<string>();
                var listBebidas = new List<string>();
                var listComidas = new List<string>();
                var listTiendas = new List<string>();

                Cafes.ListCafes.ForEach(c => listCafes.Add(Helper.Common.XmlDataSerializer<Cafe>(c)));
                Bebidas.ListBebidas.ForEach(c => listBebidas.Add(Helper.Common.XmlDataSerializer<Bebida>(c)));
                Comidas.ListComidas.ForEach(c => listComidas.Add(Helper.Common.XmlDataSerializer<Comida>(c)));
                Tiendas.ListTiendas.ForEach(c => listTiendas.Add(Helper.Common.XmlDataSerializer<Tienda>(c)));

                grupoTombstoned.Add(listCafes);
                grupoTombstoned.Add(listBebidas);
                grupoTombstoned.Add(listComidas);
                grupoTombstoned.Add(listTiendas);
                App.Settings["grupoTombstoned"] = grupoTombstoned;
                App.Settings.Save();
            }
        }

        private void PanoramaHome_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            //btnEntrar.Focus();
            //if (PanoramaHome.SelectedIndex == 2)
            //{
            //    if (App.Settings.Contains("Logged"))
            //    {
            //        if (App.Settings["Logged"] != null)
            //        {
            //            grvSalir.Visibility = System.Windows.Visibility.Visible;
            //            grvEntrar.Visibility = System.Windows.Visibility.Collapsed;
            //        }
            //        else
            //        {
            //            grvSalir.Visibility = System.Windows.Visibility.Collapsed;
            //            grvEntrar.Visibility = System.Windows.Visibility.Visible;
            //        }
            //    }
            //}
        }

        private void btnSalir_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            //puesto manual
            //cerrarSesion();
            //return;

            //if (App.Settings.Contains("Logged"))
            //{
            //    PathEstrella.Visibility = System.Windows.Visibility.Collapsed;
            //    txtEstrellasAlmacenadas.Visibility = System.Windows.Visibility.Collapsed;
            //    App.Settings["Logged"] = null;
            //    App.Settings.Save();
            //    Vaso_out.Visibility = System.Windows.Visibility.Visible;
            //    Vaso.Visibility = System.Windows.Visibility.Collapsed;
            //    grvSalir.Visibility = System.Windows.Visibility.Collapsed;
            //    grvEntrar.Visibility = System.Windows.Visibility.Visible;
            //    txtPassword.Password = string.Empty;
            //    txtUsuario.Text = string.Empty;
            //}
        }

        #region config
        private void btnLocalizaTiendas_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            NavigationService.Navigate(new Uri(StarbucksResource.View_LocalizaTiendas, UriKind.Relative));
        }

        private void btnCodigoAcceso_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            NavigationService.Navigate(new Uri(StarbucksResource.View_CodigoAcceso, UriKind.Relative));
        }

        #endregion

        private void txtPassword_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                usernamelog = txtUsuario.Text;
                passwordlog = txtPassword.Password;
                hashlog = MD5CryptoServiceProvider.GetMd5String(passwordlog);

                if (usernamelog == "" || passwordlog == "")
                {
                    System.Windows.MessageBox.Show("Los campos son obligatorios");
                    return;
                }

                progress2.Visibility = System.Windows.Visibility.Visible;
                btnEntrar.IsEnabled = false;
                wsRewardstClient.SyncLoginWSService(usernamelog, hashlog);
            }
        }

        private void txtPassword_LostFocus(object sender, System.Windows.RoutedEventArgs e)
        {
            if (string.IsNullOrEmpty(txtPassword.Password))
            {
                txtPassword.Visibility = System.Windows.Visibility.Collapsed;
                txtWaterPassword.Visibility = System.Windows.Visibility.Visible;
            }
        }

        private void txtWaterPassword_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            txtPassword.Visibility = System.Windows.Visibility.Visible;
            txtWaterPassword.Visibility = System.Windows.Visibility.Collapsed;
            txtPassword.Focus();
        }

        private void btnMapa_Click(object sender, RoutedEventArgs e)
        {
            string urlMapa = "/View/Tiendas/Mapa.xaml?latitud=" + _wacherLat + "&longitud=" + _wacherLong;
            NavigationService.Navigate(new Uri(urlMapa, UriKind.Relative));
        }

        private void txtUsuario_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                txtPassword.Visibility = System.Windows.Visibility.Visible;
                txtWaterPassword.Visibility = System.Windows.Visibility.Collapsed;
                txtPassword.Focus();
            }
            //txtWaterPassword.Focus();
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {

            var objString = WebBrowserX.InvokeScript("ver_bb");
            string s2 = objString.ToString();
            IsolatedStorageSettings.ApplicationSettings["PCHuella"] = s2;
            IsolatedStorageSettings.ApplicationSettings.Save();


            if (IsolatedStorageSettings.ApplicationSettings.Contains("PCHuella"))
            {
                System.Windows.MessageBox.Show(IsolatedStorageSettings.ApplicationSettings["PCHuella"].ToString());
                string urlRecara = "/View/Pagos/Recarga.xaml?saldo=" + IsolatedStorageSettings.ApplicationSettings["saldotemporal"].ToString() + "&actualizado=" + IsolatedStorageSettings.ApplicationSettings["fechaactualizado"].ToString() + "&imagenURL=" + IsolatedStorageSettings.ApplicationSettings["uriTarjeta"].ToString();
                NavigationService.Navigate(new Uri(urlRecara, UriKind.Relative));
            }
            
        }

        public void refreshRewards()
        {
            progress2.Visibility = System.Windows.Visibility.Visible;
            wsRewardstClient.SyncConsultaEstatusRewardsWSService();
        }

        private void ExecuteSafelyDispatcher(Action action)
        {
            if (Deployment.Current.Dispatcher.CheckAccess())
            {
                action();
            }
            else
            {
                Deployment.Current.Dispatcher.BeginInvoke(() =>
                {
                    action();
                });
            }
        }

        protected void wsSmartClient_FinishConsultaEstatusRewards(object sender, FinishSmartServiceArgs<ConsultaEstatusRewardsResponse> e)
        {


            ExecuteSafelyDispatcher(() =>
            {
                progress2.Visibility = System.Windows.Visibility.Collapsed;
                ContentPanelProgress.Visibility = System.Windows.Visibility.Collapsed;
            });

            if (e.IsTimeOut)
            {
                return;
            }

            if (!e.Success)
            {
                ExecuteSafelyDispatcher(() => System.Windows.MessageBox.Show("Ocurrio un error al consultar tus estrellas ganadas"));
                return;
            }

            if (e.Data.codigo == 0)
            {

                ExecuteSafelyDispatcher(() =>
                {
                    //Correcto
                    int numeroEstrellas;
                    int estrellasFaltantes = Convert.ToInt32(e.Data.numEstrellasFaltantes);
                    string txtNextLevel;
                    if (e.Data.nivelActual.Equals("WELCOME"))
                    {
                        numeroEstrellas = Convert.ToInt32(e.Data.numEstrellas);
                        txtNextLevel = "Necesitas " + estrellasFaltantes + " estrellas más para el nivel Green";
                    }
                    else
                    {
                        if (e.Data.nivelActual.Equals("GREEN"))
                        {
                            numeroEstrellas = Convert.ToInt32(e.Data.numEstrellas);
                            txtNextLevel = "Necesitas " + estrellasFaltantes + " estrellas más para el nivel Gold";
                        }
                        else
                        {
                            numeroEstrellas = Convert.ToInt32(e.Data.numEstrellasDoradas) % 15;
                            txtNextLevel = "Necesitas " + estrellasFaltantes + " estrellas más para tu Bebida Gratis";
                        }
                    }
                    Vaso2.Source = getImageGlassStars(Convert.ToInt32(e.Data.numEstrellas));
                    tbStars.Text = numeroEstrellas.ToString();
                    tbStarsFaltantes.Text = txtNextLevel;
                });
            }
            else if (e.Data.codigo == 3) {
                ExecuteSafelyDispatcher(() =>
                {
                    Vaso.Visibility = Visibility.Collapsed;
                });
            }
        }

        public void LoadData(string sFile)
        {
            
            //myFile.DeleteFile(sFile);
            if (!myFile.FileExists(sFile))
            {
                IsolatedStorageFileStream dataFile = myFile.CreateFile(sFile);
                dataFile.Close();
            }
            else
            {
                //Reading and loading data
                StreamReader reader = new StreamReader(new IsolatedStorageFileStream(sFile, FileMode.Open, myFile));
                string rawData = reader.ReadToEnd();
                reader.Close();

                string[] sep = new string[] { "\r\n" }; //Splittng it with new line
                string[] arrData = rawData.Split(sep, StringSplitOptions.RemoveEmptyEntries);
                try
                {
                    
                    if (arrData[0] == "sesionabierta")
                    {
                        
                        saldotext.Text = IsolatedStorageSettings.ApplicationSettings["saldotemporal"].ToString();
                        actualizadotext.Text = IsolatedStorageSettings.ApplicationSettings["fechaactualizado"].ToString();
                        grvEntrar.Visibility = Visibility.Collapsed;
                        grvSalir.Visibility = Visibility.Visible;
                        mitarjeta.Visibility = Visibility.Visible;
                        this.PanoramaHome.DefaultItem = this.PanoramaHome.DefaultItem;
                        refreshSaldo();
                        refreshRewards();
                        refreshProgramacion();
                    }
                    else if (arrData[0] == "sesioncerrada")
                    {
                        grvEntrar.Visibility = Visibility.Visible;
                        grvSalir.Visibility = Visibility.Collapsed;
                    }

                    if (arrData[0] == "actualizarsaldo")
                    {
                        guardar("Actualizar.txt");
                        mitarjeta.Visibility = Visibility.Visible;
                        this.PanoramaHome.DefaultItem = this.PanoramaHome.DefaultItem;
                        PanoramaHome.DefaultItem = PanoramaHome.Items[3];
                    }
                }
                catch
                {
                }
            }
        }

        private void guardar(string sFile)
        {
            string sMSG = "";
            if (sFile == "Sesion.txt")
            {
                sMSG = "sesionabierta";
                
                IsolatedStorageSettings.ApplicationSettings["idusuario"] = idusuariolog;
                IsolatedStorageSettings.ApplicationSettings["token"] = tokenlog;
                IsolatedStorageSettings.ApplicationSettings["idTarjeta"] = idTarjetalog;
                IsolatedStorageSettings.ApplicationSettings["uriTarjeta"] = uritarjetalog;
                IsolatedStorageSettings.ApplicationSettings["uriTarjetaMov"] = uritarjetamov;
                IsolatedStorageSettings.ApplicationSettings["saldotemporal"] = saldotext.Text;
                IsolatedStorageSettings.ApplicationSettings["fechaactualizado"] = actualizadotext.Text;
                IsolatedStorageSettings.ApplicationSettings["DatosRecarga"] = DatosRecarga;

                (App.Current as App).DatosRecarga = DatosRecarga;
                IsolatedStorageSettings.ApplicationSettings.Save();
                tokenlog = string.Empty;
            } 
            if (sFile == "Actualizar.txt") sMSG = "desactivar";
            StreamWriter sw = new StreamWriter(new IsolatedStorageFileStream(sFile, FileMode.Create, myFile));
            sw.WriteLine(sMSG); //Wrting to the file
            sw.Close();

        }

        private void guardarCerrarSesion(string sFile)
        {
            string sMSG = "";
            if (sFile == "Sesion.txt") sMSG = "sesioncerrada";
            if (sFile == "Actualizar.txt") sMSG = "desactivar";
            StreamWriter sw = new StreamWriter(new IsolatedStorageFileStream(sFile, FileMode.Create, myFile));
            sw.WriteLine(sMSG); //Wrting to the file
            sw.Close();

            IsolatedStorageSettings.ApplicationSettings.Remove("token");
            IsolatedStorageSettings.ApplicationSettings.Save();

            mitarjeta.Visibility = Visibility.Collapsed;
            LoadData("Sesion.txt");
            LoadData("Actualizar.txt");
        }

        private void TextBlock_Tap_1(object sender, System.Windows.Input.GestureEventArgs e)
        {
            NavigationService.Navigate(new Uri("/View/Pagos/Registro.xaml", UriKind.Relative));
        }

        private void Image_Tap_1(object sender, System.Windows.Input.GestureEventArgs e)
        {
            if (App.Settings.Contains("CodigoAcceso"))
            {
                isPDF417 = 1;
                Proteccion.Visibility = Visibility.Visible;
            }
            else
            {
                if (myFile.FileExists("Sesion.txt"))
                {
                    {
                        idTarjetalog = Convert.ToInt64(IsolatedStorageSettings.ApplicationSettings["idTarjeta"]);
                        setImagePdf417();
                    }
                }

                startTimer.Begin();
            }
            
        }

        void myStoryboard_Completed(object sender, EventArgs e)
        {
            paneltarjeta.Visibility = System.Windows.Visibility.Collapsed;
            //startTimer.Stop();
            paneltarjetaoff.Visibility = System.Windows.Visibility.Visible;
            startTimer1.Begin();
        }

        void myStoryboard_Completed2(object sender, EventArgs e)
        {
            //startTimer1.Stop();
        }

        private void Image_Tap_2(object sender, System.Windows.Input.GestureEventArgs e)
        {
            startTimer2.Begin();
        }

        void myStoryboard_Completed3(object sender, EventArgs e)
        {
            paneltarjetaoff.Visibility = System.Windows.Visibility.Collapsed;
            //startTimer2.Stop();
            paneltarjeta.Visibility = System.Windows.Visibility.Visible;
            startTimer3.Begin();
        }

        void myStoryboard_Completed4(object sender, EventArgs e)
        {
            //startTimer3.Stop();
        }

        private void setImagePdf417()
        {
            ZXing.BarcodeWriter pdf417 = new ZXing.BarcodeWriter();
            pdf417.Renderer = new ZXing.Rendering.WriteableBitmapRenderer();
            pdf417.Format = ZXing.BarcodeFormat.PDF_417;

            var bitmap = pdf417.Write(idTarjetalog.ToString());

            var height = bitmap.PixelHeight;
            var width = bitmap.PixelWidth;

            WriteableBitmap wb = new WriteableBitmap(width - 40, (height - 50) * 2);

            for (int i = 20; i < width - 20; i++)
            {
                for (int j = 25; j < height - 25; j++)
                {
                    wb.Pixels[((j - 25) * 2) * (width - 40) + (i - 20)] = bitmap.Pixels[j * width + i];
                    wb.Pixels[((j - 25) * 2 + 1) * (width - 40) + (i - 20)] = bitmap.Pixels[j * width + i];
                }
            }

            tarjetapdf.Source = wb;
        }

        private void Button_Tap_1(object sender, System.Windows.Input.GestureEventArgs e)
        {
            recargar = true;
            refreshSaldo();
           // refreshRewards();
        }

        private BitmapImage getImageGlassStars(int numberStars)
        {
            BitmapImage image;
            switch (numberStars)
            {
                case 1:
                    image = new BitmapImage(new Uri("Resources/rewards/rewards_glass_1_star.png", UriKind.Relative));
                    return image;
                case 2:
                    image = new BitmapImage(new Uri("Resources/rewards/rewards_glass_2_stars.png", UriKind.Relative));
                    return image;
                case 3:
                    image = new BitmapImage(new Uri("Resources/rewards/rewards_glass_3_stars.png", UriKind.Relative));
                    return image;
                case 4:
                    image = new BitmapImage(new Uri("Resources/rewards/rewards_glass_4_stars.png", UriKind.Relative));
                    return image;
                case 5:
                    image = new BitmapImage(new Uri("Resources/rewards/rewards_glass_5_stars.png", UriKind.Relative));
                    return image;
                case 6:
                    image = new BitmapImage(new Uri("Resources/rewards/rewards_glass_6_stars.png", UriKind.Relative));
                    return image;
                case 7:
                    image = new BitmapImage(new Uri("Resources/rewards/rewards_glass_7_stars.png", UriKind.Relative));
                    return image;
                case 8:
                    image = new BitmapImage(new Uri("Resources/rewards/rewards_glass_8_stars.png", UriKind.Relative));
                    return image;
                case 9:
                    image = new BitmapImage(new Uri("Resources/rewards/rewards_glass_9_stars.png", UriKind.Relative));
                    return image;
                case 10:
                    image = new BitmapImage(new Uri("Resources/rewards/rewards_glass_10_stars.png", UriKind.Relative));
                    return image;
                case 11:
                    image = new BitmapImage(new Uri("Resources/rewards/rewards_glass_11_stars.png", UriKind.Relative));
                    return image;
                case 12:
                    image = new BitmapImage(new Uri("Resources/rewards/rewards_glass_12_stars.png", UriKind.Relative));
                    return image;
                case 13:
                    image = new BitmapImage(new Uri("Resources/rewards/rewards_glass_13_stars.png", UriKind.Relative));
                    return image;
                case 14:
                    image = new BitmapImage(new Uri("Resources/rewards/rewards_glass_14_stars.png", UriKind.Relative));
                    return image;
                case 15:
                    image = new BitmapImage(new Uri("Resources/rewards/rewards_glass_15_stars.png", UriKind.Relative));
                    return image;
                case 16:
                    image = new BitmapImage(new Uri("Resources/rewards/rewards_glass_16_stars.png", UriKind.Relative));
                    return image;
                case 17:
                    image = new BitmapImage(new Uri("Resources/rewards/rewards_glass_17_stars.png", UriKind.Relative));
                    return image;
                case 18:
                    image = new BitmapImage(new Uri("Resources/rewards/rewards_glass_18_stars.png", UriKind.Relative));
                    return image;
                case 19:
                    image = new BitmapImage(new Uri("Resources/rewards/rewards_glass_19_stars.png", UriKind.Relative));
                    return image;
                case 20:
                    image = new BitmapImage(new Uri("Resources/rewards/rewards_glass_20_stars.png", UriKind.Relative));
                    return image;
                case 21:
                    image = new BitmapImage(new Uri("Resources/rewards/rewards_glass_21_star.png", UriKind.Relative));
                    return image;
                case 22:
                    image = new BitmapImage(new Uri("Resources/rewards/rewards_glass_22_stars.png", UriKind.Relative));
                    return image;
                case 23:
                    image = new BitmapImage(new Uri("Resources/rewards/rewards_glass_23_stars.png", UriKind.Relative));
                    return image;
                case 24:
                    image = new BitmapImage(new Uri("Resources/rewards/rewards_glass_24_stars.png", UriKind.Relative));
                    return image;
                case 25:
                    image = new BitmapImage(new Uri("Resources/rewards/rewards_glass_25_stars.png", UriKind.Relative));
                    return image;
                case 26:
                    image = new BitmapImage(new Uri("Resources/rewards/rewards_glass_26_stars.png", UriKind.Relative));
                    return image;
                case 27:
                    image = new BitmapImage(new Uri("Resources/rewards/rewards_glass_27_stars.png", UriKind.Relative));
                    return image;
                case 28:
                    image = new BitmapImage(new Uri("Resources/rewards/rewards_glass_28_stars.png", UriKind.Relative));
                    return image;
                case 29:
                    image = new BitmapImage(new Uri("Resources/rewards/rewards_glass_29_stars.png", UriKind.Relative));
                    return image;
                case 30:
                    image = new BitmapImage(new Uri("Resources/rewards/rewards_glass_30_stars.png", UriKind.Relative));
                    return image;
                default:
                    image = new BitmapImage(new Uri("Resources/rewards/rewards_glass.png", UriKind.Relative));
                    return image;
            }
        }

        public class ValidateToken
        {
            public int codigo;
            public string descripcion;
        }

        public class InfoLogin
        {
            public int codigo;
            public string descripcion;
            public string token;
            public long idUsuario;
            public List<Tarjetas> tarjetas;
            public DatosCliente datosCliente;
        }

        public class Tarjetas
        {
            public long idTarjeta;
            public string numeroTarjeta;
            public string nivelLealtad;
            public Imagen imagen;
            public string alias;
            public double saldo;
            public bool esPrincipal;
            public string fechaActivacion;

        }

        public class Imagen
        {
            public string grande;
            public string icon;
            public string mediana;
            public string chica;
        }

        public class InfoConsultaSaldoTarjeta
        {
            public int codigo;
            public string descripcion;
            public double saldo;
        }

        public class InfoConsultaEstatusRewards
        {
            public int codigo;
            public string descripcion;
            public int numEstrellas;
            public int numEstrellasDoradas;
            public string miembroDesde;
            public int numEstrellasFaltantes;
            public string nivelActual;
            public string siguienteNivel;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void MovimientoButton_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            string url = string.Format("{0}?saldo={1}&fecha={2}", StarbucksResource.View_ConsultaTransacciones, saldotext.Text, actualizadotext.Text);

            NavigationService.Navigate(new Uri(url, UriKind.Relative));
        }

        /// <summary>
        /// 
        /// </summary>
        public void refreshSaldo()
        {
            progress3.Visibility = Visibility.Visible;
            DateTime now = DateTime.Now;
            string fecha = "" + now;
            if (IsolatedStorageSettings.ApplicationSettings["fechaactualizado"].ToString() != fecha || recargar == true)
            {
                recargar = false;
                progress2.Visibility = System.Windows.Visibility.Visible;
                idusuariolog = Convert.ToInt64(IsolatedStorageSettings.ApplicationSettings["idusuario"]);
                idTarjetalog = Convert.ToInt64(IsolatedStorageSettings.ApplicationSettings["idTarjeta"]);
                uritarjetalog = IsolatedStorageSettings.ApplicationSettings["uriTarjeta"].ToString();
                uritarjetamov = IsolatedStorageSettings.ApplicationSettings["uriTarjetaMov"].ToString();

                Uri uri = new Uri(uritarjetalog, UriKind.Absolute);
                tarjetaGrandeImg.Source = new BitmapImage(uri);
                cardnumber.Text = "Tu número de tarjeta es: "+idTarjetalog.ToString();

                progress2.Visibility = System.Windows.Visibility.Visible;

                wsRewardstClient.SyncConsultaSaldoTarjetaWSService(idTarjetalog);
            }

        }

        protected void wsRewardsClient_FinishConsultaSaldoTarjeta(object sender, FinishSmartServiceArgs<ConsultaSaldoTarjetaResponse> e) 
        {
            ExecuteSafelyDispatcher(() =>
            {
                progress3.Visibility = System.Windows.Visibility.Collapsed;
                progress2.Visibility = System.Windows.Visibility.Collapsed;
            });

            if (e.IsTimeOut)
            {
                return;
            }

            if (!e.Success)
            {
                ExecuteSafelyDispatcher(() => System.Windows.MessageBox.Show("Ocurrio un error al actualizar tu saldo"));
                return;
            }

            if (e.Data.Codigo == 0)
            {
                ExecuteSafelyDispatcher(() =>
                {
                    //Correcto
                    double inputValue = Math.Round(e.Data.Saldo, 2);
                    saldotext.Text = "$" + String.Format("{0:0.00}", inputValue);
                    DateTime now = DateTime.Now;
                    actualizadotext.Text = "Actualizado el " + now;
                    IsolatedStorageSettings.ApplicationSettings["saldotemporal"] = saldotext.Text;
                    IsolatedStorageSettings.ApplicationSettings["fechaactualizado"] = actualizadotext.Text;
                    //mostrarVaso();
                    return;
                });
            }
            else if (e.Data.Codigo == 3)
            { 
                ExecuteSafelyDispatcher(() =>
                {
                    //Correcto
                    cerrarSesion();
                });
            }



        }
        /// <summary>
        /// 
        /// </summary>
        WSSmartSBXViewModel wsSmartClient = new WSSmartSBXViewModel();
        public void refreshProgramacion()
        {
            ContentPanelProgress.Visibility = System.Windows.Visibility.Visible;
            wsSmartClient.SyncConsultaProgramaRecargaWSService((int)CadenaEnum.STARBUCKS, (int)TipoDispositivoEnum.WindowsMobile);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void SmartClient_FinishConsultaProgramaRecarga(object sender, FinishSmartServiceArgs<ConsultaProgramaResponse> e)
        {

            ExecuteSafelyDispatcher(() => ContentPanelProgress.Visibility = System.Windows.Visibility.Collapsed);

            if (e.IsTimeOut)
            {
                return;
            }

            if (!e.Success)
            {
                ExecuteSafelyDispatcher(() => System.Windows.MessageBox.Show(e.Message));
                return;
            }

            if (e.Data.Codigo == 0)
            {
                ExecuteSafelyDispatcher(() => {
                    string NoTarjeta = IsolatedStorageSettings.ApplicationSettings["idTarjeta"].ToString();
                    var tarjeta = e.Data.TarjetasProgramadas.FirstOrDefault(x => x.NoTarjetaCadena == NoTarjeta);

                    if (tarjeta != null)
                    {
                        if (tarjeta.IDTipoRecarga == 1 && tarjeta.DiaRecarga != 100)
                        {
                            ExecuteSafelyDispatcher(() => programaciontext.Text = string.Format("Recargar {0} el día {1} de cada mes.", tarjeta.MontoRecarga, tarjeta.DiaRecarga));
                        }
                        else if (tarjeta.IDTipoRecarga == 1 && tarjeta.DiaRecarga == 100)
                        {
                            ExecuteSafelyDispatcher(() => programaciontext.Text = string.Format("Recargar {0} el último día del mes.", tarjeta.MontoRecarga));
                        }
                        else if (tarjeta.IDTipoRecarga == 2)
                        {
                            ExecuteSafelyDispatcher(() => programaciontext.Text = string.Format("Recargar {0} cuando el saldo sea menor a {1}.", tarjeta.MontoRecarga, tarjeta.LimiteMinimoSaldo));
                        }
                    }
                });
            }

            
        }
    }

}