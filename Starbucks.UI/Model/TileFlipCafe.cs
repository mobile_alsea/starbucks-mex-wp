﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Starbucks.UI.ViewModel;

namespace Starbucks.UI.Model
{
    public class TileFlipCafe : ViewModelBase
    {
        public Cafe First { get; set; }
        public Cafe Second { get; set; }
        private string _nombre;
        public string Nombre
        {
            get
            {
                var separacion = _nombre.Split(' ').Length;
                if (separacion > 3)
                {
                    var leyenda = _nombre.Split(' ');
                    if (_nombre.Length > 14)
                        return string.Format("{0} {1} ...", leyenda[0], leyenda[1]);
                    else
                        return _nombre;
                }
                else
                {
                    if (_nombre.Length > 26)
                        return string.Format("{0}...", _nombre.Substring(0, 26));
                    else
                        return _nombre;
                }
            }
            set { _nombre = value; }
        }
        public string Perfil { get; set; }
        public string PlecaTiles
        {
            get
            {
                var pleca = string.Empty;
                switch (Perfil)
                {
                    case "Dark":
                        pleca = "/Resources/pleca_tiles/starbucks_WP_tile_plecatxt_cafe_dark.png";
                        break;
                    case "Temporada":
                        pleca = "/Resources/pleca_tiles/starbucks_WP_tile_plecatxt_cafe_temporada.png";
                        break;
                    case "Medium":
                        pleca = "/Resources/pleca_tiles/starbucks_WP_tile_plecatxt_cafe_medium.png";
                        break;
                    case "Blonde":
                        pleca = "/Resources/pleca_tiles/starbucks_WP_tile_plecatxt_cafe_blonde.png";
                        break;
                    default:
                        break;
                }
                return pleca;
            }
        }
        public string UriImageFirst { get; set; }
        public string UriImageSecond { get; set; }
        private bool _isFavorito { get; set; }
        public bool isFavorito
        {
            get
            {
                return _isFavorito;
            }

            set
            {
                if (_isFavorito == value)
                {
                    return;
                }
                _isFavorito = value;
                NotifyPropertyChanged("isFavorito");
            }
        }
    }
}
