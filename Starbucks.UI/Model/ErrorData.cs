﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Starbucks.UI.Model
{
    public enum ErrorData
    {
        TimeOut = 1,
        ErrorInRequest = 2,
        ErrorInParser = 3
    }
}