﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Starbucks.UI.Data;

namespace Starbucks.UI.Model
{
    public class Bebida
    {
        public string nombre { get; set; }
        public string descripcion { get; set; }
        public string descripcioncorta { get; set; }
        public string disponibilidad { get; set; }
        public string temperatura { get; set; }
        public string categoria { get; set; }
        public string subcategoria { get; set; }
        public string subcategoriaUpper {
            get { return subcategoria.ToUpper(); }
        }
        public string marcadobebida { get; set; }
        public string imagen { get; set; }
        public bool isFavorito { get; set; }
        public Arte Arte
        {
            get 
            {
                var item = CodigosBebidas.Arte.Where(c=> c.Nombre.Equals(nombre)).SingleOrDefault();
                return item;
            }
        }
    }
}
