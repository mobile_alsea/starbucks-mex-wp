﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Starbucks.UI.Model
{
    public class Posicion
    {
        public Posicion(double latitud, double longitud)
        {
            Latitud = latitud;
            Longitud = longitud;
        }
        public double Latitud { get; set; }
        public double Longitud { get; set; }
    }

    public static class Extensiones
    {
        public const float RadioTierraKm = 6378.0F;
        public static float DistanciaKm(this Posicion posOrigen, Posicion posDestino)
        {
            var difLatitud = (posDestino.Latitud - posOrigen.Latitud).EnRadianes();
            var difLongitud = (posDestino.Longitud - posOrigen.Longitud).EnRadianes();

            var a = Math.Sin(difLatitud / 2).AlCuadrado() +
                    Math.Cos(posOrigen.Latitud.EnRadianes()) *
                    Math.Cos(posDestino.Latitud.EnRadianes()) *
                    Math.Sin(difLongitud / 2).AlCuadrado();
            var c = 2 * Math.Atan2(Math.Sqrt(a), Math.Sqrt(1 - a));
            return RadioTierraKm * Convert.ToSingle(c);
        }

        public static double AlCuadrado(this double valor)
        {
            return Math.Pow(valor, 2);
        }

        public static double EnRadianes(this double valor)
        {
            return Convert.ToDouble(Math.PI / 180) * valor;
        }
    }
}
