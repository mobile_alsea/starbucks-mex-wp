﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Starbucks.UI.Model
{
    public class Arte
    {
        public string Nombre { get; set; }
        public string Venti { get; set; }
        public string Grande { get; set; }
        public string Alto { get; set; }
    }
}
