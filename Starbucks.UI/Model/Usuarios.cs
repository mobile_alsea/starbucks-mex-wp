﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace Starbucks.UI.Model
{
    public class Usuarios
    {
        public static Usuario UsuarioActivo { get; set; }

        public static Usuario ObtieneDetalleRewards(string usuario, string contra)
        {   
            UsuarioActivo = new Usuario()
            {
                Cumple = DateTime.Now.ToShortDateString(),
                Estrellas = 3,
                FechaRegistro = DateTime.Now.AddDays(-10).ToShortDateString(),
                Mail = "Yair Lopez",
                NumeroTarjeta = "123456789987654321",
            };

            return UsuarioActivo;
        }
    }
}
