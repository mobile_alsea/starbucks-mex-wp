﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Starbucks.UI.Model
{
    public class RedesSociales
    {
        public string Imagen { get; set; }
        public string Descripcion { get; set; }
    }
}
