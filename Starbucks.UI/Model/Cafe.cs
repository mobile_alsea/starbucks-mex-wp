﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace Starbucks.UI.Model
{
    public class Cafe
    {
        public string nombre { get; set; }
        public string descripcion { get; set; }
        public string disponibilidad { get; set; }
        public string perfil { get; set; }
        public string perfilUpper {
            get { return perfil.ToUpper(); }
        }
        public string forma { get; set; }
        public string imagenprincipal { get; set; }
        public List<string> sabores { get; set; }
        public string saboresDesc { get; set; }
        public List<string> imagenes { get; set; }
        public string imagen { get; set; }
        public bool isFavorito { get; set; }
        public string Pleca
        {
            get
            {
                var _pleca = string.Empty;
                switch (perfil)
                {
                    case "Dark":
                        _pleca = "/Resources/pleca_detalle_cafe/starbucks_WP_detalle_pleca_cafe_dark.png";
                        break;
                    case "Temporada":
                        _pleca = "/Resources/pleca_detalle_cafe/starbucks_WP_detalle_pleca_cafe_temporada.png";
                        break;
                    case "Medium":
                        _pleca = "/Resources/pleca_detalle_cafe/starbucks_WP_detalle_pleca_cafe_medium.png";
                        break;
                    case "Blonde":
                        _pleca = "/Resources/pleca_detalle_cafe/starbucks_WP_detalle_pleca_cafe_blonde.png";
                        break;
                    default:
                        break;
                }
                return _pleca;
            }
        }
    }
}
