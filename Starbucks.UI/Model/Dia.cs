﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Starbucks.UI.Model
{
    public class Dia
    {
        public string nombre { get; set; }
        public string horainicio { get; set; }
        public string horafin { get; set; }
        public string Descripcion
        {
            get {
                return string.Format("{0} a {1}", horainicio, horafin);
            }
        }
    }
}
