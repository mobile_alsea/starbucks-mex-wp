﻿using Starbucks.UI.ViewModel;
using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace Starbucks.UI.Model
{
    public class Usuario : ViewModelBase
    {
        public string Mail { get; set; }
        public string Pass { get; set; }
        public string Cumple { get; set; }
        public string NumeroTarjeta { get; set; }
        private int _extrellas;
        public int Estrellas {
            get
            {
                if (Nivel.Equals("Gold"))
                    return (15 - EstrellasGold);
                else
                    return _extrellas;
            }
            set
            {
                if (_extrellas != value)
                {
                    _extrellas = value;
                    NotifyPropertyChanged("Estrellas");
                }
            }
        }
        public double Saldo { get; set; }
        public string FechaRegistro { get; set; }
        public int EstrellasFaltantes { get; set; }
        public string Nivel { get; set; }
        public string NivelSiguiente { get; set; }
        public int EstrellasGold { get; set; }
        public int EstrellasSiguienteNivel { get; set; }
        public string Mensaje { get; set; }

        //Textos para adaptar
        public string mensajeEstrellasSiguienteNivel1 { get; set; }
        public string mensajeEstrellasSiguienteNivel2 { get; set; }
        public string mensajeEstrellasSiguienteNivel3
        {
            get
            { 
                if(Nivel.Equals("Gold"))
                    return Estrellas > 0 ? string.Format("{0} estrellas, faltan {1}", Nivel.Equals("Gold") ? (15 - EstrellasGold).ToString() : Estrellas.ToString(), EstrellasFaltantes) : string.Format("Necesitas {0} estrella más para el nivel {1}", EstrellasFaltantes, NivelSiguiente);
                else
                    return Estrellas > 0 ? string.Format("{0} estrellas, faltan {1} para el nivel {2}", Nivel.Equals("Gold") ? (15 - EstrellasGold).ToString() : Estrellas.ToString(), EstrellasFaltantes, NivelSiguiente) : string.Format("Necesitas {0} estrella más para el nivel {1}", EstrellasFaltantes, NivelSiguiente);
            }
        }
        public string mensajemiembroDesde { get; set; }
        public string mensajeNivel { get; set; }
        public string mensajeTienesVSFaltan { get; set; }
       
    }
}
