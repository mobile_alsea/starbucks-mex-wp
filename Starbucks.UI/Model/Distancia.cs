﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Starbucks.UI.Model
{
    public class Distancia
    {
        public double Kilometros { get; set; }
        public double Metros { get; set; }
    }
}
