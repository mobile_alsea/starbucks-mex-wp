﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Starbucks.UI.ViewModel;

namespace Starbucks.UI.Model
{
    public class Tienda : ViewModelBase
    {
        public string nombre { get; set; }
        public string direccion { get; set; }
        public string codigopostal { get; set; }
        public string ciudad { get; set; }
        public string provincia { get; set; }
        public string zona { get; set; }
        public double latitud { get; set; }
        public double longitud { get; set; }
        public List<string> servicios { get; set; }
        public List<Dia> horario { get; set; }
        public string EstatusTienda
        {
            get {
                var respuesta = string.Empty;
                var diaSemana = (int)DateTime.Now.DayOfWeek;
                var horaDia = DateTime.Now.TimeOfDay;
                var hora = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, horaDia.Hours, horaDia.Minutes, horaDia.Seconds);

                switch (diaSemana)
                {
                    case 0:
                        var domingo = horario.Where(c => c.nombre.Equals("Domingo")).SingleOrDefault();
                        if (domingo != null)
                        {
                            //if (domingo.horafin.Equals("00:00"))
                            //    domingo.horafin = "23:59";
                            domingo.horafin = FormatoHoraFin(domingo.horafin);
                            if (hora >= Convert.ToDateTime(domingo.horainicio) && hora <= Convert.ToDateTime(domingo.horafin))
                                respuesta = "Abierto";
                            else
                                respuesta = "Cerrado";
                        }
                        break;
                    case 1:
                        var lunes = horario.Where(c => c.nombre.Equals("Lunes")).SingleOrDefault();
                        if (lunes != null)
                        {
                            //if (lunes.horafin.Equals("00:00"))
                            //    lunes.horafin = "23:59";
                            lunes.horafin = FormatoHoraFin(lunes.horafin);
                            if (hora >= Convert.ToDateTime(lunes.horainicio) && hora <= Convert.ToDateTime(lunes.horafin))
                                respuesta = "Abierto";
                            else
                                respuesta = "Cerrado";
                        }
                        break;
                    case 2:
                        var martes = horario.Where(c => c.nombre.Equals("Martes")).SingleOrDefault();
                        if (martes != null)
                        {
                            //if (martes.horafin.Equals("00:00"))
                            //    martes.horafin = "23:59";
                            martes.horafin = FormatoHoraFin(martes.horafin);
                            if (hora >= Convert.ToDateTime(martes.horainicio) && hora <= Convert.ToDateTime(martes.horafin))
                                respuesta = "Abierto";
                            else
                                respuesta = "Cerrado";
                        }
                        break;
                    case 3:
                        var miercoles = horario.Where(c => c.nombre.Equals("Miercoles")).SingleOrDefault();
                        if (miercoles != null)
                        {
                            //if (miercoles.horafin.Equals("00:00"))
                            //    miercoles.horafin = "23:59";
                            miercoles.horafin = FormatoHoraFin(miercoles.horafin);
                            if (hora >= Convert.ToDateTime(miercoles.horainicio) && hora <= Convert.ToDateTime(miercoles.horafin))
                                respuesta = "Abierto";
                            else
                                respuesta = "Cerrado";
                        }
                        break;
                    case 4:
                        var jueves = horario.Where(c => c.nombre.Equals("Jueves")).SingleOrDefault();
                        if (jueves != null)
                        {
                            //if (jueves.horafin.Equals("00:00"))
                            //    jueves.horafin = "23:59";
                            jueves.horafin = FormatoHoraFin(jueves.horafin);
                            if (hora >= Convert.ToDateTime(jueves.horainicio) && hora <= Convert.ToDateTime(jueves.horafin))
                                respuesta = "Abierto";
                            else
                                respuesta = "Cerrado";
                        }
                        break;
                    case 5:
                        var viernes = horario.Where(c => c.nombre.Equals("Viernes")).SingleOrDefault();
                        if (viernes != null)
                        {
                            //if (viernes.horafin.Equals("00:00"))
                            //    viernes.horafin = "23:59";
                            viernes.horafin = FormatoHoraFin(viernes.horafin);
                            if (hora >= Convert.ToDateTime(viernes.horainicio) && hora <= Convert.ToDateTime(viernes.horafin))
                                respuesta = "Abierto";
                            else
                                respuesta = "Cerrado";
                        }
                        break;
                    case 6:
                         var sabado = horario.Where(c => c.nombre.Equals("Sabado")).SingleOrDefault();
                         if (sabado != null)
                        {
                            //if (sabado.horafin.Equals("00:00"))
                            //    sabado.horafin = "23:59";
                            sabado.horafin = FormatoHoraFin(sabado.horafin);
                            if (hora >= Convert.ToDateTime(sabado.horainicio) && hora <= Convert.ToDateTime(sabado.horafin))
                                respuesta = "Abierto";
                            else
                                respuesta = "Cerrado";
                        }
                        break;
                    default:
                        break;
                }
                return respuesta;
            }
        }

        private string FormatoHoraFin(string hora)
        {
            string resultado = string.Empty;
            switch (hora)
            {
                case "00:00":
                    resultado = "23:59";
                    break;
                case "24:00":
                    resultado = "23:59";
                    break;
                default:
                    resultado = hora;
                    break;
            }   
            return resultado;
        }

        public string EstatusTiendaTile
        {
            get
            {
                return EstatusTienda.Equals("Abierto") ? "/Resources/elemntos_mapa/starbucks_WP_detalle_mapa_tileinfotiena_abierto.png" : "/Resources/elemntos_mapa/starbucks_WP_detalle_mapa_tileinfotiena_cerrado.png";
            }
        }
        private bool _isFavorito { get; set; }
        public bool isFavorito
        {
            get
            {
                return _isFavorito;
            }

            set
            {
                if (_isFavorito == value)
                {
                    return;
                }
                _isFavorito = value;
                NotifyPropertyChanged("isFavorito");
            }
        }
        public string Ciudad_CP
        {
            get
            {
                return string.Format("{0}, {1}", ciudad, codigopostal);
            }
        }
    }
}
