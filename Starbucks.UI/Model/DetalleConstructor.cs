﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Starbucks.UI.Data;
using Starbucks.UI.ViewModel;

namespace Starbucks.UI.Model
{
    public class DetalleConstructor : ViewModelBase
    {
        public int Id { get; set; }
        public string Nombre { get; set; }
        public string NombreUpper
        {
            get { return Nombre.ToUpper(); }
        }
        public string Descripcion { get; set; }
        public string Disponibilidad { get; set; }
        public bool IsFavorito { get; set; }
        public string Imagen { get; set; }
        public string Tamaño { get; set; }
        public string Tipo { get; set; }
        public string Descafeinado { get; set; }
        private string _shots;
        public string Shots
        {
            get
            {

                try
                {
                    return _shots.Equals("0") || _shots == null ? string.Empty : _shots;
                }
                catch (Exception)
                {
                    return string.Empty;
                }
            }
            set { _shots = value; }
        }
        private string _jarabe;
        public string Jarabe
        {
            get
            {
                var clave = string.Empty;
                var item = CodigosBebidas.Jarabes.Where(c => c.Nombre.ToLower().Equals(_jarabe.ToLower())).SingleOrDefault();
                if (item != null)
                {
                    clave = item.Clave;
                }
                return clave;
            }
            set
            {
                _jarabe = value;
            }
        }
        public string JarabeShot { get; set; }
        public string DetalleJarabe
        {
            get { return string.Format("{0}{1}", Jarabe.Equals(string.Empty) ? string.Empty : JarabeShot, Jarabe); }
        }
        private string _leche;
        public string Leche
        {
            get
            {
                var clave = string.Empty;
                var item = CodigosBebidas.Leches.Where(c => c.Nombre.ToLower().Equals(_leche.ToLower())).SingleOrDefault();
                if (item != null)
                {
                    clave = item.Clave;
                }
                return clave;
            }
            set
            {
                _leche = value;
            }
        }
        private string _personalizado;
        public string Personalizado
        {
            get
            {
                var clave = string.Empty;
                var item = CodigosBebidas.Personalizacion.Where(c => c.Nombre.ToLower().Equals(_personalizado.ToLower())).SingleOrDefault();
                if (item != null)
                {
                    clave = item.Clave;
                }
                return clave;
            }
            set
            {
                _personalizado = value;
            }
        }
        public string PersonalizadoShot { get; set; }
        public string DetallePersonalizado
        {
            get { return string.Format("{0}{1}", Personalizado.Equals(string.Empty) ? string.Empty : PersonalizadoShot, Personalizado); }
        }
        private string _codigoBebida;
        public string CodigoBebida
        {
            get
            {
                var clave = string.Empty;
                switch (Tipo)
                {
                    case "Espresso":
                        var item = CodigosBebidas.Espresso.Where(c => c.Nombre.ToLower().Equals(Nombre.ToLower())).SingleOrDefault();
                        if (item != null)
                            clave = item.Clave;
                        break;
                    case "Filtrado":
                        var itemFiltrado = CodigosBebidas.Filtrado.Where(c => c.Nombre.ToLower().Equals(Nombre.ToLower())).SingleOrDefault();
                        if (itemFiltrado != null)
                            clave = itemFiltrado.Clave;
                        break;
                    case "Infusiones":
                        var itemInfusiones = CodigosBebidas.Infusiones.Where(c => c.Nombre.ToLower().Equals(Nombre.ToLower())).SingleOrDefault();
                        if (itemInfusiones != null)
                            clave = itemInfusiones.Clave;
                        break;
                    case "Tés Latte":
                        var itemTeLatte = CodigosBebidas.TesLatte.Where(c => c.Nombre.ToLower().Equals(Nombre.ToLower())).SingleOrDefault();
                        if (itemTeLatte != null)
                            clave = itemTeLatte.Clave;
                        break;
                    case "Chocolates":
                        var itemChocolates = CodigosBebidas.Chocolates.Where(c => c.Nombre.ToLower().Equals(Nombre.ToLower())).SingleOrDefault();
                        if (itemChocolates != null)
                            clave = itemChocolates.Clave;
                        break;
                    case "Leche":
                        var itemLeche = CodigosBebidas.LecheBebidas.Where(c => c.Nombre.ToLower().Equals(Nombre.ToLower())).SingleOrDefault();
                        if (itemLeche != null)
                            clave = itemLeche.Clave;
                        break;
                    case "Bebidas Frias Con Café":
                        var itemConCafe = CodigosBebidas.ConCafe.Where(c => c.Nombre.ToLower().Equals(Nombre.ToLower())).SingleOrDefault();
                        if (itemConCafe != null)
                            clave = itemConCafe.Clave;
                        break;
                    case "Bebidas Frias Sin Café":
                        var itemSinCafe = CodigosBebidas.SinCafe.Where(c => c.Nombre.ToLower().Equals(Nombre.ToLower())).SingleOrDefault();
                        if (itemSinCafe != null)
                            clave = itemSinCafe.Clave;
                        break;
                    default:
                        break;
                }

                return clave;
            }
            set
            {
                _codigoBebida = value;
            }
        }
        public Arte Arte
        {
            get
            {
                var item = CodigosBebidas.Arte.Where(c => c.Nombre.Equals(Nombre)).SingleOrDefault();
                return item;
            }
        }
        public string Propietario { get; set; }
        private string _Nickname;
        public string Nickname
        {
            get { return _Nickname; }
            set
            {
                if (_Nickname == value)
                    return;
                _Nickname = value;
                NotifyPropertyChanged("Nickname");
            }
        }

        private string _Notas;
        public string Notas
        {
            get { return _Notas; }
            set
            {
                if (_Notas == value)
                    return;
                _Notas = value;
                NotifyPropertyChanged("Notas");
            }
        }

        public DetalleConstructor()
        {
            _jarabe = string.Empty;
            _leche = string.Empty;
            _personalizado = string.Empty;
        }
    }
}
