﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace Starbucks.UI.Model
{
    public class Version
    {
        public string nombre { get; set; }
        public string version { get; set; }
        public string fichero { get; set; }
    }
}
