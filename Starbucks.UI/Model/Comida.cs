﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Starbucks.UI.ViewModel;

namespace Starbucks.UI.Model
{
    public class Comida : ViewModelBase
    {
        public string nombre { get; set; }
        public string descripcion { get; set; }
        public string descripcioncorta { get; set; }
        public string disponibilidad { get; set; }
        public string categoria { get; set; }
        public string categoriaUpper {
            get { return categoria.ToUpper(); }
        }
        public string imagen { get; set; }
        private bool _isFavorito { get; set; }
        public bool isFavorito
        {
            get
            {
                return _isFavorito;
            }

            set
            {
                if (_isFavorito == value)
                {
                    return;
                }
                _isFavorito = value;
                NotifyPropertyChanged("isFavorito");
            }
        }
    }
}
