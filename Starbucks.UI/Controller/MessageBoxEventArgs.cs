﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Starbucks.UI.Model;

namespace Starbucks.UI.Controller
{
    public class MessageBoxEventArgs : EventArgs
    {
        public CustomMessageBoxResult Result { get; set; }
    }
}
