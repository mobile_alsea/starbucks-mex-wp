﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using Starbucks.DAL;
using Starbucks.DAL.Model;
using Starbucks.UI.Helper;
using Starbucks.UI.Model;
using System.IO.IsolatedStorage;
using System.IO;
using Newtonsoft.Json;
using System.Threading;
using JeffWilcox.Utilities.Silverlight;
using System.Security.Cryptography;
using System.Text;
using Microsoft.Phone.Controls.Maps;
using Starbucks.Entities;

namespace Starbucks.UI
{
    public partial class splash : PhoneApplicationPage
    {
        public splash()
        {
            InitializeComponent();

            //URL's de Respaldo
            if (IsolatedStorageSettings.ApplicationSettings.Contains("DATE_SWITCH"))
            {
                WebClient webClient = new WebClient();
                webClient.DownloadStringCompleted += new DownloadStringCompletedEventHandler(webClient_DownloadStringCompleted);
                webClient.DownloadStringAsync(new Uri(IsolatedStorageSettings.ApplicationSettings["URL_SWITCH"].ToString()));
            }
            else {
                IsolatedStorageSettings.ApplicationSettings["DATE_SWITCH"] = "2014-04-03 18:00";
                IsolatedStorageSettings.ApplicationSettings["URL_SWITCH"] = "http://pagatodomobile.com/config/Starbucks/settings.json";
                IsolatedStorageSettings.ApplicationSettings["URL_WS_SMART"] = "http://200.53.139.17:4090/Servicio.svc/JSON/";
                IsolatedStorageSettings.ApplicationSettings["URL_WS_REWARDS"] = "http://200.53.139.17:8080/Starbucks-WSRewards/wsrewards/";
                IsolatedStorageSettings.ApplicationSettings["DIGEST_CERT_WS_SMART_WP"] = "";
                IsolatedStorageSettings.ApplicationSettings["DIGEST_CERT_WS_REWARDS_WP"] = "";
                IsolatedStorageSettings.ApplicationSettings["URL_NOTIFICATIONS"] = "http://200.53.179.10:8042/Notifications.svc";
                IsolatedStorageSettings.ApplicationSettings.Save();
                splash_Loaded();
            }
        }

        void splash_Loaded()//(object sender, RoutedEventArgs e)
        {
            DataContext = App.SplashViewModel;
            App.SplashViewModel.LoadCompleted +=SplashViewModel_LoadCompleted;
            App.SplashViewModel.DownloadError += SplashViewModel_DownloadError;
            App.SplashViewModel.ActualizaEstrellas();


            // Create the database if it does not exist.
            using (ModelDataBaseDataContext db = new ModelDataBaseDataContext(DataBaseViewModel.DBConnectionString))
            {
                if (db.DatabaseExists() == false)
                {
                    // Create the local database.
                    db.CreateDatabase();

                    //Inicializa la base de datos
                        //Creo tabla de estados
                    var resource = Application.GetResourceStream(new Uri(@"/Starbucks.UI;component/Estados.txt", UriKind.Relative));
                    System.IO.StreamReader streamReader = new System.IO.StreamReader(resource.Stream);
                    int counter = 0;
                    string line;
                    while ((line = streamReader.ReadLine()) != null)
                    {
                        string[] arrData = line.Split(',');
                        db.Estados.InsertOnSubmit(new Estados { ID_Estado = Convert.ToInt32(arrData[0]), Descripcion = arrData[1] });
                        counter++;
                    }

                    streamReader.Close();

                        //Creo tabla de municipios
                    var resource2 = Application.GetResourceStream(new Uri(@"/Starbucks.UI;component/Municipios.txt", UriKind.Relative));
                    System.IO.StreamReader streamReader2 = new System.IO.StreamReader(resource2.Stream);
                    int counter2 = 0;
                    string line2;
                    while ((line2 = streamReader2.ReadLine()) != null)
                    {
                        string[] arrData = line2.Split(',');
                        db.Municipios.InsertOnSubmit(new Municipios { ID_Municipio = arrData[0], ID_Estado = Convert.ToInt32(arrData[1]), Descrpcion = arrData[2] });
                        counter2++;
                    }

                    streamReader2.Close();

                    // Save categories to the database.
                    db.SubmitChanges();
                }
                
            }
           

        }

        void webClient_DownloadStringCompleted(object sender, DownloadStringCompletedEventArgs e)
        {
            //Loaded += splash_Loaded;
            var container = JsonConvert.DeserializeObject<URLRespaldo>(e.Result);
            string url = "PagaTodoMobile" + container.DATE_SWITCH + container.URL_SWITCH + container.URL_WS_SMART + container.URL_WS_REWARDS + container.DIGEST_CERT_WS_SMART_IOS + container.DIGEST_CERT_WS_REWARDS_IOS + container.DIGEST_CERT_WS_SMART_ANDROID + container.DIGEST_CERT_WS_REWARDS_ANDROID + container.URL_NOTIFICATIONS + container.URL_PASSBOOK + container.DIGEST_CERT_WS_SMART_WP + container.DIGEST_CERT_WS_REWARDS_WP;
            string DIGEST = EncodeString(SHA256Encrypt(url).ToUpper());

            if (container.DATE_SWITCH != IsolatedStorageSettings.ApplicationSettings["DATE_SWITCH"].ToString()) {
                if ((DIGEST.Substring(8, 16)) == container.DIGEST)
                {
                    IsolatedStorageSettings.ApplicationSettings["DATE_SWITCH"] = container.DATE_SWITCH;
                    IsolatedStorageSettings.ApplicationSettings["URL_SWITCH"] = container.URL_SWITCH;
                    IsolatedStorageSettings.ApplicationSettings["URL_WS_SMART"] = container.URL_WS_SMART;
                    IsolatedStorageSettings.ApplicationSettings["URL_WS_REWARDS"] = container.URL_WS_REWARDS;
                    IsolatedStorageSettings.ApplicationSettings["DIGEST_CERT_WS_SMART_WP"] = container.DIGEST_CERT_WS_SMART_WP;
                    IsolatedStorageSettings.ApplicationSettings["DIGEST_CERT_WS_REWARDS_WP"] = container.DIGEST_CERT_WS_REWARDS_WP;
                    IsolatedStorageSettings.ApplicationSettings["URL_NOTIFICATIONS"] = container.URL_NOTIFICATIONS;
                    IsolatedStorageSettings.ApplicationSettings.Save();
                }
            }
            splash_Loaded();
        }

        public string Encrypt(string input, HashAlgorithm hashAlgorithm)
        {
            byte[] inputBytes = Encoding.UTF8.GetBytes(input);
            byte[] hashedBytes = hashAlgorithm.ComputeHash(inputBytes);

            StringBuilder output = new StringBuilder();

            for (int i = 0; i < hashedBytes.Length; i++)
                output.Append(hashedBytes[i].ToString("x2").ToLower());

            return output.ToString();
        }

        public string SHA256Encrypt(string input)
        {
            return Encrypt(input, new SHA256Managed());
        }

        public static string EncodeString(string password)
        {
            string md5encoded;

            using (MD5 md5 = new MD5CryptoServiceProvider())
                md5encoded = BitConverter.ToString(md5.ComputeHash(Encoding.UTF8.GetBytes(password)));

            string md5String = md5encoded.Replace("-", String.Empty);

            return md5String;
        }

        void SplashViewModel_DownloadError(object sender, EventArgs e)
        {
            switch ((ErrorData)sender)
            {
                case ErrorData.TimeOut:
                    Log.EscribeLog("TimeOut");
                    Reintentar();
                    break;
                case ErrorData.ErrorInRequest:
                    Log.EscribeLog("ErrorInRequest");
                    Reintentar();
                    break;
                case ErrorData.ErrorInParser:
                    Log.EscribeLog("ErrorInParser");
                    Reintentar();
                    break;
                default:
                    break;
            }
        }

        private void Reintentar()
        {
            var res = MessageBox.Show("Reintentar", "Error en la Red", MessageBoxButton.OKCancel);
            if (res != MessageBoxResult.OK)
                App.Quit();
            else
                //Desactiva el webservice
                App.SplashViewModel.Reintentar();
                //NavigationService.Navigate(new Uri(StarbucksResource.View_Home, UriKind.Relative));
        }

        void SplashViewModel_LoadCompleted(object sender, EventArgs e)
        {
            NavigationService.Navigate(new Uri(StarbucksResource.View_Home, UriKind.Relative));
        }
    }
}