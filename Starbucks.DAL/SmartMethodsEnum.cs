﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Starbucks.DAL
{
    public enum SmartMethodsEnum
    {
        ConsultaProgramacionRecarga,
        ConsultaMediosPago,
        BajaMedioPago,
        Recarga,
        ActualizaProgramaRecarga,
        ConsultaEstatusRecarga
    }
}
