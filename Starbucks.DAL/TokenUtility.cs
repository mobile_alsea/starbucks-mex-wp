﻿using System;
using System.Collections.Generic;
using System.IO;
using System.IO.IsolatedStorage;
using System.Linq;
using System.Text;
using System.Windows;
using Microsoft.Phone.Controls;

namespace Starbucks.DAL
{
    public class TokenUtility
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sFile"></param>
        public static void CloseSession(string sFile)
        {
            string sMSG = "";
            if (sFile == "Sesion.txt") sMSG = "sesioncerrada";
            using (IsolatedStorageFile myFile = IsolatedStorageFile.GetUserStoreForApplication())
            {
                StreamWriter sw = new StreamWriter(new IsolatedStorageFileStream(sFile, FileMode.Create, myFile));
                sw.WriteLine(sMSG); //Wrting to the file
                sw.Close();
            }
            if (Deployment.Current.Dispatcher.CheckAccess())
            {
                (Application.Current.RootVisual as PhoneApplicationFrame).Navigate(new Uri("/Home.xaml", UriKind.Relative));
                MessageBox.Show("Has ingresado a tu cuenta en otro dispositivo y solo es posible acceder en uno a la vez, por favor vuelve a iniciar sesión", "Aviso", MessageBoxButton.OK);
            }
            else
            {
                Deployment.Current.Dispatcher.BeginInvoke(() =>
                {
                    (Application.Current.RootVisual as PhoneApplicationFrame).Navigate(new Uri("/Home.xaml", UriKind.Relative));
                    MessageBox.Show("Has ingresado a tu cuenta en otro dispositivo y solo es posible acceder en uno a la vez, por favor vuelve a iniciar sesión", "Aviso", MessageBoxButton.OK);
                });
            }

        }
    }
}
