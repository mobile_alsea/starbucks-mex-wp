﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;

namespace Starbucks.DAL
{
    public static class InvariantDataService
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="target"></param>
        /// <param name="parameterName"></param>
        public static void IsNotCancelled(bool target)
        {
            if (target == true)
            {
                throw new ArgumentException(string.Format(CultureInfo.CurrentUICulture, "Ha Sido Cancelado."));
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="target"></param>
        /// <param name="parameterName"></param>
        public static void IsNotError(Exception target)
        {
            if (target != null)
            {
                throw new ArgumentException(string.Format(CultureInfo.CurrentUICulture, "Genero la Siguiente Excepción: {0}", target.Message));
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="target"></param>
        /// <param name="message"></param>
        /// <param name="parameterName"></param>
        public static void IsNotSuccess(bool target, string message)
        {
            if (target == false)
            {
                throw new ArgumentException(string.Format(CultureInfo.CurrentUICulture, "{0}", message));
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="target"></param>
        /// <param name="message"></param>
        /// <param name="parameterName"></param>
        public static void IsNotSuccess(int target, string message)
        {
            if (target != 0)
            {
                throw new ArgumentException(string.Format(CultureInfo.CurrentUICulture, "{0}", message));
            }
        }
    }

}
