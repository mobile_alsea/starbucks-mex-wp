﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Starbucks.Entities;
using wsTarjetas = Starbucks.DAL.DataServiceReference.Tarjeta;

namespace Starbucks.DAL
{
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="process"></param>
    public delegate void FinishRegistraUsuarioEventHandler(object sender, FinishRegistraUsuarioArgs e);
    /// <summary>
    /// 
    /// </summary>
    public class FinishRegistraUsuarioArgs
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="success"></param>
        /// <param name="data"></param>
        /// <param name="message"></param>
        public FinishRegistraUsuarioArgs(bool success, RegistroUsuarioResult data, string message = "")
        {
            Message = message;
            Success = success;
            Data = data;
        }
        
        public bool Success { get; private set; }
        public string Message { get; private set; }
        public RegistroUsuarioResult Data { get; private set; }
        
    }



    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="process"></param>
    public delegate void FinishConsultaTransaccionesTarjetaEventHandler(object sender, FinishConsultaTransaccionesTarjetaArgs e);
    /// <summary>
    /// 
    /// </summary>
    public class FinishConsultaTransaccionesTarjetaArgs
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="success"></param>
        /// <param name="data"></param>
        /// <param name="message"></param>
        public FinishConsultaTransaccionesTarjetaArgs(bool success, ConsultaTransaccionResult data, string message = "")
        {
            Message = message;
            Success = success;
            Data = data;
        }

        public bool Success { get; private set; }
        public string Message { get; private set; }
        public ConsultaTransaccionResult Data { get; private set; }

    }


    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="process"></param>
    public delegate void FinishSmartServiceEventHandler<T>(object sender, FinishSmartServiceArgs<T> e);
    /// <summary>
    /// 
    /// </summary>
    public class FinishSmartServiceArgs<T> : EventArgs
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="success"></param>
        /// <param name="data"></param>
        /// <param name="message"></param>
        public FinishSmartServiceArgs(bool success, T data = default(T), string message = "")
        {
            Message = message;
            Success = success;
            Data = data;
        }

        public bool Success { get; private set; }
        public string Message { get; private set; }
        public T Data { get; private set; }
        public bool IsTimeOut { get; set; }

    }
}
