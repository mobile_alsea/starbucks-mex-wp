﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Starbucks.DAL
{
    public enum RewardsMethodsEnum
    {
        registroUsuario,
        consultaTransaccionesPorTarjeta,
        login,
        consultaEstatusRewards,
        consultaSaldoTarjeta,
        validarTarjeta,
        consultaTarjetas
    }
}
