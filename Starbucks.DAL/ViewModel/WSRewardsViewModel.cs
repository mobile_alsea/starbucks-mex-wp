﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using Microsoft.Phone.Info;
using Newtonsoft.Json;
using Starbucks.Entities;
using Starbucks.Utilities;
using System.IO.IsolatedStorage;
using Microsoft.Phone.Controls;
using System.Windows;

namespace Starbucks.DAL
{
    public class WSRewardsViewModel
    {

        public event FinishSmartServiceEventHandler<LoginResponse> FinishLogin;
        public event FinishSmartServiceEventHandler<ConsultaEstatusRewardsResponse> FinishConsultaEstatusRewards;
        public event FinishSmartServiceEventHandler<ValidaTarjetaResponse> FinishValidaTarjeta;
        public event FinishSmartServiceEventHandler<ConsultaSaldoTarjetaResponse> FinishConsultaSaldoTarjeta;
        public event FinishSmartServiceEventHandler<ConsultaTransaccionesPorTarjetaResponse> FinishConsultaTransaccionesPorTarjeta;
        public event FinishSmartServiceEventHandler<RegistroUsuarioResponse> FinishRegistroUsuario;
        public event FinishSmartServiceEventHandler<ConsultaTarjetasResponse> FinishConsultaTarjetas;
        /// <summary>
        /// 
        /// </summary>
        public event FinishRegistraUsuarioEventHandler FinishRegistraUsuario;
        /// <summary>
        /// 
        /// </summary>
        public event FinishConsultaTransaccionesTarjetaEventHandler FinishConsultaTransaccionesTarjeta;
        /// <summary>
        /// 
        /// </summary>
        public void SyncRegistraUsuarioService()
        {
            System.Uri targetUri = new System.Uri(ConfigurationSettings.SERVICE_REWARDS + RewardsMethodsEnum.registroUsuario.ToString());
            HttpWebRequest request = (HttpWebRequest)HttpWebRequest.Create(targetUri);
            request.Method = "POST";
            request.ContentType = "application/x-www-form-urlencoded";
            request.BeginGetRequestStream(new AsyncCallback(delegate(IAsyncResult callbackResultRequest)
            {
                HttpWebRequest myRequest = (HttpWebRequest)callbackResultRequest.AsyncState;
                Stream postStream = myRequest.EndGetRequestStream(callbackResultRequest);

                string postData = string.Format("idPreregistro={0}&udId={1}&nombre={2}&apellidos={3}&email={4}&constrasenia={5}&fechaNacimiento={6}&colonia={7}&calleyNumero={8}&ciudad={9}&estado={10}&pais={11}&codigoPostal={12}&suscripcion={13}");
                byte[] byteArray = Encoding.UTF8.GetBytes(postData);
                postStream.Write(byteArray, 0, byteArray.Length);
                postStream.Close();

                myRequest.BeginGetResponse(new AsyncCallback(delegate(IAsyncResult callbackResultResponse)
                {
                    try
                    {
                        HttpWebRequest myResponse = (HttpWebRequest)callbackResultResponse.AsyncState;
                        HttpWebResponse response = (HttpWebResponse)request.EndGetResponse(callbackResultResponse);

                        RegistroUsuarioResult myObjects = new RegistroUsuarioResult();
                        using (StreamReader httpWebStreamReader = new StreamReader(response.GetResponseStream()))
                        {
                            string results = httpWebStreamReader.ReadToEnd();
                            myObjects = JsonConvert.DeserializeObject<RegistroUsuarioResult>(results);
                        }
                        NotifyFinish(FinishRegistraUsuario, new FinishRegistraUsuarioArgs(true, myObjects));
                    }
                    catch (Exception ex)
                    {
                        NotifyFinish(FinishRegistraUsuario, new FinishRegistraUsuarioArgs(false, null, ex.Message));
                    }
                }), myRequest);
            }), request);
        }
        /// <summary>
        /// 
        /// </summary>        
        public void SyncConsultaTransaccionesTarjetaService()
        {
            System.Uri targetUri = new System.Uri(ConfigurationSettings.SERVICE_REWARDS + RewardsMethodsEnum.consultaTransaccionesPorTarjeta.ToString());
            HttpWebRequest request = (HttpWebRequest)HttpWebRequest.Create(targetUri);
            request.Method = "POST";
            request.Headers["idUsuario"] = ConfigurationSettings.IdUsuario;
            request.Headers["token"] = ConfigurationSettings.Token;
            request.ContentType = "application/x-www-form-urlencoded";
            request.BeginGetRequestStream(new AsyncCallback(delegate(IAsyncResult callbackResultRequest)
            {
                HttpWebRequest myRequest = (HttpWebRequest)callbackResultRequest.AsyncState;
                Stream postStream = myRequest.EndGetRequestStream(callbackResultRequest);

                string postData = string.Format("idTarjeta={0}", ConfigurationSettings.IdTarjeta);
                byte[] byteArray = Encoding.UTF8.GetBytes(postData);
                postStream.Write(byteArray, 0, byteArray.Length);
                postStream.Close();

                myRequest.BeginGetResponse(new AsyncCallback(delegate(IAsyncResult callbackResultResponse)
                {
                    try
                    {
                        HttpWebRequest myResponse = (HttpWebRequest)callbackResultResponse.AsyncState;
                        HttpWebResponse response = (HttpWebResponse)request.EndGetResponse(callbackResultResponse);

                        ConsultaTransaccionResult myObjects = new ConsultaTransaccionResult();
                        ValidateToken tokenValidado;
                        using (StreamReader httpWebStreamReader = new StreamReader(response.GetResponseStream()))
                        {
                            string results = httpWebStreamReader.ReadToEnd();
                            myObjects = JsonConvert.DeserializeObject<ConsultaTransaccionResult>(results);
                            tokenValidado = JsonConvert.DeserializeObject<ValidateToken>(results) as ValidateToken;
                        }
                        NotifyFinish(FinishConsultaTransaccionesTarjeta, new FinishConsultaTransaccionesTarjetaArgs(true, myObjects));
                        if (tokenValidado.codigo == 3)
                        {
                            //Cerrar sesion
                            TokenUtility.CloseSession("Sesion.txt"); 

                        }
                    }
                    catch (Exception ex)
                    {
                        NotifyFinish(FinishConsultaTransaccionesTarjeta, new FinishConsultaTransaccionesTarjetaArgs(false, null, ex.Message));
                    }
                }), myRequest);
            }), request);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="handler"></param>
        /// <param name="e"></param>
        private void NotifyFinish(FinishRegistraUsuarioEventHandler handler, FinishRegistraUsuarioArgs e)
        {
            if (handler != null)
            {
                handler(this, e);
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="handler"></param>
        /// <param name="e"></param>
        private void NotifyFinish(FinishConsultaTransaccionesTarjetaEventHandler handler, FinishConsultaTransaccionesTarjetaArgs e)
        {
            if (handler != null)
            {
                handler(this, e);
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="title"></param>
        /// <param name="message"></param>
        private void MessageBoxShow(string title, string message)
        {
            MessageBox.Show(message, title, MessageBoxButton.OK);
        }

        public void SyncConsultaEstatusRewardsWSService()
        {
            HttpRequestRewards<ConsultaEstatusRewardsResponse> httpRequest = new HttpRequestRewards<ConsultaEstatusRewardsResponse>();
            httpRequest.Data = "";
            httpRequest.Handler = FinishConsultaEstatusRewards;
            httpRequest.Method = RewardsMethodsEnum.consultaEstatusRewards;
            httpRequest.ExecuteWS();
        }

        public void SyncLoginWSService(string email, string contrasenia)
        {
            string deviceID = ConfigurationSettings.DeviceUniqueId;
            string postData = "contrasenia=" + contrasenia + "&udId=" + deviceID + "&email=" + email + "";

            HttpRequestRewards<LoginResponse> httpRequest = new HttpRequestRewards<LoginResponse>();
            httpRequest.Data = postData;
            httpRequest.Handler = FinishLogin;
            httpRequest.Method = RewardsMethodsEnum.login;
            httpRequest.ExecuteWS();
        }

        public void SyncValidarTarjetaWSService(string NoTarjetaSbx, string CodigoVerificador)
        {
            string deviceID = ConfigurationSettings.DeviceUniqueId;
            string postData = "noTarjetaSbx=" + NoTarjetaSbx + "&codigoVerificador=" + CodigoVerificador + "&udId=" + deviceID + "";

            HttpRequestRewards<ValidaTarjetaResponse> httpRequest = new HttpRequestRewards<ValidaTarjetaResponse>();
            httpRequest.Data = postData;
            httpRequest.Handler = FinishValidaTarjeta;
            httpRequest.Method = RewardsMethodsEnum.validarTarjeta;
            httpRequest.ExecuteWS();
        }

        public void SyncConsultaSaldoTarjetaWSService(long IdTarjeta)
        {
            string postData = "idTarjeta=" + IdTarjeta + "";

            HttpRequestRewards<ConsultaSaldoTarjetaResponse> httpRequest = new HttpRequestRewards<ConsultaSaldoTarjetaResponse>();
            httpRequest.Data = postData;
            httpRequest.Handler = FinishConsultaSaldoTarjeta;
            httpRequest.Method = RewardsMethodsEnum.consultaSaldoTarjeta;
            httpRequest.ExecuteWS();
        }

        public void SyncConsultaTransaccionesPorTarjeta(long IdTarjeta)
        {
            string postData = "idTarjeta=" + IdTarjeta + "";

            HttpRequestRewards<ConsultaTransaccionesPorTarjetaResponse> httpRequest = new HttpRequestRewards<ConsultaTransaccionesPorTarjetaResponse>();
            httpRequest.Data = postData;
            httpRequest.Handler = FinishConsultaTransaccionesPorTarjeta;
            httpRequest.Method = RewardsMethodsEnum.consultaTransaccionesPorTarjeta;
            httpRequest.ExecuteWS();
        }

        public void SyncRegistroUsuarioWSService(string data)
        {
            HttpRequestRewards<RegistroUsuarioResponse> httpRequest = new HttpRequestRewards<RegistroUsuarioResponse>();
            httpRequest.Data = data;
            httpRequest.Handler = FinishRegistroUsuario;
            httpRequest.Method = RewardsMethodsEnum.registroUsuario;
            httpRequest.ExecuteWS();
        }

        public void SyncConsultaTarjetasWSService()
        {
            HttpRequestRewards<ConsultaTarjetasResponse> httpRequest = new HttpRequestRewards<ConsultaTarjetasResponse>();
            httpRequest.Data = "";
            httpRequest.Handler = FinishConsultaTarjetas;
            httpRequest.Method = RewardsMethodsEnum.consultaTarjetas;
            httpRequest.ExecuteWS();
        }
    }
}
