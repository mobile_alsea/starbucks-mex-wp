﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.IO.IsolatedStorage;
using System.Linq;
using System.Net;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.Text;
using System.Threading;
using System.Windows;
using Microsoft.Phone.Controls;
using Newtonsoft.Json;
using Starbucks.DAL.DataServiceReference;
using Starbucks.Entities;
using Starbucks.Resouces;
using Starbucks.Utilities;


namespace Starbucks.DAL
{
    /// <summary>
    /// 
    /// </summary>
    public class WSSmartSBXViewModel
    {
        /// <summary>
        /// 
        /// </summary>
        public event FinishSmartServiceEventHandler<ConsultaProgramaResponse> FinishConsultaProgramaRecarga;
        /// <summary>
        /// 
        /// </summary>
        public event FinishSmartServiceEventHandler<ConsultaMedioPagoResponse> FinishConsultaMedioPago;
        /// <summary>
        /// 
        /// </summary>
        public event FinishSmartServiceEventHandler<BajaMedioPagoResponse> FinishBajaMedioPago;
        /// <summary>
        /// 
        /// </summary>
        public event FinishSmartServiceEventHandler<RecargaResponse> FinishRecarga;
        /// <summary>
        /// 
        /// </summary>
        public event FinishSmartServiceEventHandler<ActualizaProgramaRecargaResponse> FinishActualizaProgramaRecarga;

        public event FinishSmartServiceEventHandler<EstatusRecargaResponse> FinishEstatusRecarga;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="cadena"></param>
        /// <param name="tipoDispositivo"></param>
        /// <param name="tipoBanco"></param>
        /// <param name="tarjetaCadena"></param>
        /// <param name="moneda"></param>
        /// <param name="montoRecarga"></param>
        /// <param name="tarjetaBancaria"></param>
        /// <param name="aliasTarjetaBancaria"></param>
        /// <param name="cvv"></param>
        /// <param name="fechaVencimiento"></param>
        /// <param name="detalleCliente"></param>
        /// <param name="PCHuellaDigital"></param>
        /// <param name="guardarTarjeta"></param>
        /// <param name="idReferencia"></param>
        public void SyncRecargaService(int cadena, int tipoDispositivo, string tipoBanco, string tarjetaCadena, string moneda, decimal montoRecarga, string tarjetaBancaria, string aliasTarjetaBancaria, string cvv, string fechaVencimiento, CustomerDetail detalleCliente, string PCHuellaDigital, bool guardarTarjeta, long idReferencia)
        {
            AutoResetEvent _watcherLock = new AutoResetEvent(false);
            DataServiceReference.ServicioClient client = new DataServiceReference.ServicioClient();
            client.RecargaCompleted += (s, e) =>
            {
                try
                {
                    InvariantDataService.IsNotCancelled(e.Cancelled);
                    InvariantDataService.IsNotError(e.Error);
                    InvariantDataService.IsNotSuccess(e.Result.Codigo, e.Result.Descripcion);


                }
                catch (Exception ex)
                {
                    MessageBoxShow("sync", ex.Message);
                }
                _watcherLock.Set();
            };
            using (new OperationContextScope(client.InnerChannel))
            {
                var IDUsuario = MessageHeader.CreateHeader("IDUsuario", "", ConfigurationSettings.IdUsuario);
                var Token = MessageHeader.CreateHeader("Token", "", ConfigurationSettings.Token);

                OperationContext.Current.OutgoingMessageHeaders.Add(IDUsuario);
                OperationContext.Current.OutgoingMessageHeaders.Add(Token);
                client.RecargaAsync(cadena, tipoDispositivo, tipoBanco, tarjetaCadena, moneda, montoRecarga, tarjetaBancaria, aliasTarjetaBancaria, cvv, fechaVencimiento, detalleCliente, PCHuellaDigital, guardarTarjeta, idReferencia);
            }
            // wait until watcher is initialized
            _watcherLock.WaitOne();
        }
        /// <summary>
        /// 
        /// </summary>

        public void SyncEstatusRecargaWSService(int cadena, int tipoDispositivo, string tarjetaCadena)
        {
            EstatusRecargaRequest data = new EstatusRecargaRequest();
            data.tipoDispositivo = tipoDispositivo;
            data.cadena = cadena;
            data.tarjetaCadena = tarjetaCadena;
            data.idReferencia = (long)((DateTime.UtcNow - new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc)).TotalMilliseconds / 1000);

            HttpWebRequestCustom<EstatusRecargaResponse,EstatusRecargaRequest> httpRequest = new HttpWebRequestCustom<EstatusRecargaResponse, EstatusRecargaRequest>();
            httpRequest.Data = data;
            httpRequest.Handler = FinishEstatusRecarga;
            httpRequest.Method = SmartMethodsEnum.ConsultaEstatusRecarga;
            httpRequest.ExecuteWS();
        }
         

        /// 
        /// <param name="cadena"></param>
        /// <param name="tipoDispositivo"></param>
        /// <param name="tipoBanco"></param>
        /// <param name="tarjetaCadena"></param>
        /// <param name="moneda"></param>
        /// <param name="montoRecarga"></param>
        /// <param name="tarjetaBancaria"></param>
        /// <param name="aliasTarjetaBancaria"></param>
        /// <param name="cvv"></param>
        /// <param name="fechaVencimiento"></param>
        /// <param name="detalleCliente"></param>
        /// <param name="PCHuellaDigital"></param>
        /// <param name="guardarTarjeta"></param>
        /// <param name="idReferencia"></param>
        public void SyncRecargaWSService(int cadena, int tipoDispositivo, string tipoBanco, string tarjetaCadena, string moneda, decimal montoRecarga, string tarjetaBancaria, string aliasTarjetaBancaria, string cvv, string fechaVencimiento, DatosCliente detalleCliente, string PCHuellaDigital, bool guardarTarjeta)
        {

            tarjetaBancaria = string.IsNullOrEmpty(tarjetaBancaria) ? "" : tarjetaBancaria;
            fechaVencimiento = string.IsNullOrEmpty(fechaVencimiento) ? "" : fechaVencimiento;
            DateTime MiembroDesde = string.IsNullOrEmpty(detalleCliente.miembroDesde) ? DateTime.Now : DateTime.ParseExact(detalleCliente.miembroDesde, "dd/MM/yyyy", new CultureInfo("es-MX"));


            DetalleCliente cliente = new DetalleCliente();
            
            cliente.primerNombre = detalleCliente.primerNombre;
            cliente.apellidos = detalleCliente.apellidos;
            cliente.fechaNacimiento = detalleCliente.fechaNacimiento;
            cliente.email = detalleCliente.email;
            cliente.edadCuentaCreada = (DateTime.Now - MiembroDesde).Days.ToString();
            cliente.miembroDesde = detalleCliente.miembroDesde;
            cliente.calleNumero = detalleCliente.calleyNumero;
            cliente.ciudad = detalleCliente.ciudad;
            cliente.estado = detalleCliente.estado;
            cliente.pais = detalleCliente.pais;
            cliente.codigoPostal = detalleCliente.codigoPostal;
            cliente.esClientePrevio = detalleCliente.esClientePrevio;


            RecargaRequest data = new RecargaRequest();
            data.cadena = cadena;
            data.tipoDispositivo = tipoDispositivo;
            data.tipoBanco = tipoBanco;
            data.tarjetaCadena = tarjetaCadena;
            data.moneda = moneda;
            data.montoRecarga = montoRecarga;
            data.tarjetaBancaria = tarjetaBancaria;
            data.aliasTarjetaBancaria = aliasTarjetaBancaria;
            data.cvv = cvv;
            data.fechaVencimiento = fechaVencimiento;
            data.detalleCliente = cliente;
            data.PCHuellaDigital = PCHuellaDigital;
            data.guardarTarjeta = guardarTarjeta;
            data.idReferencia = (long)((DateTime.UtcNow - new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc)).TotalMilliseconds / 1000);



            HttpWebRequestCustom<RecargaResponse, RecargaRequest> httpRequest = new HttpWebRequestCustom<RecargaResponse, RecargaRequest>();
            httpRequest.Data = data;
            httpRequest.Handler = FinishRecarga;
            httpRequest.Method = SmartMethodsEnum.Recarga;
            httpRequest.ExecuteWS();
            //ExecuteWS<RecargaResponse, RecargaRequest>(SmartMethodsEnum.Recarga, data, FinishRecarga);

        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="cadena"></param>
        /// <param name="tipoDispositivo"></param>
        public void SyncBajaMedioPagoService(int cadena, int tipoDispositivo)
        {
            AutoResetEvent _watcherLock = new AutoResetEvent(false);
            DataServiceReference.ServicioClient client = new DataServiceReference.ServicioClient();
            client.BajaMedioPagoCompleted += (s, e) =>
            {
                try
                {
                    InvariantDataService.IsNotCancelled(e.Cancelled);
                    InvariantDataService.IsNotError(e.Error);
                    InvariantDataService.IsNotSuccess(e.Result.Codigo, e.Result.Descripcion);


                }
                catch (Exception ex)
                {
                    MessageBoxShow("sync", ex.Message);
                }
                _watcherLock.Set();
            };
            using (new OperationContextScope(client.InnerChannel))
            {
                var IDUsuario = MessageHeader.CreateHeader("IDUsuario", "", ConfigurationSettings.IdUsuario);
                var Token = MessageHeader.CreateHeader("Token", "", ConfigurationSettings.Token);

                OperationContext.Current.OutgoingMessageHeaders.Add(IDUsuario);
                OperationContext.Current.OutgoingMessageHeaders.Add(Token);
                client.BajaMedioPagoAsync(cadena, tipoDispositivo);
            }
            // wait until watcher is initialized
            _watcherLock.WaitOne();
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="cadena"></param>
        /// <param name="tipoDispositivo"></param>
        public void SyncBajaMedioPagoWSService(int cadena, int tipoDispositivo)
        {
            BajaMedioPagoRequest data = new BajaMedioPagoRequest { cadena = cadena, tipoDispositivo = tipoDispositivo };
            HttpWebRequestCustom<BajaMedioPagoResponse, BajaMedioPagoRequest> httpRequest = new HttpWebRequestCustom<BajaMedioPagoResponse, BajaMedioPagoRequest>();
            httpRequest.Data = data;
            httpRequest.Handler = FinishBajaMedioPago;
            httpRequest.Method = SmartMethodsEnum.BajaMedioPago;
            httpRequest.ExecuteWS();
            //ExecuteWS<BajaMedioPagoResponse, BajaMedioPagoRequest>(SmartMethodsEnum.BajaMedioPago, data, FinishBajaMedioPago);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="cadena"></param>
        /// <param name="tipoDispositivo"></param>
        /// <param name="tipoBanco"></param>
        /// <param name="tarjetaCadena"></param>
        /// <param name="tipoRecarga"></param>
        /// <param name="moneda"></param>
        /// <param name="montoRecarga"></param>
        /// <param name="limiteMinimoSaldo"></param>
        /// <param name="diaRecarga"></param>
        /// <param name="tarjetaBancaria"></param>
        /// <param name="aliasTarjetaBancaria"></param>
        /// <param name="cvv"></param>
        /// <param name="fechaVencimiento"></param>
        /// <param name="detalleCliente"></param>
        /// <param name="PCHuellaDigital"></param>
        public void SyncActualizaProgramaRecargaService(int cadena, int tipoDispositivo, string tipoBanco, string tarjetaCadena, int tipoRecarga, string moneda, decimal montoRecarga, decimal limiteMinimoSaldo, int diaRecarga, string tarjetaBancaria, string aliasTarjetaBancaria, string cvv, string fechaVencimiento, Starbucks.DAL.DataServiceReference.CustomerDetail detalleCliente, string PCHuellaDigital)
        {
            AutoResetEvent _watcherLock = new AutoResetEvent(false);
            DataServiceReference.ServicioClient client = new DataServiceReference.ServicioClient();
            client.ActualizaProgramaRecargaCompleted += (s, e) =>
            {
                try
                {
                    InvariantDataService.IsNotCancelled(e.Cancelled);
                    InvariantDataService.IsNotError(e.Error);
                    InvariantDataService.IsNotSuccess(e.Result.Codigo, e.Result.Descripcion);


                }
                catch (Exception ex)
                {
                    MessageBoxShow("sync", ex.Message);
                }
                _watcherLock.Set();
            };
            using (new OperationContextScope(client.InnerChannel))
            {
                var IDUsuario = MessageHeader.CreateHeader("IDUsuario", "", ConfigurationSettings.IdUsuario);
                var Token = MessageHeader.CreateHeader("Token", "", ConfigurationSettings.Token);

                OperationContext.Current.OutgoingMessageHeaders.Add(IDUsuario);
                OperationContext.Current.OutgoingMessageHeaders.Add(Token);
                client.ActualizaProgramaRecargaAsync(cadena, tipoDispositivo, tipoBanco, tarjetaCadena, tipoRecarga, moneda, montoRecarga, limiteMinimoSaldo, diaRecarga, tarjetaBancaria, aliasTarjetaBancaria, cvv, fechaVencimiento, detalleCliente, PCHuellaDigital);
            }
            // wait until watcher is initialized
            _watcherLock.WaitOne();
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="cadena"></param>
        /// <param name="tipoDispositivo"></param>
        /// <param name="tipoBanco"></param>
        /// <param name="tarjetaCadena"></param>
        /// <param name="tipoRecarga"></param>
        /// <param name="moneda"></param>
        /// <param name="montoRecarga"></param>
        /// <param name="limiteMinimoSaldo"></param>
        /// <param name="diaRecarga"></param>
        /// <param name="tarjetaBancaria"></param>
        /// <param name="aliasTarjetaBancaria"></param>
        /// <param name="cvv"></param>
        /// <param name="fechaVencimiento"></param>
        /// <param name="detalleCliente"></param>
        /// <param name="PCHuellaDigital"></param>
        public void SyncActualizaProgramaRecargaWSService(int cadena, int tipoDispositivo, string tipoBanco, string tarjetaCadena, int tipoRecarga, string moneda, decimal montoRecarga, decimal limiteMinimoSaldo, int diaRecarga, string tarjetaBancaria, string aliasTarjetaBancaria, string cvv, string fechaVencimiento, DatosCliente detalleCliente, string PCHuellaDigital)
        {
            tarjetaBancaria = string.IsNullOrEmpty(tarjetaBancaria) ? "" : tarjetaBancaria;
            fechaVencimiento = string.IsNullOrEmpty(fechaVencimiento) ? "" : fechaVencimiento;
            DateTime MiembroDesde = string.IsNullOrEmpty(detalleCliente.miembroDesde) ? DateTime.Now : DateTime.ParseExact(detalleCliente.miembroDesde, "dd/MM/yyyy", new CultureInfo("es-MX"));


            DetalleCliente cliente = new DetalleCliente();
            cliente.primerNombre = detalleCliente.primerNombre;
            cliente.apellidos = detalleCliente.apellidos;
            cliente.fechaNacimiento = detalleCliente.fechaNacimiento;
            cliente.email = detalleCliente.email;
            cliente.edadCuentaCreada = (DateTime.Now - MiembroDesde).Days.ToString();
            cliente.miembroDesde = detalleCliente.miembroDesde;
            cliente.calleNumero = detalleCliente.calleyNumero;
            cliente.ciudad = detalleCliente.ciudad;
            cliente.estado = detalleCliente.estado;
            cliente.pais = detalleCliente.pais;
            cliente.codigoPostal = detalleCliente.codigoPostal;
            cliente.esClientePrevio = detalleCliente.esClientePrevio;

            ActualizaProgramaRecargaRequest data = new ActualizaProgramaRecargaRequest();
            data.cadena = cadena;
            data.tipoDispositivo = tipoDispositivo;
            data.tipoBanco = tipoBanco;
            data.tarjetaCadena = tarjetaCadena;
            data.tipoRecarga = tipoRecarga;
            data.moneda = moneda;
            data.montoRecarga = montoRecarga;
            data.limiteMinimoSaldo = limiteMinimoSaldo;
            data.diaRecarga = diaRecarga;
            data.tarjetaBancaria = tarjetaBancaria;
            data.aliasTarjetaBancaria = aliasTarjetaBancaria;
            data.cvv = cvv;
            data.fechaVencimiento = fechaVencimiento;
            data.detalleCliente = cliente;
            data.PCHuellaDigital = PCHuellaDigital;
            data.idReferencia = (long)((DateTime.UtcNow - new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc)).TotalMilliseconds / 1000);

            HttpWebRequestCustom<ActualizaProgramaRecargaResponse, ActualizaProgramaRecargaRequest> httpRequest = new HttpWebRequestCustom<ActualizaProgramaRecargaResponse, ActualizaProgramaRecargaRequest>();
            httpRequest.Data = data;
            httpRequest.Handler = FinishActualizaProgramaRecarga;
            httpRequest.Method = SmartMethodsEnum.ActualizaProgramaRecarga;
            httpRequest.ExecuteWS();
            //ExecuteWS<ActualizaProgramaRecargaResponse, ActualizaProgramaRecargaRequest>(SmartMethodsEnum.ActualizaProgramaRecarga, data, FinishActualizaProgramaRecarga);

        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="cadena"></param>
        /// <param name="tipoDispositivo"></param>
        public void SyncConsultaProgramaRecargaService(int cadena, int tipoDispositivo)
        {
            AutoResetEvent _watcherLock = new AutoResetEvent(false);
            DataServiceReference.ServicioClient client = new DataServiceReference.ServicioClient();
            client.ConsultaProgramacionRecargaXMLCompleted += (s, e) =>
            {
                try
                {
                    InvariantDataService.IsNotCancelled(e.Cancelled);
                    InvariantDataService.IsNotError(e.Error);
                    InvariantDataService.IsNotSuccess(e.Result.Codigo, e.Result.Descripcion);


                }
                catch (Exception ex)
                {
                    MessageBoxShow("sync", ex.Message);
                }
                _watcherLock.Set();
            };
            using (new OperationContextScope(client.InnerChannel))
            {
                var IDUsuario = MessageHeader.CreateHeader("IDUsuario", "", ConfigurationSettings.IdUsuario);
                var Token = MessageHeader.CreateHeader("Token", "", ConfigurationSettings.Token);

                OperationContext.Current.OutgoingMessageHeaders.Add(IDUsuario);
                OperationContext.Current.OutgoingMessageHeaders.Add(Token);
                client.ConsultaProgramacionRecargaXMLAsync(cadena, tipoDispositivo);
            }
            // wait until watcher is initialized
            _watcherLock.WaitOne();

        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="cadena"></param>
        /// <param name="tipoDispositivo"></param>
        public void SyncConsultaProgramaRecargaWSService(int cadena, int tipoDispositivo)
        {
            ConsultaProgramaRequest data = new ConsultaProgramaRequest { cadena = cadena, tipoDispositivo = tipoDispositivo };
            HttpWebRequestCustom<ConsultaProgramaResponse, ConsultaProgramaRequest> httpRequest = new HttpWebRequestCustom<ConsultaProgramaResponse, ConsultaProgramaRequest>();
            httpRequest.Data = data;
            httpRequest.Handler = FinishConsultaProgramaRecarga;
            httpRequest.Method = SmartMethodsEnum.ConsultaProgramacionRecarga;
            httpRequest.ExecuteWS();
            //ExecuteWS<ConsultaProgramaResponse, ConsultaProgramaRequest>(SmartMethodsEnum.ConsultaProgramacionRecarga, data, FinishConsultaProgramaRecarga);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="cadena"></param>
        /// <param name="tipoDispositivo"></param>
        /// <param name="idReferencia"></param>
        /// <param name="tarjetaCadena"></param>
        public void SyncConsultaEstatusRecargaService(int cadena, int tipoDispositivo, double idReferencia, string tarjetaCadena)
        {
            AutoResetEvent _watcherLock = new AutoResetEvent(false);
            DataServiceReference.ServicioClient client = new DataServiceReference.ServicioClient();
            client.ConsultaEstatusRecargaCompleted += (s, e) =>
            {
                try
                {
                    InvariantDataService.IsNotCancelled(e.Cancelled);
                    InvariantDataService.IsNotError(e.Error);
                    InvariantDataService.IsNotSuccess(e.Result.Codigo, e.Result.Descripcion);


                }
                catch (Exception ex)
                {
                    MessageBoxShow("sync", ex.Message);
                }
                _watcherLock.Set();
            };
            using (new OperationContextScope(client.InnerChannel))
            {
                var IDUsuario = MessageHeader.CreateHeader("IDUsuario", "", ConfigurationSettings.IdUsuario);
                var Token = MessageHeader.CreateHeader("Token", "", ConfigurationSettings.Token);

                OperationContext.Current.OutgoingMessageHeaders.Add(IDUsuario);
                OperationContext.Current.OutgoingMessageHeaders.Add(Token);
                client.ConsultaEstatusRecargaAsync(cadena, tipoDispositivo, idReferencia, tarjetaCadena);
            }
            // wait until watcher is initialized
            _watcherLock.WaitOne();
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="cadena"></param>
        /// <param name="tipoDispositivo"></param>
        public void SyncConsultaMediosPagoService(int cadena, int tipoDispositivo)
        {
            AutoResetEvent _watcherLock = new AutoResetEvent(false);
            DataServiceReference.ServicioClient client = new DataServiceReference.ServicioClient();
            client.ConsultaMediosPagoCompleted += (s, e) =>
            {
                try
                {
                    InvariantDataService.IsNotCancelled(e.Cancelled);
                    InvariantDataService.IsNotError(e.Error);
                    InvariantDataService.IsNotSuccess(e.Result.Codigo, e.Result.Descripcion);


                }
                catch (Exception ex)
                {
                    MessageBoxShow("sync", ex.Message);
                }
                _watcherLock.Set();
            };
            using (new OperationContextScope(client.InnerChannel))
            {
                var IDUsuario = MessageHeader.CreateHeader("IDUsuario", "", ConfigurationSettings.IdUsuario);
                var Token = MessageHeader.CreateHeader("Token", "", ConfigurationSettings.Token);

                OperationContext.Current.OutgoingMessageHeaders.Add(IDUsuario);
                OperationContext.Current.OutgoingMessageHeaders.Add(Token);
                client.ConsultaMediosPagoAsync(cadena, tipoDispositivo);
            }
            // wait until watcher is initialized
            _watcherLock.WaitOne();
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="cadena"></param>
        /// <param name="tipoDispositivo"></param>
        public void SyncConsultaMediosPagoWSService(int cadena, int tipoDispositivo)
        {
            ConsultaMedioPagoRequest data = new ConsultaMedioPagoRequest { cadena = cadena, tipoDispositivo = tipoDispositivo };
            HttpWebRequestCustom<ConsultaMedioPagoResponse, ConsultaMedioPagoRequest> httpRequest = new HttpWebRequestCustom<ConsultaMedioPagoResponse, ConsultaMedioPagoRequest>();
            httpRequest.Data = data;
            httpRequest.Handler = FinishConsultaMedioPago;
            httpRequest.Method = SmartMethodsEnum.ConsultaMediosPago;
            httpRequest.ExecuteWS();
            //ExecuteWS<ConsultaMedioPagoResponse, ConsultaMedioPagoRequest>(SmartMethodsEnum.ConsultaMediosPago, data, FinishConsultaMedioPago);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <typeparam name="W"></typeparam>
        /// <param name="method"></param>
        /// <param name="data"></param>
        /// <param name="handler"></param>
        /// <returns></returns>
        private void ExecuteWS<T, W>(SmartMethodsEnum method, W data, FinishSmartServiceEventHandler<T> handler)
        {
            System.Uri targetUri = new System.Uri(ConfigurationSettings.SERVICE_SMART + method.ToString());
            HttpWebRequest request = (HttpWebRequest)HttpWebRequest.Create(targetUri);
            request.Method = "POST";
            request.Headers["IDUsuario"] = ConfigurationSettings.IdUsuario;
            request.Headers["Token"] = ConfigurationSettings.Token;
            request.ContentType = "application/json";
            request.BeginGetRequestStream(new AsyncCallback(delegate(IAsyncResult callbackResultRequest)
            {
                HttpWebRequest myRequest = (HttpWebRequest)callbackResultRequest.AsyncState;
                Stream postStream = myRequest.EndGetRequestStream(callbackResultRequest);

                string postData = JsonConvert.SerializeObject(data);
                byte[] byteArray = Encoding.UTF8.GetBytes(postData);
                postStream.Write(byteArray, 0, byteArray.Length);
                postStream.Close();

                myRequest.BeginGetResponse(new AsyncCallback(delegate(IAsyncResult callbackResultResponse)
                {
                    try
                    {
                        HttpWebRequest myResponse = (HttpWebRequest)callbackResultResponse.AsyncState;
                        HttpWebResponse response = (HttpWebResponse)request.EndGetResponse(callbackResultResponse);

                        T myObjects;
                        ValidateToken tokenValidado;
                        using (StreamReader httpWebStreamReader = new StreamReader(response.GetResponseStream()))
                        {
                            string results = httpWebStreamReader.ReadToEnd();
                            myObjects = JsonConvert.DeserializeObject<T>(results);
                            tokenValidado = JsonConvert.DeserializeObject<ValidateToken>(results) as ValidateToken;
                        }
                        if ("ConsultaProgramacionRecarga" != method.ToString()) {
                            if (tokenValidado.codigo == 3)
                            {
                                //Cerrar sesion
                                TokenUtility.CloseSession("Sesion.txt");                            
                            }
                        }
                                NotifyFinish(handler, new FinishSmartServiceArgs<T>(true, myObjects));
                    }
                    catch (Exception ex)
                    {
                        NotifyFinish<T>(handler, new FinishSmartServiceArgs<T>(false, message: ex.Message));
                    }
                }), myRequest);
            }), request);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="handler"></param>
        /// <param name="e"></param>
        private void NotifyFinish<T>(FinishSmartServiceEventHandler<T> handler, FinishSmartServiceArgs<T> e)
        {
            if (handler != null)
            {
                handler(this, e);
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="title"></param>
        /// <param name="message"></param>
        private void MessageBoxShow(string title, string message)
        {
            MessageBox.Show(message, title, MessageBoxButton.OK);
        }
    }
}
