﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Starbucks.DAL.Model;

namespace Starbucks.DAL
{
    public class DataBaseViewModel
    {
        // Specify the local database connection string.
        public static string DBConnectionString = "Data Source=isostore:/starbucks.sdf";
        /// <summary>
        /// 
        /// </summary>
        public DataBaseViewModel()
        {

        }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public List<Estados> GetEntidades()
        {
            List<Estados> data = null;
            using (ModelDataBaseDataContext context = new ModelDataBaseDataContext(DBConnectionString))
            {
                IQueryable<Estados> query = from c in context.Estados select c;
                data = query.ToList();
            }
            return data;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public List<Municipios> GetMunicipiosByIDEstado(int IdEstado)
        {
            List<Municipios> data = null;
            using (ModelDataBaseDataContext context = new ModelDataBaseDataContext(DBConnectionString))
            {
                IQueryable<Municipios> query = from c in context.Municipios where c.ID_Estado == IdEstado select c  ;
                data = query.ToList();
            }
            return data;
        }
    }
}
