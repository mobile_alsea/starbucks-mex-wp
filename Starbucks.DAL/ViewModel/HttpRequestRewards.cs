﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using Newtonsoft.Json;
using Starbucks.Entities;
using Starbucks.Utilities;
using System.Windows;
using System.IO.IsolatedStorage;
using System.Security.Cryptography;
using JeffWilcox.Utilities.Silverlight;
using System.Security.Cryptography.X509Certificates;
using Org.BouncyCastle.Crypto.Tls;
using SocketEx;

namespace Starbucks.DAL
{
    public class HttpRequestRewards<T>
    {
        /// <summary>
        /// 
        /// </summary>
        private ManualResetEvent _waitHandle = new ManualResetEvent(false);
        /// <summary>
        /// 
        /// </summary>
        private bool _timedOut;
        /// <summary>
        /// 
        /// </summary>
        public RewardsMethodsEnum Method { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public FinishSmartServiceEventHandler<T> Handler { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string Data { get; set; }

        private SecureTcpClient CreateConnection(string serverAddress)
        {
            var uri = new Uri(serverAddress);
            var connection = new SecureTcpClient(uri.Host, uri.Port);
            return connection;
        }

        public void ExecuteWS()
        {
            string urlString = ConfigurationSettings.SERVICE_REWARDS + Method.ToString();

            if (urlString.Substring(4, 1).ToUpper() == "S")
            {
                var connection = CreateConnection(urlString);
                //Falta comparar el digesto almacenado con el digesto de la conexion cuando ya se tengan las conexiones SSL
                // if(IsolatedStorageSettings.ApplicationSettings["DIGEST_CERT_WS_REWARDS_WP"].ToString() == IsolatedStorageSettings.ApplicationSettings["Sha256Cer"].ToString())
                if (IsolatedStorageSettings.ApplicationSettings["Sha256Cer"].ToString().Length > 0)
                {
                    
                    if (Method.ToString() == "consultaEstatusRewards")
                    {
                        var request = (HttpWebRequest)WebRequest.Create(urlString);
                        request.Headers["idUsuario"] = ConfigurationSettings.IdUsuario;
                        request.Headers["token"] = ConfigurationSettings.Token;
                        request.BeginGetResponse(new AsyncCallback(GetSomeResponse), request);
                    }
                    else
                    {
                        System.Uri targetUri = new System.Uri(urlString);
                        HttpWebRequest request = (HttpWebRequest)HttpWebRequest.Create(targetUri);

                        if (Method.ToString() != "registroUsuario" && Method.ToString() != "login" && Method.ToString() != "validarTarjeta")
                        {
                            request.Headers["idUsuario"] = ConfigurationSettings.IdUsuario;
                            request.Headers["token"] = ConfigurationSettings.Token;
                        }
                        request.Method = "POST";
                        request.ContentType = "application/x-www-form-urlencoded";
                        request.BeginGetRequestStream(GetRequestStreamComplete, request);
                    }
                }
                else
                {
                    System.Windows.MessageBox.Show("La conexión con PagaTodo no es segura, intente con otra red.");
                }
            }
            else
            {
                if (Method.ToString() == "consultaEstatusRewards")
                {

                    var request = (HttpWebRequest)WebRequest.Create(urlString);
                    request.Headers["idUsuario"] = ConfigurationSettings.IdUsuario;
                    request.Headers["token"] = ConfigurationSettings.Token;
                    request.BeginGetResponse(new AsyncCallback(GetSomeResponse), request);
                }
                else
                {
                    System.Uri targetUri = new System.Uri(urlString);
                    HttpWebRequest request = (HttpWebRequest)HttpWebRequest.Create(targetUri);

                    if (Method.ToString() != "registroUsuario" && Method.ToString() != "login" && Method.ToString() != "validarTarjeta")
                    {
                        request.Headers["idUsuario"] = ConfigurationSettings.IdUsuario;
                        request.Headers["token"] = ConfigurationSettings.Token;
                    }
                    request.Method = "POST";
                    request.ContentType = "application/x-www-form-urlencoded";
                    request.BeginGetRequestStream(GetRequestStreamComplete, request);
                }
            }

        }

        private void GetRequestStreamComplete(IAsyncResult callbackResultRequest)
        {
            HttpWebRequest myRequest = (HttpWebRequest)callbackResultRequest.AsyncState;
            Stream postStream = myRequest.EndGetRequestStream(callbackResultRequest);
            
            string postData = Data;
            byte[] byteArray = Encoding.UTF8.GetBytes(postData);
            postStream.Write(byteArray, 0, byteArray.Length);
            postStream.Close();

            this._timedOut = false;
            this._waitHandle.Reset();
            myRequest.BeginGetResponse(GetResponseComplete, myRequest);

            bool signalled = this._waitHandle.WaitOne(30000);
            if (false == signalled)
            {
                this._timedOut = true;
                // Handle the timed out scenario.  
                
                myRequest.Abort();
                FinishSmartServiceArgs<T> args = new FinishSmartServiceArgs<T>(false, message: "Ocurrio un Error de TimeOut");
                args.IsTimeOut = true;
                NotifyFinish(Handler, args);
            }
        }

        private void GetResponseComplete(IAsyncResult callbackResultResponse)
        {
            try
            {
                if (false == this._timedOut)
                {
                    this._waitHandle.Set();

                    HttpWebRequest myRequest = (HttpWebRequest)callbackResultResponse.AsyncState;
                    HttpWebResponse response = (HttpWebResponse)myRequest.EndGetResponse(callbackResultResponse);

                    Deployment.Current.Dispatcher.BeginInvoke(() =>
                    {
                        int estaus = Convert.ToInt32(response.StatusCode);
                        if (estaus != 200) {
                            WebClient webClient = new WebClient();
                            webClient.DownloadStringCompleted += new DownloadStringCompletedEventHandler(webClient_DownloadStringCompleted);
                            webClient.DownloadStringAsync(new Uri(IsolatedStorageSettings.ApplicationSettings["URL_SWITCH"].ToString()));
                        }

                    }); 

                    T myObjects;
                    ValidateToken tokenValidado;
                    using (StreamReader httpWebStreamReader = new StreamReader(response.GetResponseStream()))
                    {
                        string results = httpWebStreamReader.ReadToEnd();
                        tokenValidado = JsonConvert.DeserializeObject<ValidateToken>(results) as ValidateToken;
                        var settings = new JsonSerializerSettings { NullValueHandling = NullValueHandling.Ignore, DateFormatHandling = DateFormatHandling.IsoDateFormat };
                        myObjects = JsonConvert.DeserializeObject<T>(results, settings);
                        httpWebStreamReader.Close();
                    }
                    /*if ("consultaSaldoTarjeta" == Method.ToString())
                    {
                        if (tokenValidado.codigo == 3)
                        {
                            //Cerrar sesion
                            TokenUtility.CloseSession("Sesion.txt");
                        }
                    }*/
                    
                    NotifyFinish(Handler, new FinishSmartServiceArgs<T>(true, myObjects));
                    response.Close();
                }
            }
            catch (Exception ex)
            {
                NotifyFinish(Handler, new FinishSmartServiceArgs<T>(false, message: ex.Message));
            }
        }

        private void GetSomeResponse(IAsyncResult MyResultAsync)
         {
             HttpWebRequest request = (HttpWebRequest)MyResultAsync.AsyncState;
             try
             {
                 HttpWebResponse response = (HttpWebResponse)request.EndGetResponse(MyResultAsync);
                 if (response.StatusCode == HttpStatusCode.OK && response.ContentLength > 0)
                 {
                     using (StreamReader sr = new StreamReader(response.GetResponseStream()))
                    {
                        string result = sr.ReadToEnd();
                        var settings = new JsonSerializerSettings { NullValueHandling = NullValueHandling.Ignore, DateFormatHandling = DateFormatHandling.IsoDateFormat };
                        var container = JsonConvert.DeserializeObject<T>(result, settings);
                        result = string.Empty;
                         sr.Close();
                         NotifyFinish(Handler, new FinishSmartServiceArgs<T>(true, container));

                    }
                }
                 response.Close();
            }
            catch (WebException e)
            {
                NotifyFinish(Handler, new FinishSmartServiceArgs<T>(false, message: e.Message));
            }
        }



        private void NotifyFinish(FinishSmartServiceEventHandler<T> handler, FinishSmartServiceArgs<T> e)
        {
            if (handler != null)
            {
                handler(this, e);
            }
        }

        void webClient_DownloadStringCompleted(object sender, DownloadStringCompletedEventArgs e)
        {
            //URL's de Respaldo
            var container = JsonConvert.DeserializeObject<URLRespaldo>(e.Result);
            string url = "PagaTodoMobile" + container.DATE_SWITCH + container.URL_SWITCH + container.URL_WS_SMART + container.URL_WS_REWARDS + container.DIGEST_CERT_WS_SMART_IOS + container.DIGEST_CERT_WS_REWARDS_IOS + container.DIGEST_CERT_WS_SMART_ANDROID + container.DIGEST_CERT_WS_REWARDS_ANDROID + container.URL_NOTIFICATIONS + container.URL_PASSBOOK + container.DIGEST_CERT_WS_SMART_WP + container.DIGEST_CERT_WS_REWARDS_WP;
            string DIGEST = EncodeString(SHA256Encrypt(url).ToUpper());

            if ((DIGEST.Substring(8, 16)) == container.DIGEST)
            {
                IsolatedStorageSettings.ApplicationSettings["DATE_SWITCH"] = container.DATE_SWITCH;
                IsolatedStorageSettings.ApplicationSettings["URL_SWITCH"] = container.URL_SWITCH;
                IsolatedStorageSettings.ApplicationSettings["URL_WS_SMART"] = container.URL_WS_SMART;
                IsolatedStorageSettings.ApplicationSettings["URL_WS_REWARDS"] = container.URL_WS_REWARDS;
                IsolatedStorageSettings.ApplicationSettings["DIGEST_CERT_WS_SMART_WP"] = container.DIGEST_CERT_WS_SMART_WP;
                IsolatedStorageSettings.ApplicationSettings["DIGEST_CERT_WS_REWARDS_WP"] = container.DIGEST_CERT_WS_REWARDS_WP;
                IsolatedStorageSettings.ApplicationSettings["URL_NOTIFICATIONS"] = container.URL_NOTIFICATIONS;
                IsolatedStorageSettings.ApplicationSettings.Save();
            }
        }

        public string Encrypt(string input, HashAlgorithm hashAlgorithm)
        {
            byte[] inputBytes = Encoding.UTF8.GetBytes(input);
            byte[] hashedBytes = hashAlgorithm.ComputeHash(inputBytes);

            StringBuilder output = new StringBuilder();

            for (int i = 0; i < hashedBytes.Length; i++)
                output.Append(hashedBytes[i].ToString("x2").ToLower());

            return output.ToString();
        }

        public string SHA256Encrypt(string input)
        {
            return Encrypt(input, new SHA256Managed());
        }

        public static string EncodeString(string password)
        {
            string md5encoded;

            // MD5 is disposable:
            using (MD5 md5 = new MD5CryptoServiceProvider())
                md5encoded = BitConverter.ToString(md5.ComputeHash(Encoding.UTF8.GetBytes(password)));

            string md5String = md5encoded.Replace("-", String.Empty);
            // Removing dashes to shrink result length:
            return md5String;
        }
    }
}
