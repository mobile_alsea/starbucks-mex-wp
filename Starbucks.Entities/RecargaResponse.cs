﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Starbucks.Entities
{
    public class RecargaResponse
    {
        #region Propiedades

        /// <summary>
        /// Número del código de error (0 es código correcto y diferente de cero ocurrio un error)
        /// </summary>
        public int Codigo { get; set; }

        /// <summary>
        /// Descripción si fue la transacción exitosa u ocurrio algun error
        /// </summary>
        public string Descripcion { get; set; }

        /// <summary>
        /// Se retorna el nuevo saldo cuando la transaccion fue exitosa
        /// </summary>
        public decimal NuevoSaldo { get; set; }

        /// <summary>
        /// Número de orden de Value Link
        /// </summary>
        public string NoAutVL { get; set; }

        #endregion
    }
}
