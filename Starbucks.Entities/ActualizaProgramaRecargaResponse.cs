﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Starbucks.Entities
{
    public class ActualizaProgramaRecargaResponse
    {
        #region Propiedades

        /// <summary>
        /// Número del código de error (0 es código correcto y diferente de cero ocurrio un error)
        /// </summary>
        public int Codigo { get; set; }

        /// <summary>
        /// Descripción si fue la transacción exitosa u ocurrio algun error
        /// </summary>
        public string Descripcion { get; set; }

        /// <summary>
        /// Lista de todas las tarjetas que han sido programadas
        /// </summary>
        public List<TarjetaSmart> TarjetasProgramadas { get; set; }

        #endregion
    }
}
