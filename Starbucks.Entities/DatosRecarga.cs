﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Starbucks.Entities
{
    public enum TipoRecargaEnum
    {
        DiaFijo = 1,
        MontoMinimo = 2
    }

    public class DatosRecarga
    {
        public string NombreTarjeta { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int ValidaAnio { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int ValidaMes { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string Saldo { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string FechaSaldo { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int MontoRecarga { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int MontoRecargaAutomatica { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int DiaRecarga { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string Tarjeta { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string TipoTarjeta { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public bool IsRecargaAutomatica
        {
            get { return MontoRecargaAutomatica != 0 || DiaRecarga != 0; }
        }
        /// <summary>
        /// 
        /// </summary>
        public bool IsNewMedioPago { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string DigitosTarjeta { get; set; }
        /// <summary>
        /// Indica el tipo de recarga por monto o por dia
        /// </summary>
        public TipoRecargaEnum TipoRecarga
        {
            get { return MontoRecargaAutomatica != 0 ? TipoRecargaEnum.MontoMinimo : TipoRecargaEnum.DiaFijo; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string TarjetaCadena { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public DatosCliente DatosCliente { get; set; }

        public bool GuardaTarjeta { get; set; }

        public string FechaVigencia { get; set; }

        public string CVV { get; set; }
    }
}
