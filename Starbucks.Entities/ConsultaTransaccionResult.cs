﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Starbucks.Entities
{
    public class ConsultaTransaccionResult : ServiceResult
    {
        public List<Movimiento> Movimientos { get; set; }
    }
}
