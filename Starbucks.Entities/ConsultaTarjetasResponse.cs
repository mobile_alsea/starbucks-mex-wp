﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Starbucks.Entities
{
    public class ConsultaTarjetasResponse
    {
        /// <summary>
        /// Número del código de error (0 es código correcto y diferente de cero ocurrio un error)
        /// </summary>
        public int codigo { get; set; }

        /// <summary>
        /// Descripción si fue la transacción exitosa u ocurrio algun error
        /// </summary>
        public string descripcion { get; set; }

        public List<TarjetaRewards> tarjetas { get; set; }
    }
}
