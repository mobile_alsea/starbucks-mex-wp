﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Starbucks.Entities
{
    public class Movimiento
    {
        /// <summary>
        /// 
        /// </summary>
        public int? idMovimiento { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public decimal monto { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string fecha { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string descripcion { get; set; }
    }
}
