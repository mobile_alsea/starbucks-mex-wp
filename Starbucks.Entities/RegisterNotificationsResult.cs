﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Starbucks.Entities
{
    public class RegisterNotificationsResult
    {
        public CustomResult resultado { get; set; }
        public string idTokenNotificacion { get; set; }
    }
}
