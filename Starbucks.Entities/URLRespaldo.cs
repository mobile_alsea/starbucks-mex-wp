﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Starbucks.Entities
{
    public class URLRespaldo
    {
        public string DATE_SWITCH { get; set; }
        public string URL_SWITCH { get; set; }
        public string URL_WS_SMART { get; set; }
        public string URL_WS_REWARDS { get; set; }
        public string DIGEST_CERT_WS_SMART_IOS { get; set; }
        public string DIGEST_CERT_WS_REWARDS_IOS { get; set; }
        public string DIGEST_CERT_WS_SMART_ANDROID { get; set; }
        public string DIGEST_CERT_WS_REWARDS_ANDROID { get; set; }
        public string URL_NOTIFICATIONS { get; set; }
        public string URL_PASSBOOK { get; set; }
        public string DIGEST_CERT_WS_SMART_WP { get; set; }
        public string DIGEST_CERT_WS_REWARDS_WP { get; set; }
        public string DIGEST { get; set; }
    }
}
