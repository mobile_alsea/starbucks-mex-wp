﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Starbucks.Entities
{
    public class TarjetaSmart
    {
        #region Propiedades

        /// <summary>
        /// Identifiador único de programación
        /// </summary>
        public long ID_Programacion { get; set; }

        /// <summary>
        /// Número de la tarjeta de la cadena
        /// </summary>
        public string NoTarjetaCadena { get; set; }

        /// <summary>
        /// Nombre de la cadena
        /// </summary>
        public string Cadena { get; set; }

        /// <summary>
        /// Tipo de dispositivo que dio de alta la tarjeta al realizar una recarga
        /// </summary>
        public string tipoDispositivo_Alta { get; set; }

        /// <summary>
        /// Tipo de dispositivo que realizó una modificación en la programación de una recarga con una tarjeta 
        /// </summary>
        public string TipoDispositivo_Modificacion { get; set; }

        /// <summary>
        /// Identificador único del tipo de recarga
        /// </summary>
        public int IDTipoRecarga { get; set; }

        /// <summary>
        /// Moneda o divisa con que se realiza las recargas
        /// </summary>
        public string Moneda { get; set; }

        /// <summary>
        /// Monto de la recarga
        /// </summary>
        public decimal MontoRecarga { get; set; }

        /// <summary>
        /// límite mínimo  de saldo para disparar el proceso de la recarga y su validación es gobernado por el tipo de recarga
        /// </summary>
        public decimal LimiteMinimoSaldo { get; set; }

        /// <summary>
        /// Día dl mes que se realizan las recargas periodicamente y su validación es gobernado por el tipo de recarga
        /// </summary>
        public int DiaRecarga { get; set; }

        /// <summary>
        /// Identificador únicodel estatus de la tarjeta relacionado a la programación de la recarga
        /// </summary>
        public int IDEstatus { get; set; }

        /// <summary>
        /// Descripción del estatus de la tarjeta relacionado a la programación de la recarga
        /// </summary>
        public string Estatus { get; set; }

        /// <summary>
        /// Identificador único del usuario que creo la programación de las recargas con la tarjeta
        /// </summary>
        public string IDUsuario { get; set; }

        #endregion
    }
}
