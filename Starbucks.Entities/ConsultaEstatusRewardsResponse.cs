﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Starbucks.Entities
{
    public class ConsultaEstatusRewardsResponse
    {
        public long codigo { get; set; }
        public string descripcion { get; set; }
        public long numEstrellas { get; set; }
        public long numEstrellasDoradas { get; set; }
        public string miembroDesde { get; set; }
        public long numEstrellasFaltantes { get; set; }
        public string nivelActual { get; set; }
        public string siguienteNivel { get; set; }
    }
}
