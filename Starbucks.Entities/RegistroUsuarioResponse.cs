﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Starbucks.Entities
{
    public class RegistroUsuarioResponse
    {
        public int codigo { get; set; }
        public string descripcion { get; set; }
        public List<string> datosFaltantes { get; set; }
        public string token { get; set; }
        public int idUsuario { get; set; }
        public List<TarjetaRewards> tarjetas { get; set; }
    }
}
