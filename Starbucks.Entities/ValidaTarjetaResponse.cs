﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Starbucks.Entities
{
    public class ValidaTarjetaResponse
    {
        public int Codigo { get; set; }
        public string Descripcion { get; set; }
        public int IdPreregistro { get; set; }
    }
}
