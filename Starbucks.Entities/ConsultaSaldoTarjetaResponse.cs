﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Starbucks.Entities
{
    public class ConsultaSaldoTarjetaResponse
    {
        public int Codigo { get; set; }
        public string Descripcion { get; set; }
        public double Saldo { get; set; }
    }
}
