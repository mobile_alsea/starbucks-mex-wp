﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Starbucks.Entities
{
    public enum TipoDispositivoEnum
    {
        IOS = 1,
        WEB = 2,
        POSMICROS = 3,
        Android = 4,
        WindowsMobile = 5

    }
}
