﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Collections.Generic;

namespace Starbucks.Entities
{
    public class RegistroUsuarioResult : ServiceResult
    {
        /// <summary>
        /// 
        /// </summary>
        public List<string> datosFaltantes { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string token { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int idUsuario { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public List<TarjetaRewards> tarjetas { get; set; }

    }
}
