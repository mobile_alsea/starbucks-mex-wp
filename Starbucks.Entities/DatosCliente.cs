﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Starbucks.Entities
{
    public class DatosCliente
    {
        public string codigoPostal { get; set; }
        public string ciudad { get; set; }
        public string calleyNumero { get; set; }
        public string estado { get; set; }
        public string pais { get; set; }
        public string email { get; set; }
        public string apellidos { get; set; }
        public string fechaNacimiento { get; set; }
        public string primerNombre { get; set; }
        public string esClientePrevio { get; set; }
        public string miembroDesde { get; set; }        
    }
}
