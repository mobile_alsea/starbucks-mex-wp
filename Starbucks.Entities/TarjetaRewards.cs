﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Starbucks.Entities
{
    /// <summary>
    /// 
    /// </summary>
    public class TarjetaRewards
    {
        /// <summary>
        /// 
        /// </summary>
        public long idTarjeta;
        /// <summary>
        /// 
        /// </summary>
        public string numeroTarjeta;
        /// <summary>
        /// 
        /// </summary>
        public string nivelLealtad;
        /// <summary>
        /// 
        /// </summary>
        public Imagen imagen;
        /// <summary>
        /// 
        /// </summary>
        public string alias;
        /// <summary>
        /// 
        /// </summary>
        public double saldo;
        /// <summary>
        /// 
        /// </summary>
        public bool esPrincipal;
        /// <summary>
        /// 
        /// </summary>
        public string fechaActivacion;

    }
}
