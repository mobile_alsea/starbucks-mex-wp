﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Starbucks.Entities
{
    public class ActualizaProgramaRecargaRequest
    {
       /// <summary>
       /// 
       /// </summary>
        public int cadena { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int tipoDispositivo { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string tipoBanco { get; set; }        
        /// <summary>
        /// 
        /// </summary>
        public string tarjetaCadena { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int tipoRecarga { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string moneda { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public decimal montoRecarga { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public decimal limiteMinimoSaldo { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int diaRecarga { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string tarjetaBancaria { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string aliasTarjetaBancaria { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string cvv { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string fechaVencimiento { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public DetalleCliente detalleCliente { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string PCHuellaDigital { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public long idReferencia { get; set; }
       
    }
}
