﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Starbucks.Entities
{
    public class CustomResult
    {
        public string id { get; set; }
        public string message { get; set; }
        public string title { get; set; }
    }
}
