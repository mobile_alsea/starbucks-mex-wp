﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Starbucks.Entities
{
    public class LoginResponse
    {
        public long codigo { get; set; }
        public string descripcion { get; set; }
        public string token { get; set; }
        public long idUsuario { get; set; }
        public DatosCliente datosCliente { get; set; }
        public List<TarjetaRewards> tarjetas { get; set; }
    }
}
