﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Starbucks.Entities
{
    public class ConsultaMedioPagoResponse
    {
        #region Propiedades

        /// <summary>
        /// Número del código de error (0 es código correcto y diferente de cero ocurrio un error)
        /// </summary>
        public int Codigo { get; set; }

        /// <summary>
        /// Descripción si fue la transacción exitosa u ocurrio algun error
        /// </summary>
        public string Descripcion { get; set; }

        /// <summary>
        /// Número de tarjeta bancaria conformada por 16 dígitos para VISA y MasterCard y 15 dígitos para Amer
        /// </summary>
        public string TarjetaBancaria { get; set; }

        /// <summary>
        /// Fecha de vencimiento de la tarjeta banacaría
        /// </summary>
        public string FechaVencimiento { get; set; }

        /// <summary>
        /// Datos del cliente que es dueño de la tarjeta bancarÍa
        /// </summary>
        public List<Direccion> DireccionCliente { get; set; }

        /// <summary>
        /// Nombre del cliente como aparece en la tarjeta bancaría
        /// </summary>
        public string NombreCliente { get; set; }

        /// <summary>
        /// Grupo de tarjeta a que pertenece: "MC/VISA" ó "AMEX" 
        /// </summary>
        public string TipoBanco { get; set; }

        /// <summary>
        /// Nombre o alias de la tarjeta bancaria que el usuario le asigna.
        /// </summary>
        public string AliasTarjetaBancaria { get; set; }

        #endregion
    }
}
