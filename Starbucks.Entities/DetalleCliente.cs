﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Starbucks.Entities
{
    public class DetalleCliente
    {

        //public string direccionIP { get; set; }
        //public string calleNumero { get; set; }
        //public string codigoPostal { get; set; }
        //public string edadCuentaCreada { get; set; }
        //public string telefonoCasa { get; set; }
        //public string estado { get; set; }
        //public string email { get; set; }
        //public string apellidos { get; set; }
        //public string esClientePrevio { get; set; }
        //public string primerNombre { get; set; }
        //public string segundoNombre { get; set; }
        //public string telefonoOficina { get; set; }
        //public string fechaNacimiento { get; set; }
        //public string ciudad { get; set; }
        //public string pais { get; set; }

        public string codigoPostal { get; set; }
        public string ciudad { get; set; }
        public string calleNumero { get; set; }
        public string estado { get; set; }
        public string pais { get; set; }
        public string email { get; set; }
        public string apellidos { get; set; }
        public string fechaNacimiento { get; set; }
        public string primerNombre { get; set; }
        public string miembroDesde { get; set; }        
        public string edadCuentaCreada { get; set; }
        public string esClientePrevio { get; set; }
    }

}
