﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Starbucks.Entities
{
    public class Direccion
    {
        #region Propiedaes

        /// <summary>
        /// Teléfono de casa. No debe ser mayor de 19 caracteres
        /// </summary>
        public string TelefonoCasa { get; set; }

        /// <summary>
        /// Teléfono  de oficina. No debe ser mayor de 19 caracteres
        /// </summary>
        public string TelefonoOficina { get; set; }

        /// <summary>
        /// No debe exceder de 60 caracteres para dividirse en los parametros de Address line 1 y Address line 2
        /// </summary>
        public string CalleyNumero { get; set; }

        /// <summary>
        /// Ciudad . No debe ser mayor de 20 caracteres
        /// </summary>
        public string Ciudad { get; set; }

        /// <summary>
        /// Estado. Su longitud es de 2 caracteres
        /// </summary>
        public string Estado { get; set; }

        /// <summary>
        /// Código del País. Su longitud es de 3 caracteres 
        /// </summary>
        public string Pais { get; set; }

        /// <summary>
        /// Código postal. Su longitud no debe ser mayor de 10 caracteres
        /// </summary>
        public string CodigoPostal { get; set; }

        /// <summary>
        /// Dirección IP del host del cliente. Formato nnn.nnn.nnn.nnn. Este campo no debe ser enviado si el valor no está presente, es decir callcentre, IVR, etc
        /// </summary>
        public string DireccionIP { get; set; }

        #endregion
    }
}
