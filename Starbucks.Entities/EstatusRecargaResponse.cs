﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Starbucks.Entities
{
    public class EstatusRecargaResponse
    {
        public int NoAutVL { get; set; }
        public double NuevoSaldo { get; set; }
        public int Codigo { get; set; }
        public string Descripcion { get; set; }
    }
}
