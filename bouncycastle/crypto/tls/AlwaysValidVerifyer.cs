using System;

using Org.BouncyCastle.Asn1.X509;
using System.Security.Cryptography;
using System.Text;
using System.IO.IsolatedStorage;

namespace Org.BouncyCastle.Crypto.Tls
{
    /// <remarks>
    /// A certificate verifyer, that will always return true.
    /// <pre>
    /// DO NOT USE THIS FILE UNLESS YOU KNOW EXACTLY WHAT YOU ARE DOING.
    /// </pre>
    /// </remarks>
    [Obsolete("Perform certificate verification in TlsAuthentication implementation")]
    public class AlwaysValidVerifyer
        : ICertificateVerifyer
    {
        /// <summary>Return true.</summary>
        public bool IsValid(
            X509CertificateStructure[] certs)
        {
            string sha256string = SHA256Encrypt(certs[0].TbsCertificate.ToAsn1Object().ToString());
            System.Diagnostics.Debug.WriteLine(sha256string);
            IsolatedStorageSettings.ApplicationSettings["Sha256Cer"] = sha256string;
            
                
            return true;
        }

        public string Encrypt(string input, HashAlgorithm hashAlgorithm)
        {
            byte[] inputBytes = Encoding.UTF8.GetBytes(input);
            byte[] hashedBytes = hashAlgorithm.ComputeHash(inputBytes);

            StringBuilder output = new StringBuilder();

            for (int i = 0; i < hashedBytes.Length; i++)
                output.Append(hashedBytes[i].ToString("x2").ToLower());

            return output.ToString();
        }

        public string SHA256Encrypt(string input)
        {
            return Encrypt(input, new SHA256Managed());
        }

    }
}
